
import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Field, Value, ValueRecord } from './field';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Authorization': 'my-auth-token'
  })
};
@Injectable()
export class FieldsService {
  constructor(private http: HttpClient) { }
  json: string;
  params: string;
  headers: HttpHeaders;
  /* --------------------------------------- FIELDS -----------------------------------------------------------*/
  getAllFields(): Observable<Field[]>  {
    return this.http.get<Field[]>('/fields/getAllFields')                 
  }
  getOneField(newField: Field): Observable<Field>  {
    return this.http.post<Field>('/fields/getOneField',newField)                 
  }
  createOneField(newField: Field): Observable<Field>  {
      return this.http.post<Field>('/fields/addOneField', newField)
  }
  updateOneField(updateField: Field): Observable<Field>  {
    return this.http.put<Field>('/fields/updateOneField', updateField)
  }
  updateOneFieldStatus(updateField: Field): Observable<Field>  {
    return this.http.post<Field>('/fields/updateOneFieldStatus', updateField)
  }
  loadDefaultFieldsAndValues(): Observable<any> {
    return this.http.post('/fields/loadDefault',null)
  }
  deleteOneField(delField: Field): Observable<any> {
    return this.http.post('/fields/deleteOneField',delField)
  }
  deleteAllFields(): Observable<any> {
    return this.http.delete('/fields/')
  }
  /* --------------------------------------- VALUES -----------------------------------------------------------*/
  getFieldValuesList(newField: Field): Observable<Value[]>  {
    return this.http.post<Value[]>('/fields/getFieldValues', newField)
  }
  createFieldValuesList(newFieldValuesList: Value[]): Observable<Value[]>  {
    return this.http.post<Value[]>('/fields/addFieldValues', newFieldValuesList)
  }
  updateFieldValuesList(newFieldValuesList: Value[]): Observable<Value[]>  {
    return this.http.post<Value[]>('/fields/updFieldValues', newFieldValuesList)
  }
  getFieldAndValues(): Observable<ValueRecord[][]>  {
    return this.http.get<ValueRecord[][]>('/fields/getFieldAndValues/')
  }

}
