export class Field {
    public fieldId            :number;
    public fieldName          :string;
    public fieldExplan        :string;
    public fieldValuesString  :string;
    public fieldStatus        :boolean;

  }
export class Value {
    public valueId    :number;
    public valueName  :string;
    public fieldId    :number;
  }
export class ValueRecord {
    public id     :number;
    public value  :string;
  }
export class ValueRecordNumber {
  public id     :number;
  public value  :number;
}