import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { throwError, Subject } from 'rxjs';

import { OrderPipe } from 'ngx-order-pipe';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

import { GlobalVarService } from '../aa-common/global-var.service';
import { ErrorService } from '../aa-errors/error.service';

import { Field, Value, ValueRecord } from './field';
import { ValuesCreateComponent } from './values-create.component';
import { FieldsService } from './fields.service';

@Component({
  selector      :'app-fields-create',
  templateUrl   :'./fields-create.component.html',
  styleUrls     :['./fields.component.css']
})
export class FieldsCreateComponent implements OnInit {
  public newField         :Field;
  public showCreateField  :boolean;
  public showEditField    :boolean;
  callback                :any;
  result: Subject<any> = new Subject<any>();

  public formTitles = {
    'fields'            :'Campos y Valores',
    'fieldCreate'       :'Crear Campos',
    'fieldEdit'         :'Modificar Campos',
    'fieldMoreData'     :'Mas Datos Campos',
    'fieldsList'        :'Lista de Campos y Valores',
    'fieldEditList'     :'Editar',
    'fieldDelete'       :'Borrar',
    'values'            :'Valores',
    'valueCreate'       :'Crear VALOR para el CAMPO',
    'valueEdit'         :'Editar VALOR para el CAMPO Valores',
    'valuesEdit'        :'Modificar Valores',
    'valuesMoreData'    :'Mas Datos Valores',
    'valuesList'        :'Lista Valores del Campo',
    'valuesEditList'    :'Editar',
    'valuesDelete'      :'Borrar',
    'fieldDeleteOne'    :'¿ Seguro que quiere BORRAR este CAMPO ?',
    'fieldDeleteAll'    :'¿ Seguro que quiere BORRAR TODOS los CAMPOS ?',
  };
  public formLabels = {
    'fieldId'           :'Id',
    'fieldName'         :'Nombre',
    'fieldExplan'       :'Descripción',
    'fieldStatus'       :'Estado',
    'fieldValuesString' :'Valores del Campo',
    'valueId'           :'Id',
    'valueName'         :'Field Id',
    'valueFieldId'      :'Tipo Datos',
  };
  private logToConsole              :boolean;
  public  fieldForm                 :FormGroup;
  public  valueForm                 :FormGroup; 
  public  localFieldNamesRecordList :ValueRecord [];

  public  searchField               :Field;
  public  selectedField             :Field;
  public  fieldValuesList           :Value[];
  public  newValue                  :Value;
  public  selectedValue             :Value;

  public  order                     :string;
  public  reverse                   :boolean;
  public  caseInsensitive           :boolean;
  public  sortedCollection          :any[];

  /*---------------------------------------FIELDS CREATE -- CONSTRUCTOR-----------------------------------------------------------*/
  constructor(  private viewContainer: ViewContainerRef, private fb:FormBuilder, private orderPipe: OrderPipe, 
                private bsModalRef: BsModalRef, private modalService: BsModalService, private _errorService:ErrorService, 
                public globalVar: GlobalVarService, private _fieldsService: FieldsService ) { }
  /*---------------------------------------FIELDS CREATE -- NG ON INIT-----------------------------------------------------------*/
  ngOnInit() {
    this.logToConsole = this.globalVar.logToConsole;
    this.logToConsole = true;
    this.fieldForm = this.fb.group({
      fieldId:            [''],
      fieldName:          ['', [Validators.required, Validators.minLength(3), Validators.maxLength(64)] ],
      fieldExplan:        ['', [Validators.required, Validators.minLength(3), Validators.maxLength(64)] ],
      fieldValuesString:  [''],
      fieldStatus:        [''],
    })
    this.localFieldNamesRecordList = this.globalVar.fieldNamesRecordList;

    if (this.showCreateField === true) {
      this.newField = new Field();
      this.newValue = new Value();
      this.fieldValuesList = new Array();
      this.cleanFieldFormData();
      this.fieldForm.patchValue( {fieldName: this.localFieldNamesRecordList[0].value} );
    } else {
      this.newValue = new Value();
      this.getOneField(this.newField);
    }
   }

  /*---------------------------------------FIELDS CREATE -- FORMS-----------------------------------------------------------*/
  copyFieldFormDataToNewField(){
    this.newField.fieldName         = this.fieldForm.controls.fieldName.value;
    this.newField.fieldExplan       = this.fieldForm.controls.fieldExplan.value;
    this.newField.fieldValuesString = this.fieldForm.controls.fieldValuesString.value;
    this.newField.fieldStatus       = this.fieldForm.controls.fieldStatus.value;
  }
  copyNewFieldDataToFieldForm(){
    this.fieldForm.patchValue( {fieldId: this.newField.fieldId, fieldName: this.newField.fieldName, fieldExplan: this.newField.fieldExplan, 
                                fieldValuesString: this.newField.fieldValuesString, fieldStatus: this.newField.fieldStatus} );
  }
  cleanFieldFormData(){
    this.fieldForm.patchValue( {fieldName:this.localFieldNamesRecordList[0].value, fieldExplan:'', fieldValuesString:'',fieldStatus:true} );
  }  
  /*---------------------------------------FIELDS CREATE -- GENERAL-----------------------------------------------------------*/
  setValuesStringInField(updField: Field, updFieldValuesList: Value[]){
    let valuesString="";
    for (let j=0; j<updFieldValuesList.length; j++){
      valuesString = valuesString.concat(updFieldValuesList[j].valueName);
      valuesString = valuesString.concat("-");
    }
    updField.fieldValuesString = valuesString;
  }
  /*---------------------------------------FIELDS CREATE -- BTN CLICK-----------------------------------------------------------*/ 
  closeFieldsCreateModalBtnClick(){
    if (this.bsModalRef.content.callback != null){
        //this.bsModalRef.content.callback(null);
        this.result.next(null);
        this.bsModalRef.hide();
      }
  }
  saveFieldsCreateModalBtnClick(selecteField: Field){
    if (this.bsModalRef.content.callback != null){
        //this.bsModalRef.content.callback(selecteField);
        this.result.next(selecteField);
        this.bsModalRef.hide();
      }
  }
  cleanFormFieldBtnClick(){
    this.showCreateField = true;
    this.showEditField = false;
    this.fieldValuesList = new Array();
    this.cleanFieldFormData();    
  }
  createFieldBtnClick(fieldForm: FormGroup){
    this.globalVar.consoleLog(this.logToConsole,'->FIELDS CREATE->createFieldBtnClick->creating->', null);
    this.copyFieldFormDataToNewField();  
    this.createOneField(this.newField);   
  }
  updateFieldBtnClick(fieldForm: FormGroup){
    this.globalVar.consoleLog(this.logToConsole,'->FIELDS CREATE->updateFieldBtnClick->updating->', null);
    this.copyFieldFormDataToNewField();
    this.updateOneField(this.newField);    
  } 
  /*---------------------------------------FIELDS CREATE - VALUES -- BTN CLICK-----------------------------------------------------------*/ 
  showCreateValueBtnClick(){
    this.globalVar.consoleLog(this.logToConsole,'->FIELDS CREATE->changeShowCreateValuesBtnClick->starting->', null);
    if (this.fieldForm.invalid) {
      this._errorService.showErrorModal('... Crear CAMPO ...','... Añadir Valores para el CAMPO ...',
                                      'Primero tiene que introducir','el Nombre del CAMPO','','','','','',);
    } else this.createValueModal();
  }
  createValueModal(){    
    this.globalVar.consoleLog(this.logToConsole,'->FIELDS CREATE->createValueModal->starting->', null);    
    if ( (this.fieldValuesList === null) || (this.fieldValuesList.length === 0) ) {this.fieldValuesList = new Array();}
    this.copyFieldFormDataToNewField();
    this.newValue = new Value();          
    const modalInitialState = {
      newField      :this.newField,        
      valuesTitle   :this.formTitles.valueCreate,
      callback      :'OK',   
    };
    this.globalVar.openModal(ValuesCreateComponent, modalInitialState, 'modalLgTop0Left0').
    then((result:any)=>{
      if (result != null) {
        this.newValue= result;
        let indexNewValue = -1;
        for (let j=0 ; j<this.fieldValuesList.length ; j++) {              
          if (this.newValue.valueName === this.fieldValuesList[j].valueName) {
            indexNewValue = j;
            break;
          }
        }
        if (indexNewValue === -1) {
          this.fieldValuesList.push(this.newValue);
          this.setValuesStringInField(this.newField, this.fieldValuesList); 
          this.copyNewFieldDataToFieldForm();             
        }                    
      } 
    })
   }
  deleteOneValueBtnClick(delValue: Value){
    this.fieldValuesList.splice( this.fieldValuesList.indexOf(delValue),1);      
  }
  deleteAllValuesBtnClick(){
    this.fieldValuesList = new Array();
  }  
  /*---------------------------------------FIELDS CREATE -- FIELDS -- DATABASE-----------------------------------------------------------*/ 
  getOneField(modField: Field): void {
    this.globalVar.consoleLog(this.logToConsole,'->FIELDS CREATE->getOneField->getting->', null);
    this._fieldsService.getOneField(modField)
      .subscribe(data => { this.newField = data;
                           this.copyNewFieldDataToFieldForm();
                           this.getFieldValuesList(this.newField);
                         },
                error => { if (this.globalVar.handleError(error)) { throwError(error) }; });
  }
  createOneField(newField: Field): void {
    this.globalVar.consoleLog(this.logToConsole,'->FIELDS CREATE->createOneField->creating->', null);
    this._fieldsService.createOneField(newField)
      .subscribe(data => { this.newField = data;
                           if ( (this.fieldValuesList.length === 0) || (this.fieldValuesList === null) ) { 
                             this.saveFieldsCreateModalBtnClick(this.newField);                           
                           } else {
                             this.fieldValuesList.forEach( (value:Value)=>{ value.fieldId = this.newField.fieldId } )
                             this.createFieldValuesList(this.newField,this.fieldValuesList);
                           }                          
                         },
                error => { if (this.globalVar.handleError(error)) { throwError(error) }; });
  }
  updateOneField(modField: Field): void {
    this.globalVar.consoleLog(this.logToConsole,'->FIELDS CREATE->updateOneField->updating->', null);
    this._fieldsService.updateOneField(modField)
      .subscribe(data => { this.newField = data;
                           this.updateFieldValuesList(this.newField, this.fieldValuesList);
                         },
                error => { if (this.globalVar.handleError(error)) { throwError(error) }; });
  } 

  /*---------------------------------------FIELDS CREATE - VALUES -- DATABASE-----------------------------------------------------------*/ 
  getFieldValuesList(newField: Field) {
    this.globalVar.consoleLog(this.logToConsole,'->FIELDS CREATE->getFieldValuesList->getting->', null);
    this._fieldsService.getFieldValuesList(newField)
      .subscribe(data => { this.fieldValuesList = data; },
                error => { if (this.globalVar.handleError(error)) { throwError(error) }; });
  }
  createFieldValuesList(newField: Field, newFieldValues: Value[]) {
    this.globalVar.consoleLog(this.logToConsole,'->FIELDS CREATE->createFieldValuesList->creating->', null);
    this._fieldsService.createFieldValuesList(newFieldValues)
      .subscribe(data => { this.fieldValuesList = data;
                           this.saveFieldsCreateModalBtnClick(newField);
                         },
                error => { this._fieldsService.deleteOneField(newField);
                            if (this.globalVar.handleError(error)) { throwError(error) }; });
  }
  updateFieldValuesList(newField: Field, newFieldValues: Value[]) {
    this.globalVar.consoleLog(this.logToConsole,'->FIELDS CREATE->updateFieldValuesList->creating->', null);
    this._fieldsService.updateFieldValuesList(newFieldValues)
      .subscribe(data => { this.fieldValuesList = data;
                           this.saveFieldsCreateModalBtnClick(newField);
                         },
                error => { if (this.globalVar.handleError(error)) { throwError(error) }; });
  }

}
