import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { throwError } from 'rxjs';

import { OrderPipe } from 'ngx-order-pipe';
import { BsModalService } from 'ngx-bootstrap/modal';

import { GlobalVarService } from '../aa-common/global-var.service';
import { ErrorService } from '../aa-errors/error.service';

import { Field, Value } from './field';
import { FieldsCreateComponent } from './fields-create.component';
import { FieldsService } from './fields.service';

@Component({
  selector      :'app-fields',
  templateUrl   :'./fields.component.html',
  styleUrls     :['./fields.component.css']
})
export class FieldsComponent implements OnInit {
  public formTitles = {
    'fields'          :'Campos y Valores',
    'fieldDeleteOne'  :'¿ Seguro que quiere BORRAR este CAMPO ?',
    'fieldDeleteAll'  :'¿ Seguro que quiere BORRAR TODOS los CAMPOS ?',
    'fieldsList'      :'Lista de Campos y Valores',
  };
  public formLabels = { 
    'fieldDelete'       :'Borrar',
    'fieldEditList'     :'Editar',
    'fieldId'           :'Id',
    'fieldName'         :'Campo',
    'fieldExplan'       :'Explicación',
    'fieldStatus'       :'Estado',
    'fieldValuesString' :'Valores Campo',
  };
  public  showFieldsList      :boolean;
  public  showSearchFields    :boolean;
  private showCreateField     :boolean;
  private showEditField       :boolean; 
  public  fields              :Field[];
  private newField            :Field;
  public  searchField         :Field;
  private newValue            :Value;
  public  order               :string;
  public  reverse             :boolean;
  public  caseInsensitive     :boolean;
  private sortedCollection    :any[];
  private logToConsole        :boolean;
  /*---------------------------------------FIELDS  -- CONSTRUCTOR-----------------------------------------------------------*/
  constructor(  private viewContainer: ViewContainerRef, private modalService: BsModalService, private orderPipe: OrderPipe,
                public globalVar: GlobalVarService, private _errorService:ErrorService, private _fieldsService: FieldsService, ) {   }
  /*---------------------------------------FIELDS  -- NG ON INIT-----------------------------------------------------------*/
  ngOnInit() {
    this.logToConsole = this.globalVar.logToConsole;
    this.logToConsole = true; 
    this.showSearchFields = false;
    this.newField = new Field ();
    this.newField.fieldName = '';
    this.newField.fieldExplan = '';
    this.searchField = new Field ();
    this.newValue = new Value();
    this.newValue.valueName = '';
    this.showFieldsList = true;
    this.getAllFields();
   }
  /*---------------------------------------FIELDS  -- FORMS-----------------------------------------------------------*/

  /*---------------------------------------FIELDS  -- GENERAL-----------------------------------------------------------*/
  /*---------------------------------------FIELDS  -- BTN CLICK-----------------------------------------------------------*/ 
  changeShowFieldsListBtnClick(){
    if (this.showFieldsList === true) {
      this.showFieldsList = false;
    } else {
      this.showFieldsList = true;
      this.getAllFields();
    }
  }
  loadDefaultFieldsAndValuesBtnClick(){
    this.showFieldsList = false;
    this.loadDefaultFieldsAndValues();
  }
  showCreateFieldBtnClick(){        
    this.showCreateField = true;
    this.showEditField = false;       
    this.newField = new Field();
    this.createFieldModal();
  }
  editOneFieldBtnClick(modField: Field){
    this.globalVar.consoleLog(this.logToConsole,'-> FIELDS -> editOneFieldBtnClick ->...Starting...->', null);      
    if (this.isFieldNotBlocked(modField)){
      this.showEditField = true;
      this.showCreateField = false;       
      this.newField = modField;     
      this.createFieldModal();
    }
  }
  createFieldModal(){    
    const modalInitialState = {
      newField            :this.newField,
      showCreateField     :this.showCreateField,
      showEditField       :this.showEditField,
      callback            :'OK',   
    };
    this.globalVar.openModal(FieldsCreateComponent, modalInitialState, 'modalLgTop0Left0').
    then((result:any)=>{
      if (result != null){ this.newField = result; }
      this.getAllFields(); 
    })
  }  
  isFieldNotBlocked(selectedField: Field):boolean{
    this.globalVar.consoleLog(this.logToConsole,'->-> FIELDS -> isFieldNotBlocked->...Starting...-> ', null);
    if (selectedField.fieldStatus === false){
      this._errorService.showErrorModal( "CAMPOS Y SUS VALORES", "CAMPOS Y SUS VALORES", "CAMPO BLOQUEADO"+"->",
                  "Campo->"+selectedField.fieldName, "Descripción->"+selectedField.fieldExplan, 
                  "Valores del Campo->"+selectedField.fieldValuesString,
                  "Id.Campo->"+selectedField.fieldId, "Estado->"+selectedField.fieldStatus,"");
      return false;
    } else return true;
  }
  lockFieldBtnClick(modField: Field){
    if (modField.fieldStatus === true) modField.fieldStatus = false;
    else  modField.fieldStatus = true;
    this.updateOneFieldStatus(modField);
  }
  setOrderBtnClick(value: string) {
    if (this.order === value) {
      this.reverse = !this.reverse;
    }
    this.order = value;
  }
  searchFieldsBtnClick() {
    if (this.showSearchFields === true) {
      this.showSearchFields = false;
    } else {
      this.showSearchFields = true;
    }
  }

  /*---------------------------------------FIELDS  -- VIEWS -----------------------------------------------------------*/ 

  /*---------------------------------------FIELDS  -- DATABASE-----------------------------------------------------------*/   
  getAllFields() {
    this.globalVar.consoleLog(this.globalVar.logToConsole, '-> FIELDS -> getAllFields->...Starting...-> ', null);
    this._fieldsService.getAllFields()
      .subscribe(data => { this.fields = data;
                           this.order = 'fieldName';
                           this.reverse = false;
                           this.caseInsensitive = true;
                           this.sortedCollection = this.orderPipe.transform(this.fields, 'fieldName');
                           this.fields = this.sortedCollection ;
                           this.showFieldsList = true;
                         },
                error => { if (this.globalVar.handleError(error)) { throwError(error) };  });
  }
  updateOneFieldStatus(updateField: Field): void {
    this.globalVar.consoleLog(this.globalVar.logToConsole, '-> FIELDS -> updateOneFieldStatus->...Starting...-> ', null);
    this._fieldsService.updateOneFieldStatus(updateField)
      .subscribe(data => { this.getAllFields(); },
                error => { if (this.globalVar.handleError(error)) { throwError(error) };  });
  }
  loadDefaultFieldsAndValues(){
    this.globalVar.consoleLog(this.globalVar.logToConsole, '-> FIELDS -> loadDefaultFieldsAndValues->...Starting...-> ', null);
    this._fieldsService.loadDefaultFieldsAndValues()
      .subscribe(data => { this.globalVar.getFieldAndValues();
                           this.getAllFields(); 
                          },
                error => { if (this.globalVar.handleError(error)) { throwError(error) };  }); 
  }
  /* -------------------------------------FIELDS -- DELETE -------------------------------------------------------------*/
  confirmDeleteOneFieldBtnClick(field: Field){
    this._errorService.getDeleteMsgConfirmation("deleteMsg",this.formTitles.fields,this.formTitles.fields,
                            field,this.formTitles.fieldDeleteOne,field.fieldName,field.fieldExplan,field.fieldValuesString,"","","").
        then((deleteOK:boolean)=>{
          if (deleteOK === true){ this.deleteOneField(field); }
        })
  }
  confirmDeleteAllFieldsBtnClick(){
    this.globalVar.consoleLog(this.globalVar.logToConsole, '-> FIELDS -> confirmDeleteAllFieldsBtnClick->...Starting...-> ',null);
    this._errorService.getDeleteMsgConfirmation("deleteMsg",this.formTitles.fields,this.formTitles.fields,
                            null,this.formTitles.fieldDeleteAll,"","","","","","").
        then((deleteOK:boolean)=>{
          if (deleteOK === true){ this.deleteAllFields(); }
        })
  } 
  deleteAllFields(){
    this.globalVar.consoleLog(this.globalVar.logToConsole, '-> FIELDS -> deleteAllFields->...Starting...->', null);
    this._fieldsService.deleteAllFields()
    .subscribe(data => {  this.getAllFields(); },
              error => { if (this.globalVar.handleError(error)) { throwError(error) };  });
  }
  deleteOneField(delField: Field){
    this.globalVar.consoleLog(this.globalVar.logToConsole, '-> FIELDS -> deleteOneField->...Starting...->', null);
    this._fieldsService.deleteOneField(delField)
    .subscribe(data => { this.getAllFields(); },
              error => { if (this.globalVar.handleError(error)) { throwError(error) };  });
  }

}
