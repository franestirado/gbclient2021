import { Component, OnInit } from '@angular/core';

import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { OrderPipe } from 'ngx-order-pipe';
import { throwError, Subject } from 'rxjs';

import { GlobalVarService } from '../aa-common/global-var.service';

import { Prov,ProvString } from './prov';
import { ProvsService } from './provs.service';
import { ProvsCreateComponent } from './provs-create.component';

@Component({
  selector      :'app-provs-select',
  templateUrl   :'./provs-select.component.html',
  styleUrls     :['./provs.component.css'] 
})
export class ProvsSelectComponent implements OnInit {
  public  provTitle     :string;
  public  provDate      :Date;
  callback              :any;
  result: Subject<any> = new Subject<any>();

  public formTitles = {
    'provTitle'         :'',
    'provSelTitle'      :'Seleccionar Proveedor', 
    'provDate'          :'',
  };
  formLabels = {
      'provId'          :'Id',
      'provDate'        :'Fecha',
      'provCif'         :'Cif',
      'provName'        :'Nombre',
      'provLongName'    :'Descripción',
      'provFactType'    :'Tipo Fact.', 
      'provSelect'      :'Selecc.',
      'provEditList'    :'Editar',
  };
  public  provs               :Prov[];
  private newProv             :Prov;
  public  searchProv          :ProvString;
  public  createProvFlag      :boolean;
  public  showProvs           :boolean;
  public  showSearchProvs     :boolean;
  public  caseInsensitive     :boolean;
  public  sortedCollection    :any[];
  private logToConsole        :boolean;
  public  orderProvsList      :string;
  public  reverseProvsList    :boolean; 
  
  constructor ( public bsModalRef: BsModalRef, private orderPipe: OrderPipe, private modalService: BsModalService,
                public globalVar: GlobalVarService, private _provsService: ProvsService ) { }

  ngOnInit(): void {
    this.showProvs = false;
    this.showSearchProvs = true;
    this.logToConsole = this.globalVar.logToConsole;
    this.logToConsole = true;
    this.globalVar.consoleLog(this.logToConsole,'-> SELECT PROVS -> ngOnInit ->...Starting...->', null);
    if (this.provDate === null) {      
      this.formTitles.provDate = '';
    } else {
      this.formTitles.provDate = new Date(this.provDate).toLocaleDateString();
    }       
    this.formTitles.provTitle = this.provTitle;
    this.searchProv = new ProvString();
    this.orderProvsList = 'provName';
    this.reverseProvsList = false;
    this.caseInsensitive = true;
    this.getAllProvs();
  }
  //--------------------------------------- SELECT PROV -- GET PROVS ---------------------------------------------------------
  closeSelectProvBtnClick(){
    if (this.bsModalRef.content.callback != null){
        //this.bsModalRef.content.callback(null);
        this.result.next(null);
        this.bsModalRef.hide();
      }
  }
  selectedProvBtnClick(selProv: Prov){
    if (this.bsModalRef.content.callback != null){
        //this.bsModalRef.content.callback(selProv);
        this.result.next(selProv);
        this.bsModalRef.hide();
      }
  }
  setOrderProvBtnClick(value: string) {
    if (this.orderProvsList === value) {
      this.reverseProvsList = !this.reverseProvsList;
    }
    this.orderProvsList = value;
  } 
  createProvBtnClick(selProv :Prov){
    this.newProv = new Prov();
    this.createProvFlag = true;
    this.createProv();
  }
  editProvBtnClick(selProv :Prov){
    this.newProv = selProv;
    this.createProvFlag = false;
    this.createProv();
  }
  private createProv(){
    const modalInitialState = {
      newProv         :this.newProv,
      provTitle       :this.provTitle,
      provDate        :this.provDate,
      showCreateProv  :this.createProvFlag,
      showEditProv    :!this.createProvFlag, 
      callback         :'OK',   
    };
    this.globalVar.openModal(ProvsCreateComponent, modalInitialState, 'modalLgTop0Left0').
    then((result:any)=>{

    })
  } 
  public searchProvsBtnClick() {
    if (this.showSearchProvs === true) {
      this.showSearchProvs = false;
    } else {
      this.showSearchProvs = true;
    }
  }
  //--------------------------------------- SELECT PROV -- GET PROVS -----------------------------------------------------------------
  private getAllProvs() {
    this.globalVar.consoleLog(this.logToConsole,'-> SELECT PROVS -> getAllProvs ->...Starting...->', null);
    this._provsService.getAllProvs()
      .subscribe(data => { this.provs = data;
                           this.sortedCollection = this.orderPipe.transform(this.provs,this.orderProvsList,this.reverseProvsList,this.caseInsensitive);
                           this.provs = this.sortedCollection;
                           this.showProvs = true;                             
                          },
                error => { if (this.globalVar.handleError(error)) { throwError(error) };});
  }

}