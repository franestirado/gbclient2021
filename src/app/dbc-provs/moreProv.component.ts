import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { Subject } from 'rxjs';

import { Prov,MProv } from './prov';
import { GlobalVarService } from '../aa-common/global-var.service';
import { ValueRecord } from '../dbc-fields/field';
import { ValidationService } from '../aa-common/validation.service';

@Component({
  selector      :'app-select-prov',
  templateUrl   :'./moreProv.component.html',
  styleUrls     :['./provs.component.css']
})
export class MoreProvComponent implements OnInit {
  public createMoreProvFlag :boolean;
  public newProv            :Prov;
  public newMoreProv        :MProv; 
  public moreProvsList      :MProv[]; 
  public moreProvTitle      :string;
  callback                  :any;
  result: Subject<any> = new Subject<any>();

  public formTitles = {
    'provMoreData'    :'Mas Datos Proveedor',
    'moreProvs'       :'Más Datos del PROVEEDOR',
    };
  public formLabels = {
    'provId'          :'Id',
    'provCifType'     :'Tipo CIF',
    'provCif'         :'CIF Proveedor',
    'provName'        :'Nombre Proveedor',
    'provLongName'    :'Nombre Largo',
    'mprovId'         :'Id',
    'mprovProvId'     :'Prov Id',
    'mprovType'       :'Tipo Datos',
    'mprovData1'      :'Texto',
    'mprovData2'      :'Mas...',
  };
  public moreProvForm                     :FormGroup;
  public localMoreProvTypesRecordList     :ValueRecord[];
  private logToConsole                    :boolean;
  /*---------------------------------------MORE PROVS -- CONSTRUCTOR-----------------------------------------------------------*/
  constructor ( private bsModalRef: BsModalRef, private fb:FormBuilder, 
                private viewContainer: ViewContainerRef, private globalVar: GlobalVarService, ) { }
  /*---------------------------------------MORE PROVS -- NG ON INIT-----------------------------------------------------------*/
  ngOnInit(): void {  
    this.logToConsole = true;
    this.moreProvForm = this.fb.group({
      mprovId:        [''],
      mprovProvId:    [''],
      mprovType:      ['', [Validators.required, Validators.minLength(3), Validators.maxLength(64)]  ],
      mprovData1:     ['', [Validators.required, Validators.minLength(3), Validators.maxLength(128)] ],
      mprovData2:     ['', [Validators.minLength(3), Validators.maxLength(128)] ],
    }); 
    this.localMoreProvTypesRecordList = this.globalVar.mProvTypesRecordList;
    if (this.createMoreProvFlag  === true){
      this.moreProvForm.get('mprovProvId').setValue(this.newProv.provId);  
      this.moreProvForm.get('mprovType').setValue(this.localMoreProvTypesRecordList[0].value);    
    } else {
      this.moreProvForm.get('mprovId').setValue(this.newMoreProv.mprovId);
      this.moreProvForm.get('mprovProvId').setValue(this.newMoreProv.mprovProvId);
      this.moreProvForm.get('mprovType').setValue(this.newMoreProv.mprovType);
      this.moreProvForm.get('mprovData1').setValue(this.newMoreProv.mprovData1);
      this.moreProvForm.get('mprovData2').setValue(this.newMoreProv.mprovData2);            
    }
    this.moreProvForm.get('mprovType').valueChanges.subscribe((data:string) => {   
        this.setMProvTypeValidation(data);
    });
    this.globalVar.consoleLog(this.logToConsole,'->morePROVS->onInit->starting->',null); 
  }
  /*---------------------------------------MORE PROVS -- FORMS-----------------------------------------------------------*/
  setMProvTypeValidation (selectedValue: string) {
    const mprovData1Control = this.moreProvForm.get('mprovData1');
    if (selectedValue === undefined ) {return;}
    selectedValue = selectedValue.toLowerCase();
    switch (selectedValue) {
      case "email":
        mprovData1Control.setValidators([Validators.required,ValidationService.validEmail('EMAIL')]);
        break;
      case "web":
        mprovData1Control.setValidators([Validators.required,ValidationService.validWeb('WEB')]);
        break;
      case "ñ_mobile":
        mprovData1Control.setValidators([Validators.required,ValidationService.validÑ_Mobile('Ñ_MOBILE')]);
        break;
      case "ñ_phone":
        mprovData1Control.setValidators([Validators.required,ValidationService.validÑ_Phone('Ñ_PHONE')]);
        break;
      case "mobile":
        mprovData1Control.setValidators([Validators.required,ValidationService.validMobile('MOBILE')]);
        break;
      case "phone":
        mprovData1Control.setValidators([Validators.required,ValidationService.validPhone('PHONE')]);
        break;
      default:
        mprovData1Control.setValidators([Validators.required,Validators.minLength(3),Validators.maxLength(64)]);
        break;
    }
    mprovData1Control.updateValueAndValidity();
  }
  copyMProvFormDataToMProv(){
    this.newMoreProv = new MProv();
    if (this.createMoreProvFlag  === false){
      this.newMoreProv.mprovId = this.moreProvForm.controls.mprovId.value;
    }
    this.newMoreProv.mprovProvId = this.moreProvForm.controls.mprovProvId.value;
    this.newMoreProv.mprovType   = this.moreProvForm.controls.mprovType.value;
    this.newMoreProv.mprovData1  = this.moreProvForm.controls.mprovData1.value;
    this.newMoreProv.mprovData2  = this.moreProvForm.controls.mprovData2.value;
  }
  copyNewMProvDataToMprovForm(){
    this.moreProvForm.patchValue( {mprovId: this.newMoreProv.mprovId, mprovProvId: this.newMoreProv.mprovProvId, 
      mprovType: this.newMoreProv.mprovType, mprovData1: this.newMoreProv.mprovData1, mprovData2: this.newMoreProv.mprovData2} );
  }
  cleanMProvFormData(){
    this.moreProvForm.patchValue( {mprovId:'', mprovProvId:'',mprovType: this.localMoreProvTypesRecordList[0].value, mprovData1: '', mprovData2: ''} );
  }
  /*---------------------------------------MORE PROVS -- GENERAL-----------------------------------------------------------*/
  /*---------------------------------------MORE PROVS -- BTN CLICK-----------------------------------------------------------*/  
  CloseMoreProvsCreateModalBtnClick(){
    if (this.bsModalRef.content.callback != null){     
      //this.bsModalRef.content.callback(null);
      this.result.next(null);
      this.bsModalRef.hide();
    }
  }
  saveMoreProvBtnClick(){
    if (this.bsModalRef.content.callback != null){
        this.copyMProvFormDataToMProv();
        //this.bsModalRef.content.callback(this.newMoreProv);
        this.result.next(this.newMoreProv);
        this.bsModalRef.hide();
      }
  }
  /*---------------------------------------MORE PROVS -- DATABASE-----------------------------------------------------------*/ 
}