import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { throwError } from 'rxjs';

import { OrderPipe } from 'ngx-order-pipe';
import { BsModalService } from 'ngx-bootstrap/modal';

import { GbViewsService } from '../dbc-gbviews/gbviews.service';
import { GlobalVarService } from '../aa-common/global-var.service';
import { ErrorService } from '../aa-errors/error.service';
import { ValueRecord } from '../dbc-fields/field';
import { SelectValueRecordListComponent } from '../aa-common/select-VRList.component';
import { FactModelSelectComponent } from '../dbc-factmodels/factmodel-select.component';

import { Prov,ProvString } from './prov';
import { ProvsService } from './provs.service';
import { ProvsCreateComponent } from './provs-create.component';

@Component({
  selector      :'app-provs',
  templateUrl   :'./provs.component.html',
  styleUrls     :['./provs.component.css']
})
export class ProvsComponent implements OnInit { 
  public formTitles = {    
    'provs'             :'Proveedores',
    'provsTotalString'  :'',
    'provDeleteOne'     :'¿ Seguro que quiere BORRAR este PROVEEDOR ?',
    'provDeleteAll'     :'¿ Seguro que quiere BORRAR TODOS los PROVEEDORES ?',
    'provsList'         :'Lista de Proveedores',
    'provEditList'      :'Editar',
    'provDelete'        :'Borrar',
    'listFactModelsProv':'Model.Fact',
  };
  public formLabels = {
    'id'                :'#####',
    'provDate'          :'Fecha',
    'provId'            :'Id',
    'provCifType'       :'Tipo CIF',
    'provCif'           :'CIF',
    'provName'          :'Nombre',
    'provLongName'      :'Nombre Largo',
    'provFactType'      :'Tipo Factura',
    'provPayType'       :'Forma Pago', 
    'provFactNote'      :'Fact/Nota', 
    'provFactOrNote'    :'Factura o Nota', 
    'provStatus'        :'Estado',
    'factura'           :'Fact', 
    'nota'              :'Nota', 
  };
  private showCreateProv        :boolean;
  private showEditProv          :boolean;
  public  showProvsList         :boolean;
  private currentDate           :Date;
  public  provs                 :Prov[];
  private newProv               :Prov;
  public  searchProv            :ProvString;
  public  showSearchProvs       :boolean;
  public  orderProvsList        :string;
  public  reverseProvsList      :boolean;
  public  caseInsensitive       :boolean;
  private sortedCollection      :any[];
  private logToConsole          :boolean;
  public  filterProvs           :boolean;
  public  selectedProvFactType  :string;
  public  selectedProvPayType   :string;
  public  selectedProvFactOrNote          :string;
  public  localFactOrNoteRecordList       :ValueRecord [];
  public  localFactPayTypesRecordList     :ValueRecord [];
  public  localFactTypesSubDepsRecordList :ValueRecord [];

  /*---------------------------------------PROVS  -- CONSTRUCTOR-----------------------------------------------------------*/
  constructor ( private viewContainer: ViewContainerRef, private orderPipe: OrderPipe, private modalService: BsModalService,
                public globalVar: GlobalVarService, private _errorService:ErrorService, private _gbViewsService: GbViewsService,
                private _provsService: ProvsService ) {
  //   this.getGbViews('gbprovs');
  }
  /*---------------------------------------PROVS  -- NG ON INIT-----------------------------------------------------------*/
    ngOnInit() { 
    this.logToConsole = this.globalVar.logToConsole;
    this.logToConsole = true;   
    this.currentDate = this.globalVar.currentDate;
    this.caseInsensitive = true;
    this.newProv = new Prov();
    this.searchProv = new ProvString();    
    this.showProvsList = true;  
    this.filterProvs = false;
    this.selectedProvFactType = "";
    this.selectedProvPayType  = "";
    this.selectedProvFactOrNote = "";
    this.localFactTypesSubDepsRecordList = this.globalVar.factTypesSubDepsRecordList; 
    this.localFactPayTypesRecordList = this.globalVar.factPayTypesRecordList;
    var selectedStrings = ["Factura","Nota"];
    this.localFactOrNoteRecordList = this.globalVar.prepareRecordListFromArrayOfStrings(selectedStrings);
    this.getAllProvs();
  }
  /*---------------------------------------PROVS  -- FORMS-----------------------------------------------------------*/

  /*---------------------------------------PROVS  -- GENERAL-----------------------------------------------------------*/

  /*---------------------------------------PROVS  -- BTN CLICK-----------------------------------------------------------*/ 
  changeShowProvsListBtnClick(){    
    if (this.showProvsList === true) {    
      this.showProvsList = false;
    } else {         
      this.showProvsList = true;
      this.getAllProvs();
    }    
  }
  selectProvsByFactTypeBtnClick(){
    this.globalVar.consoleLog(this.logToConsole,'->PROVS->selectFactPayTypeBtnClick->...Starting 1->', null); 
    this.showProvsList = false;
    this.selectedProvFactType = "";
    this.selectedProvPayType  = "";
    this.selectedProvFactOrNote = "";
    if (this.localFactTypesSubDepsRecordList.length < 1) { return;  }
    const modalInitialState = {
      localValueRecordList  :this.localFactTypesSubDepsRecordList,
      selectTitle1          :"Proveedores",
      selectTitle2          :"Seleccionar Tipo de Factura",
      selectMessage         :"Seleccione el Tipo de Factura de los PROVEEDORES que quiere ver",
      selectLabel           :"Tipo Factura:",       
      selectFooter          :"Proveedores",
      callback              :'OK',   
    };
    this.globalVar.openModal(SelectValueRecordListComponent, modalInitialState, 'modalLgTop0Left0').
    then((result:any)=>{
      if (result != null) {
        this.selectedProvFactType = result;
        this.filterProvs = true;
        this.getAllProvsWithFactType(this.selectedProvFactType);
      }
      else  {
        this.filterProvs = false;
        this.getAllProvs();
      }
    })
  }
  selectProvsByPayTypeBtnClick(){
    this.globalVar.consoleLog(this.logToConsole,'->PROVS->selectProvsByPayTypeBtnClick->...Starting 1->', null); 
    this.showProvsList = false;
    this.selectedProvFactType = "";
    this.selectedProvPayType  = "";
    this.selectedProvFactOrNote = "";
    if (this.localFactPayTypesRecordList.length < 1) { return;  }  
    const modalInitialState = {
      localValueRecordList  :this.localFactPayTypesRecordList,
      selectTitle1          :"Proveedores",
      selectTitle2          :"Seleccionar Forma de Pago",
      selectMessage         :"Seleccione la Forma de Pago de los PROVEEDORES que quiere ver",
      selectLabel           :"Forma de Pago:",       
      selectFooter          :"Proveedores",
      callback              :'OK',   
    };
    this.globalVar.openModal(SelectValueRecordListComponent, modalInitialState, 'modalLgTop0Left0').
    then((result:any)=>{
      if (result != null) {
        this.filterProvs = true;
        this.selectedProvPayType = result;
        this.getAllProvsWithPayType(this.selectedProvPayType);
      }
      else  {
        this.filterProvs = false;
        this.getAllProvs();
      }
    })
  }
  selectProvsByFactOrNoteBtnClick(){
    this.globalVar.consoleLog(this.logToConsole,'->PROVS->selectProvsByFactOrNoteBtnClick->...Starting 1->', null); 
    this.showProvsList = false;
    this.selectedProvFactType = "";
    this.selectedProvPayType  = "";
    this.selectedProvFactOrNote = "";
    const modalInitialState = {
      localValueRecordList  :this.localFactOrNoteRecordList,
      selectTitle1          :"Proveedores",
      selectTitle2          :"Seleccionar Factura o Nota",
      selectMessage         :"Seleccione Factura o Nota de los PROVEEDORES que quiere ver",
      selectLabel           :"Factura/Nota:",       
      selectFooter          :"Proveedores",
      callback              :'OK',   
    };
    this.globalVar.openModal(SelectValueRecordListComponent, modalInitialState, 'modalLgTop0Left0').
    then((result:any)=>{
      if (result != null) {
        this.filterProvs = true;
        this.selectedProvFactOrNote = result;
        this.getAllProvsWithFactOrNote(this.selectedProvFactOrNote);
      }
      else  {
        this.filterProvs = false;
        this.getAllProvs();
      }
    })
  }
  listFactModelsWithThisProvBtnClick(selProv :Prov){
    this.globalVar.consoleLog(this.logToConsole,'->PROVS->listFactModelsWithThisProvBtnClick->...Starting->', null); 
    const modalInitialState = {
      factModels      :null, 
      selectedProv    :selProv,
      modelsTitle     :'',
      modelsDate      :     null,
      editFactModel   :true, 
      callback        :'OK',   
    };
    this.globalVar.openModal(FactModelSelectComponent, modalInitialState, 'modalLgTop0Left0').
    then((result:any)=>{
      if (this.filterProvs === true) {
        if ((this.selectedProvFactType !=null)  &&(this.selectedProvFactType !=""))  this.getAllProvsWithFactType(this.selectedProvFactType);
        if ((this.selectedProvPayType  !=null)  &&(this.selectedProvPayType  !=""))  this.getAllProvsWithPayType(this.selectedProvPayType);
        if ((this.selectedProvFactOrNote!= null)&&(this.selectedProvFactOrNote!=""))  this.getAllProvsWithFactOrNote(this.selectedProvFactOrNote);
    } else  this.getAllProvs(); 
    })
  }
  showCreateProvBtnClick(){        
      this.showCreateProv = true;
      this.showEditProv = false;       
      this.newProv = new Prov();
      this.createProvModal();
  }
  showEditProvBtnClick(modProv: Prov){
    if (this.isProvNotBlocked(modProv)){
      this.globalVar.consoleLog(this.logToConsole,'->PROVS->showEditProvBtnClick->editing->', null);      
      this.showEditProv = true;
      this.showCreateProv = false;       
      this.newProv = modProv;     
      this.createProvModal();
    }
  }
  createProvModal(){
    const modalInitialState = {
      newProv           :this.newProv,
      provTitle         :this.formTitles.provsList,
      provDate          :this.currentDate,
      showCreateProv    :this.showCreateProv,
      showEditProv      :this.showEditProv, 
      callback          :'OK',   
    };
    this.globalVar.openModal(ProvsCreateComponent, modalInitialState, 'modalXlTop0Left3').
    then((result:any)=>{
      if (result != null) this.newProv = result;  
      this.getAllProvs();
    })
  }  
  isProvNotBlocked(selectedProv: Prov):boolean{
    this.globalVar.consoleLog(this.logToConsole,'->PROVS->isProvNotBlocked->...Starting->', null);
    if (selectedProv.provStatus === false){
      this._errorService.showErrorModal( "PROVEEDORES", "PROVEEDORES", "PROVEEDOR BLOQUEADO"+"->",
                  "Proveedor->"+selectedProv.provName, "Descripción->"+selectedProv.provLongName, 
                  selectedProv.provCifType+"->"+selectedProv.provCif,
                  "Prov.Id.->"+selectedProv.provId, "Estado->"+selectedProv.provStatus,"");
      return false;
    } else return true;
  }
  lockProvBtnClick(modProv: Prov){
    if (modProv.provStatus === true) modProv.provStatus = false;
    else  modProv.provStatus = true;
    this.updateOneProv(modProv);
  }
  searchProvsBtnClick() {
    if (this.showSearchProvs === true) {
      this.showSearchProvs = false;
    } else {
      this.showSearchProvs = true;
    }   
  }
  setOrderBtnClick(value: string) {
    if (this.orderProvsList === value) {
      this.reverseProvsList = !this.reverseProvsList;
    }
    this.orderProvsList = value;
    console.log(this.orderProvsList);
  }
  /*---------------------------------------PROVS  -- VIEWS -----------------------------------------------------------*/ 
  getGbViews(viewName: string) {
    this.globalVar.consoleLog(this.logToConsole,'->PROVS->getGbViews->...Starting->', null);
    this._gbViewsService.getGbViews(viewName)
      .subscribe(data => { this.formTitles = data['formTitles'];
                           this.formLabels = data['formLabels'];
                         },
                error => { if (this.globalVar.handleError(error)) { throwError(error) }; });
  }
  /*---------------------------------------PROVS  -- DATABASE-----------------------------------------------------------*/   
  getAllProvs() {
    this.globalVar.consoleLog(this.logToConsole,'->PROVS->getAllProvs->...Starting->', null);        
    this._provsService.getAllProvs()
      .subscribe(data => { this.provs = data;
                           this.orderProvsList = 'provName';
                           this.reverseProvsList = false;
                           this.sortedCollection = this.orderPipe.transform(this.provs, 'provName');
                           this.provs = this.sortedCollection;    
                           this.formTitles.provsTotalString =this.provs.length+'..Provs'  
                           this.showProvsList = true;  
                           this.filterProvs = false;                                      
                         },
                error => { if (this.globalVar.handleError(error)) { throwError(error) }; });
  }
  getAllProvsWithFactType(selFactType :string){
    this.globalVar.consoleLog(this.logToConsole,'->PROVS->getAllProvsWithFactType->...Starting->', null);        
    this._provsService.getAllProvsWithFactType(selFactType)
      .subscribe(data => { this.provs = data;
                           this.orderProvsList = 'provName';
                           this.reverseProvsList = false;
                           this.sortedCollection = this.orderPipe.transform(this.provs, 'provName');
                           this.provs = this.sortedCollection;    
                           this.formTitles.provsTotalString =this.provs.length+'..Provs'  
                           this.showProvsList = true;                    
                         },
                error => { if (this.globalVar.handleError(error)) { throwError(error) }; });
  }
  getAllProvsWithPayType(selPayType :string){
    this.globalVar.consoleLog(this.logToConsole,'->PROVS->getAllProvsWithPayType->...Starting->', null);        
    this._provsService.getAllProvsWithPayType(selPayType)
      .subscribe(data => { this.provs = data;
                           this.orderProvsList = 'provName';
                           this.reverseProvsList = false;
                           this.sortedCollection = this.orderPipe.transform(this.provs, 'provName');
                           this.provs = this.sortedCollection;    
                           this.formTitles.provsTotalString =this.provs.length+'..Provs'  
                           this.showProvsList = true;                    
                         },
                error => { if (this.globalVar.handleError(error)) { throwError(error) }; });
  }
  getAllProvsWithFactOrNote(factOrNote :string){
    this.globalVar.consoleLog(this.logToConsole,'->PROVS->getAllProvsWithFactOrNote->...Starting->', null);        
    this._provsService.getAllProvsWithFactOrNote(factOrNote)
      .subscribe(data => { this.provs = data;
                           this.orderProvsList = 'provName';
                           this.reverseProvsList = false;
                           this.sortedCollection = this.orderPipe.transform(this.provs, 'provName');
                           this.provs = this.sortedCollection;    
                           this.formTitles.provsTotalString =this.provs.length+'..Provs'  
                           this.showProvsList = true;                    
                         },
                error => { if (this.globalVar.handleError(error)) { throwError(error) }; });
  }
  updateOneProv(modProv: Prov): void {
    this.globalVar.consoleLog(this.logToConsole,'->PROVS->updateOneProv->updating->', null);
    this._provsService.updateOneProv(modProv)
      .subscribe(data => { this.newProv = data; },
                error => { if (this.globalVar.handleError(error)) { throwError(error) }; });
  }
  /* -------------------------------------PROVS -- DELETE -------------------------------------------------------------*/
  confirmDeleteOneProvBtnClick(prov: Prov){
    this._errorService.getDeleteMsgConfirmation("deleteMsg",this.formTitles.provs,this.formTitles.provs,
                            prov,this.formTitles.provDeleteOne,prov.provName,
                            prov.provLongName,prov.provCif,"","","",).
        then((deleteOK:boolean)=>{
          if (deleteOK === true){ this.deleteOneProv(prov); }
        })
  }
  confirmDeleteAllProvsBtnClick(){
    this._errorService.getDeleteMsgConfirmation("deleteMsg",this.formTitles.provs,this.formTitles.provs,
                            null,this.formTitles.provDeleteAll,"","","","","","").
        then((deleteOK:boolean)=>{
          if (deleteOK === true){ this.deleteAllProvs(); }
        })
  } 
  deleteAllProvs(){
    this.globalVar.consoleLog(this.logToConsole,'->PROVS->deleteAllProvs->deleting All->', null);
    this._provsService.delAllProvs()
    .subscribe(data => { this.getAllProvs(); },
              error => { if (this.globalVar.handleError(error)) { throwError(error) }; });
  }
  deleteOneProv(delProv: Prov){
    this.globalVar.consoleLog(this.logToConsole,'->PROVS->deleteOneProv->deleting One->', null);
    this._provsService.delOneProv(delProv)
    .subscribe(data => { this.getAllProvs(); },
              error => { if (this.globalVar.handleError(error)) { throwError(error) }; });
  }
  
}
