import {Routes, RouterModule} from '@angular/router';
import { NgModule } from '@angular/core';

import {ToolsComponent} from './aa-tools/tools.component';
import {ContactComponent} from './aa-contact/contact.component';
import {InfoComponent} from './aa-info/info.component';
import {LoginComponent} from './acc-login/login.component';
import {UsersComponent} from './acc-access/users.component';
import {PageNotFoundComponent} from './aa-page-not-found/page-not-found.component';

import {FieldsComponent} from './dbc-fields/fields.component';
import {BaresComponent} from './cfg-config/bares.component';
import {GbViewsComponent} from './dbc-gbviews/gbviews.component';

import {HdComponent} from './dbx-hd/hd.component';
import {ProvsComponent} from './dbc-provs/provs.component';
import {ArtsComponent} from './dbc-arts/arts.component';
import {FactsComponent} from './dbx-facts/facts.component';
import {CardsComponent} from './dbx-cards/cards.component';
import {MonthsComponent} from './dbx-months/months.component';
import {MaqsComponent} from './dbx-maqs/maqs.component';


const routes: Routes = [
  {path: 'tools',     component: ToolsComponent},
  {path: 'home',      component: ContactComponent},
  {path: 'contact',   component: ContactComponent},
  {path: 'info',      component: InfoComponent},
  {path: 'login',     component: LoginComponent},
  {path: 'users',     component: UsersComponent},
  {path: 'bares',     component: BaresComponent},  
  {path: 'fields',    component: FieldsComponent},  
  {path: 'gbviews',   component: GbViewsComponent},
  {path: 'hojasdia',  component: HdComponent},
  {path: 'provs',     component: ProvsComponent},
  {path: 'arts',      component: ArtsComponent},
  {path: 'facts',     component: FactsComponent},
  {path: 'cards',     component: CardsComponent},
  {path: 'months',    component: MonthsComponent},
  {path: 'maqs',      component: MaqsComponent},
  {path: 'error',     component: PageNotFoundComponent},
  {path: '',          redirectTo: 'login', pathMatch: 'full'},
  {path: '**',        component: PageNotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [
  ToolsComponent,  PageNotFoundComponent, ContactComponent, InfoComponent, LoginComponent, UsersComponent, 
  FieldsComponent, GbViewsComponent, 
  HdComponent, ProvsComponent, ArtsComponent, FactsComponent, CardsComponent, MonthsComponent , MaqsComponent ];
