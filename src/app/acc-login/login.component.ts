import { Component, OnInit } from '@angular/core';
import {GlobalVarService} from '../aa-common/global-var.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(public globalVar: GlobalVarService) { }

  ngOnInit() {
  }

}
