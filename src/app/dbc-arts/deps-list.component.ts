import { Component, OnInit } from '@angular/core';
import { throwError, Subject} from 'rxjs';

import { OrderPipe } from 'ngx-order-pipe';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

import { GlobalVarService } from '../aa-common/global-var.service';
import { ErrorService } from '../aa-errors/error.service';

import { Dep, DepString } from './art';
import { ArtService } from './art.service';
import { DepsCreateComponent } from './deps-create.component';
import { SubDepsListComponent } from './subDeps-list.component';


@Component({
  selector      :'app-deps-list',
  templateUrl   :'./deps-list.component.html',
  styleUrls     :['./arts.component.css'] 
}) 
export class DepsListComponent implements OnInit {
  public depsTitle    :string;
  public depsDate     :Date;
  callback            :any;
  result: Subject<any> = new Subject<any>();

  public formTitles = {
    'edit'              :'Editar',
    'delete'            :'Borrar',
    'depsList'          :'Lista de Departamentos',
    'depsTotalString'   :'Deps',
    'deps'              :'Departamentos',
    'addSubDeps'        :'SubDptos',
    'depDeleteOne'      :'¿ Seguro que quiere BORRAR este DEPARTAMENTO ?',
    'depDeleteAll'      :'¿ Seguro que quiere BORRAR TODOS los DEPARTAMENTOS ?',
  };
  public formLabels = {
    'depId'             :'DepId',
    'depDate'           :'Fecha',
    'depName'           :'Nombre Dpto.',
    'depDescription'    :'Descripción',
    'depTag'            :'Tag',
    'depStatus'         :'Estado',   
  };
  private createDep             :boolean;
  public  showDepsList          :boolean;
  public  deps                  :Dep[];
  private newDep                :Dep;
  public  searchDep             :DepString;
  public  showSelectedDep       :boolean;
  private selectedDep           :Dep;
  public  showSearchDeps        :boolean;
  public  orderDepsList         :string;
  public  reverseDepsList       :boolean;
  public  caseInsensitive       :boolean;
  private sortedCollection      :any[];
  private logToConsole          :boolean;
  //--------------------------------------- DEPS LIST -- CONSTRUCTOR -----------------------------------------------------------
  constructor ( private orderPipe: OrderPipe, private bsModalRef: BsModalRef, private modalService: BsModalService,  
                private globalVar: GlobalVarService, private _errorService: ErrorService, private _artService: ArtService) { 

  }
  //--------------------------------------- DEPS LIST -- NG ON INIT -----------------------------------------------------------
  ngOnInit(): void {
    this.searchDep = new DepString();
    this.showSearchDeps = false;
    this.orderDepsList = 'depName';
    this.reverseDepsList = false;
    this.caseInsensitive = true;
    this.getAllDeps();
  }
  //--------------------------------------- DEPS LIST --  -------------------------------------------------------------------
  //--------------------------------------- DEPS LIST --  -------------------------------------------------------------------
  //--------------------------------------- DEPS LIST --  -------------------------------------------------------------------
  //--------------------------------------- DEPS LIST -- FORMS --------------------------------------------------------------
  //--------------------------------------- DEPS LIST -- GENERAL ------------------------------------------------------------
  //--------------------------------------- DEPS LIST -- BTN CLICK -----------------------------------------------------------
  public  listSubDepsInDepBtnClick(selectedDep: Dep){
    const modalInitialState = {
      subDepsTitle  :'Departemento, Lista SubDepartamentos',
      subDepsDate   :this.depsDate,
      selectedDep   :selectedDep,
      callback      :'OK',   
    };
    this.globalVar.openModal(SubDepsListComponent, modalInitialState, 'modalXlTop0Left6')
    .then((result:any)=>{ this.getAllDeps(); }) 
  }
  public  createDepBtnClick(){
    this.newDep = new Dep();
    this.createDep = true;
    this.createDepModal();
  }
  public  editDepBtnClick(editDep: Dep){
    if (this.isDepNotBlocked(editDep)){ 
      this.newDep = editDep;
      this.createDep = false;
      this.createDepModal();
    }
  }
  public  createDepModal(){
    const modalInitialState = {
      newDep      :this.newDep,
      depsTitle   :'Departamentos',
      depsDate    :this.globalVar.currentDate,
      createDep   :this.createDep,
      callback    :'OK',   
    };
    this.globalVar.openModal(DepsCreateComponent, modalInitialState, 'modalLgTop0Left0').
    then((result:any)=>{ this.getAllDeps(); })   
  }
  public  lockDepBtnClick(modDep: Dep){
    if (modDep.depStatus === true) modDep.depStatus = false;
    else  modDep.depStatus = true;
    this.updateOneDep(modDep);
  }
  private isDepNotBlocked(selectedDep: Dep):boolean{
    this.globalVar.consoleLog(this.logToConsole,'-> ARTS -> isDepNotBlocked ->...Starting...->', null);
    if (selectedDep.depStatus === false){
      this._errorService.showErrorModal( "DEPARTAMENTOS", "DEPARTAMENTOS", "DEPARTAMENTO BLOQUEADO"+"->",
                        "Departamento->"+selectedDep.depName, "Descripión->"+selectedDep.depDescription, 
                        "Id->"+selectedDep.depId, "Estado->"+selectedDep.depStatus,
                        "Fecha->"+selectedDep.depDate, "");
      return false;
    } else return true;
  }
  public  searchDepsBtnClick() {
    if (this.showSearchDeps === true) {
      this.showSearchDeps = false;
    } else {
      this.showSearchDeps = true;
    }
  }
  public closedDepsListModalBtnClick(){
    if (this.bsModalRef.content.callback != null){
      //this.bsModalRef.content.callback(this.newDep);
      this.result.next('OK');
      this.bsModalRef.hide();
      }
  }
  //--------------------------------------- DEPS LIST -- DATABASE -----------------------------------------------------------
  private getAllDeps() {
    this.globalVar.consoleLog(this.logToConsole,'-> ARTS -> getAllDeps ->...Starting...->', null);
    this._artService.getAllDeps()
      .subscribe(data => { this.deps = data;
                           this.sortedCollection = this.orderPipe.transform(this.deps,this.orderDepsList,this.reverseDepsList,this.caseInsensitive);
                           this.deps = this.sortedCollection;
                           this.formTitles.depsTotalString =this.deps.length+'..Deps'; 
                           this.showDepsList = true; 
                           },
                error => { if (this.globalVar.handleError(error)) { throwError(error) }; });
  }
  private updateOneDep(modDep: Dep): void {
    this.globalVar.consoleLog(this.logToConsole,'-> ARTS -> updateOneDep ->...Starting...->', modDep);
    this._artService.updateOneDep(modDep)
      .subscribe(data => { this.getAllDeps(); },
                error => { if (this.globalVar.handleError(error)) { throwError(error) }; });
  }
  //--------------------------------------- DEPS LIST -- DELETE -----------------------------------------------------------
  public  confirmDeleteOneDepBtnClick(dep: Dep){
    this._errorService.getDeleteMsgConfirmation("deleteMsg",this.formTitles.deps,this.formTitles.deps,
                            dep,this.formTitles.depDeleteOne,dep.depName,dep.depDescription,dep.depTag,dep.depDate.toString(),"","",)
      .then((deleteOK:boolean)=>{
          if (deleteOK === true){ this.deleteOneDep(dep); }
       })     
  }
  public  confirmDeleteAllDepsBtnClick(){
    this._errorService.getDeleteMsgConfirmation("deleteMsg",this.formTitles.deps,this.formTitles.deps,
                            null,this.formTitles.depDeleteAll,"","","","","","")
      .then((deleteOK:boolean)=>{
          if (deleteOK === true){ this.deleteAllDeps(this.deps[0]); }
       })  
  } 
  private deleteAllDeps(delDep: Dep){
    this.globalVar.consoleLog(this.logToConsole,'-> ARTS -> deleteAllDeps ->...Starting...->', null);
    this._artService.delAllDeps(delDep)
    .subscribe(data => { this.getAllDeps(); },
              error => { if (this.globalVar.handleError(error)) { throwError(error) };});
  }
  private deleteOneDep(delDep: Dep){
    this.globalVar.consoleLog(this.logToConsole,'-> ARTS -> deleteOneDep ->...Starting...->', null);
    this._artService.delOneDep(delDep)
    .subscribe(data => { this.getAllDeps(); },
              error => { if (this.globalVar.handleError(error)) { throwError(error) }; });
  }

}