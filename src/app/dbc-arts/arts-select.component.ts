import { Component, OnInit } from '@angular/core';
import { throwError, Subject } from 'rxjs';

import { OrderPipe } from 'ngx-order-pipe';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
 
import { Art,ArtString } from './art';
import { ArtService } from './art.service';
import { ArtsCreateComponent } from './arts-create.component';
import { GlobalVarService } from '../aa-common/global-var.service';

@Component({
  selector      :'app-arts-select',
  templateUrl   :'./arts-select.component.html',
  styleUrls     :['./arts.component.css'] 
})
export class ArtsSelectComponent implements OnInit {
  public  artsTitle     :string;   
  public  artsDate      :Date;
  public  artsSelMany   :boolean;
  callback              :any;
  result: Subject<any> = new Subject<any>();

  public formTitles = {
    'artsSelTitle'      :'Seleccionar Artículos',  
    'artsTitle'         :'',   
    'artsDate'          :'',
    'arts'              :'Artículos',
    'artCreate'         :'Crear Artículo',
    'artEdit'           :'Modificar Artículo',
  };
  public formLabels = {
      'artId'           :'Id',
      'artDate'         :'Fecha',
      'artReference'    :'Referencia',
      'artName'         :'Nombre',
      'artLongName'     :'Descripción',
      'artType'         :'Tipo',
      'artSelect'       :'Seleccionar',
      'artEditList'     :'Editar',
  };
  public  arts                :Art[];
  private selectedArts        :Art[];
  public  searchArt           :ArtString;
  private newArt              :Art;
  public  showSearchArts      :boolean;
  private createArt           :boolean;
  public  title               :string;
  public  orderArtsList       :string;
  public  reverseArtsList     :boolean;
  public  caseInsensitive     :boolean;
  private sortedCollection    :any[];
  private logToConsole        :boolean;

  constructor ( private orderPipe: OrderPipe,  private bsModalRef: BsModalRef, private modalService: BsModalService,  
    private globalVar: GlobalVarService, private _artsService: ArtService) { }

  ngOnInit(): void {
    this.logToConsole = this.globalVar.logToConsole;
    this.logToConsole = true;
    this.globalVar.consoleLog(this.logToConsole,'-> SELECT ARTS -> ngOnInit ->...Starting...->', null);
    if (this.artsDate === null) {
      this.formTitles.artsDate = '';
    } else {
      this.formTitles.artsDate = new Date(this.artsDate).toLocaleDateString();
    }
    this.formTitles.artsTitle = this.artsTitle;
    this.searchArt = new ArtString();
    this.orderArtsList = 'artName';
    this.reverseArtsList = false;
    this.caseInsensitive = true;
    this.showSearchArts = true;
    this.selectedArts = new Array();
    this.getAllArts();
  }

  //--------------------------------------- SELECT ARTS -- FORMS --------------------------------------------------------------
  
  //--------------------------------------- SELECT ARTS  -- GENERAL -----------------------------------------------------------
  private saveSelectedArts(){
    this.selectedArts = null;
    if (this.arts === null) return;
    if (this.arts.length === 0) return;
    this.selectedArts = new Array();
    for(let j=0; j< this.arts.length; j++){
      if (this.arts[j].artStatus === true){
        this.selectedArts.push(this.arts[j]);
      }
    }
    if (this.selectedArts.length === 0) this.selectedArts = null;
  }
  private restoreSelectedArts(){
    if (this.selectedArts === null) return;
    if (this.selectedArts.length === 0) return;
    for(let k=0; k< this.selectedArts.length; k++){
      let readArt = this.arts.find(art => art.artId === this.selectedArts[k].artId);
      if (readArt != undefined) readArt.artStatus = true;
    }
  }
  //-------------------------------------- SELECT ARTS -- BTN CLICK -----------------------------------------------------------
  closeSelectArtsBtnClick(){
    if (this.bsModalRef.content.callback != null){
        //this.bsModalRef.content.callback(null);
        this.result.next(null);
        this.bsModalRef.hide();
      }
  }
  public selectedArstBtnClick(){
    this.saveSelectedArts();
    if (this.bsModalRef.content.callback != null){
        //this.bsModalRef.content.callback(this.selectedArts);
        this.result.next(this.selectedArts);
        this.bsModalRef.hide();
      }
  }
  public selectedArtBtnClick(selArt :Art){
    if (this.bsModalRef.content.callback != null){
      //this.bsModalRef.content.callback(selArt);
      this.result.next(selArt);
      this.bsModalRef.hide();
    }
  }
  public createArtBtnClick(){
    this.newArt = new Art();
    this.createArt = true;
    this.createArtModal();
  }  
  public editArtBtnClick(editArt: Art){
    this.newArt = editArt;
    this.createArt = false;
    this.createArtModal();
  } 
  public createArtModal(){
    // save list of selected arts
    this.saveSelectedArts();
    const modalInitialState = {
      newArt      :this.newArt,
      artsTitle   :this.artsTitle,
      artsDate    :this.artsDate,
      createArt   :this.createArt,
      callback    :'OK',   
    };
    this.globalVar.openModal(ArtsCreateComponent, modalInitialState, 'modalXlTop0Left3').
    then((result:any)=>{
      if (result != null) this.newArt = result;           
      this.getAllArts();
     }) 
  }  
  public searchArtsBtnClick() {
    if (this.showSearchArts === true) {
      this.showSearchArts = false;
    } else {
      this.showSearchArts = true;
    }
  }
  public setOrderArtBtnClick(value: string) {
    if (this.orderArtsList === value) {
      this.reverseArtsList = !this.reverseArtsList;
    }
    this.orderArtsList = value;
  }
  //--------------------------------------- SELECT ARTS -- DATABASE-------------------------------------------------------------
  private getAllArts() {
    this.globalVar.consoleLog(this.logToConsole,'-> SELECT ARTS -> getAllArts ->...Starting...->', null);
    this._artsService.getAllArts()
      .subscribe(data => {  this.arts = data;                          
                            for(let j=0; j< this.arts.length; j++){
                              this.arts[j].artStatus = false;
                            }
                            this.sortedCollection = this.orderPipe.transform(this.arts,this.orderArtsList,this.reverseArtsList = false,this.caseInsensitive);
                            this.arts = this.sortedCollection; 
                            this.restoreSelectedArts();                                                
                          },
                error => { if (this.globalVar.handleError(error)) { throwError(error) }; });
  }
}