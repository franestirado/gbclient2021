import { Component, OnInit } from '@angular/core';
import { throwError, Subject} from 'rxjs';

import { OrderPipe } from 'ngx-order-pipe';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

import { GlobalVarService } from '../aa-common/global-var.service';
import { ErrorService } from '../aa-errors/error.service';

import { SubDep, ArtType, ArtTypeString, Dep } from './art';
import { ArtService } from './art.service';
import { ArtTypesCreateComponent } from './artTypes-create.component';
import { ArtsListComponent } from './arts-list.component';

@Component({
  selector      :'app-artTypes-list',
  templateUrl   :'./artTypes-list.component.html',
  styleUrls     :['./arts.component.css'] 
}) 
export class ArtTypesListComponent implements OnInit {
  public artTypesTitle    :string;
  public artTypesDate     :Date;
  public selectedSubDep   :SubDep;
  callback                :any;
  result: Subject<any> = new Subject<any>();

  public formTitles = {
    'edit'                :'Editar',
    'delete'              :'Borrar',
    'artTypesList'        :'Lista de Tipos de Artículos',
    'artTypes'            :'Tipos de Artículos',
    'addArts'             :'Arts',
    'artTypesTotalString' :'ArtTypes',
    'addArtTypes'         :'TiposArts',
    'dep'                 :'Departamento',
    'subDep'              :'Subdepartamento',
    'selectedDep'         :'Departamento Seleccionado',
    'selectedSubDep'      :'Subdepartamento Seleccionado',
    'artTypeDeleteOne'    :'¿ Seguro que quiere BORRAR este TIPO DE ARTÍCULO ?',
    'artTypeDeleteAll'    :'¿ Seguro que quiere BORRAR TODOS los TIPOS DE ARTÍCULOS ?',
  };
  public formLabels = {
    'artTypeId'           :'ArtTypeId',
    'artTypeDate'         :'Fecha',
    'artTypeSubDepName'   :'Nombre.SubDpto.',
    'artTypeName'         :'Nombre.Tipo.Artículo',
    'artTypeDescription'  :'Descripción',
    'artTypeTag'          :'Tag',
    'artTypeSequence'     :'Sequence',
    'artTypeStatus'       :'Estado',
  };
  private createArtType       :boolean;
  public  showArtTypesList    :boolean;
  public  artTypes            :ArtType[];
  private newArtType          :ArtType;
  public  searchArtType       :ArtTypeString;
  public  showSelectedArtType :boolean;
  public  showSearchArtTypes  :boolean;
  public  orderArtTypesList   :string;
  public  reverseArtTypesList :boolean;
  public  showSelectedSubDep  :boolean;
  public  selectedDep         :Dep;
  public  caseInsensitive     :boolean;
  private sortedCollection    :any[];
  private logToConsole        :boolean;
  //--------------------------------------- ARTTYPES LIST -- CONSTRUCTOR -----------------------------------------------------------
  constructor ( private orderPipe: OrderPipe, private bsModalRef: BsModalRef, private modalService: BsModalService,  
                private globalVar: GlobalVarService, private _errorService: ErrorService, private _artService: ArtService) { 

  }
  //--------------------------------------- ARTTYPES LIST -- NG ON INIT -----------------------------------------------------------
  ngOnInit(): void {
    this.searchArtType = new ArtTypeString();
    this.orderArtTypesList = 'artTypeName';
    this.reverseArtTypesList = false;
    this.caseInsensitive = true;
    this.showSearchArtTypes = false;
    this.showSelectedSubDep = false;
    this.getArtTypesList();
  }
  //--------------------------------------- ARTTYPES LIST --  -------------------------------------------------------------------
  //--------------------------------------- ARTTYPES LIST --  -------------------------------------------------------------------
  //--------------------------------------- ARTTYPES LIST --  -------------------------------------------------------------------
  //--------------------------------------- ARTTYPES LIST -- FORMS --------------------------------------------------------------
  //--------------------------------------- ARTTYPES LIST -- GENERAL ------------------------------------------------------------
  //--------------------------------------- ARTTYPES LIST -- BTN CLICK -----------------------------------------------------------
  public  listArtsInArtTypeBtnClick(selArtType :ArtType ){
    const modalInitialState = {
      artsTitle         :'Tipo De Artículo, Lista Artículos',
      artsDate          :this.artTypesDate,
      selectedArtType   :selArtType,
      callback          :'OK',   
    };
    this.globalVar.openModal(ArtsListComponent, modalInitialState, 'modalXlTop0Left6')
    .then((result:any)=>{ this.getArtTypesList(); }) 
  }
  public  createArtTypeBtnClick(){
    this.newArtType = new ArtType();
    this.createArtType = true;
    this.createArtTypeModal();
  }
  public  editArtTypeBtnClick(editArtType: ArtType){
    if (this.isArtTypeNotBlocked(editArtType)){ 
      this.newArtType = editArtType;
      this.createArtType = false;
      this.createArtTypeModal();
    }
  }
  private createArtTypeModal(){
    const modalInitialState = {
      newArtType      :this.newArtType,
      subDeps         :null,
      artTypesTitle   :'Tipos de Artículos',
      artTypesDate    :this.globalVar.currentDate,
      createArtType   :this.createArtType,
      selectedSubDep  :this.selectedSubDep,
      callback        :'OK',   
    };
    this.globalVar.openModal(ArtTypesCreateComponent, modalInitialState, 'modalLgTop0Left0').
    then((result:any)=>{ this.getArtTypesList(); })
  }  
  private isArtTypeNotBlocked(selectedArtType: ArtType):boolean{
    this.globalVar.consoleLog(this.logToConsole,'-> ARTS -> isArtTypeNotBlocked ->...Starting...->', null);
    if (selectedArtType.artTypeStatus === false){
      this._errorService.showErrorModal( "TIPOS DE ARTÍCULOS", "TIPOS DE ARTÍCULOS", "TIPO DE ARTÍCULO BLOQUEADO"+"->",
                        "Tipo de Artículo->"+selectedArtType.artTypeName, "Descripión->"+selectedArtType.artTypeDescription, 
                        "Id->"+selectedArtType.artTypeId, "Estado->"+selectedArtType.artTypeStatus,
                        "Fecha->"+selectedArtType.artTypeDate, "");
      return false;
    } else return true;
  }
  public  checkGenericArtTypesBtnClick(){
    this.showArtTypesList  = false; 
    this.checkGenericArtTypes();
  }  
  public  lockArtTypeBtnClick(modArtType: ArtType){
    if (modArtType.artTypeStatus === true) modArtType.artTypeStatus = false;
    else  modArtType.artTypeStatus = true;
    this.updateOneArtType(modArtType);
  }  
  public  searchArtTypesBtnClick() {
    if (this.showSearchArtTypes === true) { 
      this.showSearchArtTypes = false;
    } else {
      this.showSearchArtTypes = true;
    }
  }
  public  setArtTypeOrderBtnClick(value: string) {
    if (this.orderArtTypesList === value) {
      this.reverseArtTypesList = !this.reverseArtTypesList;
    }
    this.orderArtTypesList = value;
  }  
  public closedArtTypesListModalBtnClick(){
    if (this.bsModalRef.content.callback != null){
      //this.bsModalRef.content.callback(this.newDep);
      this.result.next('OK');
      this.bsModalRef.hide();
      }
  }  
  //--------------------------------------- ARTTYPES LIST -- DATABASE -------------------------------------------------------------------
  private getArtTypesList(){
    if (this.selectedSubDep === null) this.getAllArtTypes();
    else {
      this.showSelectedSubDep = true;
      this.selectedDep = this.globalVar.getDepObjectFromDepId(this.selectedSubDep.subDepDepId);
      this.getAllArtTypesInSubDep(this.selectedSubDep);
    }
  }
  private getAllArtTypes() {
    this.globalVar.consoleLog(this.logToConsole,'-> ARTS -> getAllArtTypes ->...Starting...->', null);
    this._artService.getAllArtTypes()
      .subscribe(data => { this.artTypes = data;
                           this.sortedCollection = this.orderPipe.transform(this.artTypes,this.orderArtTypesList,this.reverseArtTypesList,this.caseInsensitive);
                           this.artTypes = this.sortedCollection;
                           this.formTitles.artTypesList ='Tipos Artículos en TODOS los Subdepartamentos';
                           this.formTitles.artTypesTotalString =this.artTypes.length+'..ArtTypes';
                           this.showArtTypesList = true;
                         },
                error => { if (this.globalVar.handleError(error)) { throwError(error) }; });
  }
  private getAllArtTypesInSubDep(selectedSubDep: SubDep){
    this.globalVar.consoleLog(this.logToConsole,'->ARTS ->getAllArtTypesInSubDep ->...Starting...->',null);
    this._artService.getAllArtTypesInSubDep(selectedSubDep)
      .subscribe(data => { this.artTypes = data;
                           this.sortedCollection = this.orderPipe.transform(this.artTypes,this.orderArtTypesList,this.reverseArtTypesList,this.caseInsensitive);
                           this.artTypes = this.sortedCollection;
                           this.formTitles.artTypesList ='Lista Tipos Artículos en el Subdepartamento ==> '+selectedSubDep.subDepName;
                           this.formTitles.dep ='Departamento ... '+this.selectedDep.depName;
                           this.formTitles.selectedDep ='->Nombre: '+this.selectedDep.depName+'->Descripcion: '+this.selectedDep.depDescription+
                                                        '->DepTag: '+this.selectedDep.depTag+'->DepId: '+this.selectedDep.depId+
                                                        '->Fecha: '+this.selectedDep.depDate;
                           this.formTitles.subDep ='Subdepartamento ... '+selectedSubDep.subDepName;
                           this.formTitles.selectedSubDep = '->Nombre: '+selectedSubDep.subDepName+'->Descripcion: '+selectedSubDep.subDepDescription+
                                                            '->DepTag: '+selectedSubDep.subDepTag+'->DepId: '+selectedSubDep.subDepId+
                                                            '->Fecha: '+selectedSubDep.subDepDate;
                           this.formTitles.artTypesTotalString =this.artTypes.length+'..ArtTypes';
                           this.showArtTypesList = true;
                         },
                error => { if (this.globalVar.handleError(error)) { throwError(error) }; });
  }
  private updateOneArtType(modArtType: ArtType): void {
    this.globalVar.consoleLog(this.logToConsole,'-> ARTS -> updateOneArtType ->...Starting...->', modArtType);
    this._artService.updateOneArtType(modArtType)
      .subscribe(data => { this.getArtTypesList(); },
                error => { if (this.globalVar.handleError(error)) { throwError(error) }; });
  }  
  private checkGenericArtTypes(): void {
    this.globalVar.consoleLog(this.logToConsole,'-> ARTS -> checkGenericArtTypes ->...Starting...->', null);
    this._artService.checkGenericArtTypes()
      .subscribe(data => { this.getArtTypesList(); },
                error => { if (this.globalVar.handleError(error)) { throwError(error) }; });
  }    
  //--------------------------------------- ARTTYPES LIST -- DELETE -------------------------------------------------------------------
  public  confirmDeleteOneArtTypeBtnClick(artType: ArtType){
    this._errorService.getDeleteMsgConfirmation("deleteMsg",this.formTitles.artTypes,this.formTitles.artTypes,
      artType,this.formTitles.artTypeDeleteOne,artType.artTypeName,artType.artTypeDescription,artType.artTypeTag,artType.artTypeTag.toString(),"","",)
      .then((deleteOK:boolean)=>{ if (deleteOK === true){ this.deleteOneArtType(artType); } }) 
  }
  public  confirmDeleteAllArtTypesBtnClick(){
    this._errorService.getDeleteMsgConfirmation("deleteMsg",this.formTitles.artTypes,this.formTitles.artTypes,null,this.formTitles.artTypeDeleteAll,"","","","","","")
      .then((deleteOK:boolean)=>{ if (deleteOK === true){ this.deleteAllArtTypes(this.artTypes[0]); } }) 
  } 
  private deleteAllArtTypes(delArtType: ArtType){
    this.globalVar.consoleLog(this.logToConsole,'-> ARTS -> deleteAllArtTypes ->...Starting...->', null);
    this._artService.delAllArtTypes(delArtType)
    .subscribe(data => { this.getArtTypesList(); },
              error => { if (this.globalVar.handleError(error)) { throwError(error) };});
  }
  private deleteOneArtType(delArtType: ArtType){
    this.globalVar.consoleLog(this.logToConsole,'-> ARTS -> deleteOneArtType ->...Starting...->', null);
    this._artService.delOneArtType(delArtType)
    .subscribe(data => { this.getArtTypesList(); },
              error => { if (this.globalVar.handleError(error)) { throwError(error) }; });
  }

}