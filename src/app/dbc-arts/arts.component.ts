import { Component, OnInit, TemplateRef, ComponentRef,ViewContainerRef } from '@angular/core';
import { throwError } from 'rxjs';

import { OrderPipe } from 'ngx-order-pipe';
import { BsModalService } from 'ngx-bootstrap/modal';

import { Art, ArtString, ArtType, Dep, SubDep } from './art';
import { ArtsCreateComponent } from './arts-create.component';
import { DepsListComponent } from './deps-list.component';
import { SubDepsListComponent } from './subDeps-list.component';
import { ArtTypesListComponent } from './artTypes-list.component';
import { ArtService } from './art.service';

import { ErrorService } from '../aa-errors/error.service';
import { GbViewsService } from '../dbc-gbviews/gbviews.service';
import { GlobalVarService } from '../aa-common/global-var.service';
import { SelectValueRecordListComponent } from '../aa-common/select-VRList.component';
import { ArtFactsListComponent } from '../dbx-facts/art-facts-list.component';

import { ValueRecord } from '../dbc-fields/field';

@Component({
  selector      :'app-arts',
  templateUrl   :'./arts.component.html',
  styleUrls     :['./arts.component.css']
})
export class ArtsComponent implements OnInit {
  public  formTitles = {
    'edit'              :'Editar',
    'delete'            :'Borrar',
    'arts'              :'Artículos',
    'deps'              :'Departamentos',
    'subDeps'           :'SubDepartamentos',
    'artTypes'          :'Tipos de Artículos',
    'addSubDeps'        :'SubDptos',
    'addArtTypes'       :'TiposArts',
    'addArts'           :'Arts',
    'artsTotalString'   :'Arts',
    'artDeleteOne'      :'¿ Seguro que quiere BORRAR este ARTÍCULO ?',
    'artDeleteAll'      :'¿ Seguro que quiere BORRAR TODOS los ARTÍCULOS ?',
    'artsList'          :'Lista de Artículos',
    'listArtFacts'      :'InFacts',
    'listArtFactModels' :'InModels',
    'dep'               :'Departamento',
    'selectedDep'       :'Departamento Seleccionado',
    'subDep'            :'Subdepartamento',
    'selectedSubDep'    :'Subdepartamento Seleccionado',
    'artType'           :'Tipo de Artículo',
    'selectedArtType'   :'Tipo de Artículo Seleccionado',
  };
  public  formLabels = { 
    'id'                :'#####',
    'artId'             :'Id',
    'artDate'           :'Fecha',
    'artType'           :'Tipo.Art.',
    'artReference'      :'Referencia',
    'artName'           :'Artículo',
    'artLongName'       :'Descripción',
    'artUnitPrice'      :'€/Ud.',
    'artPackageUnits'   :'Uds/Pk.',
    'artPackagePrice'   :'€/Pk.',
    'artTax'            :'IVA',
  };  
  private createArt           :boolean;
  public  showArtsList        :boolean;
  public  showArtsList2       :boolean;
  public  arts                :Art[];
  private newArt              :Art;
  public  searchArt           :ArtString;
  public  showSearchArts      :boolean;
  public  orderArtsList       :string;
  public  reverseArtsList     :boolean;
  private workingDate         :Date;
  private filterArts          :boolean;
  private selectedArtTypeName :string;
  private selectedDep         :Dep;
  private selectedSubDep      :SubDep;
  private selectedArtType     :ArtType;
  public  showSelectedArtType :boolean;
  public  caseInsensitive     :boolean;
  private sortedCollection    :any[];
  private logToConsole        :boolean;
  public  localArtTypesNamesRecordList  :ValueRecord [];

  /*---------------------------------------ARTS  -- CONSTRUCTOR-----------------------------------------------------------*/
  constructor(  private orderPipe: OrderPipe, private viewContainer: ViewContainerRef,  private modalService: BsModalService,
                public globalVar: GlobalVarService, private _errorService: ErrorService, private _gbViewsService: GbViewsService,
                private _artService: ArtService ) {
                    //   this.getGbViews('gbarts'); 
              }
  /*---------------------------------------ARTS  -- NG ON INIT-----------------------------------------------------------*/
  ngOnInit(): void {
    this.logToConsole = this.globalVar.logToConsole;
    this.logToConsole = true;
    this.orderArtsList = 'artName';
    this.reverseArtsList = false;
    this.caseInsensitive = true;
    this.workingDate = this.globalVar.workingDate;
    this.arts = null;
    this.newArt = new Art();
    this.searchArt = new ArtString();
    this.localArtTypesNamesRecordList = this.globalVar.artTypesNamesRecordList;
    this.filterArts = false;
    this.showSelectedArtType = false;
    this.getArtsList();
  }
  /*--------------------------------------- ARTS -- FORMS -----------------------------------------------------------*/ 
  /*--------------------------------------- ARTS  -- GENERAL -----------------------------------------------------------*/
  /*--------------------------------------- ARTS -- BTN CLICK-----------------------------------------------------------*/
  public  changeShowArtsListBtnClick(){    
    if (this.showArtsList === true) { 
      this.showArtsList     = false;
      this.showArtsList2    = true;
    } else {     
      this.showArtsList     = true;  
      this.showArtsList2    = false;
    }   
  }
  public  showAllArtsBtnClick(){  
    this.showArtsList2 = false;
    this.showArtsList = false;
    this.filterArts = false;
    this.showSelectedArtType = false;
    this.getArtsList();
  }
  public  openDepListBtnClick(){
    const modalInitialState = {
      depsTitle   :'Artículos, Lista Departamentos',
      depsDate    :this.workingDate,
      callback    :'OK',   
    };
    this.globalVar.openModal(DepsListComponent, modalInitialState, 'modalXlTop0Left3')
    .then((result:any)=>{ this.getArtsList(); }) 
  }
  public  openSubDepListBtnClick(){
    const modalInitialState = {
      subDepsTitle  :'Artículos, Lista SubDepartamentos',
      subDepsDate   :this.workingDate,
      selectedDep   :null,
      callback      :'OK',   
    };
    this.globalVar.openModal(SubDepsListComponent, modalInitialState, 'modalXlTop0Left3')
    .then((result:any)=>{  this.getArtsList(); })  
  }
  public  openArtTypeListBtnClick(){
    const modalInitialState = {
      artTypesTitle     :'Artículos, Lista Tipos de Artículos',
      artTypesDate      :this.workingDate,
      selectedSubDep    :null,
      callback          :'OK',   
    };
    this.globalVar.openModal(ArtTypesListComponent, modalInitialState, 'modalXlTop0Left3')
    .then((result:any)=>{  this.getArtsList(); }) 
  }
  public  editArtBtnClick(editArt: Art){
    if (this.isArtNotBlocked(editArt)){ 
      this.newArt = editArt;
      this.createArt = false;
      this.createArtModal(); 
    }
  } 
  public  createArtBtnClick(){
    this.createArt = true;
    this.newArt = new Art()
    this.createArtModal(); 
  } 
  private createArtModal(){
    const modalInitialState = {
      newArt      :this.newArt,
      artsTitle   :'Artículos',
      artsDate    :this.workingDate,
      createArt   :this.createArt,
      callback    :'OK', 
    }
    this.globalVar.openModal(ArtsCreateComponent, modalInitialState, 'modalXlTop0Left3').
    then((result:any)=>{  this.getArtsList(); })    
  }  
  public  selectArtsByArtTypeBtnClick(){
    this.globalVar.consoleLog(this.logToConsole,'-> ARTS -> selectArtsByArtTypeBtnClick ->...Starting...->', null);
    if (this.localArtTypesNamesRecordList.length < 1) { return;  }  
    const modalInitialState = {
      localValueRecordList  :this.localArtTypesNamesRecordList,
      selectTitle1          :"Artículos",
      selectTitle2          :"Seleccionar Tipo de Artículo",
      selectMessage         :"Seleccione el Tipo de Artículo de los ARTÍCULOS que quiere ver",
      selectLabel           :"Tipo Artículo:",       
      selectFooter          :"Artículos",
      callback              :'OK',   
    };
    this.globalVar.openModal(SelectValueRecordListComponent, modalInitialState, 'modalLgTop0Left0').
    then((result:any)=>{
      this.selectedArtTypeName = "";   
      if (result != null) {
        this.selectedArtTypeName = result;
        this.selectedArtType = this.globalVar.getArtTypeObjectFromArtTypeName(this.selectedArtTypeName);
        this.filterArts = true;
      }
      else  {
        this.filterArts = false;
        this.showSelectedArtType = false;
      }
      this.getArtsList();
    })
  }
  public  listArtsFactsWithThisArtBtnClick(selectedArt: Art){
    this.globalVar.consoleLog(this.logToConsole,'-> ARTS -> listArtsFactsWithThisArtBtnClick ->...Starting...->', null);
    const modalInitialState = {
      selectedArt   :selectedArt,
      listArtFacts  :true,
      callback      :'OK',   
    };
    this.globalVar.openModal(ArtFactsListComponent, modalInitialState, 'modalXlTop0Left3').
    then((result:any)=>{ this.getArtsList(); }) 
  }
  public  listArtsFactModelsWithThisArtBtnClick(selectedArt: Art){
    this.globalVar.consoleLog(this.logToConsole,'-> ARTS -> listArtsFactModelsWithThisArtBtnClick ->...Starting...->', null);
    const modalInitialState = {
      selectedArt   :selectedArt,
      listArtFacts  :false,
      callback      :'OK',   
    };
    this.globalVar.openModal(ArtFactsListComponent, modalInitialState, 'modalXlTop0Left3').
    then((result:any)=>{ this.getArtsList(); }) 
  }
  private isArtNotBlocked(selectedArt: Art):boolean{
    this.globalVar.consoleLog(this.logToConsole,'-> ARTS -> isArtNotBlocked ->...Starting...->', null);
    if (selectedArt.artStatus === false){
      this._errorService.showErrorModal( "ARTÍCULOS", "ARTÍCULOS", "ARTÍCULO BLOQUEADO"+"->",
                        "Artículo->"+selectedArt.artName, "Descripión->"+selectedArt.artLongName, 
                        "Tipo->"+selectedArt.artType, "Referencia->"+selectedArt.artReference,
                        "Id->"+selectedArt.artId, "Tamaño->"+selectedArt.artSize);
      return false;
    } else return true;
  }
  public  lockArtBtnClick(modArt: Art){
    if (modArt.artStatus === true) modArt.artStatus = false;
    else  modArt.artStatus = true;
    this.updateOneArt(modArt);
  }
  public  searchArtsBtnClick() {
    if (this.showSearchArts === true) {
      this.showSearchArts = false;
    } else {
      this.showSearchArts = true;
    }
  }
  public  setOrderBtnClick(value: string) {
    if (this.orderArtsList === value) {
      this.reverseArtsList = !this.reverseArtsList;
    }
    this.orderArtsList = value;
  }
  /*--------------------------------------- ARTS  -- DATABASE-----------------------------------------------------------*/
  private getArtsList(){
    if (this.filterArts === false) {
      this.getAllArts();
    } else {             
      var selectedArtTypeTag = this.globalVar.getArtTypeTagFromArtTypeName(this.selectedArtTypeName);
      this.getAllArtsWithArtTypeTag(selectedArtTypeTag);
    }
  }
  private getAllArts() {
    this.globalVar.consoleLog(this.logToConsole,'-> ARTS -> getAllArts ->...Starting...->', null);
    this._artService.getAllArts()
      .subscribe( data => { this.arts = data;
                            this.sortedCollection = this.orderPipe.transform(this.arts,this.orderArtsList,this.reverseArtsList,this.caseInsensitive);
                            this.arts = this.sortedCollection;
                            this.formTitles.artsList ='Lista de TODOS los Artículos';
                            this.formTitles.artsTotalString =this.arts.length+'..Arts'; 
                            this.showArtsList2    = true; 
                            },
                 error => { if (this.globalVar.handleError(error)) { throwError(error) }; });
  }
  private getAllArtsWithArtTypeTag(selArtTypeTag :string){
    this.globalVar.consoleLog(this.logToConsole,'-> ARTS -> getAllArtsWithArtTypeTag ->...Starting...->', null);
    this._artService.getAllArtsWithArtTypeTag(selArtTypeTag)
      .subscribe( data => { this.arts = data;
                            this.sortedCollection = this.orderPipe.transform(this.arts,this.orderArtsList,this.reverseArtsList,this.caseInsensitive);
                            this.arts = this.sortedCollection;
                            this.formTitles.artsTotalString =this.arts.length+'..Arts'; 
                            this.formTitles.artsList ='Lista Artículos del Tipo de Artículo ==> '+this.selectedArtType.artTypeName;
                            this.selectedSubDep = this.globalVar.getSubDepObjectFromSubDepId(this.selectedArtType.artTypeSubDepId);
                            this.selectedDep = this.globalVar.getDepObjectFromDepId(this.selectedSubDep.subDepDepId);
                            this.formTitles.dep ='Departamento ... '+this.selectedDep.depName;
                            this.formTitles.selectedDep ='->Nombre: '+this.selectedDep.depName+'->Descripcion: '+this.selectedDep.depDescription+
                              '->DepTag: '+this.selectedDep.depTag+'->DepId: '+this.selectedDep.depId+'->Fecha: '+this.selectedDep.depDate;
                            this.formTitles.subDep ='Subdepartamento ... '+this.selectedSubDep.subDepName;
                            this.formTitles.selectedSubDep = '->Nombre: '+this.selectedSubDep.subDepName+'->Descripcion: '+this.selectedSubDep.subDepDescription+
                              '->DepTag: '+this.selectedSubDep.subDepTag+'->DepId: '+this.selectedSubDep.subDepId+'->Fecha: '+this.selectedSubDep.subDepDate;
                            this.formTitles.artType ='Tipo de Artículo ... '+this.selectedArtType.artTypeName;
                            this.formTitles.selectedArtType = '->Nombre: '+this.selectedArtType.artTypeName+'->Descripcion: '+this.selectedArtType.artTypeDescription+
                              '->DepTag: '+this.selectedArtType.artTypeTag+'->DepId: '+this.selectedArtType.artTypeId+'->Fecha: '+this.selectedArtType.artTypeDate;                              
                            this.showArtsList2 = true; 
                            this.showSelectedArtType = true;
                            },
                 error => { if (this.globalVar.handleError(error)) { throwError(error) }; });
  }
  private updateOneArt(modArt: Art): void {
    this.globalVar.consoleLog(this.logToConsole,'-> ARTS -> updateOneArt ->...Starting...->', modArt);
    this._artService.updateOneArt(modArt)
      .subscribe( data => { this.getArtsList(); },
                 error => { if (this.globalVar.handleError(error)) { throwError(error) }; });
  }
  /*--------------------------------------- ARTS  -- DELETE-----------------------------------------------------------*/
  public  confirmDeleteOneArtBtnClick(art: Art){
    this._errorService.getDeleteMsgConfirmation("deleteMsg",this.formTitles.arts,this.formTitles.arts,
      art,this.formTitles.artDeleteOne,art.artName,art.artLongName,art.artType,art.artMeasurement,"","",)
      .then((deleteOK:boolean)=>{ if (deleteOK === true){ this.deleteOneArt(art); } })  
  }
  public  confirmDeleteAllArtsBtnClick(){
    this._errorService.getDeleteMsgConfirmation("deleteMsg",this.formTitles.arts,this.formTitles.arts,null,this.formTitles.artDeleteAll,"","","","","","")
      .then((deleteOK:boolean)=>{ if (deleteOK === true){ this.deleteAllArts(); } })  
  } 
  private deleteAllArts(){
    this.globalVar.consoleLog(this.logToConsole,'-> ARTS -> deleteAllArts ->...Starting...->', null);
    this._artService.delAllArts()
    .subscribe(data => { this.getArtsList(); },
              error => { if (this.globalVar.handleError(error)) { throwError(error) };});
  }
  private deleteOneArt(delArt: Art){
    this.globalVar.consoleLog(this.logToConsole,'-> ARTS -> deleteOneArt ->...Starting...->', null);
    this._artService.delOneArt(delArt)
    .subscribe(data => { this.getArtsList(); },
              error => { if (this.globalVar.handleError(error)) { throwError(error) }; });
  }

}
