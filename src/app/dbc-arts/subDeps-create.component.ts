import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { throwError, Subject } from 'rxjs';

import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';

import { SubDep, Dep } from './art';
import { ArtService } from './art.service';
import { GlobalVarService } from '../aa-common/global-var.service';
import { ValueRecord } from '../dbc-fields/field';
import { ErrorService } from '../aa-errors/error.service';

@Component({
  selector      :'app-subDeps-create',
  templateUrl   :'./subDeps-create.component.html',
  styleUrls     :['./arts.component.css'] 
}) 
export class SubDepsCreateComponent implements OnInit {
  public newSubDep      :SubDep;
  public subDepsTitle   :string;
  public subDepsDate    :Date;
  public createSubDep   :boolean;
  public selectedDep    :Dep;
  callback              :any;
  result: Subject<SubDep> = new Subject<SubDep>();

  public formTitles = {
    'subDepsTitle'      :'',   
    'subDepsDate'       :'',
    'subDeps'           :'Departamento',
    'subDepCreate'      :'Crear',
    'subDepEdit'        :'Modificar',
  };
  public formLabels = {
    'id'                :'#####',
    'subDepId'          :'Id',
    'subDepDate'        :'Fecha',
    'subDepName'        :'Nombre',
    'subDepDescription' :'Descripción',
    'subDepTag'         :'Tag',
    'subDepSequence'    :'Sequence',
    'subDepDepName'     :'Departamento',
    'subDepDepId'       :'Dep.Id',
    'subDepStatus'      :'Estado',   
  };
  public  subDepForm        :FormGroup;
  public  subDeps           :SubDep[];
  public  currentSubDepDate :Date;
  public  selectDep         :boolean;
  private logToConsole      :boolean;
  public  editSubDep        :boolean;
  public  disableSubDepNameAndTag   :boolean;
  public  localDepNamesRecordList   :ValueRecord [];
  public  subDepsDatePickerConfig   :Partial <BsDatepickerConfig>;

 /*---------------------------------------DEPS CREATE -- CONSTRUCTOR-----------------------------------------------------------*/
  constructor ( private fb:FormBuilder, private bsModalRef: BsModalRef, private modalService: BsModalService,  
                private globalVar: GlobalVarService, private _errorService: ErrorService, private _artsService: ArtService) { 
    this.subDepsDatePickerConfig = Object.assign ( {}, {
      containerClass:   'theme-dark-blue',
      showWeekNumbers:  true,
      minDate:          this.globalVar.workingPreviousYear,
      maxDate:          this.globalVar.workingNextYear,
      dateInputFormat:  'DD-MM-YYYY'
      } );
  }
/*---------------------------------------DEPS CREATE -- NG ON INIT-----------------------------------------------------------*/
  ngOnInit(): void {
    this.globalVar.consoleLog(this.logToConsole,'-> SUBDEPS CREATE -> ngOnInit->...Starting...->', null);

    this.logToConsole = this.globalVar.logToConsole;
    this.logToConsole = true;
    this.currentSubDepDate = this.subDepsDate;
    if (this.subDepsDate === null) {
      this.formTitles.subDepsDate = '';
    } else {
      this.formTitles.subDepsDate = this.subDepsDate.toLocaleDateString();
    }
    this.formTitles.subDepsTitle = this.subDepsTitle;
    if (this.selectedDep === null) this.selectDep = true;
    else this.selectDep = false;
    this.disableSubDepNameAndTag = false;
    if (this.createSubDep === false) {
      this.disableSubDepNameAndTag = true;
    }
    this.globalVar.consoleLog(this.logToConsole,'-> SUBDEPS CREATE -> ngOnInit->...1...->', null);

    this.subDepForm = this.fb.group ({
      subDepId             :[{value:'',disabled:true}],
      subDepDate           :[{value:this.currentSubDepDate,disabled:true},[Validators.required]],
      subDepName           :[{value:'',disabled:this.disableSubDepNameAndTag},[Validators.required,Validators.minLength(3),Validators.maxLength(32)]],
      subDepDescription    :[{value:'',disabled:false},[Validators.required,Validators.minLength(3),Validators.maxLength(64)]],
      subDepTag            :[{value:'',disabled:this.disableSubDepNameAndTag},[Validators.required,Validators.minLength(3),Validators.maxLength(16)]],
      subDepSequence       :[{value:0,disabled:true},[Validators.required,Validators.minLength(3),Validators.maxLength(8)]],
      subDepDepName        :[{value:'',disabled:false},[Validators.required]],
      subDepDepId          :[{value:'',disabled:false},[Validators.required]],
      subDepStatus         :[{value:true,disabled:false},[Validators.required]],
    })
    this.subDepForm.get('subDepDate').patchValue(new Date(this.currentSubDepDate));
    this.localDepNamesRecordList = this.globalVar.depNamesRecordList;
    this.globalVar.consoleLog(this.logToConsole,'-> SUBDEPS CREATE -> ngOnInit->...2...->', null);

    if ( (this.localDepNamesRecordList === null) || (this.localDepNamesRecordList.length === 0) ) {
      this._errorService.showErrorModal( "SUBDEPARTAMENTOS", "SUBDEPARTAMENTOS", "LISTA DE DEPARTAMENTOS VACIA","","","","","","");
      this.closeSubDepsCreateModalBtnClick();
    }
    this.globalVar.consoleLog(this.logToConsole,'-> SUBDEPS CREATE -> ngOnInit->...3...->', null);

    if (this.selectedDep === null) {
      this.selectDep = true;
      this.subDepForm.get('subDepDepName').patchValue(this.localDepNamesRecordList[0].value); 
      this.subDepForm.get('subDepDepId').patchValue(0); 
    } else {
      this.selectDep = false;
      this.subDepForm.get('subDepDepName').patchValue(this.selectedDep.depName); 
      this.subDepForm.get('subDepDepId').patchValue(this.selectedDep.depId); 
    }
    this.globalVar.consoleLog(this.logToConsole,'-> SUBDEPS CREATE -> ngOnInit->...4...->', null);

    if (this.createSubDep === true) {
        this.editSubDep = false;
        this.newSubDep = new SubDep();   
    } else {
      this.createSubDep = false;
      this.editSubDep = true;
      this.getOneSubDep(this.newSubDep);
    }
  }
  /*---------------------------------------DEPS CREATE -- FORMS-----------------------------------------------------------*/    
  private copySubDepFormDataToSubDep(){
    let newSubDepDate = new Date(this.subDepForm.controls.subDepDate.value);
    this.newSubDep.subDepDate        = this.subDepForm.controls.subDepDate.value;
    this.newSubDep.subDepName        = this.subDepForm.controls.subDepName.value;
    this.newSubDep.subDepDescription = this.subDepForm.controls.subDepDescription.value;
    this.newSubDep.subDepTag         = this.subDepForm.controls.subDepTag.value;
    this.newSubDep.subDepSequence    = this.subDepForm.controls.subDepSequence.value;
    this.newSubDep.subDepDepName     = this.subDepForm.controls.subDepDepName.value;
    this.newSubDep.subDepStatus      = this.subDepForm.controls.subDepStatus.value;
    let selectedDepId = this.globalVar.getDepIdFromDepName(this.newSubDep.subDepDepName);
    if (selectedDepId != -1)  this.newSubDep.subDepDepId = selectedDepId;
    else this.newSubDep.subDepDepId = -1;
  }
  private copyNewSubDepDataToDepForm(){
    this.subDepForm.patchValue( { subDepId:this.newSubDep.subDepId,subDepDate:this.newSubDep.subDepDate,subDepName:this.newSubDep.subDepName, 
                                  subDepDescription:this.newSubDep.subDepDescription,subDepTag:this.newSubDep.subDepTag,
                                  subDepSequence:this.newSubDep.subDepSequence,subDepDepName:this.newSubDep.subDepDepName,
                                  subDepDepId:this.newSubDep.subDepDepId,subDepStatus:this.newSubDep.subDepStatus} );
    this.subDepForm.get('subDepDate').patchValue(new Date(this.newSubDep.subDepDate));
  }
  private cleanSubDepFormData(){
    this.subDepForm.patchValue( { subDepName:'',subDepDescription:'',subDepTag:'',subDepSequence:'',
                                  subDepDepName:this.localDepNamesRecordList[0].value,subDepDepId:'',subDepStatus:true} );
  }
  /*---------------------------------------DEPS CREATE  -- GENERAL -----------------------------------------------------------*/ 
  public changeDisableSubDepNameAndTagBtnClick(){
    if (this.disableSubDepNameAndTag === true) {
      this.subDepForm.get('subDepName').enable(); 
      this.subDepForm.get('subDepTag').enable(); 
      this.disableSubDepNameAndTag = false;
    } else {
      this.subDepForm.get('subDepName').disable(); 
      this.subDepForm.get('subDepTag').disable(); 
      this.disableSubDepNameAndTag = true;
    }
  }
  /* ---------------------------------------DEPS CREATE BTN CLICK-----------------------------------------------------------*/
  public closeSubDepsCreateModalBtnClick(){
    if (this.bsModalRef.content.callback != null){
      this.globalVar.getFactTypesFromSubDeps();
      //this.bsModalRef.content.callback(this.newSubDep);
      this.result.next(this.newSubDep);
      this.bsModalRef.hide();
      }
  }
  public cleanFormSubDepBtnClick(){
    this.createSubDep = true;
    this.editSubDep = false;
    this.cleanSubDepFormData();
  }
  public createSubDepBtnClick(subDepForm: FormGroup){
    this.globalVar.consoleLog(this.logToConsole,'-> SUBDEPS CREATE -> createSubDepBtnClick ->...Starting...->', null);
    this.copySubDepFormDataToSubDep();
    this.createOneSubDep(this.newSubDep);
  }
  public updateSubDepBtnClick(subDepForm: FormGroup){
    this.globalVar.consoleLog(this.logToConsole,'-> SUBDEPS CREATE -> updateSubDepBtnClick ->...Starting...->', null);
    this.copySubDepFormDataToSubDep();  
    if ((this.createSubDep === false) && (this.disableSubDepNameAndTag === true)) {
      this.updateOneSubDep(this.newSubDep);
    } 
    if ((this.createSubDep === false) && (this.disableSubDepNameAndTag === false)) {
      this.updateOneSubDepSpecial(this.newSubDep);
    } 
  }
  /* ---------------------------------------DEPS CREATE --GET DEPS ---------------------------------------------------------*/
  private createOneSubDep(newSubDep: SubDep): void {
    this.globalVar.consoleLog(this.logToConsole,'-> SUBDEPS CREATE -> createOneSubDep ->...Starting...->',null);
    this._artsService.createOneSubDep(newSubDep)
      .subscribe(data => { this.newSubDep = data;
                           this.closeSubDepsCreateModalBtnClick();
                         },
                error => { if (this.globalVar.handleError(error)) { throwError(error) }; });
  }
  private getOneSubDep(modSubDep: SubDep): void {
    this.globalVar.consoleLog(this.logToConsole,'-> SUBDEPS CREATE -> getOneSubDep ->...Starting...->',null);
    this._artsService.getOneSubDep(modSubDep)
      .subscribe(data => { this.newSubDep = data;
                           this.copyNewSubDepDataToDepForm();                          
                         },
                error => { if (this.globalVar.handleError(error)) { throwError(error) }; });
  }
  private updateOneSubDep(modSubDep: SubDep): void {
    this.globalVar.consoleLog(this.logToConsole,'-> SUBDEPS CREATE -> updateOneDep ->...Starting...->',null);
    this._artsService.updateOneSubDep(modSubDep)
      .subscribe(data => { this.newSubDep = data;
                           this.closeSubDepsCreateModalBtnClick();
                         },
                error => { if (this.globalVar.handleError(error)) { throwError(error) }; });
  }
  private updateOneSubDepSpecial(modSubDep: SubDep): void {
    this.globalVar.consoleLog(this.logToConsole,'-> SUBDEPS CREATE -> updateOneSubDepSpecial ->...Starting...->',null);
    this._artsService.updateOneSubDepSpecial(modSubDep)
      .subscribe(data => { this.newSubDep = data;
                           this.closeSubDepsCreateModalBtnClick();
                         },
                error => { if (this.globalVar.handleError(error)) { throwError(error) }; });
  }

}