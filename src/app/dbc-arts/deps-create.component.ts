import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { throwError, Subject} from 'rxjs';

import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';

import { Dep } from './art';
import { ArtService } from './art.service';
import { GlobalVarService } from '../aa-common/global-var.service';

@Component({
  selector      :'app-deps-create',
  templateUrl   :'./deps-create.component.html',
  styleUrls     :['./arts.component.css'] 
}) 
export class DepsCreateComponent implements OnInit {
  public newDep       :Dep;
  public depsTitle    :string;
  public depsDate     :Date;
  public createDep    :boolean;
  callback            :any;
  result: Subject<Dep> = new Subject<Dep>();

  public formTitles = {
    'depsTitle'           :'',   
    'depsDate'            :'',
    'deps'                :'Departamento',
    'depCreate'           :'Crear',
    'depEdit'             :'Modificar',
  };
  public formLabels = {
    'id'                  :'#####',
    'depId'               :'Id',
    'depDate'             :'Fecha',
    'depName'             :'Nombre',
    'depDescription'      :'Descripción',
    'depTag'              :'Tag',
    'depStatus'           :'Estado',    
  };
  public  depForm         :FormGroup;
  public  currentDepDate  :Date;
  private logToConsole    :boolean;
  public  showDep         :boolean;
  public  editDep         :boolean;
  public  disableDepName  :boolean;
  public  depsDatePickerConfig: Partial <BsDatepickerConfig>;

 /*---------------------------------------DEPS CREATE -- CONSTRUCTOR-----------------------------------------------------------*/
  constructor ( private fb:FormBuilder, private bsModalRef: BsModalRef, private modalService: BsModalService,  
                private globalVar: GlobalVarService, private _depsService: ArtService) { 
    this.depsDatePickerConfig = Object.assign ( {}, {
      containerClass:   'theme-dark-blue',
      showWeekNumbers:  true,
      minDate:          this.globalVar.workingPreviousYear,
      maxDate:          this.globalVar.workingNextYear,
      dateInputFormat:  'DD-MM-YYYY'
      } );
  }
/*---------------------------------------DEPS CREATE -- NG ON INIT-----------------------------------------------------------*/
  ngOnInit(): void {
    this.logToConsole = this.globalVar.logToConsole;
    this.logToConsole = true;
    this.currentDepDate = this.depsDate;
    if (this.depsDate === null) {
      this.formTitles.depsDate = '';
    } else {
      this.formTitles.depsDate = this.depsDate.toLocaleDateString();
    }
    this.formTitles.depsTitle = this.depsTitle;  
    this.disableDepName = false;
    if (this.createDep === false) {
      this.disableDepName = true;
    } 
    this.depForm = this.fb.group ({
      depId             :[{value:'',disabled:true}],
      depDate           :[{value:this.currentDepDate,disabled:true},[Validators.required]],
      depName           :[{value:'',disabled:this.disableDepName},[Validators.required,Validators.minLength(5),Validators.maxLength(32)]],
      depDescription    :[{value:'',disabled:false},[Validators.required,Validators.minLength(3),Validators.maxLength(64)]],
      depTag            :[{value:'',disabled:true},[Validators.required,Validators.minLength(3),Validators.maxLength(16)]],
      depStatus         :[{value:true,disabled:false},[Validators.required]],
    })
    this.depForm.get('depDate').patchValue(new Date(this.currentDepDate)); 
    if (this.createDep === true) {
      this.editDep = false;
      this.newDep = new Dep();      
    } else {
      this.createDep = false;
      this.editDep = true;
      this.getOneDep(this.newDep);
    }
  }
  /*---------------------------------------DEPS CREATE -- FORMS-----------------------------------------------------------*/    
  private copyDepFormDataToDep(){
    let newDepDate = new Date(this.depForm.controls.depDate.value);
    this.newDep.depDate        = this.depForm.controls.depDate.value;
    this.newDep.depName        = this.depForm.controls.depName.value;
    this.newDep.depDescription = this.depForm.controls.depDescription.value;
    this.newDep.depTag         = this.depForm.controls.depTag.value;
    this.newDep.depStatus      = this.depForm.controls.depStatus.value;
  }
  private copyNewDepDataToDepForm(){
    this.depForm.patchValue( {  depId:this.newDep.depId,depDate:this.newDep.depDate,depName:this.newDep.depName, 
                                depDescription:this.newDep.depDescription,depTag:this.newDep.depTag,depStatus:this.newDep.depStatus} );
    this.depForm.get('depDate').patchValue(new Date(this.newDep.depDate)); 
  }
  private cleanDepFormData(){
    this.depForm.patchValue( {depName:'',depDescription:'',depTag:'',depStatus:true} );
  }
  /*---------------------------------------DEPS CREATE  -- GENERAL -----------------------------------------------------------*/
  public changeDisableDepNameBtnClick(){
    if (this.disableDepName === true) {
      this.depForm.get('depName').enable(); 
      this.disableDepName = false;
    } else {
      this.depForm.get('depName').disable(); 
      this.disableDepName = true;
    }
  }
  /* ---------------------------------------DEPS CREATE BTN CLICK-----------------------------------------------------------*/
  public closedDepsCreateModalBtnClick(){
    if (this.bsModalRef.content.callback != null){
      this.globalVar.getAllDeps();
      //this.bsModalRef.content.callback(this.newDep);
      this.result.next(this.newDep);
      this.bsModalRef.hide();
      }
  }
  public cleanFormDepBtnClick(){
    this.createDep = true;
    this.editDep = false;
    this.cleanDepFormData();
  }
  public createDepBtnClick(depForm: FormGroup){
    this.globalVar.consoleLog(this.logToConsole,'-> DEPS CREATE -> createDepBtnClick ->...Starting...->', null);
    this.copyDepFormDataToDep();
    this.createOneDep(this.newDep);
  }
  public updateDepBtnClick(depForm: FormGroup){
    this.globalVar.consoleLog(this.logToConsole,'-> DEPS CREATE -> updateDepBtnClick ->...Starting...->', null);
    this.copyDepFormDataToDep();    
    if ((this.createDep === false) && (this.disableDepName === true)) {
      this.updateOneDep(this.newDep);
    } 
    if ((this.createDep === false) && (this.disableDepName === false)) {
      this.updateOneDepSpecial(this.newDep);
    } 
  }
  /* ---------------------------------------DEPS CREATE --GET DEPS ---------------------------------------------------------*/
  private createOneDep(newDep: Dep): void {
    this.globalVar.consoleLog(this.logToConsole,'-> DEPS CREATE -> createOneDep ->...Starting...->', null);
    this._depsService.createOneDep(newDep)
      .subscribe(data => {
                          this.newDep = data;
                          this.closedDepsCreateModalBtnClick();
                        },
                error => {
                          if (this.globalVar.handleError(error)) { throwError(error) };
                          });
  }
  private getOneDep(modDep: Dep): void {
    this.globalVar.consoleLog(this.logToConsole,'-> DEPS CREATE -> getOneDep ->...Starting...->', null);
    this._depsService.getOneDep(modDep)
      .subscribe(data => {
                          this.newDep = data;
                          this.copyNewDepDataToDepForm();                          
                        },
                error => {
                          if (this.globalVar.handleError(error)) { throwError(error) };
                          });
  }
  private updateOneDep(modDep: Dep): void {
    this.globalVar.consoleLog(this.logToConsole,'-> DEPS CREATE -> updateOneDep ->...Starting...->', null);
    this._depsService.updateOneDep(modDep)
      .subscribe(data => {
                          this.newDep = data;
                          this.closedDepsCreateModalBtnClick();
                        },
                error => {
                          if (this.globalVar.handleError(error)) { throwError(error) };
                          });
  }
  private updateOneDepSpecial(modDep: Dep): void {
    this.globalVar.consoleLog(this.logToConsole,'-> DEPS CREATE -> updateOneDepSpecial ->...Starting...->', null);
    this._depsService.updateOneDepSpecial(modDep)
      .subscribe(data => {
                          this.newDep = data;
                          this.closedDepsCreateModalBtnClick();
                        },
                error => {
                          if (this.globalVar.handleError(error)) { throwError(error) };
                          });
  }
}