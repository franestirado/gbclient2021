import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Fact, ArtFact } from './facts';
import { Art } from '../dbc-arts/art';
import { Prov } from '../dbc-provs/prov';
import { ValueRecord } from '../dbc-fields/field';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Authorization': 'my-auth-token'
  })
};

@Injectable()
export class FactsService {

  constructor(private http: HttpClient) { }

  private json: string;
  private params: string;
  private headers: HttpHeaders;

  /* ---------------------------------------FACTS-----------------------------------------------------------*/
  getAllFacts(): Observable<Fact[]>  {
    return this.http.get<Fact[]>('/facts/getAll')
  }
  getAllFactsMonth(firstAndLastDayOfMonth: Date[]): Observable<Fact[]>  {
    return this.http.post<Fact[]>('/facts/getAllMonth',firstAndLastDayOfMonth)
  }
  getAllFactsTypeMonth(factsTypeVRList: ValueRecord[]): Observable<Fact[]>  {
    return this.http.post<Fact[]>('/facts/getAllTypeMonth',factsTypeVRList)
  }
  getAllFactsFilter(factsFilterVRList: ValueRecord[]): Observable<Fact[]>  {
    return this.http.post<Fact[]>('/facts/getAllFilter',factsFilterVRList)
  }
  createOneFact(newFact: Fact): Observable<any>  {
    return this.http.post('/facts/createOne', newFact)
  }
  getOneFact(editFact: Fact): Observable<Fact>  {
    return this.http.post<Fact>('/facts/getOne', editFact)
  }
  getOneFactById(factid :number): Observable<Fact>  {
    return this.http.post<Fact>('/facts/getOneById', factid)
  }
  updateOneFact(newFact: Fact): Observable<any>  {
    return this.http.put('/facts/updOne', newFact)
  }
  delOneFact(delFact: Fact): Observable<any> {
    return this.http.post('/facts/delOne', delFact)
  }
  deleteOneFactById(delFactId :number): Observable<any> {
    return this.http.post('/facts/delOneById', delFactId)
  }
  delAllFacts(): Observable<any> {
    return this.http.delete('/facts/')
  }
  getAllFactsHd(workingDate: Date): Observable<Fact[]>  {
    return this.http.post<Fact[]>('/facts/getAllHd',workingDate)
  }
  delAllFactsHd(workingDate: Date): Observable<Fact[]>  {
    return this.http.post<Fact[]>('/facts/deleteAllHd',workingDate)
  }

  /* ---------------------------------------ARTS  FACTS-----------------------------------------------------------*/
  createArtsFact(newArtsFacts: ArtFact[]): Observable<ArtFact[]>  {
    return this.http.post<ArtFact[]>('/facts/createArtsFact', newArtsFacts)
  }
  getArtsFact(updateFact: Fact): Observable<ArtFact[]>  {
    return this.http.post<ArtFact[]>('/facts/getArtsFact', updateFact)
  }
  getArtsFactByFactId(factId :number): Observable<ArtFact[]>  {
    return this.http.post<ArtFact[]>('/facts/getArtsFactByFactId', factId)
  }
  getArtFactsWithArt(selArt: Art): Observable<ArtFact[]>  {
    return this.http.post<ArtFact[]>('/facts/getArtsFactWithArt', selArt)
  }
  updateArtsFact(updateArtsFacts: ArtFact[]): Observable<ArtFact[]>  {   
    return this.http.post<ArtFact[]>('/facts/updateArtsFact', updateArtsFacts)
  }
  deleteArtsFact(deleteArtsFacts: ArtFact[]): Observable<number>  {  
    return this.http.post<number>('/facts/deleteArtsFact', deleteArtsFacts)
  }
  deleteAllArtsFact(): Observable<number>  {  
    return this.http.post<number>('/facts/deleteAllArtsFact', 0)
  }
}
