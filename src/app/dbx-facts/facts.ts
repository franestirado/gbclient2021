export class Fact {
    factId          :number;
    factProvId      :number;
    factProvName    :string;
    factNumber      :string;
    factDate        :Date;
    factType        :string;
    factNetCost     :number;
    factTaxes       :number;
    factTotalCost   :number;
    factPayType     :string;
    factDetailed    :boolean;
    factNote        :boolean;
    factStatus      :boolean;
}
export class FactString {
    factId          :string;
    factProvId      :string;
    factProvName    :string;
    factNumber      :string;
    factDate        :string;
    factType        :string;
    factNetCost     :string;
    factTaxes       :string;
    factTotalCost   :string;
    factPayType     :string;
    factDetailed    :string;
    factNote        :boolean;
    factStatus      :string;
}
export class ArtFact {
    artFactId               :number;
    artFactFactId           :number;
    artFactFactDate         :Date;
    artFactArtId            :number;
    artFactArtName          :string;
    artFactArtType          :string;
    artFactArtSize          :number;
    artFactArtMeasurement   :string;    
    artFactArtUnitPrice     :number;
    artFactArtPackageUnits  :number;
    artFactArtPackagePrice  :number;
    artFactArtTax           :number;
    artFactQuantity         :number;
    artFactDiscount         :number;
    artFactNetCost          :number;   
    artFactTax              :number;
    artFactTotalCost        :number;
}
export class ArtFactString {
    artFactId               :number;
    artFactFactId           :number;
    artFactFactDate         :string;
    artFactArtId            :number;
    artFactArtName          :string;
    artFactArtType          :string;
    artFactArtSize          :number;
    artFactArtMeasurement   :string;    
    artFactArtUnitPrice     :number;
    artFactArtPackageUnits  :number;
    artFactArtPackagePrice  :number;
    artFactArtTax           :number;
    artFactQuantity         :number;
    artFactDiscount         :number;
    artFactNetCost          :number;   
    artFactTax              :number;
    artFactTotalCost        :number;
}