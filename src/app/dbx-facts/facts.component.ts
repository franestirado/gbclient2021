import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { throwError } from 'rxjs';

import { OrderPipe } from 'ngx-order-pipe';
import { BsModalService } from 'ngx-bootstrap/modal';

import { GbViewsService } from '../dbc-gbviews/gbviews.service';
import { GlobalVarService } from '../aa-common/global-var.service';
import { ValueRecord } from '../dbc-fields/field';
import { ErrorService } from '../aa-errors/error.service';
import { EnterMonthComponent } from '../aa-common/enter-month.component';
import { SelectManyVRListComponent } from '../aa-common/select-manyVRList.component';

import { Fact, FactString } from './facts';
import { FactModels, FactModelsString } from '../dbc-factmodels/factmodel';
import { FactsCreateComponent } from './facts-create.component';
import { FactModelCreateComponent } from '../dbc-factmodels/factmodel-create.component';
import { FactsService } from './facts.service';
import { FactModelsService } from '../dbc-factmodels/factmodel.service';

@Component({
  selector      :'app-facts',
  templateUrl   :'./facts.component.html',
  styleUrls     :['./facts.component.css']
})
export class FactsComponent implements OnInit {
  public  formTitles = { 
    'factsListStart'            :'',
    'factsListEnd'              :'',
    'factsListMonthString'      :'',
    'factsListYearString'       :'',
    'factsListMonth'            :'Mes ',
    'factsTotalString'          :'',
    'factModelsTotalString'     :'',
    'facts'                     :'Facturas',
    'factDeleteOne'             :'¿ Seguro que quiere BORRAR este Factura ?',
    'factDeleteAll'             :'¿ Seguro que quiere BORRAR TODOS los Factura ?',
    'factsList'                 :'Lista de Facturas',
    'factsListFilter'           :'Filtro...',
    'factEditList'              :'Editar',
    'factDelete'                :'Borrar',
    'factModels'                :'MODELOS para Facturas',
    'factModelDeleteOne'        :'¿ Seguro que quiere BORRAR este MODELO de Factura ?',
    'factModelDeleteAll'        :'¿ Seguro que quiere BORRAR TODOS los MODELOS de Facturas ?',   
    'factModelsList'            :'Lista de MODELOS para Facturas',   
    'createProvFactModel'       :'Crear',
  };
  public  formLabels = {
    'id'                    :'#####',
    'factId'                :'Fact_Id',
    'factProvId'            :'Prov_Id',
    'factProvName'          :'Proveedor',
    'factNumber'            :'Nº Factura',
    'factDate'              :'Fecha',
    'factType'              :'Tipo',
    'factTypeL'             :'Tipo.Fact',
    'factNetCost'           :'Neto',
    'factTaxes'             :'IVA',
    'factTotalCost'         :'Total',
    'factPayType'           :'Pago',
    'factPayTypeL'          :'Forma.Pago',
    'factStatus'            :'Estado',
    'factModelsProvId'      :'Prov_Id',
    'factModelsProvName'    :'Proveedor',
    'factModelsId'          :'Model_Id',
    'factModelsDate'        :'Fecha',
    'factModelsName'        :'Nombre Modelo Factura',
    'factModelsFactType'    :'Tipo',     
    'factModelsPayType'     :'Pago',     
    'factModelsIsFactOrNote':'Fact/Nota',     
  };
  private localFactTypesRecordList      :ValueRecord [];
  private localPayFactTypesRecordList   :ValueRecord [];
  private filterRecordList              :ValueRecord [];
  private showCreateFact                :boolean;
  private showCreateDetailedFact        :boolean;
  private showEditFact                  :boolean;
  private showEditDetailedFact          :boolean;
  public  showFactsList                 :boolean;

  private newFact                       :Fact;
  public  facts                         :Fact[];
  private factsFirstAndLastDayOfMonth   :Date[];
  public  showFactModelsList            :boolean;
  private showCreateFactModel           :boolean;
  private showEditFactModel             :boolean;

  private newFactModel              :FactModels;
  public  factModels                :FactModels[];
  
  public  searchFact                :FactString;
  public  showSearchFacts           :boolean;
  public  orderFactsList            :string;
  public  reverseFactsList          :boolean;
  
  public  searchFactModel           :FactModelsString;
  public  showSearchFactModels      :boolean;
  public  orderFactModelsList       :string;
  public  reverseFactModelsList     :boolean;
  public  filterFacts               :boolean;
  public  selectedProv              :string;
  public  selectedFactType          :string;
  public  selectedPayType           :string;
  public  selectedFactOrNote        :string;

  public  caseInsensitive           :boolean;
  private sortedCollection          :any[];
  private logToConsole              :boolean;
 /*---------------------------------------FACTS  -- CONSTRUCTOR-----------------------------------------------------------*/
  constructor(  private orderPipe: OrderPipe, private viewContainer: ViewContainerRef, private modalService: BsModalService, 
                public globalVar: GlobalVarService, private _gbViewsService: GbViewsService, private _factsService: FactsService, 
                private _factModelService:  FactModelsService, private _errorService: ErrorService ) { 
      //   this.getGbViews('gbfacts');
  }
/*---------------------------------------FACTS CREATE -- NG ON INIT-----------------------------------------------------------*/
  ngOnInit(): void {
    this.logToConsole = this.globalVar.logToConsole;
    this.logToConsole = true;
    this.formTitles.factsTotalString = 'Total... 0.00';
    this.formTitles.factModelsTotalString = 'Total... 0.00';
    this.localFactTypesRecordList = this.globalVar.factTypesSubDepsRecordList;  
    this.localPayFactTypesRecordList = this.globalVar.factPayTypesRecordList 
    this.showEditFact = false;
    this.caseInsensitive = true;
    this.newFact = new Fact();
    this.searchFact = new FactString();       
    this.searchFactModel = new FactModelsString();
    this.factsFirstAndLastDayOfMonth = this.globalVar.workingFirstAndLastDayOfMonth;
    this.resetShowFactFlags();
    this.getAllFactsFilter(this.filterRecordList);  
  }
  
  /*---------------------------------------FACTS -- FORMS -----------------------------------------------------------*/
  
  /*---------------------------------------FACTS  -- GENERAL -----------------------------------------------------------*/
  resetShowFactFlags(){
    this.showFactsList          = false;
    this.showCreateFact         = false;
    this.showCreateDetailedFact = false;
    this.showEditFact           = false;
    this.showEditDetailedFact   = false;
    this.showFactModelsList     = false;
    this.showCreateFactModel    = false;
    this.showEditFactModel      = false;  
    this.filterFacts            = false; 
    this.filterRecordList       = this.globalVar.prepareFilterFactsValueRecordList(this.factsFirstAndLastDayOfMonth[0],this.factsFirstAndLastDayOfMonth[1],"","","","");
    this.selectedProv           = "";
    this.selectedFactType       = "";
    this.selectedPayType        = "";
    this.selectedFactOrNote     = "";

  }
  calculateFactsTotal(){
    this.globalVar.consoleLog(this.logToConsole,'...FACTS..>..calculateFactsTotal..->..Starting..>>', null); 
    this.formTitles.factsTotalString = '0..Facts..Total-> 0';
    if (this.facts === null)      return;
    if (this.facts.length === 0)  return
    let factsTotal = 0;
    let oneFactTotal = 0;
    for (let i = 0 ; i < this.facts.length ; i++ ) {
      if ( isNaN(Number(this.facts[i].factTotalCost)) === true ) {
        oneFactTotal = 0;
      } else {
        oneFactTotal = Number(this.facts[i].factTotalCost);
      }     
      factsTotal = factsTotal + oneFactTotal;
    }
    this.formTitles.factsTotalString = this.facts.length+'..Facts..Total->'+ factsTotal.toFixed(2);
  }
  /* ---------------------------------------FACTS BTN CLICK-----------------------------------------------------------*/
  changeShowFactsListBtnClick(){    
    this.resetShowFactFlags();  
    this.filterRecordList = this.globalVar.prepareFilterFactsValueRecordList(this.factsFirstAndLastDayOfMonth[0],this.factsFirstAndLastDayOfMonth[1],"","","","");
    this.getAllFactsFilter(this.filterRecordList);        
  }
  selectFactsToShowBtnClick(){
    this.globalVar.consoleLog(this.logToConsole,'...FACTS..>..selectFactsToShowBtnClick..->..Starting..>>', null); 
    if (this.localPayFactTypesRecordList.length < 1) { return;  }   
    const modalInitialState = {
      selectOption    :0,
      selectTitle1    :"Facturas Mes",
      selectTitle2    :"Seleccionar condiciones Facturas",
      selectMessage   :"Seleccione la CONDICIONES de las FACTURAS que quiere ver",
      selectFooter    :"Facturas Mes",
      callback        :'OK',   
    };
    this.globalVar.openModal(SelectManyVRListComponent, modalInitialState, 'modalLgTop0Left0').
    then((result:any)=>{
      this.resetShowFactFlags();     
      this.filterFacts = true;     
      this.filterRecordList = result;
      this.getAllFactsFilter(this.filterRecordList);   
    })
  }
  showCreateFactBtnClick(){
    this.globalVar.consoleLog(this.logToConsole,'->FACTS->createFactBtnClick->starting->', null);
    this.showCreateFact         = true;
    this.showCreateDetailedFact = false;
    this.showEditFact           = false;
    this.showEditDetailedFact   = false;
    this.newFact = new Fact();
    this.newFact.factDate = this.globalVar.workingDate;
    this.newFact.factStatus = true;
    this.newFact.factDetailed = false;
    this.newFact.factPayType = this.localPayFactTypesRecordList[1].value;
    this.newFact.factType = this.localFactTypesRecordList[0].value;
    this.newFact.factNetCost = 0;
    this.newFact.factTaxes = 0;
    this.newFact.factTotalCost = 0;
    this.createFactModal();
  }
  showCreateDetailedFactBtnClick(){
    this.globalVar.consoleLog(this.logToConsole,'->FACTS->createDetailedFactBtnClick->starting->', null);
    this.showCreateFact         = false;
    this.showCreateDetailedFact = true;
    this.showEditFact           = false;
    this.showEditDetailedFact   = false;
    this.newFact = new Fact();
    this.newFact.factDate = this.globalVar.workingDate;
    this.newFact.factStatus = true;
    this.newFact.factDetailed = true;
    this.newFact.factPayType = this.localPayFactTypesRecordList[1].value;
    this.newFact.factType = this.localFactTypesRecordList[0].value; 
    this.newFact.factNetCost = 0;
    this.newFact.factTaxes = 0;
    this.newFact.factTotalCost = 0;   
    this.createFactModal();
  }
  showEditFactBtnClick(modFact: Fact){
    if (this.isFactNotBlocked(modFact)){ 
      this.globalVar.consoleLog(this.logToConsole,'->FACTS->showEditFactBtnClick->editing->', null);
      this.newFact = modFact;            
      if (modFact.factDetailed === false) { 
        this.showCreateFact         = false;
        this.showCreateDetailedFact = false;
        this.showEditFact           = true;
        this.showEditDetailedFact   = false;
      }
      if (modFact.factDetailed === true)  { 
        this.showCreateFact         = false;
        this.showCreateDetailedFact = false;
        this.showEditFact           = false;
        this.showEditDetailedFact   = true;
      } 
      this.createFactModal();
    }
  }
  createFactModal(){
    this.globalVar.consoleLog(this.logToConsole,'->FACTS->createFactModal->starting->', null);   
    const modalInitialState = {
      newFact               :this.newFact,
      factsTitle            :'Facturas Mes',
      factsDate             :null, 
      createFact            :this.showCreateFact,             
      createDetailedFact    :this.showCreateDetailedFact,
      editFact              :this.showEditFact,
      editDetailedFact      :this.showEditDetailedFact,
      changeFactPayType     :true,
      changeFactDate        :true,
      hdFlag                :false,
      monthFlag             :false,
      callback              :'OK',   
    };
    this.globalVar.openModal(FactsCreateComponent, modalInitialState, 'modalXlTop0Left0').
    then((result:any)=>{
      this.getAllFactsFilter(this.filterRecordList);
    })
  }   
  enterFactsMonthBtnClick(){
    this.globalVar.consoleLog(this.logToConsole,'->HDs->enterHdDateBtnClick->starting->', null);
    let inputDate = new Date();
    let minDate = new Date();
    let maxDate = new Date();
    inputDate = this.globalVar.workingDate;
    minDate = this.globalVar.workingFirstDayOfYear;
    maxDate = this.globalVar.workingLastDayOfYear;
    const modalInitialState = {
      dateTitle   :"FACTURAS del MES",
      callback    :'OK',   
    };
    this.globalVar.openModal(EnterMonthComponent, modalInitialState, 'modalLgTop0Left0').
    then((selectedDate:any)=>{
      if (selectedDate != null) { 
        this.globalVar.changeWorkingDates(selectedDate); 
        this.factsFirstAndLastDayOfMonth = this.globalVar.workingFirstAndLastDayOfMonth;
        this.filterRecordList = this.globalVar.prepareFilterFactsValueRecordList(this.factsFirstAndLastDayOfMonth[0],this.factsFirstAndLastDayOfMonth[1],"","","","");
        this.getAllFactsFilter(this.filterRecordList); 
      }
    })
  }
  isFactNotBlocked(selectedFact: Fact):boolean{
    this.globalVar.consoleLog(this.logToConsole,'->HDS->isHdNotBlocked->starting->', null);
    if (selectedFact.factStatus === false){
      this._errorService.showErrorModal( "FACTURAS", "FACTURAS", "FACTURAS BLOQUEADA"+"->",
       "Proveedor->"+selectedFact.factProvName, "Nº Factura->"+selectedFact.factNumber, "Fecha->"+selectedFact.factDate, 
       "Tipo Factura->"+selectedFact.factType,"Forma Pago->"+selectedFact.factPayType, "Total Factura->"+selectedFact.factTotalCost)
      return false;
    } else return true;
  }  
  lockFactBtnClick(modFact: Fact){
    if (modFact.factStatus === true) modFact.factStatus = false;
    else  modFact.factStatus = true;
    this.updateOneFact(modFact);
  }
  searchFactsBtnClick() {
    if (this.showSearchFacts === true) {
      this.showSearchFacts = false;
    } else {
      this.showSearchFacts = true;
    }
    return;
  }
  setOrderFactBtnClick(value: string) {
    if (this.orderFactsList === value) {
      this.reverseFactsList = !this.reverseFactsList;
    }
    this.orderFactsList = value;
  }
  /*---------------------------------------FACTS  -- DATABASE-----------------------------------------------------------*/
  getAllFactsFilter(factsFilterValueRecordList:ValueRecord[]) {
    this.globalVar.consoleLog(this.logToConsole,'->FACTS->getAllFactsFilter->starting->', null);
    this._factsService.getAllFactsFilter(factsFilterValueRecordList)   
      .subscribe(data => {
                          this.facts = data;
                          this.orderFactsList = 'factProvName';
                          this.reverseFactsList = false;
                          this.sortedCollection = this.orderPipe.transform(this.facts,this.orderFactsList,this.reverseFactsList,this.caseInsensitive);
                          this.facts = this.sortedCollection;                          
                          this.formTitles.factsListStart = factsFilterValueRecordList[0].value; 
                          this.formTitles.factsListEnd = factsFilterValueRecordList[1].value;
                          this.selectedProv            = factsFilterValueRecordList[2].value;
                          this.selectedFactType        = factsFilterValueRecordList[3].value;
                          this.selectedPayType         = factsFilterValueRecordList[4].value;
                          this.selectedFactOrNote      = factsFilterValueRecordList[5].value;
                          let startDate = new Date(factsFilterValueRecordList[0].value);
                          this.formTitles.factsListMonthString = startDate.toLocaleString('default',{month:'long'});
                          let workingYear = startDate.getFullYear();
                          this.formTitles.factsListYearString = workingYear.toString();
                          this.calculateFactsTotal();
                          this.showFactsList = true;
                        },
                error => {
                          if (this.globalVar.handleError(error)) { throwError(error) };
                          });
  }

  updateOneFact(updateFact: Fact): void {
    this.globalVar.consoleLog(this.logToConsole,'->FACTS->updateOneFact->updating->', null);
    this._factsService.updateOneFact(updateFact)
      .subscribe(data => {
                          this.newFact = data;                          
                        },
                error => {
                          if (this.globalVar.handleError(error)) { throwError(error) };
                          });
  }
  /* -------------------------------------FACTS DELETE -------------------------------------------------------------*/
  confirmDeleteOneFactBtnClick(fact: Fact){
    this.globalVar.consoleLog(this.logToConsole,'->FACTS->confirmDeleteOneFactBtnClick->...Starting->', null);
    this._errorService.getDeleteMsgConfirmation("deleteMsg",this.formTitles.facts,this.formTitles.facts,
                            fact,this.formTitles.factDeleteOne,fact.factProvName,fact.factId.toString(),
                            fact.factTotalCost.toString(),"","","",).
        then((deleteOK:boolean)=>{
          if (deleteOK === true){
            if (fact.factDetailed === false) { this.deleteOneFact(fact); }                      
            else { this.deleteOneFact(fact) } 
          }
        })
  }
  confirmDeleteAllFactsBtnClick(){
    this.globalVar.consoleLog(this.logToConsole,'->FACTS->confirmDeleteAllFactsBtnClick->...Starting->', null);
    this._errorService.getDeleteMsgConfirmation("deleteMsg",this.formTitles.facts,this.formTitles.facts,
                            null,this.formTitles.factDeleteAll,"","","","","","",).
        then((deleteOK:boolean)=>{
          if (deleteOK === true){
            this.deleteAllFacts();
          }
        })
  } 
  deleteAllFacts(){
    this.globalVar.consoleLog(this.logToConsole,'->FACTS->deleteAllFacts->deleting All->', null);
    this._factsService.delAllFacts()
    .subscribe(data => { this.getAllFactsFilter(this.filterRecordList); },
              error => { if (this.globalVar.handleError(error)) { throwError(error) }; });
  }
  deleteOneFact(delFact: Fact){
    this.globalVar.consoleLog(this.logToConsole,'->FACTS->deleteOneFact->deleting One->', null);
    this._factsService.delOneFact(delFact)
    .subscribe(data => { this.getAllFactsFilter(this.filterRecordList); },
              error => { if (this.globalVar.handleError(error)) { throwError(error) }; });
  }
  /* ---------------------------------------MODEL FACTS BTN CLICK-----------------------------------------------------------*/
  changeShowFactModelListBtnClick(){
    this.resetShowFactFlags();
    this.getAllFactModels();
  }
  creatProvFactModelBtnClick(modFactModel: FactModels){
    this.globalVar.consoleLog(this.logToConsole,'->FACTS->creatProvFactModelBtnClick->editing->', null);
    this.showEditFactModel = true;
    this.showCreateFactModel = true;
    this.newFactModel = modFactModel;
    this.createFactModel();
  }
  showCreateFactModelBtnClick(){
    this.globalVar.consoleLog(this.logToConsole,'->FACTS->showCreateFactModelBtnClick->creating->', null);
    this.showCreateFactModel = true; 
    this.showEditFactModel = false;
    this.newFactModel = new FactModels();
    this.createFactModel();    
  }
  showEditFactModelBtnClick(modFactModel: FactModels){
    this.globalVar.consoleLog(this.logToConsole,'->FACTS->showEditFactModelBtnClick->editing->', null);
    this.showEditFactModel = true;
    this.showCreateFactModel = false;
    this.newFactModel = modFactModel;
    this.createFactModel();
  }
  private createFactModel(){
    const modalInitialState = {
      newFactModel          :this.newFactModel,
      modelsTitle           :'Facturas Mes',
      modelsDate            :null,  
      showCreateFactModel   :this.showCreateFactModel,
      showEditFactModel     :this.showEditFactModel,
      callback              :'OK',   
    };
    this.globalVar.openModal(FactModelCreateComponent, modalInitialState, 'modalXlTop0Left0').
    then((result:any)=>{
      this.getAllFactModels();
    })
  }
  searchFactModelsBtnClick() {
    if (this.showSearchFactModels === true) {
      this.showSearchFactModels = false;
    } else {
      this.showSearchFactModels = true;
    }
    return;
  }
  setOrderFactModelBtnClick(value: string) {
    if (this.orderFactModelsList === value) {
      this.reverseFactModelsList = !this.reverseFactModelsList;
    }
    this.orderFactModelsList = value;
  }
  /* ---------------------------------------MODEL FACTS DATABASE-----------------------------------------------------------*/
  getAllFactModels() {
    this.globalVar.consoleLog(this.logToConsole,'->FACTS->getAllFactModels->starting->', null);
    this._factModelService.getAllFactModels()
      .subscribe(data => { this.factModels = data;
                           this.orderFactModelsList = 'factModelsProvName';
                           this.reverseFactModelsList = false;
                           this.sortedCollection = this.orderPipe.transform(this.factModels,this.orderFactModelsList,this.reverseFactModelsList,this.caseInsensitive);
                           this.factModels = this.sortedCollection; 
                           this.showFactModelsList = true; 
                           this.formTitles.factModelsTotalString = this.factModels.length+'..Modelos Facts';
                         },
                error => { if (this.globalVar.handleError(error)) { throwError(error) }; });
  }
  /* -------------------------------------MODEL FACTS DELETE -------------------------------------------------------------*/
  confirmDeleteOneFactModelBtnClick(factModel: FactModels){
    this.globalVar.consoleLog(this.logToConsole,'->FACTS->confirmDeleteOneFactBtnClick->...Starting->', null);
    this._errorService.getDeleteMsgConfirmation("deleteMsg",this.formTitles.factModels,this.formTitles.factModels,
                                  factModel,this.formTitles.factModelDeleteOne,factModel.factModelsName,factModel.factModelsProvName,
                                  factModel.factModelsId.toString(),"","","",).
        then((deleteOK:boolean)=>{
          if (deleteOK === true){
            this.deleteOneFactModel(factModel);              
          }
        })
    return;
  }
  confirmDeleteAllFactModelsBtnClick(){
    this.globalVar.consoleLog(this.logToConsole,'->FACTS->confirmDeleteAllFactsBtnClick->...Starting->', null);
    this._errorService.getDeleteMsgConfirmation("deleteMsg",this.formTitles.factModels,this.formTitles.facts,
                            null,this.formTitles.factModelDeleteAll,"","","","","","",).
        then((deleteOK:boolean)=>{
          if (deleteOK === true){
            this.deleteAllFactModels();
          }
        })
  } 
  deleteAllFactModels(){
    this.globalVar.consoleLog(this.logToConsole,'->FACTS->deleteAllFactModels->deleting All->', null);
    this._factModelService.delAllFactModels()
    .subscribe(data => {  this.getAllFactModels(); },
              error => { if (this.globalVar.handleError(error)) { throwError(error) }; });
  }
  deleteOneFactModel(delFactModel: FactModels){
    this.globalVar.consoleLog(this.logToConsole,'->FACTS->deleteOneFactModel->deleting One->', null);
    this._factModelService.delOneFactModel(delFactModel)
    .subscribe(data => {  this.getAllFactModels(); },
              error => { if (this.globalVar.handleError(error)) { throwError(error) }; });
  }

}
