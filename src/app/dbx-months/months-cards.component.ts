import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';

import { BsModalRef } from 'ngx-bootstrap/modal';
import { OrderPipe } from 'ngx-order-pipe';
import { BsModalService } from 'ngx-bootstrap/modal';

import { GlobalVarService } from '../aa-common/global-var.service';
import { ValueRecord } from '../dbc-fields/field';

import { Month,MonthCardsSummary, MonthBanksCardsSummary } from './months';
import { CardTpv } from '../dbx-cards/cards';

@Component({
  selector      :'app-months-cards',
  templateUrl   :'./months-cards.component.html',
  styleUrls     :['./months.component.css'] 
})
export class MonthsCardsComponent implements OnInit {
  public  arrayWorkingDates     :Date[];
  public  cardTpvsMonth         :CardTpv[];
  public  monthHdNamesBankCaja  :ValueRecord[];
  public  monthSummRecords      :Month[];
  callback                      :any;
  result: Subject<any> = new Subject<any>();

  public formTitles = {     
    'monthListYearString'       :'',
    'monthListMonthString'      :'',
    'monthCards'                :'Tarjetas Y Vales Comida TOTALIZADOS y NO',
    'monthBankCards'            :'Tarjetas Y Vales Comida TOTALIZADOS a Banco',
    'monthsCardsTotalString'    :'',
    'monthsBankCardsTotalString':'',
    'cardsListDateStart'        :'',
    'cardsListDateEnd'          :'',
  };
  public formLabels = { 
    'monthCardTpvId'    :'Id.Tpv.Tarjeta',
    'monthCardName'     :'Tarjeta/Vales',
    'monthCardBank'     :'Banco',
    'monthCardPrevNot'  :'Ant.NO.Tot,',
    'monthCardPayNot'   :'Mes.NO.Tot.',
    'monthCardPayTot'   :'Mes.Totaliz.',
    'monthCardHds'      :'Mes.Suma',
    'monthCardTotMoney' :'Mes.Total.Din.',
    'monthCardTotExp'   :'Mes.Total.Gastos',
    'monthCardTotBank'  :'Mes.Total.Banco',
    'monthBankName'     :'Banco',
    'monthBankTotal'    :'Total',
    'monthCard0'        :'',
    'monthCard1'        :'',
    'monthCard2'        :'',
    'monthCard3'        :'',
    'monthCard4'        :'',
    'monthCard5'        :'',
    'monthCard6'        :'',
    'monthCard7'        :'',
    'monthCard8'        :'',
    'monthCard9'        :'',
  };
  public  monthCardsSummary     :MonthCardsSummary[];
  private logToConsole          :boolean;
  private sortedCollection      :any[];
  public  monthsHdsTotalString  :string;
  public  monthBankCardsArray   :MonthBanksCardsSummary[];
  //---------------------------------------MONTHS CARDS -- CONSTRUCTOR-------------------------------------------------------------
  constructor (   private bsModalRef: BsModalRef, private orderPipe: OrderPipe, private modalService: BsModalService, 
                  private globalVar: GlobalVarService) { }
  //---------------------------------------MONTHS CARDS -- NG ON INIT-------------------------------------------------------------
  ngOnInit(): void {
    this.logToConsole = this.globalVar.logToConsole;
    this.logToConsole = true;
    this.formTitles.monthListMonthString = this.globalVar.workingDate.toLocaleString('default',{month:'long'});
    let workingYear = this.globalVar.workingDate.getFullYear();
    this.formTitles.monthListYearString = workingYear.toString();
    this.formTitles.cardsListDateStart = this.globalVar.workingFirstDayOfMonth.toLocaleDateString(); 
    this.formTitles.cardsListDateEnd = this.globalVar.workingLastDayOfMonth.toLocaleDateString();
    this.monthsHdsTotalString = '';
    this.createCardsMonthSummViewRecords();
    this.createBanksCardsMonthSummViewRecords();
  }

  //---------------------------------------MONTHS  -- GENERAL --createCardsMonthSummViewRecords---------------------------------------
  createBanksCardsMonthSummViewRecords(){
    this.globalVar.consoleLog(this.logToConsole,'-> MONTH CARDS -> createBanksCardsMonthSummViewRecords ->...Starting...->', null);
    for (let j=0;j<this.cardTpvsMonth.length;j++){
      tpvName = this.cardTpvsMonth[j].cardTpvName.slice(0,10);
      switch (j){
        case 0: this.formLabels.monthCard0 =  tpvName; break;
        case 1: this.formLabels.monthCard1 =  tpvName; break;
        case 2: this.formLabels.monthCard2 =  tpvName; break;
        case 3: this.formLabels.monthCard3 =  tpvName; break;
        case 4: this.formLabels.monthCard4 =  tpvName; break;
        case 5: this.formLabels.monthCard5 =  tpvName; break;
        case 6: this.formLabels.monthCard6 =  tpvName; break;
        case 7: this.formLabels.monthCard7 =  tpvName; break;
        case 8: this.formLabels.monthCard8 =  tpvName; break;
        case 9: this.formLabels.monthCard9 =  tpvName; break;
        default: break;
      }
    }      
    var index = -1;
    var moneyToBank = 0;
    var bankName = '';
    var tpvName = '';
    this.monthBankCardsArray = new Array();
    for (let k=0;k<this.monthHdNamesBankCaja.length;k++){
      bankName = this.monthHdNamesBankCaja[k].value;
      var monthBankCardsItem = new MonthBanksCardsSummary();
      monthBankCardsItem.monthBankName = bankName;
      for (let j=0;j<this.cardTpvsMonth.length;j++){
        tpvName = this.cardTpvsMonth[j].cardTpvName;
        index = this.monthSummRecords.findIndex(item => item.monthName === tpvName && item.monthPlace === bankName && item.monthType === 'CARDSTOTBANK');
        if (index != -1) moneyToBank = this.monthSummRecords[index].monthAmount;
        else moneyToBank = 0;
        switch (j){
          case 0: monthBankCardsItem.monthCard0 =  moneyToBank; break;
          case 1: monthBankCardsItem.monthCard1 =  moneyToBank; break;
          case 2: monthBankCardsItem.monthCard2 =  moneyToBank; break;
          case 3: monthBankCardsItem.monthCard3 =  moneyToBank; break;
          case 4: monthBankCardsItem.monthCard4 =  moneyToBank; break;
          case 5: monthBankCardsItem.monthCard5 =  moneyToBank; break;
          case 6: monthBankCardsItem.monthCard6 =  moneyToBank; break;
          case 7: monthBankCardsItem.monthCard7 =  moneyToBank; break;
          case 8: monthBankCardsItem.monthCard8 =  moneyToBank; break;
          case 9: monthBankCardsItem.monthCard9 =  moneyToBank; break;
          default: break;
        }
      }
      this.monthBankCardsArray.push(monthBankCardsItem);
    }
    // calculate total for each bank, and total of all banks
    var sumTotalBanks = 0;
    var sumBankCards = 0;
    for (let h=0;h<this.monthBankCardsArray.length;h++){
      sumBankCards = 0;
      if ( isNaN(Number(this.monthBankCardsArray[h].monthCard0)) != true ) sumBankCards = sumBankCards + this.monthBankCardsArray[h].monthCard0;
      if ( isNaN(Number(this.monthBankCardsArray[h].monthCard1)) != true ) sumBankCards = sumBankCards + this.monthBankCardsArray[h].monthCard1;
      if ( isNaN(Number(this.monthBankCardsArray[h].monthCard2)) != true ) sumBankCards = sumBankCards + this.monthBankCardsArray[h].monthCard2;
      if ( isNaN(Number(this.monthBankCardsArray[h].monthCard3)) != true ) sumBankCards = sumBankCards + this.monthBankCardsArray[h].monthCard3;
      if ( isNaN(Number(this.monthBankCardsArray[h].monthCard4)) != true ) sumBankCards = sumBankCards + this.monthBankCardsArray[h].monthCard4;
      if ( isNaN(Number(this.monthBankCardsArray[h].monthCard5)) != true ) sumBankCards = sumBankCards + this.monthBankCardsArray[h].monthCard5;
      if ( isNaN(Number(this.monthBankCardsArray[h].monthCard6)) != true ) sumBankCards = sumBankCards + this.monthBankCardsArray[h].monthCard6;
      if ( isNaN(Number(this.monthBankCardsArray[h].monthCard7)) != true ) sumBankCards = sumBankCards + this.monthBankCardsArray[h].monthCard7;
      if ( isNaN(Number(this.monthBankCardsArray[h].monthCard8)) != true ) sumBankCards = sumBankCards + this.monthBankCardsArray[h].monthCard8;
      if ( isNaN(Number(this.monthBankCardsArray[h].monthCard9)) != true ) sumBankCards = sumBankCards + this.monthBankCardsArray[h].monthCard9; 
      this.monthBankCardsArray[h].monthBankTotal = sumBankCards;
      sumTotalBanks = sumTotalBanks + sumBankCards;
    }
    this.formTitles.monthsBankCardsTotalString = this.monthBankCardsArray.length+'..Bancos..Total->'+sumTotalBanks.toFixed(2);
  }
  createCardsMonthSummViewRecords(){
    this.globalVar.consoleLog(this.logToConsole,'-> MONTH CARDS -> createCardsMonthSummViewRecords ->...Starting...->', null); 
    this.sortedCollection = this.orderPipe.transform(this.cardTpvsMonth,'cardTpvName',true,false); 
    this.cardTpvsMonth = this.sortedCollection;
    this.monthCardsSummary = new Array();
    var index = -1;
    var newMonthCardSummary = new MonthCardsSummary();
    var tpvName = '';
    // first element with cards Totals
    newMonthCardSummary = new MonthCardsSummary();
    newMonthCardSummary.monthCardTpvId = -1;
    newMonthCardSummary.monthCardName = 'TOTALES Tarjetas y Vales';
    newMonthCardSummary.monthCardBank = ' =========> ';
    index = this.monthSummRecords.findIndex(item => item.monthType === 'TCARDSPREVNOT');
    if (index != -1) newMonthCardSummary.monthCardPrevNot = this.monthSummRecords[index].monthAmount;
    else newMonthCardSummary.monthCardPrevNot = 0;
    index = this.monthSummRecords.findIndex(item => item.monthType === 'TCARDSTPV');
    if (index != -1) newMonthCardSummary.monthCardHds = this.monthSummRecords[index].monthAmount;
    else newMonthCardSummary.monthCardHds = 0;
    index = this.monthSummRecords.findIndex(item => item.monthType === 'TCARDSPAYNOT');
    if (index != -1) newMonthCardSummary.monthCardPayNot = this.monthSummRecords[index].monthAmount;
    else newMonthCardSummary.monthCardPayNot = 0;
    index = this.monthSummRecords.findIndex(item => item.monthType === 'TCARDSPAYTOT');
    if (index != -1) newMonthCardSummary.monthCardPayTot = this.monthSummRecords[index].monthAmount;
    else newMonthCardSummary.monthCardPayTot = 0;
    index = this.monthSummRecords.findIndex(item => item.monthType === 'TCARDSTOTMONEY');
    if (index != -1) newMonthCardSummary.monthCardTotMoney = this.monthSummRecords[index].monthAmount;
    else newMonthCardSummary.monthCardTotMoney = 0;
    index = this.monthSummRecords.findIndex(item => item.monthType === 'TCARDSTOTEXP');
    if (index != -1) newMonthCardSummary.monthCardTotExp = this.monthSummRecords[index].monthAmount;
    else newMonthCardSummary.monthCardTotExp = 0;
    index = this.monthSummRecords.findIndex(item => item.monthType === 'TCARDSTOTBANK');
    if (index != -1) newMonthCardSummary.monthCardTotBank = this.monthSummRecords[index].monthAmount;
    else newMonthCardSummary.monthCardTotBank = 0;
    // Adding the element to the array
    this.monthCardsSummary.push(newMonthCardSummary);
    for (let k=0;k<this.cardTpvsMonth.length;k++){
      newMonthCardSummary = new MonthCardsSummary();
      tpvName = this.cardTpvsMonth[k].cardTpvName;
      newMonthCardSummary.monthCardTpvId = this.cardTpvsMonth[k].cardTpvId;
      newMonthCardSummary.monthCardName = tpvName;
      newMonthCardSummary.monthCardBank = this.cardTpvsMonth[k].cardTpvBank;
      index = this.monthSummRecords.findIndex(item => item.monthName === tpvName && item.monthType === 'CARDSPREVNOT');
      if (index != -1) newMonthCardSummary.monthCardPrevNot = this.monthSummRecords[index].monthAmount;
      else newMonthCardSummary.monthCardPrevNot = 0;
      index = this.monthSummRecords.findIndex(item => item.monthName === tpvName && item.monthType === 'CARDSTPV');
      if (index != -1) newMonthCardSummary.monthCardHds = this.monthSummRecords[index].monthAmount;
      else newMonthCardSummary.monthCardHds = 0;
      index = this.monthSummRecords.findIndex(item => item.monthName === tpvName && item.monthType === 'CARDSPAYNOT');
      if (index != -1) newMonthCardSummary.monthCardPayNot = this.monthSummRecords[index].monthAmount;
      else newMonthCardSummary.monthCardPayNot = 0;
      index = this.monthSummRecords.findIndex(item => item.monthName === tpvName && item.monthType === 'CARDSPAYTOT');
      if (index != -1) newMonthCardSummary.monthCardPayTot = this.monthSummRecords[index].monthAmount;
      else newMonthCardSummary.monthCardPayTot = 0;
      index = this.monthSummRecords.findIndex(item => item.monthName === tpvName && item.monthType === 'CARDSTOTMONEY');
      if (index != -1) newMonthCardSummary.monthCardTotMoney = this.monthSummRecords[index].monthAmount;
      else newMonthCardSummary.monthCardTotMoney = 0;
      index = this.monthSummRecords.findIndex(item => item.monthName === tpvName && item.monthType === 'CARDSTOTEXP');
      if (index != -1) newMonthCardSummary.monthCardTotExp = this.monthSummRecords[index].monthAmount;
      else newMonthCardSummary.monthCardTotExp = 0;
      index = this.monthSummRecords.findIndex(item => item.monthName === tpvName && item.monthType === 'CARDSTOTBANK');
      if (index != -1) newMonthCardSummary.monthCardTotBank = this.monthSummRecords[index].monthAmount;
      else newMonthCardSummary.monthCardTotBank = 0;
      if ( (newMonthCardSummary.monthCardPrevNot != 0) || (newMonthCardSummary.monthCardHds != 0) || (newMonthCardSummary.monthCardPayNot != 0) ||
           (newMonthCardSummary.monthCardPayTot != 0) || (newMonthCardSummary.monthCardTotMoney != 0) || (newMonthCardSummary.monthCardTotExp != 0) ||
           (newMonthCardSummary.monthCardTotBank != 0) )
        this.monthCardsSummary.push(newMonthCardSummary);
    }
    this.formTitles.monthsCardsTotalString = (this.monthCardsSummary.length - 1)+'..Tarjetas..Total->'+this.monthCardsSummary[0].monthCardHds.toFixed(2);
  }  
  //---------------------------------------MONTHS CARDS -- BTN CLICK-------------------------------------------------------------
  closeMonthCardsModalBtnClick(){
    if (this.bsModalRef.content.callback != null){
      //this.bsModalRef.content.callback('OK');
      this.result.next('OK');
      this.bsModalRef.hide();  
    }
  }

}