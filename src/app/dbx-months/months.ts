export class MonthSummary {
    monthSummId         :number;
    monthSummDate       :Date;
    monthSummMonth      :string;
    monthSummIncomes    :number;
    monthSummFacts      :number;
    monthSummDif        :number;
    monthSummTpvs       :number;
    monthSummDin        :number;
    monthSummMaqB       :number;
    monthSummMaqA       :number;
    monthSummTab        :number;
    monthSummGoods      :number;
    monthSummOther      :number;
    monthSummFactsHd    :number;
    monthSummCards      :number;
    monthSummStatus     :boolean;
}
export class Month {
    monthId         :number;
    monthDate       :Date;
    monthName       :string;
    monthType       :string;
    monthPlace      :string;
    monthAmount     :number;
    monthNotes      :string;
    monthStatus     :boolean;
}
export class MonthString {
    monthId         :string;
    monthDate       :string;
    monthName       :string;
    monthType       :string;
    monthPlace      :string;
    monthAmount     :string;
    monthNotes      :string;
    monthStatus     :string;
}
export class MonthTpv {
    monthTpvId      :number;
    monthTpvDate    :Date;
    monthTpvName    :string;
    monthTpvType    :string;
	monthTpvTickets :number;
	monthTpvTicket  :number;	
    monthTpvMoney   :number;	
}
export class MonthCardsSummary {
    monthCardTpvId      :number;
    monthCardName       :string;
    monthCardBank       :string;
    monthCardPrevNot    :number;
    monthCardPayNot     :number;
    monthCardPayTot     :number;
    monthCardHds        :number;
    monthCardTotMoney   :number;
    monthCardTotExp     :number;
    monthCardTotBank    :number;
}
export class MonthBanksCardsSummary {
    monthBankName       :string;
    monthBankTotal      :number;
    monthCard0          :number;
    monthCard1          :number;
    monthCard2          :number;
    monthCard3          :number;
    monthCard4          :number;
    monthCard5          :number;
    monthCard6          :number;
    monthCard7          :number;
    monthCard8          :number;
    monthCard9          :number;
}