import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { throwError, Subject } from 'rxjs';

import { BsModalRef } from 'ngx-bootstrap/modal';
import { OrderPipe } from 'ngx-order-pipe';
import { BsModalService } from 'ngx-bootstrap/modal';

import { GlobalVarService } from '../aa-common/global-var.service';
import { ValidationService } from '../aa-common/validation.service';
import { ValueRecord } from '../dbc-fields/field';

import { Month } from './months';
import { MonthsService } from './months.service';
import { HdType } from '../dbx-hd/hd'; 

@Component({
  selector      :'app-months-dinout',
  templateUrl   :'./months-dinout.component.html',
  styleUrls     :['./months.component.css'] 
})
export class MonthsDinoutComponent implements OnInit {
  public  arrayWorkingDates :Date[];
  public  hdTypesMonth      :HdType[];
  public  monthSummRecords  :Month[];
  callback                  :any;
  result: Subject<any> = new Subject<any>();

  public formTitles = {     
    'monthListYearString'       :'',
    'monthListMonthString'      :'',
    'monthEdit'                 :'Dinero Bancos',
    'monthsTotalString'         :'',
    'monthsListDateStart'       :'',
    'monthsListDateEnd'         :'',
  };
  public formLabels = { 
    'monthBCName'       :'BANCO/CAJA',
    'monthRetName'      :'RETIRADO',
    'monthBankCajaDSum' :'Ingresado',
    'monthBankCajaDRes' :'Retirado',
    'monthResDDif'      :'Dif.Ingr-Ret',
    'monthResDSum'      :'Entregado',
    'monthResDRes'      :'Devuelto',
  };
  public  monthDinBankCajaForm          :FormGroup;
  public  monthDinBankCajaFormArray     :FormArray;
  public  monthDinRetForm               :FormGroup;
  public  monthDinRetFormArray          :FormArray;
  public  monthResTotalForm             :FormGroup;
  public  monthResTotalFormArray        :FormArray;
  public  workingFirstAndLastDayOfMonth :Date[];
  private monthHdNamesBankCaja          :ValueRecord[];
  private monthHdNamesRet               :ValueRecord[];
  public  totalDifDinMonthRESString     :string;
  public  showMonthDinout               :boolean;
  private logToConsole                  :boolean;
  private sortedCollection              :any[];
  //---------------------------------------MONTHS DIN IN OUT -- CONSTRUCTOR-------------------------------------------------------------
  constructor (   private bsModalRef: BsModalRef, private fb:FormBuilder, private orderPipe: OrderPipe, private modalService: BsModalService, 
                  private _monthsService: MonthsService, private globalVar: GlobalVarService) { }
  //---------------------------------------MONTHS DIN IN OUT -- NG ON INIT-------------------------------------------------------------
  ngOnInit(): void {
    this.logToConsole = this.globalVar.logToConsole;
    this.logToConsole = true;
    this.showMonthDinout = false;
    this.workingFirstAndLastDayOfMonth = this.arrayWorkingDates;
    this.formTitles.monthListMonthString = this.globalVar.workingDate.toLocaleString('default',{month:'long'});
    let workingYear = this.globalVar.workingDate.getFullYear();
    this.formTitles.monthListYearString = workingYear.toString();
    this.formTitles.monthsListDateStart = this.globalVar.workingFirstDayOfMonth.toLocaleDateString(); 
    this.formTitles.monthsListDateEnd = this.globalVar.workingLastDayOfMonth.toLocaleDateString();
    this.monthResTotalFormArray =   new FormArray([]);
    this.monthResTotalForm =        new FormGroup({ monthResTotalFormArray: this.monthResTotalFormArray });
    this.prepareHdTypesMonthVRList();
    this.mixNamesRESHdTypes();
    this.createBankCajaMonthSummArray();
    this.createRetMonthSummArray();
    this.setTotalDifDinMonthRES();
    this.showMonthDinout = true;
  }
  /*---------------------------------------MONTHS DIN IN OUT -- GENERAL -----------------------------------------*/
  prepareHdTypesMonthVRList() { 
    this.globalVar.consoleLog(this.logToConsole, '-> MONTHs DIN IN OUT -> prepareHdTypesMonthVRList ->...Starting...->', null); 
    if (this.hdTypesMonth === null) return;  
    if (this.hdTypesMonth.length < 1) return;
    this.sortedCollection = this.orderPipe.transform(this.hdTypesMonth,'hdTypeName',false,false);
    this.hdTypesMonth = this.sortedCollection;   
    this.monthHdNamesRet = new Array();
    this.monthHdNamesBankCaja = new Array();
    let indexRet = 0;
    let indexBankCaja = 0;
    for (let i = 0 ; i < this.hdTypesMonth.length ; i++ ) {
      if (this.hdTypesMonth[i].hdTypeType =='RET') {       
        this.monthHdNamesRet[indexRet] = new ValueRecord();
        this.monthHdNamesRet[indexRet].id = indexRet;
        this.monthHdNamesRet[indexRet].value = this.hdTypesMonth[i].hdTypeName;
        indexRet = indexRet + 1;
      }
      if ( (this.hdTypesMonth[i].hdTypeType =='BANK') || (this.hdTypesMonth[i].hdTypeType =='CAJA') ){       
        this.monthHdNamesBankCaja[indexBankCaja] = new ValueRecord();
        this.monthHdNamesBankCaja[indexBankCaja].id = indexBankCaja;
        this.monthHdNamesBankCaja[indexBankCaja].value = this.hdTypesMonth[i].hdTypeName;
        indexBankCaja = indexBankCaja + 1;
      }
    }
  } 
  mixNamesRESHdTypes(){
    for (let i = 0 ; i < this.monthSummRecords.length ; i++ ) {
      if (this.monthSummRecords[i].monthType === 'RET') { 
        let indexHdsRet = this.monthHdNamesRet.findIndex(item => item.value === this.monthSummRecords[i].monthName);
        if (indexHdsRet === -1) { // not found
          let index = this.monthHdNamesRet.length;
          this.monthHdNamesRet[index] = new ValueRecord();
          this.monthHdNamesRet[index].id = index;
          this.monthHdNamesRet[index].value = this.monthSummRecords[i].monthName;
        } 
      }
      if ( (this.monthSummRecords[i].monthType === 'BANK') || (this.monthSummRecords[i].monthType === 'CAJA') ) { 
        let indexHdsBankCaja = this.monthHdNamesBankCaja.findIndex(item => item.value === this.monthSummRecords[i].monthName);
        if (indexHdsBankCaja === -1) { // not found
          let index = this.monthHdNamesBankCaja.length;
          this.monthHdNamesBankCaja[index] = new ValueRecord();
          this.monthHdNamesBankCaja[index].id = index;
          this.monthHdNamesBankCaja[index].value = this.monthSummRecords[i].monthName;
        } 
      }
    }
  }
  //--------------------------------------- MONTHS DIN IN OUT -- FORMS -- BANKCAJA FORM ARRAY -----------------------------------------------------
  createBankCajaMonthSummArray(){
    this.globalVar.consoleLog(this.logToConsole,'-> MONTHs DIN IN OUT -> createBankCajaMonthSummArray ->...Starting...->',null);
    this.monthDinBankCajaFormArray = new FormArray([]);
    this.monthDinBankCajaForm = new FormGroup({ monthDinBankCajaFormArray: this.monthDinBankCajaFormArray });   
    var index = 0;
    for (let i = 0 ; i < this.monthHdNamesBankCaja.length ; i++ ) {     
      let monthBankCajaDStart = 0;
      index = this.monthSummRecords.findIndex(item => item.monthName === this.monthHdNamesBankCaja[i].value && item.monthType === 'RBC-DSTART');
      if (index != -1) monthBankCajaDStart = this.monthSummRecords[index].monthAmount;
      let monthBankCajaTSum = 0;
      index = this.monthSummRecords.findIndex(item => item.monthName === this.monthHdNamesBankCaja[i].value && item.monthType === 'RBC-TSUM');
      if (index != -1) monthBankCajaTSum = this.monthSummRecords[index].monthAmount;
      let monthBankCajaTFact = 0;
      index = this.monthSummRecords.findIndex(item => item.monthName === this.monthHdNamesBankCaja[i].value && item.monthType === 'RBC-TFACT');
      if (index != -1) monthBankCajaTFact = this.monthSummRecords[index].monthAmount;
      let monthBankCajaDEnd = 0;
      index = this.monthSummRecords.findIndex(item => item.monthName === this.monthHdNamesBankCaja[i].value && item.monthType === 'RBC-DEND');
      if (index != -1) monthBankCajaDEnd = this.monthSummRecords[index].monthAmount;
      let monthBankCajaDCount = 0;
      index = this.monthSummRecords.findIndex(item => item.monthName === this.monthHdNamesBankCaja[i].value && item.monthType === 'RBC-DCOUNT');
      if (index != -1) monthBankCajaDCount = this.monthSummRecords[index].monthAmount;
      let monthBankCajaDSum = 0;
      index = this.monthSummRecords.findIndex(item => item.monthName === this.monthHdNamesBankCaja[i].value && item.monthType === 'RBC-DSUM');
      if (index != -1) monthBankCajaDSum = this.monthSummRecords[index].monthAmount;
      let monthBankCajaDRes = 0;
      index = this.monthSummRecords.findIndex(item => item.monthName === this.monthHdNamesBankCaja[i].value && item.monthType === 'RBC-DRES');
      if (index != -1) monthBankCajaDRes = this.monthSummRecords[index].monthAmount;
      let monthDinBankCajaDDif = monthBankCajaDSum - monthBankCajaDRes;           
     
      this.monthDinBankCajaFormArray.push(this.addMonthDinBankCajaFormGroup(this.monthHdNamesBankCaja[i].value,monthBankCajaDSum.toFixed(2),
                                                                      monthBankCajaDRes.toFixed(2),monthDinBankCajaDDif.toFixed(2))); 
      }
    this.monthDinBankCajaFormArray.controls.forEach(
      control => {
          control.get('monthBankCajaDSum').valueChanges.subscribe(
                () => { this.setDDifDinMonthBankCaja(this.monthDinBankCajaFormArray.controls.indexOf(control)); } )
          control.get('monthBankCajaDRes').valueChanges.subscribe(
                () => { this.setDDifDinMonthBankCaja(this.monthDinBankCajaFormArray.controls.indexOf(control)); } )
          control.get('monthBankCajaDDif').valueChanges.subscribe(
                () => { this.setTotalDifDinMonthRES(); } )
      }
    ) 
  }
  setTotalDifDinMonthRES(){
    var totalDifDinMonthRES = 0;
    this.monthDinRetFormArray.controls.forEach(
      control => { totalDifDinMonthRES = totalDifDinMonthRES + Number(control.get('monthRetDDif').value); } )
    this.monthDinBankCajaFormArray.controls.forEach(
      control => { totalDifDinMonthRES = totalDifDinMonthRES + Number(control.get('monthBankCajaDDif').value); } )
    this.totalDifDinMonthRESString = 'Total Dif ->  '+ totalDifDinMonthRES.toFixed(2);    
  }
  setDDifDinMonthBankCaja(indexOfChangedArrayElement: number){
    const monthDinBankCajaDSumControl = this.monthDinBankCajaFormArray.at(indexOfChangedArrayElement).get('monthBankCajaDSum');
    const monthDinBankCajaDResControl = this.monthDinBankCajaFormArray.at(indexOfChangedArrayElement).get('monthBankCajaDRes');
    const monthDinBankCajaDDifControl = this.monthDinBankCajaFormArray.at(indexOfChangedArrayElement).get('monthBankCajaDDif');
    let dDifDinMonthBankCaja = 0;
    if ( (isNaN(Number(monthDinBankCajaDSumControl.value)) === false) && (isNaN(Number(monthDinBankCajaDResControl.value)) === false) ){
      dDifDinMonthBankCaja = Number(monthDinBankCajaDSumControl.value) - Number(monthDinBankCajaDResControl.value);
    }
    monthDinBankCajaDDifControl.patchValue(dDifDinMonthBankCaja.toFixed(2));
  }  
  addMonthBankCajaFormGroup ( retName:string,retDStart:string,retTSum:string,retDSum:string,
                              retDRes:string,retTFact:string,retDEnd:string,retDCount:string ):FormGroup{
    var retDDif = Number(retDCount) - Number(retDEnd);
    var retDDifStr = retDDif.toFixed(2);
    return this.fb.group ({
      monthBankCajaName:   [{value:retName,  disabled:true } ],
      monthBankCajaDStart: [{value:retDStart,disabled:false},[Validators.required,ValidationService.decimalValidation] ],
      monthBankCajaTSum:   [{value:retTSum,  disabled:true },[Validators.required] ],
      monthBankCajaDSum:   [{value:retDSum,  disabled:true },[Validators.required] ],
      monthBankCajaDRes:   [{value:retDRes,  disabled:true },[Validators.required] ],
      monthBankCajaTFact:  [{value:retTFact, disabled:true },[Validators.required] ],
      monthBankCajaDEnd:   [{value:retDEnd,  disabled:true },[Validators.required] ],
      monthBankCajaDCount: [{value:retDCount,disabled:false},[Validators.required,ValidationService.decimalValidation] ],   
      monthBankCajaDDif:   [{value:retDDifStr,disabled:true},[Validators.required] ],      
    });
  }
  addMonthDinBankCajaFormGroup(retName:string,retDSum:string,retDRes:string,retDDif:string):FormGroup{    
    return this.fb.group ({
      monthBankCajaName: [{value:retName,disabled:true } ],
      monthBankCajaDSum: [{value:retDSum,disabled:false},[Validators.required,ValidationService.decimalValidation] ],
      monthBankCajaDRes: [{value:retDRes,disabled:false},[Validators.required,ValidationService.decimalValidation] ],
      monthBankCajaDDif: [{value:retDDif,disabled:true },[Validators.required] ],  
    });
  }  

  /*--------------------------------------- MONTHS DIN IN OUT  -- FORMS -- RET FORM ARRAY ---------------------------------------------------*/  
  createRetMonthSummArray(){
    this.globalVar.consoleLog(this.logToConsole,'-> MONTHs DIN IN OUT -> createRetMonthSummArray ->...Starting...->',null);
    this.monthDinRetFormArray = new FormArray([]); 
    this.monthDinRetForm = new FormGroup({ monthDinRetFormArray: this.monthDinRetFormArray });
    var index = 0;    
    for (let i = 0 ; i < this.monthHdNamesRet.length ; i++ ) {                  
      let monthRetDStart = 0;
      index = this.monthSummRecords.findIndex(item => item.monthName === this.monthHdNamesRet[i].value && item.monthType === 'RRT-DSTART');
      if (index != -1) monthRetDStart = this.monthSummRecords[index].monthAmount;
      let monthRetTSum = 0;
      index = this.monthSummRecords.findIndex(item => item.monthName === this.monthHdNamesRet[i].value && item.monthType === 'RRT-TSUM');
      if (index != -1) monthRetTSum = this.monthSummRecords[index].monthAmount;
      let monthRetDEnd = 0;
      index = this.monthSummRecords.findIndex(item => item.monthName === this.monthHdNamesRet[i].value && item.monthType === 'RRT-DEND');
      if (index != -1) monthRetDEnd = this.monthSummRecords[index].monthAmount;
      let monthRetDCount = 0;
      index = this.monthSummRecords.findIndex(item => item.monthName === this.monthHdNamesRet[i].value && item.monthType === 'RRT-DCOUNT');
      if (index != -1) monthRetDCount = this.monthSummRecords[index].monthAmount;
      let monthRetDSum = 0;
      index = this.monthSummRecords.findIndex(item => item.monthName === this.monthHdNamesRet[i].value && item.monthType === 'RRT-DSUM');
      if (index != -1) monthRetDSum = this.monthSummRecords[index].monthAmount;
      let monthRetDRes = 0;
      index = this.monthSummRecords.findIndex(item => item.monthName === this.monthHdNamesRet[i].value && item.monthType === 'RRT-DRES');
      if (index != -1) monthRetDRes = this.monthSummRecords[index].monthAmount;
      let monthDinRetDDif = monthRetDSum - monthRetDRes;
      this.monthDinRetFormArray.push(this.addMonthDinRetFormGroup(this.monthHdNamesRet[i].value,monthRetDSum.toFixed(2),
                                                            monthRetDRes.toFixed(2),monthDinRetDDif.toFixed(2)));    
    }
    this.monthDinRetFormArray.controls.forEach(
      control => {
          control.get('monthRetDSum').valueChanges.subscribe(
                () => { this.setDDifDinMonthRet(this.monthDinRetFormArray.controls.indexOf(control)); } )
          control.get('monthRetDRes').valueChanges.subscribe(
                () => { this.setDDifDinMonthRet(this.monthDinRetFormArray.controls.indexOf(control)); } )
          control.get('monthRetDDif').valueChanges.subscribe(
                () => { this.setTotalDifDinMonthRES(); } )
      }
    ) 
  }
  setDDifDinMonthRet(indexOfChangedArrayElement: number){
    const monthDinRetDSumControl = this.monthDinRetFormArray.at(indexOfChangedArrayElement).get('monthRetDSum');
    const monthDinRetDResControl = this.monthDinRetFormArray.at(indexOfChangedArrayElement).get('monthRetDRes');
    const monthDinRetDDifControl = this.monthDinRetFormArray.at(indexOfChangedArrayElement).get('monthRetDDif');
    let dDifDinMonthRet = 0;
    if ( (isNaN(Number(monthDinRetDSumControl.value)) === false) && (isNaN(Number(monthDinRetDResControl.value)) === false) ){
      dDifDinMonthRet = Number(monthDinRetDSumControl.value) - Number(monthDinRetDResControl.value);
    }
    monthDinRetDDifControl.patchValue(dDifDinMonthRet.toFixed(2));
  }
  addMonthDinRetFormGroup(retName:string,retDSum:string,retDRes:string,retDDif:string):FormGroup{    
    return this.fb.group ({
      monthRetName: [{value:retName,disabled:true } ],
      monthRetDSum: [{value:retDSum,disabled:false},[Validators.required,ValidationService.decimalValidation] ],
      monthRetDRes: [{value:retDRes,disabled:false},[Validators.required,ValidationService.decimalValidation] ],
      monthRetDDif: [{value:retDDif,disabled:true },[Validators.required] ],
    });
  }
  public setNumberDecimal($event) {
    if (isNaN(parseFloat($event.target.value))=== true) {
      $event.target.value = '';
    } else {
      $event.target.value = parseFloat($event.target.value).toFixed(2);
    }   
  }  
  copyDinMonthBankCajaFormArrayToMonthSummRecords(){
    this.globalVar.consoleLog(this.logToConsole,'-> MONTHs DIN IN OUT -> copyDinMonthBankCajaFormArrayToMonthSummRecords ->...Starting...->', null);
    var index = 0;
    for(let j=0; j< this.monthDinBankCajaFormArray.length; j++){
      let monthDinBankCajaName   = this.monthDinBankCajaFormArray.at(j).get('monthBankCajaName').value;
      let monthDinBankCajaDSum   = Number(this.monthDinBankCajaFormArray.at(j).get('monthBankCajaDSum').value);
      let monthDinBankCajaDRes   = Number(this.monthDinBankCajaFormArray.at(j).get('monthBankCajaDRes').value);
      let monthDinBankCajaTSum   = 0; 
      let monthDinBankCajaTCards = 0; 
      let monthDinBankCajaTFact  = 0; 
      let monthDinBankCajaDStart = 0;
      index = this.monthSummRecords.findIndex(item => item.monthName === monthDinBankCajaName && item.monthType === 'RBC-TSUM');
      if (index != -1) monthDinBankCajaTSum = this.monthSummRecords[index].monthAmount;
      index = this.monthSummRecords.findIndex(item => item.monthName === monthDinBankCajaName && item.monthType === 'RBC-TCARDS');
      if (index != -1) monthDinBankCajaTCards = this.monthSummRecords[index].monthAmount;
      index = this.monthSummRecords.findIndex(item => item.monthName === monthDinBankCajaName && item.monthType === 'RBC-TFACT');
      if (index != -1) monthDinBankCajaTFact = this.monthSummRecords[index].monthAmount;
      index = this.monthSummRecords.findIndex(item => item.monthName === monthDinBankCajaName && item.monthType === 'RBC-DSTART');
      if (index != -1) monthDinBankCajaDStart = this.monthSummRecords[index].monthAmount;
      let monthDinBankCajaDEnd   = monthDinBankCajaDStart + monthDinBankCajaTSum + monthDinBankCajaDSum - 
                                    monthDinBankCajaDRes + monthDinBankCajaTCards  - monthDinBankCajaTFact; 
      index = this.monthSummRecords.findIndex(item => item.monthName === monthDinBankCajaName && item.monthType === 'RBC-DEND');
      if (index != -1) this.monthSummRecords[index].monthAmount = monthDinBankCajaDEnd;      
      index = this.monthSummRecords.findIndex(item => item.monthName === monthDinBankCajaName && item.monthType === 'RBC-DSUM');
      if (index != -1) { 
        this.monthSummRecords[index].monthAmount = monthDinBankCajaDSum;
      } else { 
        index = this.monthSummRecords.length;
        this.monthSummRecords[index] = new Month();
        this.monthSummRecords[index].monthDate   = this.workingFirstAndLastDayOfMonth[0];
        this.monthSummRecords[index].monthName   = monthDinBankCajaName;
        this.monthSummRecords[index].monthType   = 'RBC-DSUM';
        this.monthSummRecords[index].monthPlace  = 'RESBANKCAJA';
        this.monthSummRecords[index].monthStatus = true;
        this.monthSummRecords[index].monthAmount = monthDinBankCajaDSum;
      }
      index = this.monthSummRecords.findIndex(item => item.monthName === monthDinBankCajaName && item.monthType === 'RBC-DRES');
      if (index != -1) { this.monthSummRecords[index].monthAmount = monthDinBankCajaDRes; 
      } else { 
        index = this.monthSummRecords.length;
        this.monthSummRecords[index] = new Month();
        this.monthSummRecords[index].monthDate   = this.workingFirstAndLastDayOfMonth[0];
        this.monthSummRecords[index].monthName   = monthDinBankCajaName;
        this.monthSummRecords[index].monthType   = 'RBC-DRES';
        this.monthSummRecords[index].monthPlace  = 'RESBANKCAJA';
        this.monthSummRecords[index].monthStatus = true;
        this.monthSummRecords[index].monthAmount = monthDinBankCajaDRes;
      }
    }       
  }
  copyDinMonthRetFormArrayToMonthSummRecords(){
    this.globalVar.consoleLog(this.logToConsole,'-> MONTHs DIN IN OUT -> copyDinMonthRetFormArrayToMonthSummRecords ->...Starting...->', null);
    var index = 0;
    for(let j=0; j< this.monthDinRetFormArray.length; j++){
      let monthDinRetName   = this.monthDinRetFormArray.at(j).get('monthRetName').value;
      let monthDinRetDSum   = Number(this.monthDinRetFormArray.at(j).get('monthRetDSum').value);
      let monthDinRetDRes   = Number(this.monthDinRetFormArray.at(j).get('monthRetDRes').value);
      let monthDinRetTSum   = 0; 
      let monthDinRetDStart = 0;
      index = this.monthSummRecords.findIndex(item => item.monthName === monthDinRetName && item.monthType === 'RRT-TSUM');
      if (index != -1) monthDinRetTSum = this.monthSummRecords[index].monthAmount;
      index = this.monthSummRecords.findIndex(item => item.monthName === monthDinRetName && item.monthType === 'RRT-DSTART');
      if (index != -1) monthDinRetDStart = this.monthSummRecords[index].monthAmount;
      let monthDinRetDEnd   = monthDinRetDStart + monthDinRetTSum + monthDinRetDSum - monthDinRetDRes; 
      index = this.monthSummRecords.findIndex(item => item.monthName === monthDinRetName && item.monthType === 'RRT-DEND');
      if (index != -1) this.monthSummRecords[index].monthAmount = monthDinRetDEnd;      
      index = this.monthSummRecords.findIndex(item => item.monthName === monthDinRetName && item.monthType === 'RRT-DSUM');
      if (index != -1) { 
        this.monthSummRecords[index].monthAmount = monthDinRetDSum;
      } else { 
        index = this.monthSummRecords.length;
        this.monthSummRecords[index] = new Month();
        this.monthSummRecords[index].monthDate   = this.workingFirstAndLastDayOfMonth[0];
        this.monthSummRecords[index].monthName   = monthDinRetName;
        this.monthSummRecords[index].monthType   = 'RRT-DSUM';
        this.monthSummRecords[index].monthPlace  = 'RESRET';
        this.monthSummRecords[index].monthStatus = true;
        this.monthSummRecords[index].monthAmount = monthDinRetDSum;
      }
      index = this.monthSummRecords.findIndex(item => item.monthName === monthDinRetName && item.monthType === 'RRT-DRES');
      if (index != -1) { 
        this.monthSummRecords[index].monthAmount = monthDinRetDRes;
      } else { 
        index = this.monthSummRecords.length;
        this.monthSummRecords[index] = new Month();
        this.monthSummRecords[index].monthDate   = this.workingFirstAndLastDayOfMonth[0];
        this.monthSummRecords[index].monthName   = monthDinRetName;
        this.monthSummRecords[index].monthType   = 'RRT-DRES';
        this.monthSummRecords[index].monthPlace  = 'RESRET';
        this.monthSummRecords[index].monthStatus = true;
        this.monthSummRecords[index].monthAmount = monthDinRetDRes;
      }
    }
  }
  //---------------------------------------MONTHS DIN IN OUT -- BTN CLICK -------------------------------------------------------------------
  saveMonthDinoutBtnClick(){
    this.copyDinMonthBankCajaFormArrayToMonthSummRecords();
    this.copyDinMonthRetFormArrayToMonthSummRecords();
    this.updateInDBMonthSummRecords(this.monthSummRecords); 
  }  
  closeMonthDinoutModalBtnClick(){
    if (this.bsModalRef.content.callback != null){
      //this.bsModalRef.content.callback(null);
      this.result.next(null);
      this.bsModalRef.hide();  
    }
  }
  //-------------------------------------- MONTHS DIN IN OUT -- MONTHS DATABASE ------------------------------------------------------------
  updateInDBMonthSummRecords(updatedMonthSummRecords: Month[]): void {
    this.globalVar.consoleLog(this.logToConsole, '-> MONTHs DIN IN OUT -> updateInDBMonthSummRecords ->...Starting...->', null);
    this._monthsService.updateInDBMonthSummRecords(updatedMonthSummRecords)
      .subscribe(data => { this.monthSummRecords = new Array();
                           this.monthSummRecords = data;
                           //this.bsModalRef.content.callback(this.monthSummRecords);
                           this.result.next(this.monthSummRecords);
                           this.bsModalRef.hide();  
                         },
                error => { if (this.globalVar.handleError(error)) { throwError(error) }; });
  }

}