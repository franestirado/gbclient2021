import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { throwError, Subject } from 'rxjs';

import { BsModalRef } from 'ngx-bootstrap/modal';
import { OrderPipe } from 'ngx-order-pipe';
import { BsModalService } from 'ngx-bootstrap/modal';

import { GlobalVarService } from '../aa-common/global-var.service';
import { ValidationService } from '../aa-common/validation.service';
import { ValueRecord } from '../dbc-fields/field';

import { Month } from './months';
import { MonthsService } from './months.service';
import { HdType } from '../dbx-hd/hd'; 
import { CardTpv } from '../dbx-cards/cards';

@Component({
  selector      :'app-months-dinstart',
  templateUrl   :'./months-dinstart.component.html',
  styleUrls     :['./months.component.css']  
})
export class MonthsDinStartComponent implements OnInit {
  public  arrayWorkingDates :Date[];
  public  hdTypesMonth      :HdType[];
  public  cardTpvsMonth     :CardTpv[];
  public  monthSummRecords  :Month[];
  callback                  :any;
  result: Subject<any> = new Subject<any>();

  public formTitles = {     
    'monthListYearString'       :'',
    'monthListMonthString'      :'',
    'monthEdit'                 :'Dinero Inicio Fin',
    'monthsTotalString'         :'',
    'monthsListDateStart'       :'',
    'monthsListDateEnd'         :'',
  };
  public formLabels = { 
    'monthBCName'       :'BANCO/CAJA',
    'monthRetName'      :'RETIRADO',
    'monthBankCajaDSum' :'Ingresado',
    'monthBankCajaDRes' :'Retirado',
    'monthResDDif'      :'Dif.Ingr-Ret',
    'monthResDSum'      :'Entregado',
    'monthResDRes'      :'Devuelto',
    'monthResDStart'    :'D.Inicio',
    'monthResTSum'      :'T.Sumas',
    'monthResTCards'    :'T.Tarjetas',
    'monthResTFact'     :'T.Fact',
    'monthResDEnd'      :'D.Fin',
    'monthResDCont'     :'D.Contado',
    'monthResTotalDDif' :'D.Contado-D.Fin',
  };
  public  monthBankCajaForm             :FormGroup;
  public  monthBankCajaFormArray        :FormArray;
  public  monthRetForm                  :FormGroup;
  public  monthRetFormArray             :FormArray;
  public  monthResTotalForm             :FormGroup;
  public  monthResTotalFormArray        :FormArray;
  public  workingFirstAndLastDayOfMonth :Date[];
  private monthHdNamesBankCaja          :ValueRecord[];
  private monthHdNamesRet               :ValueRecord[];
  public  totalSupDinMonthRESString     :string;
  public  showMonthDinStart             :boolean;
  private resTotalDStart                :number;
  private resTotalDSum                  :number;
  private resTotalDRes                  :number;
  private resTotalTSum                  :number;
  private resTotalTFact                 :number;
  private resTotalTCards                :number;
  private resTotalDEnd                  :number;
  private resTotalDCount                :number;
  private resTotalDDif                  :number;
  private resTotalHdsDStart             :number;
  private resTotalHdsDEnd               :number;
  private resTotalCardsPrevNot          :number;
  private resTotalCardsNot              :number;
  private resTotalCardsAllNoTot         :number;
  private logToConsole                  :boolean;
  private sortedCollection              :any[];
  //--------------------------------------- MONTHS DIN START END -- CONSTRUCTOR-------------------------------------------------------------
  constructor (   private bsModalRef: BsModalRef, private fb:FormBuilder, private orderPipe: OrderPipe, private modalService: BsModalService, 
                  private _monthsService: MonthsService, private globalVar: GlobalVarService) { }
  //--------------------------------------- MONTHS DIN START END -- NG ON INIT--------------------------------------------------------------
  ngOnInit(): void {
    this.logToConsole = this.globalVar.logToConsole;
    this.logToConsole = true;
    this.showMonthDinStart = false;
    this.workingFirstAndLastDayOfMonth = this.arrayWorkingDates;
    this.formTitles.monthListMonthString = this.globalVar.workingDate.toLocaleString('default',{month:'long'});
    let workingYear = this.globalVar.workingDate.getFullYear();
    this.formTitles.monthListYearString = workingYear.toString();
    this.formTitles.monthsListDateStart = this.globalVar.workingFirstDayOfMonth.toLocaleDateString(); 
    this.formTitles.monthsListDateEnd = this.globalVar.workingLastDayOfMonth.toLocaleDateString();

    this.monthRetFormArray =        new FormArray([]);
    this.monthRetForm =             new FormGroup({ monthRetFormArray: this.monthRetFormArray });
    this.monthBankCajaFormArray =   new FormArray([]);
    this.monthBankCajaForm =        new FormGroup({ monthBankCajaFormArray: this.monthBankCajaFormArray });
    this.monthResTotalFormArray =   new FormArray([]);
    this.monthResTotalForm =        new FormGroup({ monthResTotalFormArray: this.monthResTotalFormArray });
    this.prepareHdTypesMonthVRList();
    this.mixNamesRESHdTypes();
    this.createBankCajaMonthSummArray();
    this.createRetMonthSummArray();
    this.increaseResTotalNumbers(true);
    this.getHdsDinStartEndFromMSR();
    this.createResTotalMonthSummArray();
    this.setMonthResTotal();     
    this.showMonthDinStart = true;
  }
  //--------------------------------------- MONTHS DIN START END -- GENERAL --------------------------------------------------------------
  prepareHdTypesMonthVRList() { 
    this.globalVar.consoleLog(this.logToConsole, '-> MONTHs DIN IN OUT -> prepareHdTypesMonthVRList ->...Starting...->', null); 
    if (this.hdTypesMonth === null) return;  
    if (this.hdTypesMonth.length < 1) return;
    this.sortedCollection = this.orderPipe.transform(this.hdTypesMonth,'hdTypeName',false,false);
    this.hdTypesMonth = this.sortedCollection;   
    this.monthHdNamesRet = new Array();
    this.monthHdNamesBankCaja = new Array();
    let indexRet = 0;
    let indexBankCaja = 0;
    for (let i = 0 ; i < this.hdTypesMonth.length ; i++ ) {
      if (this.hdTypesMonth[i].hdTypeType =='RET') {       
        this.monthHdNamesRet[indexRet] = new ValueRecord();
        this.monthHdNamesRet[indexRet].id = indexRet;
        this.monthHdNamesRet[indexRet].value = this.hdTypesMonth[i].hdTypeName;
        indexRet = indexRet + 1;
      }
      if ( (this.hdTypesMonth[i].hdTypeType =='BANK') || (this.hdTypesMonth[i].hdTypeType =='CAJA') ){       
        this.monthHdNamesBankCaja[indexBankCaja] = new ValueRecord();
        this.monthHdNamesBankCaja[indexBankCaja].id = indexBankCaja;
        this.monthHdNamesBankCaja[indexBankCaja].value = this.hdTypesMonth[i].hdTypeName;
        indexBankCaja = indexBankCaja + 1;
      }
    }
  } 
  mixNamesRESHdTypes(){
    for (let i = 0 ; i < this.monthSummRecords.length ; i++ ) {
      if (this.monthSummRecords[i].monthType === 'RET') { 
        let indexHdsRet = this.monthHdNamesRet.findIndex(item => item.value === this.monthSummRecords[i].monthName);
        if (indexHdsRet === -1) { // not found
          let index = this.monthHdNamesRet.length;
          this.monthHdNamesRet[index] = new ValueRecord();
          this.monthHdNamesRet[index].id = index;
          this.monthHdNamesRet[index].value = this.monthSummRecords[i].monthName;
        } 
      }
      if ( (this.monthSummRecords[i].monthType === 'BANK') || (this.monthSummRecords[i].monthType === 'CAJA') ) { 
        let indexHdsBankCaja = this.monthHdNamesBankCaja.findIndex(item => item.value === this.monthSummRecords[i].monthName);
        if (indexHdsBankCaja === -1) { // not found
          let index = this.monthHdNamesBankCaja.length;
          this.monthHdNamesBankCaja[index] = new ValueRecord();
          this.monthHdNamesBankCaja[index].id = index;
          this.monthHdNamesBankCaja[index].value = this.monthSummRecords[i].monthName;
        } 
      }
    }
  }
  getHdsDinStartEndFromMSR(){
    this.resTotalHdsDStart = 0;
    this.resTotalHdsDEnd   = 0;
    this.resTotalCardsPrevNot = 0;
    this.resTotalCardsNot = 0;
    this.resTotalCardsAllNoTot = 0;
    var index = -1;
    index = this.monthSummRecords.findIndex(item => item.monthType === 'THD-DSTART');
    if (index != -1) this.resTotalHdsDStart = this.monthSummRecords[index].monthAmount;
    index = this.monthSummRecords.findIndex(item => item.monthType === 'THD-DEND');
    if (index != -1) this.resTotalHdsDEnd = this.monthSummRecords[index].monthAmount;
    index = this.monthSummRecords.findIndex(item => item.monthType === 'TCARDSPREVNOT');
    if (index != -1) this.resTotalCardsPrevNot = this.monthSummRecords[index].monthAmount;
    index = this.monthSummRecords.findIndex(item => item.monthType === 'TCARDSPAYNOT');
    if (index != -1) this.resTotalCardsNot = this.monthSummRecords[index].monthAmount;
    index = this.monthSummRecords.findIndex(item => item.monthType === 'TCARDSALLNOTOT');
    if (index != -1) this.resTotalCardsAllNoTot = this.monthSummRecords[index].monthAmount;
  }
  increaseResTotalNumbers ( resetFlag:boolean,resTotalDStart?:number,resTotalDSum?:number,resTotalDRes?:number,resTotalTSum?:number,resTotalTCards?:number,
                            resTotalTFact?:number,resTotalDEnd?:number,resTotalDCount?:number,resTotalHdsDStart?:number, resTotalHdsDEnd?:number) {
    if (resetFlag === true) {
      this.resTotalDStart = 0;
      this.resTotalDSum   = 0;
      this.resTotalDRes   = 0;
      this.resTotalTSum   = 0;
      this.resTotalTCards = 0;
      this.resTotalTFact  = 0;
      this.resTotalDEnd   = 0;
      this.resTotalDCount = 0;
      this.resTotalDDif   = 0;
      this.resTotalHdsDStart = 0;
      this.resTotalHdsDEnd   = 0;
    } else {
      this.resTotalDStart = this.resTotalDStart + resTotalDStart + resTotalHdsDStart;
      this.resTotalDSum   = this.resTotalDSum   + resTotalDSum ;
      this.resTotalDRes   = this.resTotalDRes   + resTotalDRes ;
      this.resTotalTSum   = this.resTotalTSum   + resTotalTSum ;
      this.resTotalTCards = this.resTotalTCards + resTotalTCards ;
      this.resTotalTFact  = this.resTotalTFact  + resTotalTFact ;
      this.resTotalDEnd   = this.resTotalDEnd   + resTotalDEnd  + resTotalHdsDEnd;
      this.resTotalDCount = this.resTotalDCount + resTotalDCount + resTotalHdsDEnd;
      this.resTotalDDif   = this.resTotalDCount - this.resTotalDEnd ;
      this.resTotalHdsDStart = this.resTotalHdsDStart + resTotalHdsDStart ;
      this.resTotalHdsDEnd   = this.resTotalHdsDEnd + resTotalHdsDEnd ;
    }
  }
  //--------------------------------------- MONTHS DIN START END -- FORMS -- RESTOTAL FORM ARRAY ---------------------------------------------
  createResTotalMonthSummArray(){
    this.globalVar.consoleLog(this.logToConsole,'-> MONTHs DIN IN OUT -> createResTotalMonthSummArray ->...Starting...->',null);
    let initialValue = 0;
    this.monthResTotalFormArray = new FormArray([]);
    this.monthResTotalForm = new FormGroup({ monthResTotalFormArray: this.monthResTotalFormArray });
    this.monthResTotalFormArray.push(this.addMonthResTotalFormGroup('TOTALES',this.resTotalDStart.toFixed(2), this.resTotalTSum.toFixed(2),
                                                                              this.resTotalDSum.toFixed(2),   this.resTotalDRes.toFixed(2),
                                                                              this.resTotalTCards.toFixed(2), this.resTotalTFact.toFixed(2),
                                                                              this.resTotalDEnd.toFixed(2),
                                                                              this.resTotalDCount.toFixed(2) ,this.resTotalDDif.toFixed(2)));
    this.monthResTotalFormArray.push(this.addMonthResTotalFormGroup('Din BAR',this.resTotalHdsDStart.toFixed(2),initialValue.toFixed(2),
                                                                              initialValue.toFixed(2),initialValue.toFixed(2),initialValue.toFixed(2),
                                                                              initialValue.toFixed(2),this.resTotalHdsDEnd.toFixed(2),
                                                                              this.resTotalHdsDEnd.toFixed(2),initialValue.toFixed(2)));
    this.monthResTotalFormArray.push(this.addMonthResTotalFormGroup('Pend.Tarj.',this.resTotalCardsPrevNot.toFixed(2),initialValue.toFixed(2),
                                                                              initialValue.toFixed(2),initialValue.toFixed(2),initialValue.toFixed(2),
                                                                              initialValue.toFixed(2),this.resTotalCardsAllNoTot.toFixed(2),
                                                                              this.resTotalCardsAllNoTot.toFixed(2),initialValue.toFixed(2)));                                                                              
  }
  addMonthResTotalFormGroup ( resTotalName:string,resTotalDStart:string,resTotalTSum:string,resTotalDSum:string,resTotalDRes:string,
                              resTotalTCards:string,resTotalTFact:string,resTotalDEnd:string,resTotalDCount:string,resTotalDDif:string):FormGroup{    
      return this.fb.group ({
          monthResTotalName:  [{value:resTotalName,  disabled:true } ],
          monthResTotalDStart:[{value:resTotalDStart,disabled:true },[Validators.required] ],
          monthResTotalTSum:  [{value:resTotalTSum,  disabled:true },[Validators.required] ],
          monthResTotalDSum:  [{value:resTotalDSum,  disabled:true },[Validators.required] ],
          monthResTotalDRes:  [{value:resTotalDRes,  disabled:true },[Validators.required] ],
          monthResTotalTCards:[{value:resTotalTCards,disabled:true },[Validators.required] ],
          monthResTotalTFact: [{value:resTotalTFact, disabled:true },[Validators.required] ],
          monthResTotalDEnd:  [{value:resTotalDEnd,  disabled:true },[Validators.required] ],
          monthResTotalDCount:[{value:resTotalDCount,disabled:true },[Validators.required] ],
          monthResTotalDDif:  [{value:resTotalDDif,  disabled:true },[Validators.required] ],         
          });
  }
  //--------------------------------------- MONTHS DIN START END -- FORMS -- BANKCAJA FORM ARRAY ---------------------------------------------
  createBankCajaMonthSummArray(){
    this.globalVar.consoleLog(this.logToConsole,'-> MONTHs DIN IN OUT -> createBankCajaMonthSummArray ->...Starting...->',null);
    this.monthBankCajaFormArray = new FormArray([]);
    this.monthBankCajaForm = new FormGroup({ monthBankCajaFormArray: this.monthBankCajaFormArray });
    var index = 0;
    for (let i = 0 ; i < this.monthHdNamesBankCaja.length ; i++ ) {     
      let monthBankCajaDStart = 0;
      index = this.monthSummRecords.findIndex(item => item.monthName === this.monthHdNamesBankCaja[i].value && item.monthType === 'RBC-DSTART');
      if (index != -1) monthBankCajaDStart = this.monthSummRecords[index].monthAmount;
      let monthBankCajaTSum = 0;
      index = this.monthSummRecords.findIndex(item => item.monthName === this.monthHdNamesBankCaja[i].value && item.monthType === 'RBC-TSUM');
      if (index != -1) monthBankCajaTSum = this.monthSummRecords[index].monthAmount;
      let monthBankCajaTCards = 0;
      index = this.monthSummRecords.findIndex(item => item.monthName === this.monthHdNamesBankCaja[i].value && item.monthType === 'RBC-TCARDS');     
      if (index != -1) monthBankCajaTCards = this.monthSummRecords[index].monthAmount;
      let monthBankCajaTFact = 0;
      index = this.monthSummRecords.findIndex(item => item.monthName === this.monthHdNamesBankCaja[i].value && item.monthType === 'RBC-TFACT');
      if (index != -1) monthBankCajaTFact = this.monthSummRecords[index].monthAmount;   
      let monthBankCajaDEnd = 0;
      index = this.monthSummRecords.findIndex(item => item.monthName === this.monthHdNamesBankCaja[i].value && item.monthType === 'RBC-DEND');
      if (index != -1) monthBankCajaDEnd = this.monthSummRecords[index].monthAmount;
      let monthBankCajaDCount = 0;
      index = this.monthSummRecords.findIndex(item => item.monthName === this.monthHdNamesBankCaja[i].value && item.monthType === 'RBC-DCOUNT');
      if (index != -1) monthBankCajaDCount = this.monthSummRecords[index].monthAmount;
      let monthBankCajaDSum = 0;
      index = this.monthSummRecords.findIndex(item => item.monthName === this.monthHdNamesBankCaja[i].value && item.monthType === 'RBC-DSUM');
      if (index != -1) monthBankCajaDSum = this.monthSummRecords[index].monthAmount;
      let monthBankCajaDRes = 0;
      index = this.monthSummRecords.findIndex(item => item.monthName === this.monthHdNamesBankCaja[i].value && item.monthType === 'RBC-DRES');
      if (index != -1) monthBankCajaDRes = this.monthSummRecords[index].monthAmount;
      let monthDinBankCajaDDif = monthBankCajaDSum - monthBankCajaDRes;      
      this.monthBankCajaFormArray.push(this.addMonthBankCajaFormGroup(this.monthHdNamesBankCaja[i].value,monthBankCajaDStart.toFixed(2),
                                                                      monthBankCajaTSum.toFixed(2),monthBankCajaDSum.toFixed(2),
                                                                      monthBankCajaDRes.toFixed(2),monthBankCajaTFact.toFixed(2),
                                                                      monthBankCajaTCards.toFixed(2),
                                                                      monthBankCajaDEnd.toFixed(2),monthBankCajaDCount.toFixed(2)));       
     }
    this.monthBankCajaFormArray.controls.forEach(
      control => {
          control.get('monthBankCajaDStart').valueChanges.subscribe(
                        () => { this.setDEndMonthBankCaja(this.monthBankCajaFormArray.controls.indexOf(control)); } )
          control.get('monthBankCajaDStart').valueChanges.subscribe(
                        () => { this.setMonthResTotal(); } )                                
          control.get('monthBankCajaDCount').valueChanges.subscribe(
                        () => { this.setDEndMonthBankCaja(this.monthBankCajaFormArray.controls.indexOf(control)); } )
          control.get('monthBankCajaDCount').valueChanges.subscribe(
                        () => { this.setMonthResTotal(); } )
      }
    )
  }
  setMonthResTotal(){
    var resTotalDStart = 0;
    var resTotalDSum   = 0;
    var resTotalDRes   = 0;
    var resTotalTSum   = 0;
    var resTotalTCards = 0;
    var resTotalTFact  = 0;
    var resTotalDEnd   = 0;
    var resTotalDCount = 0;
    var resTotalDDif = 0;
    var resTotalSupDin = 0;
    this.monthBankCajaFormArray.controls.forEach(
      control => {  resTotalDStart = resTotalDStart + Number(control.get('monthBankCajaDStart').value);
                    resTotalDSum   = resTotalDSum   + Number(control.get('monthBankCajaDSum').value);
                    resTotalDRes   = resTotalDRes   + Number(control.get('monthBankCajaDRes').value);
                    resTotalTSum   = resTotalTSum   + Number(control.get('monthBankCajaTSum').value);
                    resTotalTCards = resTotalTCards + Number(control.get('monthBankCajaTCards').value);
                    resTotalTFact  = resTotalTFact  + Number(control.get('monthBankCajaTFact').value);
                    resTotalDEnd   = resTotalDEnd   + Number(control.get('monthBankCajaDEnd').value);
                    resTotalDCount = resTotalDCount + Number(control.get('monthBankCajaDCount').value); 
                   } )
    this.monthRetFormArray.controls.forEach(
      control => {  resTotalDStart = resTotalDStart + Number(control.get('monthRetDStart').value);
                    resTotalDSum   = resTotalDSum   + Number(control.get('monthRetDSum').value);
                    resTotalDRes   = resTotalDRes   + Number(control.get('monthRetDRes').value);
                    resTotalTSum   = resTotalTSum   + Number(control.get('monthRetTSum').value);
                    resTotalDEnd   = resTotalDEnd   + Number(control.get('monthRetDEnd').value);
                    resTotalDCount = resTotalDCount + Number(control.get('monthRetDCount').value); 
                  } )
    // adding hds din start end to res total
    const monthResTotalHdsDStartControl = this.monthResTotalFormArray.at(1).get('monthResTotalDStart');
    const monthResTotalHdsDEndControl   = this.monthResTotalFormArray.at(1).get('monthResTotalDEnd');
    const monthResTotalHdsDCountControl = this.monthResTotalFormArray.at(1).get('monthResTotalDCount');
    resTotalDStart = resTotalDStart + Number(monthResTotalHdsDStartControl.value);
    resTotalDEnd   = resTotalDEnd   + Number(monthResTotalHdsDEndControl.value);
    resTotalDCount = resTotalDCount + Number(monthResTotalHdsDCountControl.value);
    resTotalDDif =  resTotalDCount - resTotalDEnd;
    // adding cards no totalized previously and in this month to res total
    const monthResTotalCardsPrevNotDStartControl = this.monthResTotalFormArray.at(2).get('monthResTotalDStart');
    const monthResTotalCardsPayNotDEndControl  = this.monthResTotalFormArray.at(2).get('monthResTotalDEnd');
    const monthResTotalCardsPayNotDCountControl = this.monthResTotalFormArray.at(2).get('monthResTotalDCount');
    resTotalDStart = resTotalDStart + Number(monthResTotalCardsPrevNotDStartControl.value);
    resTotalDEnd   = resTotalDEnd   + Number(monthResTotalCardsPayNotDEndControl.value);
    resTotalDCount = resTotalDCount + Number(monthResTotalCardsPayNotDCountControl.value);
    resTotalDDif =  resTotalDCount - resTotalDEnd;
    // updating total values in res total
    const monthResTotalDStartControl  = this.monthResTotalFormArray.at(0).get('monthResTotalDStart');
    const monthResTotalDSumControl    = this.monthResTotalFormArray.at(0).get('monthResTotalDSum');
    const monthResTotalDResControl    = this.monthResTotalFormArray.at(0).get('monthResTotalDRes');
    const monthResTotalTSumControl    = this.monthResTotalFormArray.at(0).get('monthResTotalTSum');
    const monthResTotalTCardsControl  = this.monthResTotalFormArray.at(0).get('monthResTotalTCards');
    const monthResTotalTFactControl   = this.monthResTotalFormArray.at(0).get('monthResTotalTFact');
    const monthResTotalDCountControl  = this.monthResTotalFormArray.at(0).get('monthResTotalDCount');
    const monthResTotalDEndControl    = this.monthResTotalFormArray.at(0).get('monthResTotalDEnd');
    const monthResTotalDDifControl    = this.monthResTotalFormArray.at(0).get('monthResTotalDDif');
    monthResTotalDStartControl.patchValue(  resTotalDStart.toFixed(2));
    monthResTotalDSumControl.patchValue(    resTotalDSum.toFixed(2));
    monthResTotalDResControl.patchValue(    resTotalDRes.toFixed(2));
    monthResTotalTSumControl.patchValue(    resTotalTSum.toFixed(2));
    monthResTotalTCardsControl.patchValue(  resTotalTCards.toFixed(2));
    monthResTotalTFactControl.patchValue(   resTotalTFact.toFixed(2));
    monthResTotalDCountControl.patchValue(  resTotalDCount.toFixed(2));
    monthResTotalDEndControl.patchValue(    resTotalDEnd.toFixed(2));
    monthResTotalDDifControl.patchValue(    resTotalDDif.toFixed(2))
    resTotalSupDin = resTotalDCount - resTotalDStart;
    this.totalSupDinMonthRESString = 'Total Dif ->  '+ resTotalSupDin.toFixed(2);    

  }
  setDEndMonthBankCaja(indexOfChangedArrayElement: number){
    const monthBankCajaDStartControl = this.monthBankCajaFormArray.at(indexOfChangedArrayElement).get('monthBankCajaDStart');
    const monthBankCajaTSumControl   = this.monthBankCajaFormArray.at(indexOfChangedArrayElement).get('monthBankCajaTSum');
    const monthBankCajaDSumControl   = this.monthBankCajaFormArray.at(indexOfChangedArrayElement).get('monthBankCajaDSum');
    const monthBankCajaDResControl   = this.monthBankCajaFormArray.at(indexOfChangedArrayElement).get('monthBankCajaDRes');
    const monthBankCajaTCardsControl = this.monthBankCajaFormArray.at(indexOfChangedArrayElement).get('monthBankCajaTCards');
    const monthBankCajaTFactControl  = this.monthBankCajaFormArray.at(indexOfChangedArrayElement).get('monthBankCajaTFact');
    const monthBankCajaDEndControl   = this.monthBankCajaFormArray.at(indexOfChangedArrayElement).get('monthBankCajaDEnd');
    const monthBankCajaDCountControl = this.monthBankCajaFormArray.at(indexOfChangedArrayElement).get('monthBankCajaDCount');
    const monthBankCajaDDifControl   = this.monthBankCajaFormArray.at(indexOfChangedArrayElement).get('monthBankCajaDDif');
    let dEndMonthBankCaja = 0;
    let dDifMonthBankCaja = 0;
    if (  (isNaN(Number(monthBankCajaDStartControl.value)) === false) && (isNaN(Number(monthBankCajaTSumControl.value)) === false)
       && (isNaN(Number(monthBankCajaDSumControl.value)) === false)   && (isNaN(Number(monthBankCajaDResControl.value)) === false)
       && (isNaN(Number(monthBankCajaTCardsControl.value)) === false) && (isNaN(Number(monthBankCajaTFactControl.value)) === false) ) {
      dEndMonthBankCaja = Number(monthBankCajaDStartControl.value) + Number(monthBankCajaTSumControl.value) +
                          Number(monthBankCajaDSumControl.value)   - Number(monthBankCajaDResControl.value) +
                          Number(monthBankCajaTCardsControl.value) - Number(monthBankCajaTFactControl.value);
      dDifMonthBankCaja = Number(monthBankCajaDCountControl.value) - dEndMonthBankCaja;
    }
    monthBankCajaDEndControl.patchValue(dEndMonthBankCaja.toFixed(2));
    monthBankCajaDDifControl.patchValue(dDifMonthBankCaja.toFixed(2));
  }
  addMonthBankCajaFormGroup ( retName:string,retDStart:string,retTSum:string,retDSum:string,
                              retDRes:string,retTFact:string,retTCards:string,retDEnd:string,retDCount:string ):FormGroup{
    var retDDif = Number(retDCount) - Number(retDEnd);
    var retDDifStr = retDDif.toFixed(2);
    return this.fb.group ({
      monthBankCajaName:   [{value:retName,  disabled:true } ],
      monthBankCajaDStart: [{value:retDStart,disabled:false},[Validators.required,ValidationService.decimalValidation] ],
      monthBankCajaTSum:   [{value:retTSum,  disabled:true },[Validators.required] ],
      monthBankCajaDSum:   [{value:retDSum,  disabled:true },[Validators.required] ],
      monthBankCajaDRes:   [{value:retDRes,  disabled:true },[Validators.required] ],
      monthBankCajaTFact:  [{value:retTFact, disabled:true },[Validators.required] ],
      monthBankCajaTCards: [{value:retTCards,disabled:true },[Validators.required] ],
      monthBankCajaDEnd:   [{value:retDEnd,  disabled:true },[Validators.required] ],
      monthBankCajaDCount: [{value:retDCount,disabled:false},[Validators.required,ValidationService.decimalValidation] ],   
      monthBankCajaDDif:   [{value:retDDifStr,disabled:true},[Validators.required] ],      
    });
  }
  addMonthDinBankCajaFormGroup(retName:string,retDSum:string,retDRes:string,retDDif:string):FormGroup{    
    return this.fb.group ({
      monthBankCajaName: [{value:retName,disabled:true } ],
      monthBankCajaDSum: [{value:retDSum,disabled:false},[Validators.required,ValidationService.decimalValidation] ],
      monthBankCajaDRes: [{value:retDRes,disabled:false},[Validators.required,ValidationService.decimalValidation] ],
      monthBankCajaDDif: [{value:retDDif,disabled:true },[Validators.required] ],  
    });
  }  
  //--------------------------------------- MONTHS DIN START END -- FORMS -- RET FORM ARRAY ---------------------------------------------
  createRetMonthSummArray(){
    this.globalVar.consoleLog(this.logToConsole,'-> MONTHs DIN IN OUT -> createRetMonthSummArray ->...Starting...->',null);
    this.monthRetFormArray = new FormArray([]); 
    this.monthRetForm = new FormGroup({ monthRetFormArray: this.monthRetFormArray });
    var index = 0;    
    for (let i = 0 ; i < this.monthHdNamesRet.length ; i++ ) {                  
      let monthRetDStart = 0;
      index = this.monthSummRecords.findIndex(item => item.monthName === this.monthHdNamesRet[i].value && item.monthType === 'RRT-DSTART');
      if (index != -1) monthRetDStart = this.monthSummRecords[index].monthAmount;
      let monthRetTSum = 0;
      index = this.monthSummRecords.findIndex(item => item.monthName === this.monthHdNamesRet[i].value && item.monthType === 'RRT-TSUM');
      if (index != -1) monthRetTSum = this.monthSummRecords[index].monthAmount;
      let monthRetDEnd = 0;
      index = this.monthSummRecords.findIndex(item => item.monthName === this.monthHdNamesRet[i].value && item.monthType === 'RRT-DEND');
      if (index != -1) monthRetDEnd = this.monthSummRecords[index].monthAmount;
      let monthRetDCount = 0;
      index = this.monthSummRecords.findIndex(item => item.monthName === this.monthHdNamesRet[i].value && item.monthType === 'RRT-DCOUNT');
      if (index != -1) monthRetDCount = this.monthSummRecords[index].monthAmount;
      let monthRetDSum = 0;
      index = this.monthSummRecords.findIndex(item => item.monthName === this.monthHdNamesRet[i].value && item.monthType === 'RRT-DSUM');
      if (index != -1) monthRetDSum = this.monthSummRecords[index].monthAmount;
      let monthRetDRes = 0;
      index = this.monthSummRecords.findIndex(item => item.monthName === this.monthHdNamesRet[i].value && item.monthType === 'RRT-DRES');
      if (index != -1) monthRetDRes = this.monthSummRecords[index].monthAmount;
      let monthDinRetDDif = monthRetDSum - monthRetDRes;
      this.monthRetFormArray.push(this.addMonthRetFormGroup(this.monthHdNamesRet[i].value,monthRetDStart.toFixed(2),monthRetTSum.toFixed(2),
                                                              monthRetDSum.toFixed(2),monthRetDRes.toFixed(2),
                                                              monthRetDEnd.toFixed(2),monthRetDCount.toFixed(2)));    
    }
    this.monthRetFormArray.controls.forEach(
      control => {
          control.get('monthRetDStart').valueChanges.subscribe(
                        () => { this.setDEndMonthRet(this.monthRetFormArray.controls.indexOf(control)); } )
          control.get('monthRetDStart').valueChanges.subscribe(
                        () => { this.setMonthResTotal(); } )
          control.get('monthRetDCount').valueChanges.subscribe(
                        () => { this.setDEndMonthRet(this.monthRetFormArray.controls.indexOf(control)); } )
          control.get('monthRetDCount').valueChanges.subscribe(
                        () => { this.setMonthResTotal(); } )
      }
    )
  }
  setDEndMonthRet(indexOfChangedArrayElement: number){
    const monthRetDStartControl = this.monthRetFormArray.at(indexOfChangedArrayElement).get('monthRetDStart');
    const monthRetTSumControl   = this.monthRetFormArray.at(indexOfChangedArrayElement).get('monthRetTSum');
    const monthRetDSumControl   = this.monthRetFormArray.at(indexOfChangedArrayElement).get('monthRetDSum');
    const monthRetDResControl   = this.monthRetFormArray.at(indexOfChangedArrayElement).get('monthRetDRes');
    const monthRetDEndControl   = this.monthRetFormArray.at(indexOfChangedArrayElement).get('monthRetDEnd');
    const monthRetDCountControl = this.monthRetFormArray.at(indexOfChangedArrayElement).get('monthRetDCount');
    const monthRetDDifControl   = this.monthRetFormArray.at(indexOfChangedArrayElement).get('monthRetDDif');
    let dEndMonthRet = 0;
    let dDifMonthRet = 0;
    if (  (isNaN(Number(monthRetDStartControl.value)) === false) && (isNaN(Number(monthRetTSumControl.value)) === false )
       && (isNaN(Number(monthRetDSumControl.value)) === false )  && (isNaN(Number(monthRetDResControl.value) ) === false) ){
      dEndMonthRet =  Number(monthRetDStartControl.value) + Number(monthRetTSumControl.value) + 
                      Number(monthRetDSumControl.value)   - Number(monthRetDResControl.value);
      dDifMonthRet = Number(monthRetDCountControl.value) - dEndMonthRet;
    }
    monthRetDEndControl.patchValue(dEndMonthRet.toFixed(2));
    monthRetDDifControl.patchValue(dDifMonthRet.toFixed(2));
  }
  addMonthRetFormGroup(retName:string,retDStart:string,retTSum:string,retDSum:string,retDRes:string,retDEnd:string,retDCount:string):FormGroup{    
    var retDDif = Number(retDCount) - Number(retDEnd);
    var retDDifStr = retDDif.toFixed(2);
    return this.fb.group ({
      monthRetName:   [{value:retName,  disabled:true } ],
      monthRetDStart: [{value:retDStart,disabled:false},[Validators.required,ValidationService.decimalValidation] ],
      monthRetTSum:   [{value:retTSum,  disabled:true },[Validators.required] ],
      monthRetDSum:   [{value:retDSum,  disabled:true },[Validators.required] ],
      monthRetDRes:   [{value:retDRes,  disabled:true },[Validators.required] ],
      monthRetDEnd:   [{value:retDEnd,  disabled:true },[Validators.required] ],
      monthRetDCount: [{value:retDCount,disabled:false},[Validators.required,ValidationService.decimalValidation] ],
      monthRetDDif:   [{value:retDDifStr,disabled:true},[Validators.required] ],         
    });
  }  
  public setNumberDecimal($event) {
    if (isNaN(parseFloat($event.target.value))=== true) {
      $event.target.value = '';
    } else {
      $event.target.value = parseFloat($event.target.value).toFixed(2);
    }   
  }  
  copyMonthBankCajaFormArrayToMonthSummRecords(){
    this.globalVar.consoleLog(this.logToConsole,'-> MONTHs DIN IN OUT -> copyMonthBankCajaFormArrayToMonthSummRecords ->...Starting...->', null);
    var index = 0;
    for(let j=0; j< this.monthBankCajaFormArray.length; j++){
      let monthBankCajaTSum   = Number(this.monthBankCajaFormArray.at(j).get('monthBankCajaTSum').value);
      let monthBankCajaTFact  = Number(this.monthBankCajaFormArray.at(j).get('monthBankCajaTFact').value);
      let monthBankCajaName   = this.monthBankCajaFormArray.at(j).get('monthBankCajaName').value;
      let monthBankCajaDStart = Number(this.monthBankCajaFormArray.at(j).get('monthBankCajaDStart').value);
      index = this.monthSummRecords.findIndex(item => item.monthName === monthBankCajaName && item.monthType === 'RBC-DSTART');
      if (index != -1) this.monthSummRecords[index].monthAmount = monthBankCajaDStart;
      let monthBankCajaDEnd   = Number(this.monthBankCajaFormArray.at(j).get('monthBankCajaDEnd').value);
      index = this.monthSummRecords.findIndex(item => item.monthName === monthBankCajaName && item.monthType === 'RBC-DEND');
      if (index != -1) this.monthSummRecords[index].monthAmount = monthBankCajaDEnd;
      let monthBankCajaDCount = Number(this.monthBankCajaFormArray.at(j).get('monthBankCajaDCount').value);
      index = this.monthSummRecords.findIndex(item => item.monthName === monthBankCajaName && item.monthType === 'RBC-DCOUNT');
      if (index != -1) this.monthSummRecords[index].monthAmount = monthBankCajaDCount; 
    }       
  }
  copyMonthRetFormArrayToMonthSummRecords(){
    this.globalVar.consoleLog(this.logToConsole,'-> MONTHs DIN IN OUT -> copyMonthRetFormArrayToMonthSummRecords ->...Starting...->', null);
    var index = 0;
    for(let j=0; j< this.monthRetFormArray.length; j++){
      let monthRetTSum   = Number(this.monthRetFormArray.at(j).get('monthRetTSum').value);
      let monthRetName   = this.monthRetFormArray.at(j).get('monthRetName').value;
      let monthRetDStart = Number(this.monthRetFormArray.at(j).get('monthRetDStart').value);
      index = this.monthSummRecords.findIndex(item => item.monthName === monthRetName && item.monthType === 'RRT-DSTART');
      if (index != -1) this.monthSummRecords[index].monthAmount = monthRetDStart;
      let monthRetDEnd   = Number(this.monthRetFormArray.at(j).get('monthRetDEnd').value);
      index = this.monthSummRecords.findIndex(item => item.monthName === monthRetName && item.monthType === 'RRT-DEND');
      if (index != -1) this.monthSummRecords[index].monthAmount = monthRetDEnd;
      let monthRetDCount = Number(this.monthRetFormArray.at(j).get('monthRetDCount').value);
      index = this.monthSummRecords.findIndex(item => item.monthName === monthRetName && item.monthType === 'RRT-DCOUNT');
      if (index != -1) this.monthSummRecords[index].monthAmount = monthRetDCount;
    }       
  }
  copyMonthResTotalFormArrayToMonthSummRecords(){
        this.globalVar.consoleLog(this.logToConsole,'-> MONTHs DIN IN OUT -> copyMonthResTotalFormArrayToMonthSummRecords ->...Starting...->', null);
    var index = 0;
    var monthResTotalDStart = 0;
    var monthResTotalDSum   = 0;
    var monthResTotalDRes   = 0;
    var monthResTotalTSum   = 0;
    var monthResTotalTCards = 0;
    var monthResTotalTFact  = 0;
    var monthResTotalDEnd   = 0;
    var monthResTotalDCount = 0; 
    var monthResTotalDDif   = 0; 
    var monthResTotalSupDin = 0;    
    var monthResTotalSupDif = 0;
    var monthResTotalSupTeot = 0;    
    var j = 0;
    monthResTotalDStart   = Number(this.monthResTotalFormArray.at(j).get('monthResTotalDStart').value);
    index = this.monthSummRecords.findIndex(item => item.monthType === 'RES-DSTART');
    if (index != -1) this.monthSummRecords[index].monthAmount = monthResTotalDStart;
    monthResTotalDSum     = Number(this.monthResTotalFormArray.at(j).get('monthResTotalDSum').value);
    index = this.monthSummRecords.findIndex(item => item.monthType === 'RES-DSUM');
    if (index != -1) this.monthSummRecords[index].monthAmount = monthResTotalDSum;
    monthResTotalDRes     = Number(this.monthResTotalFormArray.at(j).get('monthResTotalDRes').value);
    index = this.monthSummRecords.findIndex(item => item.monthType === 'RES-DRES');
    if (index != -1) this.monthSummRecords[index].monthAmount = monthResTotalDRes;
    monthResTotalTSum     = Number(this.monthResTotalFormArray.at(j).get('monthResTotalTSum').value);
    index = this.monthSummRecords.findIndex(item => item.monthType === 'RES-TSUM');
    if (index != -1) this.monthSummRecords[index].monthAmount = monthResTotalTSum;
    monthResTotalTCards   = Number(this.monthResTotalFormArray.at(j).get('monthResTotalTCards').value);
    index = this.monthSummRecords.findIndex(item => item.monthType === 'RES-TCARDS');
    if (index != -1) this.monthSummRecords[index].monthAmount = monthResTotalTCards;
    monthResTotalTFact    = Number(this.monthResTotalFormArray.at(j).get('monthResTotalTFact').value);
    index = this.monthSummRecords.findIndex(item => item.monthType === 'RES-TFACT');
    if (index != -1) this.monthSummRecords[index].monthAmount = monthResTotalTFact;
    monthResTotalDEnd     = Number(this.monthResTotalFormArray.at(j).get('monthResTotalDEnd').value);
    index = this.monthSummRecords.findIndex(item => item.monthType === 'RES-DEND');
    if (index != -1) this.monthSummRecords[index].monthAmount = monthResTotalDEnd;
    monthResTotalDCount   = Number(this.monthResTotalFormArray.at(j).get('monthResTotalDCount').value);
    index = this.monthSummRecords.findIndex(item => item.monthType === 'RES-DCOUNT');
    if (index != -1) this.monthSummRecords[index].monthAmount = monthResTotalDCount;
    monthResTotalDDif     = Number(this.monthResTotalFormArray.at(j).get('monthResTotalDDif').value);
    index = this.monthSummRecords.findIndex(item => item.monthType === 'RES-DDIF');
    if (index != -1) this.monthSummRecords[index].monthAmount = monthResTotalDDif;
    
    monthResTotalSupDin= monthResTotalDCount - monthResTotalDStart
    index = this.monthSummRecords.findIndex(item => item.monthType === 'SUP-DIN');
    if (index != -1) this.monthSummRecords[index].monthAmount = monthResTotalSupDin;

    index = this.monthSummRecords.findIndex(item => item.monthType === 'SUP-TEOR');
    if (index != -1) monthResTotalSupTeot = this.monthSummRecords[index].monthAmount;  

    monthResTotalSupDif = monthResTotalDCount - monthResTotalDStart -monthResTotalSupTeot;
    index = this.monthSummRecords.findIndex(item => item.monthType === 'SUP-DIF');
    if (index != -1) this.monthSummRecords[index].monthAmount = monthResTotalSupDif;
    
    var monthResTotalHdsDStart = this.resTotalHdsDStart; 
    var monthResTotalHdsDEnd   = this.resTotalHdsDEnd; 
    this.increaseResTotalNumbers(true);
    this.increaseResTotalNumbers(false, monthResTotalDStart,monthResTotalDSum, monthResTotalDRes, monthResTotalTSum ,monthResTotalTCards,
                                        monthResTotalTFact ,monthResTotalDEnd, monthResTotalDCount,
                                        monthResTotalHdsDStart,monthResTotalHdsDEnd );
  }
  //--------------------------------------- MONTHS DIN START END -- BTN CLICK -------------------------------------------------------------
  saveMonthDinStartEndBtnClick(){
    this.copyMonthBankCajaFormArrayToMonthSummRecords();
    this.copyMonthRetFormArrayToMonthSummRecords();
    this.copyMonthResTotalFormArrayToMonthSummRecords();
    this.updateInDBMonthSummRecords(this.monthSummRecords);   
  }
  closeMonthDinStartModalBtnClick(){
    if (this.bsModalRef.content.callback != null){
      //this.bsModalRef.content.callback(null);
      this.result.next(null);
      this.bsModalRef.hide();  
    }
  }
  //--------------------------------------- MONTHS DIN START END -- MONTHS DATABASE --------------------------------------------------------
  updateInDBMonthSummRecords(updatedMonthSummRecords: Month[]): void {
    this.globalVar.consoleLog(this.logToConsole, '-> MONTHs DIN IN OUT -> updateInDBMonthSummRecords ->...Starting...->', null);
    this._monthsService.updateInDBMonthSummRecords(updatedMonthSummRecords)
      .subscribe(data => { this.monthSummRecords = new Array();
                           this.monthSummRecords = data;
                           //this.bsModalRef.content.callback(this.monthSummRecords);
                           this.result.next(this.monthSummRecords);
                           this.bsModalRef.hide();  
                         },
                error => { if (this.globalVar.handleError(error)) { throwError(error) }; });
  }

}