import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { formatDate} from '@angular/common';
import { forkJoin, throwError } from 'rxjs';

import { OrderPipe } from 'ngx-order-pipe';
import { BsModalService } from 'ngx-bootstrap/modal';

import { GbViewsService } from '../dbc-gbviews/gbviews.service';
import { GlobalVarService } from '../aa-common/global-var.service';
import { ValueRecord } from '../dbc-fields/field';
import { SelectMonthComponent } from '../aa-common/select-month.component';

import { ErrorService } from '../aa-errors/error.service';

import { Month,MonthTpv,MonthSummary,MonthCardsSummary } from './months';
import { MonthsHdsComponent } from './months-hds.component'; 
import { MonthsCardsComponent } from './months-cards.component'; 
import { MonthsDinStartComponent } from './months-dinstart.component'; 
import { MonthsDinoutComponent } from './months-dinout.component'; 
import { MonthsSummComponent } from './months-summ.component';
import { MonthsService } from './months.service';

import { Hd,HdType,HdTpv } from '../dbx-hd/hd'; 
import { HdService } from '../dbx-hd/hd.service';

import { Fact } from '../dbx-facts/facts';
import { FactsHdComponent } from '../dbx-facts/facts-hd.component';
import { FactsService } from '../dbx-facts/facts.service';

import { CardPayout, CardTpv, CardTotal } from '../dbx-cards/cards';
import { CardsService } from '../dbx-cards/cards.service';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Component({
  selector      :'app-months',
  templateUrl   :'./months.component.html',
  styleUrls     :['./months.component.css']
})
export class MonthsComponent implements OnInit {
  public formTitles = {     
    'monthHeader'         :'Resumen Mes',
    'monthChange'         :'Cambiar Mes',
    'monthSummYear'       :'Año',
    'monthSummYearString' :'',
    'monthListMonth'      :'Mes ',
    'monthEditMonthString':'',
    'monthListStart'      :'',
    'monthListEnd'        :'',
    'monthEdit'           :'Modificar',
    'Edit'                :'Editar',
    'Delete'              :'Borrar',
    'monthSummChangeYear' :'Cambiar AÑO Resumenes Mensuales',
    'monthSummSelectMonth':'Seleccionar MES Crear Resumen Mensual',
    'monthSummCreateOne'  : '¿ Seguro que quiere CREAR el Resument de este MES?',
    'monthSummDeleteOne'  : '¿ Seguro que quiere BORRAR el Resumen de este MES ?',
    'monthSummDeleteAll'  : '¿ Seguro que quiere BORRAR los Resúmenes de TODOS los MESES ?',
  };
  public formLabels = { 
    'id'                :'#####',
    'monthTpvName'      :'Caja',  
    'monthTpvTickets'   :'Tickets',
    'monthTpvTicket'    :'Ticket',
    'monthTpvMoney'     :'Dinero',
    'monthTpvDiff'      :'Dif.Din-Ticket',
    'monthConcept'      :'Concepto',
    'monthSuper'        :'Super',
    'monthIncome'       :'Ingresos',
    'monthExpenses'     :'Gastos',
    'monthCards'        :'Tarjetas',
    'monthBankCaja'     :'Bancos/Caja',
    'monthRet'          :'Restan',
    'monthFactsProv'    :'Proveedor',
    'monthFactsType'    :'Tipo Fact',    
    'monthRetName'      :'RETIRADO',
    'monthBCName'       :'BANCO/CAJA',
    'monthResDStart'    :'D.Inicio',
    'monthResTSum'      :'T.Sumas',
    'monthResTFact'     :'T.Fact',
    'monthResDEnd'      :'D.Fin',
    'monthResDCont'     :'D.Contado',
    'monthBankCajaDSum' :'Ingresado',
    'monthBankCajaDRes' :'Retirado',
    'monthResDSum'      :'Entregado',
    'monthResDRes'      :'Devuelto',
    'monthResDDif'      :'Dif.Ingr-Ret',
    'monthResTotalDDif' :'D.Contado-D.Fin',
    'monthSummDate'     :'Fecha', 
    'monthSummMonth'    :'Mes', 
    'monthSummIncomes'  :'Ingresos',  
    'monthSummDif'      :'Super',    
    'monthSummTpvs'     :'Tpvs', 
    'monthSummDin'      :'Venta',     
    'monthSummMaqB'     :'Maq.Traga',
    'monthSummMaqA'     :'Maq.Otras', 
    'monthSummTab'      :'Tabaco', 
    'monthSummGoods'    :'Genero', 
    'monthSummOther'    :'Otros', 
    'monthSummFacts'    :'Gastos', 
    'monthSummFactsHd'  :'Facts Hd', 
    'monthSummStatus'   :'Estado',
    'monthCardTpvId'    :'Tpv.Id',
    'monthCardName'     :'Tarjeta',
    'monthCardBank'     :'Banco',
    'monthCardPrevNot'  :'Ant.NO.Tot,',
    'monthCardPayNot'   :'Mes.NO.Tot.',
    'monthCardPayTot'   :'Mes.Totaliz.',
    'monthCardHds'      :'Mes.Suma',
    'monthCardTotMoney' :'Din.Totaliz.',
    'monthCardTotExp'   :'Gastos.Totaliz.',
    'monthCardTotBank'  :'Din.Banco',
  };
  public  totalDifDinMonthRESString :string;
  public  monthsSummTotalString     :string;
  public  totalDinYear              :number;
  public  showMonthsSummList        :boolean;
  public  showEditMonth             :boolean;
  public  showEditMonthsRESDStartDCount  :boolean;
  public  showEditMonthsRESDSumDRes :boolean;
  public  showSearchMonthSumm       :boolean;
  private resTotalDStart            :number;
  private resTotalDSum              :number;
  private resTotalDRes              :number;
  private resTotalTSum              :number;
  private resTotalTCards            :number;
  private resTotalTFact             :number;
  private resTotalDEnd              :number;
  private resTotalDCount            :number;
  private resTotalDDif              :number;
  private resTotalHdsDStart         :number;
  private resTotalHdsDEnd           :number;
  private resTotalSupTeor           :number;
  private resTotalCardsPrevNot      :number;
  private resTotalCardsNot          :number;
  private resTotalCardsAllNoTot     :number;

  private workingDate                     :Date;
  private workingFirstAndLastDayOfMonth   :Date[];
  private workingFirstAndLastDayOfYear    :Date[];
  private monthsYearList            :Month[];
  private foundMonthsYearList       :Month[];
  public  monthsSummYearList        :MonthSummary[];
  public  searchMonthSumm           :MonthSummary;

  private hdTypesMonth            :HdType[];
  private factsMonth              :Fact[];
  private cardTpvsMonth           :CardTpv[];
  private cardPayoutsMonth        :CardPayout[];
  private cardPayoutsMonthPrev    :CardPayout[];
  private cardPayoutsAllNoTot       :CardPayout[];
  private cardTotalsMonth         :CardTotal[];
  private hdTpvsMonth             :HdTpv[];
  private hdsMonth                :Hd[];
  private retCountPrevMonthSummRecords    :Month[];
  private retStartCountMonthSummRecords   :Month[];
  public  monthTpvSummRecords     :MonthTpv[];
  public  monthSummRecords        :Month[]; 

  private monthHdNamesBankCaja    :ValueRecord[];
  private monthHdNamesRet         :ValueRecord[];

  public  wMonthFactsProvs            :Month[];
  public  monthIndexFactsProvs        :number;
  public  wMonthFactsProvsSum         :Month[];
  public  monthIndexFactsProvsSum     :number;
  public  wMonthFactsPayTypes         :Month[];
  public  monthIndexFactsPayTypes     :number;
  public  wMonthFactsPayTypesSum      :Month[];
  public  monthIndexFactsPayTypesSum  :number;

  public  wMonthCardPayoutsTpv        :Month[];
  public  monthIndexCardPayouts       :number;
  public  wMonthCardPayoutsSum        :Month[];
  public  monthIndexCardPayoutsSum    :number;

  public  wMonthHdsDStart    :Month[];
  public  monthIndexDStart   :number;
  public  wMonthHdsDEnd      :Month[];
  public  monthIndexDEnd     :number;
  public  wMonthHdsDCont     :Month[];
  public  monthIndexDCont    :number;
  public  wMonthHdsDDif      :Month[];
  public  monthIndexDDif     :number;
  public  wMonthHdsDayT      :Month[];
  public  monthIndexDayT     :number;
  public  wMonthHdsDayD      :Month[];
  public  monthIndexDayD     :number;
  public  wMonthHdsDayF      :Month[];
  public  monthIndexDayF     :number;
  public  wMonthHdsFact      :Month[];
  public  monthIndexFact     :number;
  public  wMonthHdsCards     :Month[];
  public  monthIndexCards    :number;
  public  wMonthHdsMaqA      :Month[];
  public  monthIndexMaqA     :number;
  public  wMonthHdsMaqB      :Month[];
  public  monthIndexMaqB     :number;
  public  wMonthHdsTab       :Month[];
  public  monthIndexTab      :number;
  public  wMonthHdsGoods     :Month[];
  public  monthIndexGoods    :number;
  public  wMonthHdsOther     :Month[];
  public  monthIndexOther    :number;

  public  wMonthHdsMaqASum   :Month[];
  public  monthIndexMaqASum  :number;
  public  wMonthHdsMaqBSum   :Month[];
  public  monthIndexMaqBSum  :number;
  public  wMonthHdsTabSum    :Month[];
  public  monthIndexTabSum   :number;
  public  wMonthHdsGoodsSum  :Month[];
  public  monthIndexGoodsSum :number;
  public  wMonthHdsOtherSum  :Month[];
  public  monthIndexOtherSum :number;
  public  wMonthHdsIngrTot   :Month[];
  public  monthIndexIngrTot  :number;
  public  wMonthSupTeor      :Month[];
  public  monthIndexSupTeor  :number;
  public  wMonthSupDin       :Month[];
  public  monthIndexSupDin   :number;
  public  wMonthSupDif       :Month[];
  public  monthIndexSupDif    :number;
  public  wMonthHdsFactSum   :Month[];
  public  monthIndexFactSum  :number;

  public  wMonthHdsResBankCajaSum   :Month[];
  public  monthIndexResBankCajaSum  :number;
  public  wMonthHdsResRetSum        :Month[];
  public  monthIndexResRetSum       :number;
  public  monthCardsSummary         :MonthCardsSummary[];
  public  showMonthCardsSummary     :boolean;

  private orderMonthSummList: string;
  private reverseMonthSummList: boolean;
  public  caseInsensitive   :boolean;
  public  sortedCollection  :any[];
  private logToConsole      :boolean;
  //--------------------------------------- MONTHS -- CONSTRUCTOR -------------------------------------------------------------------------
  constructor ( private orderPipe: OrderPipe, private modalService: BsModalService, 
                public globalVar: GlobalVarService, private _errorService: ErrorService, private _monthsService: MonthsService, 
                private _hdService: HdService, private _factsService: FactsService, private _cardsService: CardsService ) { }
  //--------------------------------------- MONTHS -- NG ON INIT -------------------------------------------------------------------------
  ngOnInit(): void {
    this.logToConsole = this.globalVar.logToConsole;
    this.logToConsole = true;
    this.showMonthCardsSummary = false;
    this.getMonthSummData();
  }
  //--------------------------------------- MONTHS -- GENERAL -------------------------------------------------------------------------
  public getClass(monthTpvType){
    var classList='';
    if (monthTpvType === 'TPV1T' || monthTpvType === 'TPV2T' || monthTpvType === 'TPV3T') {
       classList = 'table-primary text-dark font-weight-bold'; 
    }else if (monthTpvType === 'TPVT'){
        classList = 'bg-highlight text-dark font-weight-bold';
    }
    return classList;
  }
  public getArrow(monthTpvType){
    var classList='';
    if (monthTpvType === 'TPV1T' || monthTpvType === 'TPV2T' || monthTpvType === 'TPV3T' || monthTpvType === 'TPVT') {
       classList = 'fas fa-long-arrow-alt-right'; 
    }
    return classList;
  }
  //--------------------------------------- MONTHS -- FORMS -------------------------------------------------------------------------
  //--------------------------------------- MONTHS -- FORMS ---- VIEWS --createMonthSummViewArrays ----------------------------------
  private createMonthSummViewArrays(){
    this.globalVar.consoleLog(this.logToConsole,'-> MONTHs -> createMonthSummViewArrays ->...Starting...->', null);      
    this.wMonthFactsProvs           = new Array();
    this.monthIndexFactsProvs       = 0;
    this.wMonthFactsProvsSum        = new Array();
    this.monthIndexFactsProvsSum    = 0;
    this.wMonthFactsPayTypes        = new Array();
    this.monthIndexFactsPayTypes    = 0;
    this.wMonthFactsPayTypesSum     = new Array();
    this.monthIndexFactsPayTypesSum = 0;

    this.wMonthCardPayoutsTpv       = new Array()
    this.monthIndexCardPayouts      = 0;
    this.wMonthCardPayoutsSum       = new Array()
    this.monthIndexCardPayoutsSum   = 0;

    this.wMonthHdsIngrTot    = new Array();
    this.monthIndexIngrTot   = 0;
    this.wMonthSupTeor       = new Array();
    this.monthIndexSupTeor   = 0;
    this.wMonthSupDin        = new Array();
    this.monthIndexSupDin    = 0;
    this.wMonthSupDif        = new Array();
    this.monthIndexSupDif    = 0;
    this.wMonthHdsMaqASum    = new Array();
    this.monthIndexMaqASum   = 0;
    this.wMonthHdsMaqBSum    = new Array();
    this.monthIndexMaqBSum   = 0;
    this.wMonthHdsTabSum     = new Array();
    this.monthIndexTabSum    = 0;
    this.wMonthHdsGoodsSum   = new Array();
    this.monthIndexGoodsSum  = 0;
    this.wMonthHdsOtherSum   = new Array();
    this.monthIndexOtherSum  = 0;

    this.wMonthHdsFactSum = new Array();
    this.monthIndexFactSum= 0;

    this.wMonthHdsDStart  = new Array();
    this.monthIndexDStart = 0;
    this.wMonthHdsDEnd    = new Array();
    this.monthIndexDEnd   = 0;
    this.wMonthHdsDCont   = new Array();
    this.monthIndexDCont  = 0;
    this.wMonthHdsDDif    = new Array();
    this.monthIndexDDif   = 0;
    this.wMonthHdsDayT    = new Array();
    this.monthIndexDayT   = 0;
    this.wMonthHdsDayD    = new Array();
    this.monthIndexDayD   = 0;
    this.wMonthHdsDayF    = new Array();
    this.monthIndexDayF   = 0;
    this.wMonthHdsFact    = new Array();
    this.monthIndexFact   = 0;
    this.wMonthHdsCards   = new Array();
    this.monthIndexCards  = 0;
    this.wMonthHdsMaqA    = new Array();
    this.monthIndexMaqA   = 0;
    this.wMonthHdsMaqB    = new Array();
    this.monthIndexMaqB   = 0;
    this.wMonthHdsTab     = new Array();
    this.monthIndexTab    = 0;
    this.wMonthHdsGoods   = new Array();
    this.monthIndexGoods  = 0;
    this.wMonthHdsOther   = new Array();
    this.monthIndexOther  = 0;

    this.wMonthHdsResBankCajaSum  = new Array();
    this.monthIndexResBankCajaSum = 0;
    this.wMonthHdsResRetSum       = new Array();
    this.monthIndexResRetSum      = 0;

    for (let i = 0 ; i < this.monthSummRecords.length ; i++ ) {
      switch (this.monthSummRecords[i].monthType){  
        /*----------------------------------------------------------------------- FACTS -> PROVS + SUM -> PAYTYPES + SUM **/
        case 'FACTPROV':      { this.wMonthFactsProvs[this.monthIndexFactsProvs]              = this.monthSummRecords[i];
                                this.monthIndexFactsProvs = this.monthIndexFactsProvs + 1;
                                break; }
        case 'TFACTPROV':     { this.wMonthFactsProvsSum[this.monthIndexFactsProvsSum]        = this.monthSummRecords[i];
                                this.monthIndexFactsProvsSum = this.monthIndexFactsProvsSum + 1;
                                break; }
        case 'FACTPAYTYPE':   { this.wMonthFactsPayTypes[this.monthIndexFactsPayTypes]        = this.monthSummRecords[i];
                                this.monthIndexFactsPayTypes = this.monthIndexFactsPayTypes + 1;
                                break; }
        case 'TFACTPAYTYPE':  { this.wMonthFactsPayTypesSum[this.monthIndexFactsPayTypesSum]  = this.monthSummRecords[i];
                                this.monthIndexFactsPayTypesSum = this.monthIndexFactsPayTypesSum + 1;
                                break; }
        case 'CARDSTPV':      { this.wMonthCardPayoutsTpv[this.monthIndexCardPayouts]         = this.monthSummRecords[i];
                                this.monthIndexCardPayouts = this.monthIndexCardPayouts + 1;
                                break; }
        case 'TCARDSTPV':     { this.wMonthCardPayoutsSum[this.monthIndexCardPayoutsSum]      = this.monthSummRecords[i];
                                this.monthIndexCardPayoutsSum = this.monthIndexCardPayoutsSum + 1;
                                break; }
        /*--------------------------------------------------------- TOTALS , just one value adding all values */      
        case 'TDIN':      { this.wMonthHdsIngrTot[this.monthIndexIngrTot]     = this.monthSummRecords[i];
                            this.monthIndexIngrTot = this.monthIndexIngrTot + 1;
                            break; }
        case 'SUP-TEOR':  { this.wMonthSupTeor[this.monthIndexSupTeor]         = this.monthSummRecords[i];
                            this.monthIndexSupTeor = this.monthIndexSupTeor + 1;
                            break; }     
        case 'SUP-DIN':   { this.wMonthSupDin[this.monthIndexSupDin]         = this.monthSummRecords[i];
                              this.monthIndexSupDin = this.monthIndexSupDin + 1;
                              break; } 
        case 'SUP-DIF':   { this.wMonthSupDif[this.monthIndexSupDif]         = this.monthSummRecords[i];
                                this.monthIndexSupDif = this.monthIndexSupDif + 1;
                                break; } 
        case 'THD-MAQB':  { this.wMonthHdsMaqBSum[this.monthIndexMaqBSum]     = this.monthSummRecords[i];
                            this.monthIndexMaqBSum = this.monthIndexMaqBSum + 1;
                            break; }
        case 'THD-MAQA':  { this.wMonthHdsMaqASum[this.monthIndexMaqASum]     = this.monthSummRecords[i];
                            this.monthIndexMaqASum = this.monthIndexMaqASum + 1;
                            break; } 
        case 'THD-TAB':   { this.wMonthHdsTabSum[this.monthIndexTabSum]       = this.monthSummRecords[i];
                            this.monthIndexTabSum = this.monthIndexTabSum + 1;
                            break; } 
        case 'THD-GOODS': { this.wMonthHdsGoodsSum[this.monthIndexGoodsSum]   = this.monthSummRecords[i];
                            this.monthIndexGoodsSum = this.monthIndexGoodsSum + 1;
                            break; } 
        case 'THD-OTHER': { this.wMonthHdsOtherSum[this.monthIndexOtherSum]   = this.monthSummRecords[i];          
                            this.monthIndexOtherSum = this.monthIndexOtherSum + 1;
                            break; } 
        case 'THD-FACT':  { this.wMonthHdsFactSum[this.monthIndexFactSum]     = this.monthSummRecords[i];
                            this.monthIndexFactSum = this.monthIndexFactSum + 1;
                            break; }
        /*---------------------------------------------------------------------- FIX , just one value each*/
        case 'DSTART':  { this.wMonthHdsDStart[this.monthIndexDStart]   = this.monthSummRecords[i];
                          this.monthIndexDStart = this.monthIndexDStart + 1;
                          break; }
        case 'DEND':    { this.wMonthHdsDEnd[this.monthIndexDEnd]       = this.monthSummRecords[i];
                          this.monthIndexDEnd = this.monthIndexDEnd + 1;         
                          break; }
        case 'DCONT':   { this.wMonthHdsDCont[this.monthIndexDCont]     = this.monthSummRecords[i];
                          this.monthIndexDCont = this.monthIndexDCont + 1;
                          break; }
        case 'DDIF':    { this.wMonthHdsDDif[this.monthIndexDDif]       = this.monthSummRecords[i];
                          this.wMonthHdsDDif[this.monthIndexDDif].monthAmount = this.wMonthHdsDDif[0].monthAmount * (-1);
                          this.monthIndexDDif = this.monthIndexDDif + 1;
                          break; }
        case 'DAY-T':   { this.wMonthHdsDayT[this.monthIndexDayT]       = this.monthSummRecords[i];
                          this.monthIndexDayT = this.monthIndexDayT + 1;
                          break; }
        case 'DAY-D':   { this.wMonthHdsDayD[this.monthIndexDayD]       = this.monthSummRecords[i];
                          this.monthIndexDayD = this.monthIndexDayD + 1;
                          break; }
        case 'DAY-F':   { this.wMonthHdsDayF[this.monthIndexDayF]       = this.monthSummRecords[i];
                          this.monthIndexDayF = this.monthIndexDayF + 1;
                          break; }
        case 'FACT':    { this.wMonthHdsFact[this.monthIndexFact]       = this.monthSummRecords[i];
                          this.monthIndexFact = this.monthIndexFact + 1;
                          break; }
        case 'CARDS':   { this.wMonthHdsCards[this.monthIndexCards]     = this.monthSummRecords[i];
                          this.monthIndexCards = this.monthIndexCards + 1;
                          break; }                                       
        /* -------------------------------------------------SUM -- , several values each, defined within application */
        case 'MAQA':    { this.wMonthHdsMaqA[this.monthIndexMaqA]   = this.monthSummRecords[i];
                          this.monthIndexMaqA = this.monthIndexMaqA + 1;
                          break; }
        case 'MAQB':    { this.wMonthHdsMaqB[this.monthIndexMaqB]   = this.monthSummRecords[i];
                          this.monthIndexMaqB = this.monthIndexMaqB + 1;
                          break; }
        case 'TAB':     { this.wMonthHdsTab[this.monthIndexTab]     = this.monthSummRecords[i];
                          this.monthIndexTab = this.monthIndexTab + 1;
                          break; }
        case 'GOODS':   { this.wMonthHdsGoods[this.monthIndexGoods] = this.monthSummRecords[i];
                          this.monthIndexGoods = this.monthIndexGoods + 1;
                          break; }                                                      
        case 'OTHER':   { this.wMonthHdsOther[this.monthIndexOther] = this.monthSummRecords[i];
                          this.monthIndexOther = this.monthIndexOther + 1;
                          break; }   
        /*-------------------------------------------------RES --  several values each, defined within application */
        case 'RRT-TSUM':
        case 'RRT-DSUM':
        case 'RRT-DRES':{ this.wMonthHdsResRetSum[this.monthIndexResRetSum] = this.monthSummRecords[i];
                          this.monthIndexResRetSum = this.monthIndexResRetSum + 1;
                          break; }
        case 'RBC-TSUM':
        case 'RBC-DSUM':
        case 'RBC-DRES':{ this.wMonthHdsResBankCajaSum[this.monthIndexResBankCajaSum] = this.monthSummRecords[i];
                          this.monthIndexResBankCajaSum = this.monthIndexResBankCajaSum + 1;
                          break; }
        default:          break;
      }      
    }
    this.sortedCollection = this.orderPipe.transform(this.wMonthFactsProvs,'monthAmount',true,false); 
    this.wMonthFactsProvs = this.sortedCollection;
    this.sortedCollection = this.orderPipe.transform(this.wMonthFactsPayTypes,'monthName',true,false);
    this.wMonthFactsPayTypes = this.sortedCollection;
    this.sortedCollection = this.orderPipe.transform(this.wMonthCardPayoutsTpv,'monthName',true,false);
    this.wMonthCardPayoutsTpv = this.sortedCollection;
    // remove bank caja data with value 0
    var localResBankCajaSum :Month[];
    localResBankCajaSum = this.wMonthHdsResBankCajaSum;
    this.wMonthHdsResBankCajaSum = new Array();
    for(let k=0;k<localResBankCajaSum.length;k++){
      if (localResBankCajaSum[k].monthAmount !=0) this.wMonthHdsResBankCajaSum.push(localResBankCajaSum[k]);
    }
    // remove ret data with value 0
    var localResRetSum :Month[];
    localResRetSum = this.wMonthHdsResRetSum;
    this.wMonthHdsResRetSum = new Array();
    for(let k=0;k<localResRetSum.length;k++){
      if (localResRetSum[k].monthAmount !=0) this.wMonthHdsResRetSum.push(localResRetSum[k]);
    }
  }  
  //--------------------------------------- MONTHS -- GENERAL -------------------------------------------------------------------------
  //--------------------------------------- MONTHS -- GENERAL -- setMonthSummData -----------------------------------------------------
  private getMonthSummData(){
    this.showMonthsSummList = true;
    this.showEditMonth = false;
    this.showEditMonthsRESDStartDCount = false;
    this.showEditMonthsRESDSumDRes = false;
    this.showSearchMonthSumm = false;
    this.searchMonthSumm = new MonthSummary();
    this.monthsSummTotalString = 'Total',
    this.totalDifDinMonthRESString = 'Total Dif ->  '
    this.changeDatesData(this.globalVar.workingDate);
    this.monthSummRecords = new Array();
    this.getYearMonthSummRecords(this.workingFirstAndLastDayOfYear);
  }
  private changeDatesData(changedDate: Date){
    this.globalVar.changeWorkingDates(changedDate);
    this.workingDate = changedDate;
    this.workingFirstAndLastDayOfMonth = new Array();
    this.workingFirstAndLastDayOfMonth[0] = new Date(this.globalVar.workingFirstDayOfMonth);
    this.workingFirstAndLastDayOfMonth[1] = new Date(this.globalVar.workingLastDayOfMonth); 
    this.workingFirstAndLastDayOfYear  =  new Array();
    this.workingFirstAndLastDayOfYear[0] =  new Date(this.globalVar.workingFirstDayOfYear);
    this.workingFirstAndLastDayOfYear[1] =  new Date(this.globalVar.workingLastDayOfYear);
    this.formTitles.monthEditMonthString = this.workingDate.toLocaleString('default',{month:'long'});
    let workingYear = this.workingDate.getFullYear();
    this.formTitles.monthSummYearString = workingYear.toString();
    this.formTitles.monthListStart = this.workingFirstAndLastDayOfMonth[0].toLocaleDateString(); 
    this.formTitles.monthListEnd = this.workingFirstAndLastDayOfMonth[1].toLocaleDateString();
  }    
  private prepareMonthSummList(){
    this.globalVar.consoleLog(this.logToConsole, '-> MONTHs -> prepareMonthSummList ->...Starting...->', null);
    this.monthsSummYearList = new Array();
    if (this.monthsYearList === null) return;
    if (this.monthsYearList.length < 1) return;
    this.foundMonthsYearList = new Array();
    this.foundMonthsYearList = this.monthsYearList.filter(item => item.monthType === 'TDIN');
    if (this.foundMonthsYearList === null) return;
    if (this.foundMonthsYearList.length < 1) return;
    this.totalDinYear = 0;
    var index = -1;
    var indexMonthsSumm = 0;    
    for (let i=0; i<this.foundMonthsYearList.length; i++){
      index = this.monthsYearList.findIndex(item => item.monthDate === this.foundMonthsYearList[i].monthDate && item.monthType === 'TDIN');
      if (index != -1) {
        this.monthsSummYearList[i] = new MonthSummary();
        var monthDate = new Date(this.monthsYearList[index].monthDate);
        this.monthsSummYearList[indexMonthsSumm].monthSummMonth = monthDate.toLocaleString('default',{month:'long'});
        this.monthsSummYearList[indexMonthsSumm].monthSummIncomes = this.monthsYearList[index].monthAmount;
        this.monthsSummYearList[indexMonthsSumm].monthSummDate = this.monthsYearList[index].monthDate;
        this.monthsSummYearList[indexMonthsSumm].monthSummStatus = this.monthsYearList[index].monthStatus;
        index = this.monthsYearList.findIndex(item => item.monthDate === this.foundMonthsYearList[i].monthDate && item.monthType === 'DAY-D');
        if (index != -1) {
          this.monthsSummYearList[indexMonthsSumm].monthSummDin = this.monthsYearList[index].monthAmount;
          this.totalDinYear = this.totalDinYear + this.monthsSummYearList[indexMonthsSumm].monthSummDin;
        }
        index = this.monthsYearList.findIndex(item => item.monthDate === this.foundMonthsYearList[i].monthDate && item.monthType === 'TFACTPAYTYPE');
        if (index != -1)  
          this.monthsSummYearList[indexMonthsSumm].monthSummFacts = this.monthsYearList[index].monthAmount;
        index = this.monthsYearList.findIndex(item => item.monthDate === this.foundMonthsYearList[i].monthDate && item.monthType === 'SUP-DIN');
        if (index != -1) 
          this.monthsSummYearList[indexMonthsSumm].monthSummDif = this.monthsYearList[index].monthAmount;
        index = this.monthsYearList.findIndex(item => item.monthDate === this.foundMonthsYearList[i].monthDate && item.monthType === 'DAY-T');
        if (index != -1) 
          this.monthsSummYearList[indexMonthsSumm].monthSummTpvs = this.monthsYearList[index].monthAmount;
        index = this.monthsYearList.findIndex(item => item.monthDate === this.foundMonthsYearList[i].monthDate && item.monthType === 'THD-MAQB');
        if (index != -1) 
          this.monthsSummYearList[indexMonthsSumm].monthSummMaqB = this.monthsYearList[index].monthAmount;
        index = this.monthsYearList.findIndex(item => item.monthDate === this.foundMonthsYearList[i].monthDate && item.monthType === 'THD-MAQA');
        if (index != -1) 
          this.monthsSummYearList[indexMonthsSumm].monthSummMaqA = this.monthsYearList[index].monthAmount;
        index = this.monthsYearList.findIndex(item => item.monthDate === this.foundMonthsYearList[i].monthDate && item.monthType === 'THD-TAB');
        if (index != -1) 
          this.monthsSummYearList[indexMonthsSumm].monthSummTab = this.monthsYearList[index].monthAmount;
        index = this.monthsYearList.findIndex(item => item.monthDate === this.foundMonthsYearList[i].monthDate && item.monthType === 'THD-GOODS');
        if (index != -1) 
          this.monthsSummYearList[indexMonthsSumm].monthSummGoods = this.monthsYearList[index].monthAmount;
        index = this.monthsYearList.findIndex(item => item.monthDate === this.foundMonthsYearList[i].monthDate && item.monthType === 'THD-OTHER');
        if (index != -1) 
          this.monthsSummYearList[indexMonthsSumm].monthSummOther = this.monthsYearList[index].monthAmount;
        index = this.monthsYearList.findIndex(item => item.monthDate === this.foundMonthsYearList[i].monthDate && item.monthType === 'THD-FACT');
        if (index != -1) 
          this.monthsSummYearList[indexMonthsSumm].monthSummFactsHd = this.monthsYearList[index].monthAmount;
        index = this.monthsYearList.findIndex(item => item.monthDate === this.foundMonthsYearList[i].monthDate && item.monthType === 'TCARDS-TPV');
        if (index != -1) 
          this.monthsSummYearList[indexMonthsSumm].monthSummCards = this.monthsYearList[index].monthAmount;
        indexMonthsSumm = indexMonthsSumm + 1;
      }
    }
    this.sortedCollection = this.orderPipe.transform(this.monthsSummYearList,'monthSummDate',false,false); 
    this.monthsSummYearList = this.sortedCollection;
    this.monthsSummTotalString = this.monthsSummYearList.length.toString()+"..Meses..Total.Venta->"+this.totalDinYear.toFixed(2)+
                              "..Mes.Venta->"+(this.totalDinYear/this.monthsSummYearList.length).toFixed(2);
  }
  private createMonthSummRecords(createMonthsFlag:boolean,firstAndLastDayOfMonth: Date[]){
    this.createFactsMSRs(firstAndLastDayOfMonth[0]);
    this.createCardPayoutsPrevMSRs(firstAndLastDayOfMonth[0]);
    this.createCardPayoutsAllNoTotMSRs(firstAndLastDayOfMonth[0]);
    this.createCardPayoutsMSRs(firstAndLastDayOfMonth[0]);
    this.createCardTotalsMSRs(firstAndLastDayOfMonth[0]);
    this.createHdTpvsMTpvSRs(firstAndLastDayOfMonth[0]);
    this.createHdsMSRs(firstAndLastDayOfMonth[0]);   
    this.createFixMSRs(firstAndLastDayOfMonth[0]); 
    // Mixing monthHdNamesRES with
    this.mixNamesRESHdTypes();
    this.createBankCajaMSRs(createMonthsFlag,firstAndLastDayOfMonth[0]);
    this.createRetMSRs(createMonthsFlag,firstAndLastDayOfMonth[0]);
    this.createResTotalMSRs(firstAndLastDayOfMonth[0]);

    this.saveInDBMonthSummRecords(createMonthsFlag,this.monthSummRecords,this.monthTpvSummRecords);
  }  
  //--------------------------------------- MONTHS -- GENERAL -- prepareHdTypesMonthVRList ----------------------------------------------------
  private prepareHdTypesMonthVRList() { 
    this.globalVar.consoleLog(this.logToConsole, '-> MONTHs -> prepareHdTypesMonthVRList ->...Starting...->', null); 
    if (this.hdTypesMonth === null) return;  
    if (this.hdTypesMonth.length < 1) return;
    this.sortedCollection = this.orderPipe.transform(this.hdTypesMonth,'hdTypeName',false,false);
    this.hdTypesMonth = this.sortedCollection;   
    this.monthHdNamesRet = new Array();
    this.monthHdNamesBankCaja = new Array();
    let indexRet = 0;
    let indexBankCaja = 0;
    for (let i = 0 ; i < this.hdTypesMonth.length ; i++ ) {
      if (this.hdTypesMonth[i].hdTypeType =='RET') {       
        this.monthHdNamesRet[indexRet] = new ValueRecord();
        this.monthHdNamesRet[indexRet].id = indexRet;
        this.monthHdNamesRet[indexRet].value = this.hdTypesMonth[i].hdTypeName;
        indexRet = indexRet + 1;
      }
      if ( (this.hdTypesMonth[i].hdTypeType =='BANK') || (this.hdTypesMonth[i].hdTypeType =='CAJA') ){       
        this.monthHdNamesBankCaja[indexBankCaja] = new ValueRecord();
        this.monthHdNamesBankCaja[indexBankCaja].id = indexBankCaja;
        this.monthHdNamesBankCaja[indexBankCaja].value = this.hdTypesMonth[i].hdTypeName;
        indexBankCaja = indexBankCaja + 1;
      }
    }
  }   
  //--------------------------------------- MONTHS -- GENERAL -- mixNamesRESHdTypes ----------------------------------------------------
  private mixNamesRESHdTypes(){
    for (let i = 0 ; i < this.monthSummRecords.length ; i++ ) {
      if (this.monthSummRecords[i].monthType === 'RET') { 
        let indexHdsRet = this.monthHdNamesRet.findIndex(item => item.value === this.monthSummRecords[i].monthName);
        if (indexHdsRet === -1) { // not found
          let index = this.monthHdNamesRet.length;
          this.monthHdNamesRet[index] = new ValueRecord();
          this.monthHdNamesRet[index].id = index;
          this.monthHdNamesRet[index].value = this.monthSummRecords[i].monthName;
        } 
      }
      if ( (this.monthSummRecords[i].monthType === 'BANK') || (this.monthSummRecords[i].monthType === 'CAJA') ) { 
        let indexHdsBankCaja = this.monthHdNamesBankCaja.findIndex(item => item.value === this.monthSummRecords[i].monthName);
        if (indexHdsBankCaja === -1) { // not found
          let index = this.monthHdNamesBankCaja.length;
          this.monthHdNamesBankCaja[index] = new ValueRecord();
          this.monthHdNamesBankCaja[index].id = index;
          this.monthHdNamesBankCaja[index].value = this.monthSummRecords[i].monthName;
        } 
      }
    }
  }
  private checkReceivedMonthData(firstAndLastDayOfMonth: Date[]){
    var message3 = 'hdTypes ----------------> NO HAY';
    if (this.hdTypesMonth != null)      message3 = 'hdTypes ----------------> '+this.hdTypesMonth.length;
    var message4 = 'Facturas ---------------> NO HAY';
    if (this.factsMonth != null)        message4 = 'Facturas ---------------> '+this.factsMonth.length;
    var message5 = 'TPVs Tarjetas ----------> NO HAY';
    if (this.cardTpvsMonth != null)     message5 = 'TPVs Tarjetas ----------> '+this.cardTpvsMonth.length;
    var message6 = 'Pagos Tarjetas ---------> NO HAY';
    if (this.cardPayoutsMonth != null)  message6 = 'Pagos Tarjetas ---------> '+this.cardPayoutsMonth.length;
    var message7 = 'Registros Hojas Dia ----> NO HAY';
    if (this.hdsMonth != null)          message7 = 'Registros Hojas Dia ----> '+this.hdsMonth.length;
    var message2 = this.formTitles.monthEditMonthString+'--->'+this.formTitles.monthSummYearString;
    this._errorService.getDeleteMsgConfirmation("infoMsg",this.formTitles.monthHeader,this.formTitles.monthHeader,
                        null,this.formTitles.monthSummCreateOne,message2,message3,message4,message5,message6,message7).
        then((createMonthSummOK:boolean)=>{
          if (createMonthSummOK === true){ this.createMonthSummRecords(true,firstAndLastDayOfMonth); }
        })
  }
  private checkIfExistsMonthSumm(firstAndLastDayOfMonth):boolean{
    this.globalVar.consoleLog(this.logToConsole,'-> MONTHs -> checkIfExistsMonthSumm ->...Starting...->', null);
    var selectedDate = formatDate(firstAndLastDayOfMonth[0], 'yyyy-MM-dd', 'en-US');
    var index = this.monthsSummYearList.findIndex(item => item.monthSummDate.toString() === selectedDate);
    if (index != -1) return true;
    else return false;
  }
  private prepareMonthSummView(){
    this.createMonthSummViewArrays();
    this.showMonthsSummList = false;
    this.showEditMonth = true;
    this.showEditMonthsRESDStartDCount = false;
    this.showSearchMonthSumm = false;
  }
  //--------------------------------------- MONTHS -- GENERAL -- Month Summ Records -- MSR Items -------------------------------------------
  private createMSRItem(itemDate :Date, itemName :string, itemType :string, itemPlace :string, itemAmount :number) :Month {
    var localMSRItem = new Month();
    localMSRItem.monthDate   = itemDate;
    localMSRItem.monthName   = itemName;
    localMSRItem.monthType   = itemType;
    localMSRItem.monthPlace  = itemPlace;
    localMSRItem.monthAmount = itemAmount;
    localMSRItem.monthStatus = true;
    return localMSRItem;
  }
  private checkIfExistMSRItem(MSRItem :Month) :number{
    var itemIndex = -1;
    if (this.monthSummRecords?.length > 0) 
      itemIndex = this.monthSummRecords.findIndex(  item => item.monthDate === MSRItem.monthDate && item.monthName === MSRItem.monthName 
                                                  && item.monthType === MSRItem.monthType  && item.monthPlace === MSRItem.monthPlace)
    return itemIndex;
  }
  private addOrUpdateMSRItem(itemDate :Date, itemName :string, itemType :string, itemPlace :string, itemAmount :number){
    var newMSRItem = this.createMSRItem(itemDate,itemName,itemType,itemPlace,itemAmount);
    var indexNewMSRItem = this.checkIfExistMSRItem(newMSRItem);
    if (indexNewMSRItem === -1) this.monthSummRecords.push(newMSRItem);
    else this.monthSummRecords[indexNewMSRItem].monthAmount = itemAmount;
  }
  private checkIfExistMSRItemNoName(MSRItem :Month) :number{
    var itemIndex = -1;
    if (this.monthSummRecords?.length > 0) 
      itemIndex = this.monthSummRecords.findIndex(  item => item.monthDate === MSRItem.monthDate 
                                                  && item.monthType === MSRItem.monthType  && item.monthPlace === MSRItem.monthPlace)
    return itemIndex;
  }
  private addOrUpdateMSRItemNoName(itemDate :Date, itemName :string, itemType :string, itemPlace :string, itemAmount :number){
    var newMSRItem = this.createMSRItem(itemDate,itemName,itemType,itemPlace,itemAmount);
    var indexNewMSRItem = this.checkIfExistMSRItemNoName(newMSRItem);
    if (indexNewMSRItem === -1) this.monthSummRecords.push(newMSRItem);
    else {  this.monthSummRecords[indexNewMSRItem].monthAmount = itemAmount;
            this.monthSummRecords[indexNewMSRItem].monthName  = itemName;
    }
  }
  //--------------------------------------- MONTHS -- GENERAL -- Month TPV Summ Records -- MTpvSR Items -------------------------------------------
  private createMTpvSRItem(itemDate:Date,itemName:string,itemType:string,itemTickets:number,itemTicket:number,itemMoney:number):MonthTpv {
    var localTpvSRItem = new MonthTpv();
    localTpvSRItem = new MonthTpv();
    localTpvSRItem.monthTpvDate    = itemDate;
    localTpvSRItem.monthTpvName    = itemName;
    localTpvSRItem.monthTpvType    = itemType;
    localTpvSRItem.monthTpvTickets = itemTickets;
    localTpvSRItem.monthTpvTicket  = itemTicket;
    localTpvSRItem.monthTpvMoney   = itemMoney;
    return localTpvSRItem;
  }
  private checkIfExistMTpvSRItem(TpvSRItem :MonthTpv) :number{
    var itemIndex = -1;
    if (this.monthTpvSummRecords?.length > 0) 
      itemIndex = this.monthTpvSummRecords.findIndex( item => item.monthTpvDate === TpvSRItem.monthTpvDate 
                                          && item.monthTpvName === TpvSRItem.monthTpvName  && item.monthTpvType === TpvSRItem.monthTpvType )
    return itemIndex;
  }
  private addOrUpdateMTpvSRItem(itemDate:Date,itemName:string,itemType:string,itemTickets:number,itemTicket:number,itemMoney:number){
    var newTpvSRItem = this.createMTpvSRItem(itemDate,itemName,itemType,itemTickets,itemTicket,itemMoney);
    var indexNewMSRItem = this.checkIfExistMTpvSRItem(newTpvSRItem);
    if (indexNewMSRItem === -1) this.monthTpvSummRecords.push(newTpvSRItem);
    else {
      this.monthTpvSummRecords[indexNewMSRItem].monthTpvTickets = itemTickets;
      this.monthTpvSummRecords[indexNewMSRItem].monthTpvTicket = itemTicket;
      this.monthTpvSummRecords[indexNewMSRItem].monthTpvMoney = itemMoney;
    }
  }
  //--------------------------------------- MONTHS -- GENERAL -- createFactsMSRs ----------------------------------------------------
  private createFactsMSRs(factsDate :Date){
    this.globalVar.consoleLog(this.logToConsole,'-> MONTHs -> createFactsMSRs ->...Starting...->', null);
    if (this.factsMonth === null) return;
    if (this.factsMonth.length < 1) return;  
    this.sortedCollection = this.orderPipe.transform(this.factsMonth,'factProvName',true,true);
    this.factsMonth = this.sortedCollection;
    var provName = this.factsMonth[0].factProvName;
    var indexStartFactsProvs = this.monthSummRecords.length;
    this.addOrUpdateMSRItem(factsDate,provName,'FACTPROV','FACTPROV',0);  
    for (var i=1; i<(this.factsMonth.length); i++) {
      if (this.factsMonth[i].factProvName!=this.factsMonth[i-1].factProvName) {
        provName = this.factsMonth[i].factProvName;
        this.addOrUpdateMSRItem(factsDate,provName,'FACTPROV','FACTPROV',0);  
      } 
    }
    var indexEndFactsProvs = this.monthSummRecords.length;
    var sum = 0;
    var sumFactsTotal = 0;
    for (var k= indexStartFactsProvs; k<indexEndFactsProvs; k++) {
      sum = 0;
      sum = this.factsMonth.filter(item => item.factProvName === this.monthSummRecords[k].monthName).reduce((sum,current) => sum + current.factTotalCost,0)
      this.monthSummRecords[k].monthAmount = sum;
      sumFactsTotal = sumFactsTotal + sum;     
    }
    var qtyProvs = indexEndFactsProvs - indexStartFactsProvs;
    this.addOrUpdateMSRItemNoName(factsDate,qtyProvs.toFixed(0)+'..Pr-->'+this.factsMonth.length.toFixed(0)+'..Facts==>','TFACTPROV','TFACTPROV',sumFactsTotal);  
    this.sortedCollection = this.orderPipe.transform(this.factsMonth,'factPayType',true,true);
    this.factsMonth = this.sortedCollection;
    var indexStartFactsPayType = this.monthSummRecords.length;
    var factPayType = this.factsMonth[0].factPayType;
    this.addOrUpdateMSRItem(factsDate,factPayType,'FACTPAYTYPE','FACTPAYTYPE',0);    
    for (var i=1; i<(this.factsMonth.length); i++) {
      if (this.factsMonth[i].factPayType!=this.factsMonth[i-1].factPayType) {
        factPayType = this.factsMonth[i].factPayType;
        this.addOrUpdateMSRItem(factsDate,factPayType,'FACTPAYTYPE','FACTPAYTYPE',0);      
      } 
    }     
    var indexEndFactsPayType = this.monthSummRecords.length;
    var sum = 0;
    var sumFactsTotal = 0;
    for (var k=indexStartFactsPayType; k<indexEndFactsPayType; k++) {
      sum = 0;
      sum = this.factsMonth.filter(item => item.factPayType === this.monthSummRecords[k].monthName).reduce((sum,current) => sum + current.factTotalCost,0)
      this.monthSummRecords[k].monthAmount = sum;
      sumFactsTotal = sumFactsTotal + sum;
    }
    var qtyPayTypes = indexEndFactsPayType - indexStartFactsPayType
    this.addOrUpdateMSRItemNoName(factsDate,qtyPayTypes.toFixed(0)+'..PayTypes-->'+this.factsMonth.length.toFixed(0)+'..Facts==>','TFACTPAYTYPE','TFACTPAYTYPE',sumFactsTotal);  
  }
  //--------------------------------------- MONTHS -- GENERAL -- createCardPayoutsMSRs --------------------------------
  private createCardPayoutsMSRs(cardsDate:Date){
    this.globalVar.consoleLog(this.logToConsole,'-> MONTHs -> createCardPayoutsMSRs ->...Starting...->', null); 
    this.resTotalCardsNot = 0;
    if (this.cardPayoutsMonth === null)  return;
    if (this.cardPayoutsMonth.length < 1)  return;
    var lastDayMonth = this.workingFirstAndLastDayOfMonth[1];
    // ordering card payouts list based on tpv id
    this.sortedCollection = this.orderPipe.transform(this.cardPayoutsMonth,'cardPayoutTpvId',false,true);
    this.cardPayoutsMonth = this.sortedCollection;
    var sumCardPayoutsTpvAll = 0;
    var sumCardPayoutsTpvTot = 0;
    var sumCardPayoutsTpvNot = 0;
    var numberCardPayoutsTpvAll = 0;
    var numberCardPayoutsTpvTot = 0;
    var numberCardPayoutsTpvNot = 0;
    var sumCardPayoutsAll = 0;
    var sumCardPayoutsTot = 0;
    var sumCardPayoutsNot = 0;
    var numberCardPayoutsAll = 0;
    var numberCardPayoutsTot = 0;
    var numberCardPayoutsNot = 0;
    sumCardPayoutsTpvAll = sumCardPayoutsTpvAll + (this.cardPayoutsMonth[0].cardPayoutTotal * this.cardPayoutsMonth[0].cardPayoutMulti);
    numberCardPayoutsTpvAll = numberCardPayoutsTpvAll + this.cardPayoutsMonth[0].cardPayoutMulti;
    var localCardPayoutTotalDate = new Date(this.cardPayoutsMonth[0].cardPayoutTotalDate);
    if ((this.cardPayoutsMonth[0].cardPayoutTotalId != null) && (localCardPayoutTotalDate <= lastDayMonth) )  {
      sumCardPayoutsTpvTot = sumCardPayoutsTpvTot + (this.cardPayoutsMonth[0].cardPayoutTotal * this.cardPayoutsMonth[0].cardPayoutMulti);
      numberCardPayoutsTpvTot = numberCardPayoutsTpvTot + this.cardPayoutsMonth[0].cardPayoutMulti;
    } else {
      sumCardPayoutsTpvNot = sumCardPayoutsTpvNot + (this.cardPayoutsMonth[0].cardPayoutTotal * this.cardPayoutsMonth[0].cardPayoutMulti);
      numberCardPayoutsTpvNot = numberCardPayoutsTpvNot + this.cardPayoutsMonth[0].cardPayoutMulti;
    } 
    for (var i=1; i<this.cardPayoutsMonth.length; i++) {
      if (this.cardPayoutsMonth[i].cardPayoutTpvId != this.cardPayoutsMonth[i-1].cardPayoutTpvId) {
        var tpvId = this.cardPayoutsMonth[i-1].cardPayoutTpvId;
        var tpvName = this.globalVar.getCardTpvName(tpvId);
        var tpvObject = this.globalVar.getCardTpvObjectFromTpvName(tpvName);
        var tpvBankName = tpvObject.cardTpvBank;
        this.addOrUpdateMSRItem(cardsDate,tpvName,'CARDSTPV',tpvId.toFixed(0),sumCardPayoutsTpvAll);
        this.addOrUpdateMSRItem(cardsDate,tpvName,'CARDSPAYTOT',tpvBankName,sumCardPayoutsTpvTot);
        this.addOrUpdateMSRItem(cardsDate,tpvName,'CARDSPAYNOT',tpvBankName,sumCardPayoutsTpvNot);
        sumCardPayoutsAll = sumCardPayoutsAll + sumCardPayoutsTpvAll;
        sumCardPayoutsTot = sumCardPayoutsTot + sumCardPayoutsTpvTot;
        sumCardPayoutsNot = sumCardPayoutsNot + sumCardPayoutsTpvNot;
        sumCardPayoutsTpvAll = 0;
        sumCardPayoutsTpvTot = 0;
        sumCardPayoutsTpvNot = 0;
        numberCardPayoutsAll = numberCardPayoutsAll + numberCardPayoutsTpvAll;
        numberCardPayoutsTot = numberCardPayoutsTot + numberCardPayoutsTpvTot;
        numberCardPayoutsNot = numberCardPayoutsNot + numberCardPayoutsTpvNot;
        numberCardPayoutsTpvAll = 0;
        numberCardPayoutsTpvTot = 0;
        numberCardPayoutsTpvNot = 0;
      }
      sumCardPayoutsTpvAll = sumCardPayoutsTpvAll + (this.cardPayoutsMonth[i].cardPayoutTotal * this.cardPayoutsMonth[i].cardPayoutMulti);
      numberCardPayoutsTpvAll = numberCardPayoutsTpvAll + this.cardPayoutsMonth[i].cardPayoutMulti;
      localCardPayoutTotalDate = new Date(this.cardPayoutsMonth[i].cardPayoutTotalDate);
      if ((this.cardPayoutsMonth[i].cardPayoutTotalId != null) && (localCardPayoutTotalDate <= lastDayMonth) )  {
        sumCardPayoutsTpvTot = sumCardPayoutsTpvTot + (this.cardPayoutsMonth[i].cardPayoutTotal * this.cardPayoutsMonth[i].cardPayoutMulti);
        numberCardPayoutsTpvTot = numberCardPayoutsTpvTot + this.cardPayoutsMonth[i].cardPayoutMulti;
      } else {
        sumCardPayoutsTpvNot = sumCardPayoutsTpvNot + (this.cardPayoutsMonth[i].cardPayoutTotal * this.cardPayoutsMonth[i].cardPayoutMulti);
        numberCardPayoutsTpvNot = numberCardPayoutsTpvNot + this.cardPayoutsMonth[i].cardPayoutMulti;
      } 
    }
    // create record of last tpv
    var indexLast = this.cardPayoutsMonth.length - 1;
    var tpvId = this.cardPayoutsMonth[indexLast].cardPayoutTpvId;
    var tpvName = this.globalVar.getCardTpvName(tpvId);
    var tpvObject = this.globalVar.getCardTpvObjectFromTpvName(tpvName);
    var tpvBankName = tpvObject.cardTpvBank;
    this.addOrUpdateMSRItem(cardsDate,tpvName,'CARDSTPV',tpvId.toFixed(0),sumCardPayoutsTpvAll);
    this.addOrUpdateMSRItem(cardsDate,tpvName,'CARDSPAYTOT',tpvBankName,sumCardPayoutsTpvTot);
    this.addOrUpdateMSRItem(cardsDate,tpvName,'CARDSPAYNOT',tpvBankName,sumCardPayoutsTpvNot);
    sumCardPayoutsAll = sumCardPayoutsAll + sumCardPayoutsTpvAll;
    sumCardPayoutsTot = sumCardPayoutsTot + sumCardPayoutsTpvTot;
    sumCardPayoutsNot = sumCardPayoutsNot + sumCardPayoutsTpvNot;
    numberCardPayoutsAll = numberCardPayoutsAll + numberCardPayoutsTpvAll;
    numberCardPayoutsTot = numberCardPayoutsTot + numberCardPayoutsTpvTot;
    numberCardPayoutsNot = numberCardPayoutsNot + numberCardPayoutsTpvNot;
    // create records for totals of all tpvs
    this.resTotalCardsNot = sumCardPayoutsNot;
    this.addOrUpdateMSRItemNoName(cardsDate,numberCardPayoutsAll.toFixed(0)+'..Tarjetas-->..Total==>','TCARDSTPV','TCARDSTPV',sumCardPayoutsAll);
    this.addOrUpdateMSRItemNoName(cardsDate,numberCardPayoutsTot.toFixed(0)+'..Tarjetas-->Totalizadas==>','TCARDSPAYTOT','TCARDSPAYTOT',sumCardPayoutsTot);
    this.addOrUpdateMSRItemNoName(cardsDate,numberCardPayoutsNot.toFixed(0)+'..Tarjetas-->NO Totalizadas==>','TCARDSPAYNOT','TCARDSPAYNOT',sumCardPayoutsNot);
  }
  //--------------------------------------- MONTHS -- GENERAL -- createCardPayoutsPrevMSRs --------------------------------
  private createCardPayoutsPrevMSRs(cardsDate:Date){
    this.globalVar.consoleLog(this.logToConsole,'-> MONTHs -> createCardPayoutsPrevMSRs ->...Starting...->', null); 
    this.resTotalCardsPrevNot = 0; 
    if (this.cardPayoutsMonthPrev === null)  return;
    if (this.cardPayoutsMonthPrev.length < 1)  return;
    // ordering card payouts list based on tpv id
    this.sortedCollection = this.orderPipe.transform(this.cardPayoutsMonthPrev,'cardPayoutTpvId',true,true);
    this.cardPayoutsMonthPrev = this.sortedCollection; 
    var sumCardPayoutsTpvNot = 0;
    var sumCardPayoutsNot = 0;
    var numberCardPayoutsTpvNot = 0;
    var numberCardPayoutsNot = 0;
    sumCardPayoutsTpvNot = sumCardPayoutsTpvNot + (this.cardPayoutsMonthPrev[0].cardPayoutTotal * this.cardPayoutsMonthPrev[0].cardPayoutMulti);        
    numberCardPayoutsTpvNot = numberCardPayoutsTpvNot + this.cardPayoutsMonthPrev[0].cardPayoutMulti;
    for (var i=1; i<this.cardPayoutsMonthPrev.length; i++) {
      if (this.cardPayoutsMonthPrev[i].cardPayoutTpvId != this.cardPayoutsMonthPrev[i-1].cardPayoutTpvId) {
        var tpvId = this.cardPayoutsMonthPrev[i-1].cardPayoutTpvId;
        var tpvName = this.globalVar.getCardTpvName(tpvId);
        var tpvObject = this.globalVar.getCardTpvObjectFromTpvName(tpvName);
        var tpvBankName = tpvObject.cardTpvBank;
        this.addOrUpdateMSRItem(cardsDate,tpvName,'CARDSPREVNOT',tpvBankName,sumCardPayoutsTpvNot);
        sumCardPayoutsNot = sumCardPayoutsNot + sumCardPayoutsTpvNot;
        numberCardPayoutsNot = numberCardPayoutsNot + numberCardPayoutsTpvNot;
        sumCardPayoutsTpvNot = 0;        
        numberCardPayoutsTpvNot = 0;
      }
      sumCardPayoutsTpvNot = sumCardPayoutsTpvNot + (this.cardPayoutsMonthPrev[i].cardPayoutTotal * this.cardPayoutsMonthPrev[i].cardPayoutMulti);        
      numberCardPayoutsTpvNot = numberCardPayoutsTpvNot + this.cardPayoutsMonthPrev[i].cardPayoutMulti;
    }
    // create record of last tpv
    var indexLast = this.cardPayoutsMonthPrev.length - 1;
    var tpvId = this.cardPayoutsMonthPrev[indexLast].cardPayoutTpvId;
    var tpvName = this.globalVar.getCardTpvName(tpvId);
    var tpvObject = this.globalVar.getCardTpvObjectFromTpvName(tpvName);
    var tpvBankName = tpvObject.cardTpvBank;
    this.addOrUpdateMSRItem(cardsDate,tpvName,'CARDSPREVNOT',tpvBankName,sumCardPayoutsTpvNot);
    sumCardPayoutsNot = sumCardPayoutsNot + sumCardPayoutsTpvNot;
    numberCardPayoutsNot = numberCardPayoutsNot + numberCardPayoutsTpvNot;
    // create records for NOT totals of all tpvs
    this.resTotalCardsPrevNot = sumCardPayoutsNot;
    this.addOrUpdateMSRItemNoName(cardsDate,numberCardPayoutsNot.toFixed(0) + '..Tarjetas Prev-->NO Totalizadas==>','TCARDSPREVNOT','TCARDSPREVNOT',sumCardPayoutsNot);
  }
  //--------------------------------------- MONTHS -- GENERAL -- createCardPayoutsAllNoTotMSRs --------------------------------
  private createCardPayoutsAllNoTotMSRs(cardsDate:Date){
    this.globalVar.consoleLog(this.logToConsole,'-> MONTHs -> createCardPayoutsAllNoTotMSRs ->...Starting...->', null); 
    this.resTotalCardsAllNoTot = 0; 
    if (this.cardPayoutsAllNoTot === null)  return;
    if (this.cardPayoutsAllNoTot.length < 1)  return;
    // ordering card payouts list based on tpv id
    this.sortedCollection = this.orderPipe.transform(this.cardPayoutsAllNoTot,'cardPayoutTpvId',true,true);
    this.cardPayoutsAllNoTot = this.sortedCollection; 
    var sumCardPayoutsTpvNot = 0;
    var sumCardPayoutsNot = 0;
    var numberCardPayoutsTpvNot = 0;
    var numberCardPayoutsNot = 0;
    sumCardPayoutsTpvNot = sumCardPayoutsTpvNot + (this.cardPayoutsAllNoTot[0].cardPayoutTotal * this.cardPayoutsAllNoTot[0].cardPayoutMulti);        
    numberCardPayoutsTpvNot = numberCardPayoutsTpvNot + this.cardPayoutsAllNoTot[0].cardPayoutMulti;
    for (var i=1; i<this.cardPayoutsAllNoTot.length; i++) {
      if (this.cardPayoutsAllNoTot[i].cardPayoutTpvId != this.cardPayoutsAllNoTot[i-1].cardPayoutTpvId) {
        var tpvId = this.cardPayoutsAllNoTot[i-1].cardPayoutTpvId;
        var tpvName = this.globalVar.getCardTpvName(tpvId);
        var tpvObject = this.globalVar.getCardTpvObjectFromTpvName(tpvName);
        var tpvBankName = tpvObject.cardTpvBank;
        this.addOrUpdateMSRItem(cardsDate,tpvName,'CARDSALLNOTOT',tpvBankName,sumCardPayoutsTpvNot);
        sumCardPayoutsNot = sumCardPayoutsNot + sumCardPayoutsTpvNot;
        numberCardPayoutsNot = numberCardPayoutsNot + numberCardPayoutsTpvNot;
        sumCardPayoutsTpvNot = 0;        
        numberCardPayoutsTpvNot = 0;
      }
      sumCardPayoutsTpvNot = sumCardPayoutsTpvNot + (this.cardPayoutsAllNoTot[i].cardPayoutTotal * this.cardPayoutsAllNoTot[i].cardPayoutMulti);        
      numberCardPayoutsTpvNot = numberCardPayoutsTpvNot + this.cardPayoutsAllNoTot[i].cardPayoutMulti;
    }
    // create record of last tpv
    var indexLast = this.cardPayoutsAllNoTot.length - 1;
    var tpvId = this.cardPayoutsAllNoTot[indexLast].cardPayoutTpvId;
    var tpvName = this.globalVar.getCardTpvName(tpvId);
    var tpvObject = this.globalVar.getCardTpvObjectFromTpvName(tpvName);
    var tpvBankName = tpvObject.cardTpvBank;
    this.addOrUpdateMSRItem(cardsDate,tpvName,'CARDSALLNOTOT',tpvBankName,sumCardPayoutsTpvNot);
    sumCardPayoutsNot = sumCardPayoutsNot + sumCardPayoutsTpvNot;
    numberCardPayoutsNot = numberCardPayoutsNot + numberCardPayoutsTpvNot;
    // create records for NOT totals of all tpvs
    this.resTotalCardsAllNoTot = sumCardPayoutsNot;
    this.addOrUpdateMSRItemNoName(cardsDate,numberCardPayoutsNot.toFixed(0) + '..Tarjetas-->NO Totalizadas==>','TCARDSALLNOTOT','TCARDSALLNOTOT',sumCardPayoutsNot);
  }
  //--------------------------------------- MONTHS -- GENERAL -- createCardTotalsMSRs --------------------------------
  private createCardTotalsMSRs(cardsDate:Date){
    this.globalVar.consoleLog(this.logToConsole,'-> MONTHs -> createCardTotalsMSRs ->...Starting...->', null);  
    if (this.cardTotalsMonth === null)  return;
    if (this.cardTotalsMonth.length < 1)  return;
    // Create array with all different tpv ids in the list of card totalizations of this month
    var localCardTotalsTpvIdList = new Array();   
    var indexTpvIdlist = 0;
    this.sortedCollection = this.orderPipe.transform(this.cardTotalsMonth,'cardTotalCardTpvId',true,true);
    this.cardTotalsMonth = this.sortedCollection;      
    localCardTotalsTpvIdList[indexTpvIdlist] = this.cardTotalsMonth[0].cardTotalCardTpvId; 
    for (var i=1; i<(this.cardTotalsMonth.length); i++) {
      if (this.cardTotalsMonth[i].cardTotalCardTpvId!=this.cardTotalsMonth[i-1].cardTotalCardTpvId) {
        indexTpvIdlist = indexTpvIdlist + 1;
        localCardTotalsTpvIdList[indexTpvIdlist] = this.cardTotalsMonth[i].cardTotalCardTpvId;       
      } 
    }  
    // calculate for each tpv id, total money, money to bank and total expenses
    var sumCardsTotals = 0;
    var sumCardsBank = 0;
    var sumCardsExpTotal = 0;
    var sumCardsExpTaxes = 0;
    var sumTpvTotals = 0;
    var sumTpvBank = 0;
    var sumTpvExpTotal = 0;
    var sumTpvExpTaxes = 0;
    for (var k=0; k<localCardTotalsTpvIdList.length; k++) {
      var tpvId = localCardTotalsTpvIdList[k];
      var tpvName = this.globalVar.getCardTpvName(tpvId);
      var tpvObject = this.globalVar.getCardTpvObjectFromTpvName(tpvName);
      var tpvBankName = tpvObject.cardTpvBank;
      sumTpvTotals = 0;
      sumTpvTotals = this.cardTotalsMonth.filter(item => 
                          ( (item.cardTotalCardTpvId.toFixed(0) === localCardTotalsTpvIdList[k].toFixed(0)) ) 
                          ).reduce((sumTpvTotals,current) => sumTpvTotals + current.cardTotalTotalMoney,0)
      sumCardsTotals = sumCardsTotals + sumTpvTotals;
      sumTpvBank = 0;
      sumTpvBank = this.cardTotalsMonth.filter(item => 
                          ( (item.cardTotalCardTpvId.toFixed(0) === localCardTotalsTpvIdList[k].toFixed(0)) ) 
                          ).reduce((sumTpvBank,current) => sumTpvBank + current.cardTotalBankMoney,0)
      sumCardsBank = sumCardsBank + sumTpvBank;
      sumTpvExpTotal = 0;
      sumTpvExpTotal = this.cardTotalsMonth.filter(item => 
                          ( (item.cardTotalCardTpvId.toFixed(0) === localCardTotalsTpvIdList[k].toFixed(0)) ) 
                          ).reduce((sumTpvExpTotal,current) => sumTpvExpTotal + current.cardTotalExpTotal,0)
      sumCardsExpTotal = sumCardsExpTotal + sumTpvExpTotal;
      sumTpvExpTaxes = 0;
      sumTpvExpTaxes = this.cardTotalsMonth.filter(item => 
                          ( (item.cardTotalCardTpvId.toFixed(0) === localCardTotalsTpvIdList[k].toFixed(0)) ) 
                          ).reduce((sumTpvExpTaxes,current) => sumTpvExpTaxes + current.cardTotalExpTaxesMoney,0)
      sumCardsExpTaxes = sumCardsExpTaxes + sumTpvExpTaxes;
      this.addOrUpdateMSRItem(cardsDate,tpvName,'CARDSTOTMONEY',tpvBankName,sumTpvTotals);
      this.addOrUpdateMSRItem(cardsDate,tpvName,'CARDSTOTBANK',tpvBankName,sumTpvBank);
      this.addOrUpdateMSRItem(cardsDate,tpvName,'CARDSTOTEXP',tpvBankName,sumTpvExpTotal + sumTpvExpTaxes);
    }
    this.addOrUpdateMSRItem(cardsDate,'Tarj.Totalizadas->Din.Total==>','TCARDSTOTMONEY','TCARDSTOTMONEY',sumCardsTotals);
    this.addOrUpdateMSRItem(cardsDate,'Tarj.Totalizadas->Din.Bancos==>','TCARDSTOTBANK','TCARDSTOTBANK',sumCardsBank);
    this.addOrUpdateMSRItem(cardsDate,'Tarj.Totalizadas->Comisiones==>','TCARDSTOTEXP','TCARDSTOTEXP',sumCardsExpTotal + sumCardsExpTaxes);
  }
  //--------------------------------------- MONTHS -- GENERAL -- createHdTpvsMTpvSRs --------------------------------
  private createHdTpvsMTpvSRs(tpvsDate:Date){
    this.globalVar.consoleLog(this.logToConsole,'-> MONTHs -> createHdTpvsMTpvSRs ->...Starting...->', null);
    this.monthTpvSummRecords = new Array();
    if (this.hdTpvsMonth === null) return;
    if (this.hdTpvsMonth.length < 1) return;
    this.sortedCollection = this.orderPipe.transform(this.hdTpvsMonth,'hdTpvName',false,true);
    this.hdTpvsMonth = this.sortedCollection;
    this.addOrUpdateMTpvSRItem(tpvsDate,this.hdTpvsMonth[0].hdTpvName,this.hdTpvsMonth[0].hdTpvType,0,0,0); 
    for (var i=1; i<(this.hdTpvsMonth.length); i++) {
      if (this.hdTpvsMonth[i].hdTpvName!=this.hdTpvsMonth[i-1].hdTpvName) {
        this.addOrUpdateMTpvSRItem(tpvsDate,this.hdTpvsMonth[i].hdTpvName,this.hdTpvsMonth[i].hdTpvType,0,0,0); 
      } 
    }   
    var sum = 0;
    for (var k= 0; k<this.monthTpvSummRecords.length; k++) {
      var tpvName = this.monthTpvSummRecords[k].monthTpvName;
      sum = 0;      
      sum = this.hdTpvsMonth.filter(item => item.hdTpvName === tpvName).reduce((sum,current) => sum + current.hdTpvTickets,0)
      this.monthTpvSummRecords[k].monthTpvTickets = sum;
      sum = 0;
      sum = this.hdTpvsMonth.filter(item => item.hdTpvName === tpvName).reduce((sum,current) => sum + current.hdTpvTicket,0)
      this.monthTpvSummRecords[k].monthTpvTicket = sum;
      sum = 0;
      sum = this.hdTpvsMonth.filter(item => item.hdTpvName === tpvName).reduce((sum,current) => sum + current.hdTpvMoney,0)
      this.monthTpvSummRecords[k].monthTpvMoney = sum;
    }
    this.sortedCollection = this.orderPipe.transform(this.monthTpvSummRecords,'monthTpvType',false,false);
    this.monthTpvSummRecords = this.sortedCollection;
  }
  //--------------------------------------- MONTHS -- GENERAL -- createHdsMSRs --------------------------------
  private createHdsMSRs(hdsDate:Date){
    this.globalVar.consoleLog(this.logToConsole,'-> MONTHs -> createHdsMSRs ->...Starting...->', null);   
    if (this.hdsMonth === null) return;  
    if (this.hdsMonth.length < 1) return;   
    this.sortedCollection = this.orderPipe.transform(this.hdsMonth,'hdName',true,true);
    this.hdsMonth = this.sortedCollection; 
    var indexStartMonthHdsNames = this.monthSummRecords.length;
    this.addOrUpdateMSRItem(hdsDate,this.hdsMonth[0].hdName,this.hdsMonth[0].hdType,this.hdsMonth[0].hdPlace,0); 
    for (var i=1; i<this.hdsMonth.length; i++) {
      if (this.hdsMonth[i].hdName!=this.hdsMonth[i-1].hdName) {
        this.addOrUpdateMSRItem(hdsDate,this.hdsMonth[i].hdName,this.hdsMonth[i].hdType,this.hdsMonth[i].hdPlace,0);      
      } 
    }   
    var indexEndMonthHdsNames = this.monthSummRecords.length;
    var sum = 0;
    for (var k= indexStartMonthHdsNames; k<indexEndMonthHdsNames; k++) {
      sum = 0;
      sum = this.hdsMonth.filter(item => item.hdName === this.monthSummRecords[k].monthName).reduce((sum,current) => sum + current.hdAmount,0)
      this.monthSummRecords[k].monthAmount = sum; 
    }
  }
  //--------------------------------------- MONTHS -- GENERAL -- createFixMSRs -----------------------------------------
  private createFixMSRs(monthDate:Date){
    this.globalVar.consoleLog(this.logToConsole,'-> MONTHs -> createFixMSRs ->...Starting...->', null);      
    //----------------------------------------------------------------- TOTAL  DIN DIAS -------------------
    var sumDayD = 0; 
    sumDayD = this.monthSummRecords.filter(item => item.monthType === 'DAY-D').reduce((sum,current) => sum + current.monthAmount,0)
    this.addOrUpdateMSRItem(monthDate,'Total..TPVs','THD-DAY-D','THD-DAY-D',sumDayD);      
    //----------------------------------------------------------------- TOTAL  MAQ B -------------------
    var sumMaqB = 0; 
    sumMaqB = this.monthSummRecords.filter(item => item.monthType === 'MAQB').reduce((sum,current) => sum + current.monthAmount,0)
    this.addOrUpdateMSRItem(monthDate,'Total..MaqB','THD-MAQB','THD-MAQB',sumMaqB);      
    //----------------------------------------------------------------- TOTAL  MAQ A -------------------
    var sumMaqA = 0;
    sumMaqA = this.monthSummRecords.filter(item => item.monthType === 'MAQA').reduce((sum,current) => sum + current.monthAmount,0)
    this.addOrUpdateMSRItem(monthDate,'Total..MaqA','THD-MAQA','THD-MAQA',sumMaqA);      
    //----------------------------------------------------------------- TOTAL  TAB -------------------
    var sumTab = 0;
    sumTab = this.monthSummRecords.filter(item => item.monthType === 'TAB').reduce((sum,current) => sum + current.monthAmount,0)
    this.addOrUpdateMSRItem(monthDate,'Total..Tabaco','THD-TAB','THD-TAB',sumTab);      
    //----------------------------------------------------------------- TOTAL  GOODS -------------------
    var sumGoods = 0;
    sumGoods = this.monthSummRecords.filter(item => item.monthType === 'GOODS').reduce((sum,current) => sum + current.monthAmount,0)
    this.addOrUpdateMSRItem(monthDate,'Total..Genero','THD-GOODS','THD-GOODS',sumGoods);      
    //----------------------------------------------------------------- TOTAL  OTHER -------------------
    var sumOther = 0;
    sumOther = this.monthSummRecords.filter(item => item.monthType === 'OTHER').reduce((sum,current) => sum + current.monthAmount,0);
    this.addOrUpdateMSRItem(monthDate,'Total..Otros','THD-OTHER','THD-OTHER',sumOther);      
    //----------------------------------------------------------------- TOTAL  INGRESOS -------------------
    var sumTotalIncome = sumDayD + sumMaqB + sumMaqA + sumTab + sumGoods + sumOther;
    this.addOrUpdateMSRItem(monthDate,'Total..Ingresos','TDIN','TDIN',sumTotalIncome);      
    //----------------------------------------------------------------- TOTAL  FACTURAS -------------------
    var sumTotalFacts = 0;
    sumTotalFacts = this.monthSummRecords.filter(item => item.monthType === 'TFACTPAYTYPE').reduce((sum,current) => sum + current.monthAmount,0)
    //----------------------------------------------------------------- SUPER TEORIA -------------------
    var sumMonthSupTeor = sumTotalIncome - sumTotalFacts;
    this.resTotalSupTeor = sumMonthSupTeor;
    this.addOrUpdateMSRItem(monthDate,'Sup..Teor','SUP-TEOR','SUP-TEOR',sumMonthSupTeor);      
    //----------------------------------------------------------------- TOTAL  FACTS HD -------------------
    var sumFactsHd = 0;
    sumFactsHd = this.monthSummRecords.filter(item => item.monthType === 'FACT').reduce((sum,current) => sum + current.monthAmount,0)
    this.addOrUpdateMSRItem(monthDate,'Total..Facts..Hd','THD-FACT','THD-FACT',sumFactsHd);      
    //----------------------------------------------------------------- DIN START HD -------------------
    var dinHdStart = 0;
    var indexFound = 0;
    var selectedDate = formatDate(monthDate, 'yyyy-MM-dd', 'en-US');
    indexFound = this.hdsMonth.findIndex(item => item.hdDate.toString() === selectedDate && item.hdType === 'DSTART')
    if (indexFound != -1) dinHdStart = this.hdsMonth[indexFound].hdAmount;
    this.addOrUpdateMSRItem(monthDate,'Hd..Din.Inicio','THD-DSTART','THD-DSTART',dinHdStart);      
    //----------------------------------------------------------------- DIN END HD -------------------
    var dinHdEnd = 0;
    var indexFound = 0;
    var selDate = new Date(monthDate);
    var selectedDate = formatDate(monthDate, 'yyyy-MM-dd', 'en-US');
    do {
      indexFound = this.hdsMonth.findIndex(item => item.hdDate.toString() === selectedDate && item.hdType === 'DCONT')
      if (indexFound != -1) dinHdEnd = this.hdsMonth[indexFound].hdAmount;
      selDate.setDate(selDate.getDate() + 1);
      selectedDate = formatDate(selDate, 'yyyy-MM-dd', 'en-US');
    } while (indexFound != -1);
    this.addOrUpdateMSRItem(monthDate,'Hd..Din.Fin','THD-DEND','THD-DEND',dinHdEnd);      
    this.increaseResTotalNumbers(true);
    this.increaseResTotalNumbers(false,0,0,0,0,0,0,0,0,dinHdStart,dinHdEnd)   
  }
  //--------------------------------------- MONTHS -- GENERAL -- createBankCajaMSRs -----------------------------------------
  private createBankCajaMSRs(createMonthsFlag:boolean,bankCajaDate: Date){
    this.globalVar.consoleLog(this.logToConsole,'-> MONTHs -> createBankCajaMSRs ->...Starting...->',null);      
    for (let i = 0 ; i < this.monthHdNamesBankCaja.length ; i++ ) {             
      var dinBankCajaDStart = 0;
      var dinBankCajaDSum   = 0;
      var dinBankCajaDRes   = 0;
      var dinBankCajaDCount = 0;
      var indexFound = 0;
      var bankCajaName = this.monthHdNamesBankCaja[i].value;
      if (createMonthsFlag === true) {
        indexFound = this.retCountPrevMonthSummRecords.findIndex(item => item.monthName === bankCajaName)
        if (indexFound != -1) dinBankCajaDStart = this.retCountPrevMonthSummRecords[indexFound].monthAmount;        
      } else {
        indexFound = this.retStartCountMonthSummRecords.findIndex(item => item.monthName === bankCajaName && item.monthType === 'RBC-DSTART')
        if (indexFound != -1) dinBankCajaDStart = this.retStartCountMonthSummRecords[indexFound].monthAmount;
        indexFound = this.retStartCountMonthSummRecords.findIndex(item => item.monthName === bankCajaName && item.monthType === 'RBC-DSUM')
        if (indexFound != -1) dinBankCajaDSum   = this.retStartCountMonthSummRecords[indexFound].monthAmount;  
        indexFound = this.retStartCountMonthSummRecords.findIndex(item => item.monthName === bankCajaName && item.monthType === 'RBC-DRES')  
        if (indexFound != -1) dinBankCajaDRes   = this.retStartCountMonthSummRecords[indexFound].monthAmount;
        indexFound = this.retStartCountMonthSummRecords.findIndex(item => item.monthName === bankCajaName && item.monthType === 'RBC-DCOUNT')
        if (indexFound != -1) dinBankCajaDCount = this.retStartCountMonthSummRecords[indexFound].monthAmount;    
      }
      this.addOrUpdateMSRItem(bankCajaDate,bankCajaName,'RBC-DSTART','RESBANKCAJA',dinBankCajaDStart);      
      this.addOrUpdateMSRItem(bankCajaDate,bankCajaName,'RBC-DSUM','RESBANKCAJA',dinBankCajaDSum);      
      this.addOrUpdateMSRItem(bankCajaDate,bankCajaName,'RBC-DRES','RESBANKCAJA',dinBankCajaDRes);      
      var dinBankCajaTSum = 0;
      dinBankCajaTSum = this.hdsMonth.filter(item => item.hdName === bankCajaName).reduce((sum,current) => sum + current.hdAmount,0)
      this.addOrUpdateMSRItem(bankCajaDate,bankCajaName,'RBC-TSUM','RESBANKCAJA',dinBankCajaTSum);      
      var dinBankCajaTCards = 0;
      dinBankCajaTCards = this.monthSummRecords.filter(item => item.monthPlace === bankCajaName && item.monthType === 'CARDSTOTMONEY').
                                                        reduce((sum,current) => sum + current.monthAmount,0)     
      this.addOrUpdateMSRItem(bankCajaDate,bankCajaName,'RBC-TCARDS','RESBANKCAJA',dinBankCajaTCards);      
      var dinBankCajaTFact = 0;
      dinBankCajaTFact = this.factsMonth.filter(item => item.factPayType === bankCajaName).reduce((sum,current) => sum + current.factTotalCost,0)
      this.addOrUpdateMSRItem(bankCajaDate,bankCajaName,'RBC-TFACT','RESBANKCAJA',dinBankCajaTFact);      
      var dinBankCajaDEnd = dinBankCajaDStart + dinBankCajaTSum + dinBankCajaDSum - dinBankCajaDRes + dinBankCajaTCards - dinBankCajaTFact;
      this.addOrUpdateMSRItem(bankCajaDate,bankCajaName,'RBC-DEND','RESBANKCAJA',dinBankCajaDEnd);      
      this.addOrUpdateMSRItem(bankCajaDate,bankCajaName,'RBC-DCOUNT','RESBANKCAJA',dinBankCajaDCount);      
      this.increaseResTotalNumbers(false, dinBankCajaDStart,dinBankCajaDSum,dinBankCajaDRes,dinBankCajaTSum,dinBankCajaTCards,
                                          dinBankCajaTFact,dinBankCajaDEnd,dinBankCajaDCount,0,0);      
    }
  }
  //--------------------------------------- MONTHS -- GENERAL -- createRetMSRs -----------------------------------------
  private createRetMSRs(createMonthsFlag:boolean,retDate: Date){
    this.globalVar.consoleLog(this.logToConsole,'-> MONTHs -> createRetMSRs ->...Starting...->',null);  
    for (let i = 0 ; i < this.monthHdNamesRet.length ; i++ ) {          
      var dinRetDStart  = 0;
      var dinRetDSum    = 0;
      var dinRetDRes    = 0;
      var dinRetDCount  = 0;
      var indexFound    = 0;
      var hdNameRet = this.monthHdNamesRet[i].value;
      if (createMonthsFlag === true) {
        var indexPrevMonthDCount = this.retCountPrevMonthSummRecords.findIndex(item => item.monthName === hdNameRet)
        if (indexPrevMonthDCount != -1) dinRetDStart  = this.retCountPrevMonthSummRecords[indexPrevMonthDCount].monthAmount;          
      } else {
        indexFound = this.retStartCountMonthSummRecords.findIndex(item => item.monthName === hdNameRet && item.monthType === 'RRT-DSTART')
        if (indexFound != -1) dinRetDStart = this.retStartCountMonthSummRecords[indexFound].monthAmount;        
        indexFound = this.retStartCountMonthSummRecords.findIndex(item => item.monthName === hdNameRet && item.monthType === 'RRT-DSUM')
        if (indexFound != -1) dinRetDSum   = this.retStartCountMonthSummRecords[indexFound].monthAmount;
        indexFound = this.retStartCountMonthSummRecords.findIndex(item => item.monthName === hdNameRet && item.monthType === 'RRT-DRES')
        if (indexFound != -1) dinRetDRes   = this.retStartCountMonthSummRecords[indexFound].monthAmount;
        indexFound = this.retStartCountMonthSummRecords.findIndex(item => item.monthName === hdNameRet && item.monthType === 'RRT-DCOUNT')
        if (indexFound != -1) dinRetDCount = this.retStartCountMonthSummRecords[indexFound].monthAmount;         
      }
      this.addOrUpdateMSRItem(retDate,hdNameRet,'RRT-DSTART','RESRET',dinRetDStart);      
      this.addOrUpdateMSRItem(retDate,hdNameRet,'RRT-DSUM','RESRET',dinRetDSum);      
      this.addOrUpdateMSRItem(retDate,hdNameRet,'RRT-DRES','RESRET',dinRetDRes);       
      var dinRetTSum = 0;
      dinRetTSum = this.hdsMonth.filter(item => item.hdName === hdNameRet).reduce((sum,current) => sum + current.hdAmount,0)
      this.addOrUpdateMSRItem(retDate,hdNameRet,'RRT-TSUM','RESRET',dinRetTSum);       
      var dinRetDEnd = dinRetDStart + dinRetTSum + dinRetDSum - dinRetDRes;
      this.addOrUpdateMSRItem(retDate,hdNameRet,'RRT-DEND','RESRET',dinRetDEnd);       
      this.addOrUpdateMSRItem(retDate,hdNameRet,'RRT-DCOUNT','RESRET',dinRetDCount);       
      this.increaseResTotalNumbers(false, dinRetDStart,dinRetDSum,dinRetDRes,dinRetTSum,0,0,dinRetDEnd,dinRetDCount,0,0);       
    }  
  }
  //--------------------------------------- MONTHS -- GENERAL -- createResTotalMSRs -----------------------------------------
  private createResTotalMSRs(resTotalDate: Date){
    this.globalVar.consoleLog(this.logToConsole,'-> MONTHs -> createResTotalMSRs ->...Starting...->',null);
    this.resTotalDEnd   = this.resTotalDEnd   + this.resTotalCardsAllNoTot;
    this.resTotalDCount = this.resTotalDCount + this.resTotalCardsAllNoTot;
    this.resTotalDStart = this.resTotalDStart + this.resTotalCardsPrevNot;
    this.resTotalDDif   = this.resTotalDCount - this.resTotalDEnd;
    this.addOrUpdateMSRItem(resTotalDate,'Total Inicio','RES-DSTART','RESTOTAL',this.resTotalDStart);       
    this.addOrUpdateMSRItem(resTotalDate,'Sumas Mes','RES-DSUM','RESTOTAL',this.resTotalDSum);       
    this.addOrUpdateMSRItem(resTotalDate,'Restas Mes','RES-DRES','RESTOTAL',this.resTotalDRes);       
    this.addOrUpdateMSRItem(resTotalDate,'Sumas Hd','RES-TSUM','RESTOTAL',this.resTotalTSum);    
    this.addOrUpdateMSRItem(resTotalDate,'Sumas Tarjetas','RES-TCARDS','RESTOTAL',this.resTotalTCards);          
    this.addOrUpdateMSRItem(resTotalDate,'Restas Facts','RES-TFACT','RESTOTAL',this.resTotalTFact);
    this.addOrUpdateMSRItem(resTotalDate,'Total Fin','RES-DEND','RESTOTAL',this.resTotalDEnd);       
    this.addOrUpdateMSRItem(resTotalDate,'Total Contado','RES-DCOUNT','RESTOTAL',this.resTotalDCount);       
    this.addOrUpdateMSRItem(resTotalDate,'Dif.Dinero','RES-DDIF','RESTOTAL',this.resTotalDCount - this.resTotalDEnd);       
    //----------------------------------------------------------------- SUPER DINERO -------------------
    this.addOrUpdateMSRItem(resTotalDate,'Sup..Din','SUP-DIN','SUP-DIN',this.resTotalDCount - this.resTotalDStart);       
    //----------------------------------------------------------------- SUPER DIFERENCIA DINERO - TEORIA  -------------------
    this.addOrUpdateMSRItem(resTotalDate,'Sup..Dif','SUP-DIF','SUP-DIF',this.resTotalDCount - this.resTotalDStart -this.resTotalSupTeor);       
  }
  //--------------------------------------- MONTHS -- GENERAL -- increaseResTotalNumbers -----------------------------------------
  private increaseResTotalNumbers (resetFlag:boolean,resTotalDStart?:number,resTotalDSum?:number,resTotalDRes?:number,resTotalTSum?:number,
                                    resTotalTCards?:number,resTotalTFact?:number,resTotalDEnd?:number,resTotalDCount?:number,
                                            resTotalHdsDStart?:number, resTotalHdsDEnd?:number) {
    if (resetFlag === true) {
      this.resTotalDStart = 0;
      this.resTotalDSum   = 0;
      this.resTotalDRes   = 0;
      this.resTotalTSum   = 0;
      this.resTotalTCards = 0;
      this.resTotalTFact  = 0;
      this.resTotalDEnd   = 0;
      this.resTotalDCount = 0;
      this.resTotalDDif   = 0;
      this.resTotalHdsDStart = 0;
      this.resTotalHdsDEnd   = 0;
    } else {
      this.resTotalDStart = this.resTotalDStart + resTotalDStart + resTotalHdsDStart;
      this.resTotalDSum   = this.resTotalDSum   + resTotalDSum ;
      this.resTotalDRes   = this.resTotalDRes   + resTotalDRes ;
      this.resTotalTSum   = this.resTotalTSum   + resTotalTSum ;
      this.resTotalTCards = this.resTotalTCards + resTotalTCards ;
      this.resTotalTFact  = this.resTotalTFact  + resTotalTFact ;
      this.resTotalDEnd   = this.resTotalDEnd   + resTotalDEnd  + resTotalHdsDEnd;
      this.resTotalDCount = this.resTotalDCount + resTotalDCount + resTotalHdsDEnd;
      this.resTotalDDif   = this.resTotalDCount - this.resTotalDEnd ;
      this.resTotalHdsDStart = this.resTotalHdsDStart + resTotalHdsDStart ;
      this.resTotalHdsDEnd   = this.resTotalHdsDEnd + resTotalHdsDEnd ;
    }
  }
  //--------------------------------------- MONTHS -- BTN CLICK ------------------------------------------------------------------
  public changeShowMonthsListBtnClick(){
    if (this.showMonthsSummList === true) {
      this.showMonthsSummList = false;
    } else {
      this.getMonthSummData();
    }
  }
  public selectYearBtnClick(){
    this.globalVar.consoleLog(this.logToConsole,'-> MONTHs -> selectYearBtnClick ->...Starting...->', null);
    const modalInitialState = {
      dateHeader        :this.formTitles.monthHeader,
      dateTitle         :this.formTitles.monthSummChangeYear,
      changeDateFlag    :false,
      changeDayFlag     :false,
      changeMonthFlag   :false,
      changeYearFlag    :true,
      callback          :'OK',   
    };
    this.globalVar.openModal(SelectMonthComponent, modalInitialState, 'modalLgTop0Left0').
    then((selectedDate:any)=>{
      if (selectedDate != null) { 
        this.changeDatesData(selectedDate);
        this.getMonthSummData();                                       
      }
    })
  }
  public createMonthSummBtnClick(){
    this.globalVar.consoleLog(this.logToConsole,'-> MONTHs -> createMonthSummBtnClick ->...Starting...->', null);
    const modalInitialState = {
      dateHeader        :this.formTitles.monthHeader,
      dateTitle         :this.formTitles.monthSummSelectMonth,
      changeDateFlag    :false,
      changeDayFlag     :false,
      changeMonthFlag   :true,
      changeYearFlag    :false,
      callback          :'OK',   
    };
    this.globalVar.openModal(SelectMonthComponent, modalInitialState, 'modalLgTop0Left0').
    then((selectedDate:any)=>{
      if (selectedDate != null) { 
        this.changeDatesData(selectedDate);
        var existsMontSumm = this.checkIfExistsMonthSumm(this.workingFirstAndLastDayOfMonth);
        if (existsMontSumm === true) {
          this._errorService.showErrorModal( "RESUMEN MENSUAL", "RESUMEN MENSUAL", "YA EXISTE RESUMEN MES"+"->",
                                        this.formTitles.monthEditMonthString,this.formTitles.monthSummYearString,"","","","");               
        } else {
          this.getAllMonthDataToCreate(this.workingFirstAndLastDayOfMonth);
        }                                     
      }
    })
  }
  public editMonthSummBtnClick(monthSumm:MonthSummary){   
    var selectedDate = new Date(monthSumm.monthSummDate);
    this.changeDatesData(selectedDate);
    this.getAllMonthDataToEdit(this.workingFirstAndLastDayOfMonth);
  }
  public showEditMonthDinStartEndBtnClick(){
    this.globalVar.consoleLog(this.logToConsole,'-> MONTHS -> showEditMonthDinInOutBtnClick ->...Starting...->', null);
    const modalInitialState = {
      arrayWorkingDates   :this.workingFirstAndLastDayOfMonth,
      hdTypesMonth        :this.hdTypesMonth,
      cardTpvsMonth       :this.cardTpvsMonth,
      monthSummRecords    :this.monthSummRecords,
      callback            :'OK',   
    };
    this.globalVar.openModal(MonthsDinStartComponent, modalInitialState, 'modalXlTop0Left0').
    then((result:any)=>{
      if (result != null) {
        this.monthSummRecords = result; 
        this.prepareMonthSummView(); 
      }
    })
  }
  public showEditMonthDinInOutBtnClick(){
    this.globalVar.consoleLog(this.logToConsole,'-> MONTHS -> showEditMonthDinInOutBtnClick ->...Starting...->', null);
    const modalInitialState = {
      arrayWorkingDates   :this.workingFirstAndLastDayOfMonth,
      hdTypesMonth        :this.hdTypesMonth,
      monthSummRecords    :this.monthSummRecords,
      callback            :'OK',   
    };
    this.globalVar.openModal(MonthsDinoutComponent, modalInitialState, 'modalXlTop0Left0').
    then((result:any)=>{
      if (result != null) {
        this.monthSummRecords = result; 
        this.prepareMonthSummView(); 
      }
    })
  }
  public showMonthCardsSummaryBtnClick(){
    this.globalVar.consoleLog(this.logToConsole,'-> MONTHS -> showMonthCardsSummaryBtnClick ->...Starting...->', null);
    const modalInitialState = {
      arrayWorkingDates     :this.workingFirstAndLastDayOfMonth,
      cardTpvsMonth         :this.cardTpvsMonth,
      monthHdNamesBankCaja  :this.monthHdNamesBankCaja,
      monthSummRecords      :this.monthSummRecords,
      callback              :'OK',   
    };
    this.globalVar.openModal(MonthsCardsComponent, modalInitialState, 'modalXlTop0Left0').
    then((result:any)=>{

    })
  }
  public showEditMonthFactsBtnClick(){
    this.globalVar.consoleLog(this.logToConsole,'-> MONTHS -> showEditMonthFactsBtnClick ->...Starting...->', null);
    const modalInitialState = {
      arrayWorkingDates   :this.workingFirstAndLastDayOfMonth,
      hdFlag              :false,
      monthFlag           :true,
      receivedHdsFacts    :null,
      callback            :'OK',   
    };
    this.globalVar.openModal(FactsHdComponent, modalInitialState, 'modalXlTop0Left0').
    then((result:any)=>{
      this.getFactsMonth(this.workingFirstAndLastDayOfMonth);
    })
  }
  public showMonthsHdsBtnClick(){
    this.globalVar.consoleLog(this.logToConsole,'-> MONTHS -> showMonthsHdsBtnClick ->...Starting...->', null);
    if (this.hdsMonth === null) return;
    if (this.hdsMonth.length < 1) return;
    const modalInitialState = {
      hdsMonth    :this.hdsMonth,
      callback    :'OK',   
    };
    this.globalVar.openModal(MonthsHdsComponent, modalInitialState, 'modalLgTop0Left0').
    then((result:any)=>{
      return;        
    })
  } 
  public showMonthSummRecordsBtnClick(){   
      this.globalVar.consoleLog(this.logToConsole,'-> MONTHS -> showMonthSummRecordsBtnClick ->...Starting...->', null);
      if (this.monthSummRecords === null) return;
      if (this.monthSummRecords.length < 1) return;
      const modalInitialState = {
        monthSummRecords    :this.monthSummRecords,
        callback            :'OK',   
      };
      this.globalVar.openModal(MonthsSummComponent, modalInitialState, 'modalXlTop0Left0').
      then((result:any)=>{
        return;      
      })
  }
  public lockMonthSummBtnClick(monthSumm:MonthSummary){

  }
  public showSearchMonthsSummBtnClick(){
    if (this.showSearchMonthSumm === true) {
      this.showSearchMonthSumm = false;
    } else {
      this.showSearchMonthSumm = true;    
    }
  }
  public setMonthSummOrderBtnClick(value: string){
    if (this.orderMonthSummList === value) {
      this.reverseMonthSummList = !this.reverseMonthSummList;
    }
    this.orderMonthSummList = value;   
  }
  //--------------------------------------- MONTHS -- DELETE ------------------------------------------------------------------

  //--------------------------------------- MONTHS -- DATABASE ------------------------------------------------------------------
  private getYearMonthSummRecords(firstAndLastDayOfYear: Date[]){
      this.globalVar.consoleLog(this.logToConsole, '-> MONTHs -> getYearMonthSummRecords ->...Starting...->', null);
      this._monthsService.getMonthSummRecords(firstAndLastDayOfYear)
        .subscribe(data => { this.monthsYearList = new Array();
                             this.monthsYearList = data;                            
                             this.prepareMonthSummList();
                           },
                  error => { if (this.globalVar.handleError(error)) { throwError(error) }; });  
  }
  private getRetStartCountMonthSummRecords(firstAndLastDayOfMonth: Date[]){
    this.globalVar.consoleLog(this.logToConsole, '-> MONTHs -> getRetStartCountMonthSummRecords ->...Starting...->', null);
    this._monthsService.getRetStartCountMonthSummRecords(firstAndLastDayOfMonth)
      .subscribe(data => { this.retCountPrevMonthSummRecords  = new Array();
                           this.retStartCountMonthSummRecords = new Array();
                           this.retStartCountMonthSummRecords = data;
                           this.deleteMonthsRecords(true,firstAndLastDayOfMonth);                        
                         },
                error => { if (this.globalVar.handleError(error)) { throwError(error) }; });  
  }
  private getMonthTpvSummRecords(firstAndLastDayOfMonth: Date[]){
    this.globalVar.consoleLog(this.logToConsole, '-> MONTHs -> getMonthTpvSummRecords ->...Starting...->', null);
    this._monthsService.getMonthTpvSummRecords(firstAndLastDayOfMonth)
      .subscribe(data => { this.monthTpvSummRecords = new Array();
                           this.monthTpvSummRecords = data;
                           this.sortedCollection = this.orderPipe.transform(this.monthTpvSummRecords,'monthTpvType',false,false);
                           this.monthTpvSummRecords = this.sortedCollection;                          
                           this.prepareMonthSummView();                           
                         },
                error => { if (this.globalVar.handleError(error)) { throwError(error) }; });  
  }
  private saveInDBMonthSummRecords(createMonthsFlag:boolean, newMonthSummRecords: Month[], newMonthTpvSummRecords: MonthTpv[]): void {
    this.globalVar.consoleLog(this.logToConsole, '-> MONTHs -> saveInDBMonthSummRecords ->...Starting...->', null);
    if (newMonthSummRecords?.length > 0) {
      this._monthsService.saveInDBMonthSummRecords(newMonthSummRecords)
      .subscribe(data => { this.monthSummRecords = new Array();
                           this.monthSummRecords = data;
                           this.saveInDBMonthsTpvSummRecords(newMonthTpvSummRecords);
                         },
                error => { if (this.globalVar.handleError(error)) { throwError(error) }; });
    } else this.saveInDBMonthsTpvSummRecords(newMonthTpvSummRecords);
  }
  private saveInDBMonthsTpvSummRecords(newMonthTpvSummRecords: MonthTpv[]): void {
    this.globalVar.consoleLog(this.logToConsole, '-> MONTHs -> saveInDBMonthsTpvSummRecords ->...Starting...->', null);
    if (newMonthTpvSummRecords?.length > 0) {
      this._monthsService.saveInDBMonthsTpvSummRecords(newMonthTpvSummRecords)
      .subscribe(data => { this.monthTpvSummRecords = new Array();
                           this.monthTpvSummRecords = data;
                           this.prepareMonthSummView();                                                    
                          },
                error => { if (this.globalVar.handleError(error)) { throwError(error) }; });
    } else this.prepareMonthSummView();
  }
  //--------------------------------------- MONTHS -- FACTS DATABASE ------------------------------------------------------------------
  private getFactsMonth(firstAndLastDayOfMonth: Date[]) {
    this.globalVar.consoleLog(this.logToConsole,'-> MONTHs -> getFactsMonth ->...Starting...->', null);
    let factsTypeValueRecordList = this.globalVar.prepareStringDatesValueRecordList('ALL',firstAndLastDayOfMonth[0],firstAndLastDayOfMonth[1]);
    this._factsService.getAllFactsTypeMonth(factsTypeValueRecordList)
      .subscribe(data => {  /// new Facts can have been added after showing all facts, banks mainly
                            this.factsMonth = data;
                            this.monthSummRecords = new Array();
                            this.monthTpvSummRecords = new Array();
                            this.getRetStartCountMonthSummRecords(firstAndLastDayOfMonth);                           
                         },
                error => { if (this.globalVar.handleError(error)) { throwError(error) }; });
  }
  //--------------------------------------- MONTHS -- DELETE -----------------------------------------------------------------------------
  public confirmDeleteOneMonthSummBtnClick(monthSumm:MonthSummary){
    var selectedDate = new Date(monthSumm.monthSummDate);
    this.changeDatesData(selectedDate);   
    this._errorService.getDeleteMsgConfirmation("deleteMsg",this.formTitles.monthHeader,this.formTitles.monthHeader,
                        monthSumm,this.formTitles.monthSummDeleteOne,this.workingDate.toLocaleDateString(),
                        "","","","","",).
        then((deleteOK:boolean)=>{
          if (deleteOK === true){ this.deleteMonthsRecords(false,this.workingFirstAndLastDayOfMonth); }
        })
  }
  public confirmDeleteAllMonthsSummBtnClick(){
    this._errorService.getDeleteMsgConfirmation("deleteMsg",this.formTitles.monthHeader,this.formTitles.monthHeader,
                        null,this.formTitles.monthSummDeleteAll,this.workingFirstAndLastDayOfYear[0].toLocaleDateString(),
                        this.workingFirstAndLastDayOfYear[1].toLocaleDateString(),"","","","",).
        then((deleteOK:boolean)=>{
          if (deleteOK === true){ this.deleteMonthsRecords(false,this.workingFirstAndLastDayOfYear);            
          }
        })
  }
  private deleteMonthsRecords(editMonthSummRecords:boolean,firstAndLastDayOfMonth: Date[]) {
    this.globalVar.consoleLog(this.logToConsole, '-> MONTHs -> deleteMonthsRecords ->...Starting...->', null);
    this._monthsService.deleteMonthsRecords(firstAndLastDayOfMonth)
      .subscribe(data => { if (editMonthSummRecords === false){ this.getMonthSummData(); } 
                           else { this.createMonthSummRecords(false,firstAndLastDayOfMonth); }                           
                          },
                error => { if (this.globalVar.handleError(error)) { throwError(error) }; });
    }
  //--------------------------------------- MONTHS -- GETTING ALL DATA TO CREATE ------------------------------------------------------------
  private getAllMonthDataToCreate(firstAndLastDayOfMonth: Date[]){
    this.globalVar.consoleLog(this.logToConsole, '-> MONTHs -> getAllMonthDataToCreate ->...Starting...->', null);
    var allMonthVRList = this.globalVar.prepareStringDatesValueRecordList('ALL',firstAndLastDayOfMonth[0],firstAndLastDayOfMonth[1]);
    var selectedDate = new Date(firstAndLastDayOfMonth[0]);
    var year  = selectedDate.getFullYear();
    var month = selectedDate.getMonth();
    let endDate = new Date(year,month,0); // last day of previous month
    let startDate =  new Date(year-1,month,1);  // 1 year before endDate
    var allPrevMonthVRList = this.globalVar.prepareStringDatesValueRecordList('ALL',startDate,endDate);
    var noTotMonthVRList = this.globalVar.prepareStringDatesValueRecordList('ALL',startDate,firstAndLastDayOfMonth[1]); 
    forkJoin({hdTypes         :this._hdService.getAllHdTypes(),
              facts           :this._factsService.getAllFactsTypeMonth(allMonthVRList),
              cardTpvs        :this._cardsService.getAllCardTpvs(),
              cardPayouts     :this._cardsService.getAllCardPayouts(allMonthVRList),
              prevCardPayouts :this._cardsService.getCardPayoutsNoTotal(allPrevMonthVRList),
              noTotCardPayouts:this._cardsService.getCardPayoutsNoTotal(noTotMonthVRList),
              cardTotals      :this._cardsService.getAllCardsTotals(allMonthVRList),
              hdTpvs          :this._hdService.getHdTpvsMonth(firstAndLastDayOfMonth),
              hds             :this._hdService.getHdsMonth(firstAndLastDayOfMonth),
              prevMonthMSRs   :this._monthsService.getRetCountPrevMonthSummRecords(firstAndLastDayOfMonth)
              })
      .subscribe((data)=> { 
          this.hdTypesMonth         = data.hdTypes;
          this.factsMonth           = data.facts;
          this.cardTpvsMonth        = data.cardTpvs;
          this.cardPayoutsMonth     = data.cardPayouts;
          this.cardPayoutsMonthPrev = data.prevCardPayouts;
          this.cardPayoutsAllNoTot    = data.noTotCardPayouts;
          this.cardTotalsMonth      = data.cardTotals;
          this.hdTpvsMonth          = data.hdTpvs;
          this.hdsMonth             = data.hds;
          this.retStartCountMonthSummRecords = new Array();
          this.retCountPrevMonthSummRecords  = new Array();
          this.retCountPrevMonthSummRecords  = data.prevMonthMSRs;
          this.prepareHdTypesMonthVRList();
          this.checkReceivedMonthData(firstAndLastDayOfMonth);                                           
        },
        error => { if (this.globalVar.handleError(error)) { throwError(error) }; });
  }
//--------------------------------------- MONTHS -- GETTING ALL DATA TO EDIT ----------------------------------------------------------------
private getAllMonthDataToEdit(firstAndLastDayOfMonth: Date[]){
  this.globalVar.consoleLog(this.logToConsole, '-> MONTHs -> getAllMonthDataToEdit ->...Starting...->', null);
  var allMonthVRList = this.globalVar.prepareStringDatesValueRecordList('ALL',firstAndLastDayOfMonth[0],firstAndLastDayOfMonth[1]);
  var selectedDate = new Date(firstAndLastDayOfMonth[0]);
  var year  = selectedDate.getFullYear();
  var month = selectedDate.getMonth();
  let endDate = new Date(year,month,0); // last day of previous month
  let startDate =  new Date(year-1,month,1);  // 1 year before endDate
  var allPrevMonthVRList = this.globalVar.prepareStringDatesValueRecordList('ALL',startDate,endDate); 
  var noTotMonthVRList = this.globalVar.prepareStringDatesValueRecordList('ALL',startDate,firstAndLastDayOfMonth[1]); 
  forkJoin({hdTypes         :this._hdService.getAllHdTypes(),
            facts           :this._factsService.getAllFactsTypeMonth(allMonthVRList),
            cardTpvs        :this._cardsService.getAllCardTpvs(),
            cardPayouts     :this._cardsService.getAllCardPayouts(allMonthVRList),
            prevCardPayouts :this._cardsService.getCardPayoutsNoTotal(allPrevMonthVRList),
            noTotCardPayouts:this._cardsService.getCardPayoutsNoTotal(noTotMonthVRList),
            cardTotals      :this._cardsService.getAllCardsTotals(allMonthVRList),
            hdTpvs          :this._hdService.getHdTpvsMonth(firstAndLastDayOfMonth),
            hds             :this._hdService.getHdsMonth(firstAndLastDayOfMonth),
            retStartMonthMSRs :this._monthsService.getRetStartCountMonthSummRecords(firstAndLastDayOfMonth)
            })
    .subscribe((data)=> { 
        this.hdTypesMonth         = data.hdTypes;
        this.factsMonth           = data.facts;
        this.cardTpvsMonth        = data.cardTpvs;
        this.cardPayoutsMonth     = data.cardPayouts;
        this.cardPayoutsMonthPrev = data.prevCardPayouts;
        this.cardPayoutsAllNoTot    = data.noTotCardPayouts;
        this.cardTotalsMonth      = data.cardTotals;
        this.hdTpvsMonth          = data.hdTpvs;
        this.hdsMonth             = data.hds;
        this.retCountPrevMonthSummRecords  = new Array();
        this.retStartCountMonthSummRecords = new Array();
        this.retStartCountMonthSummRecords = data.retStartMonthMSRs;
        this.prepareHdTypesMonthVRList();
        this.deleteMonthsRecords(true,firstAndLastDayOfMonth);      
      },
      error => { if (this.globalVar.handleError(error)) { throwError(error) }; });
  }  

}