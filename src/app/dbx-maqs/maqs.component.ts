import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { throwError } from 'rxjs';

import { OrderPipe } from 'ngx-order-pipe';
import { BsModalService } from 'ngx-bootstrap/modal';

import { GbViewsService } from '../dbc-gbviews/gbviews.service';
import { GlobalVarService } from '../aa-common/global-var.service';
import { ValueRecord } from '../dbc-fields/field';
import { SelectManyVRListComponent } from '../aa-common/select-manyVRList.component';

import { ErrorService } from '../aa-errors/error.service';

import { Maq, MaqString } from './maqs';
import { MaqsCreateComponent } from './maqs-create.component';
import { MaqsService } from './maqs.service';
import { Hd } from '../dbx-hd/hd';

@Component({
  selector      :'app-maqs',
  templateUrl   :'./maqs.component.html',
  styleUrls     :['./maqs.component.css']
})
export class MaqsComponent implements OnInit {
  public formTitles = {     
    'maqsHeader'          :'Resumen Máquinas',
    'maqsListYear'        :'Año',
    'maqsListYearString'  :'',
    'maqsListStart'       :'',
    'maqsListEnd'         :'',
    'maqsMonthString'     :'',
    'maqsListTotalString' :'',
    'Edit'                :'Editar',
    'Delete'              :'Borrar',
    'maqsDeleteOne'       :'¿ Seguro que quiere BORRAR este Resumen de Máquina ?',
    'maqsDeleteAll'       :'¿ Seguro que quiere BORRAR TODOS los Resumenes de Máquinas ?',
  };
  public formLabels = { 
    'id'                              :'#####',
    'maqId'                           :'Id',   
    'maqMaqBName'                     :'Máquina',  
    'maqMaqMonth'                     :'Mes',   
    'maqStartDate'                    :'Desde',   
    'maqEndDate'                      :'Hasta',   
    'maqSumType'                      :'Tipo Máquina',   
    'maqSumDataType'                  :'Tipo',   
    'maqCoin1StartInputDinAdded'      :'Din.Rec',   
    'maqCoin2StartOutputDinDifDays'   :'Dif.Dia',   
    'maqCoin3StartDrawerDinDifCoins'  :'Dif.Tol',   
    'maqCoin4EndInputDinEnd'          :'Din.Fin',   
    'maqCoin5EndOutputDinMechCntr'    :'Cnt.Mec',   
    'maqCoin6EndDrawerDinPartCntr'    :'Cnt.Par',
    'maqDifDinMechCntrDinEnd'         :'Diff.',     
    'maqStatus'                       :'Estado',                  
  };

  private workingDate                     :Date;
  private workingFirstAndLastDayOfMonth   :Date[];
  private workingFirstAndLastDayOfYear    :Date[];
  public  showMaqsList                    :boolean;
  public  maqsList                        :Maq[];
  public  selectedMaq                     :Maq;
  public  newMaq                          :Maq;
  public  searchMaq                       :MaqString;
  public  showSearchMaqsList              :boolean;
  private createMaqFlag                   :boolean;
  private localMaqBRecordList             :ValueRecord[];
  public  maqRecHdsList                   :Hd[];
  private orderMaqsList                   :string;
  private reverseMaqsList                 :boolean;
  public  caseInsensitive                 :boolean;
  public  sortedCollection                :any[];
  private logToConsole                    :boolean;
  /*--------------------------------------- MAQS -- CONSTRUCTOR -----------------------------------------------------------*/
  constructor ( private orderPipe: OrderPipe, private modalService: BsModalService, 
                public globalVar: GlobalVarService, private _errorService: ErrorService, 
                private _maqsService: MaqsService  ) { }
  /*--------------------------------------- MAQS -- NG ON INIT -----------------------------------------------------------*/
  ngOnInit(): void {
    this.logToConsole = this.globalVar.logToConsole;
    this.logToConsole = true; 
    this.orderMaqsList    = 'maqEndDate';
    this.reverseMaqsList  = true;
    this.localMaqBRecordList = this.globalVar.maqBRecordList;    
    this.getMaqsListData();
  }

  /*--------------------------------------- MAQS -- FORMS --------------------------------------------------------------*/  


  /*--------------------------------------- MAQS  -- GENERAL -----------------------------------------------------------*/
  getMaqsListData(){
    this.showMaqsList = true;
    this.showSearchMaqsList = false;
    this.searchMaq = new MaqString();
    this.formTitles.maqsListTotalString = 'Total',
    this.changeDatesData(this.globalVar.workingDate);
    this.maqsList = new Array();
    this.getMaqsLits(this.workingFirstAndLastDayOfYear);
  }
  changeDatesData(changedDate: Date){
    this.globalVar.changeWorkingDates(changedDate);
    this.workingDate = changedDate;
    this.workingFirstAndLastDayOfMonth = this.globalVar.workingFirstAndLastDayOfMonth;   
    this.workingFirstAndLastDayOfYear  = this.globalVar.workingFirstAndLastDayOfYear;
    this.formTitles.maqsMonthString = this.workingDate.toLocaleString('default',{month:'long'});
    let workingYear = this.workingDate.getFullYear();
    this.formTitles.maqsListYearString = workingYear.toString();
    this.formTitles.maqsListStart = this.workingFirstAndLastDayOfMonth[0].toLocaleDateString(); 
    this.formTitles.maqsListEnd = this.workingFirstAndLastDayOfMonth[1].toLocaleDateString();
  } 
  /*--------------------------------------- MAQS  -- BTN CLICK ---------------------------------------------------------*/
  changeShowMaqsListBtnClick(){
    if (this.showMaqsList === true) {
      this.showMaqsList = false;
    } else {
      this.getMaqsListData();
    }
  }
  createMaqBtnClick(){
    this.globalVar.consoleLog(this.logToConsole,'...MAQS..>..createMaqBtnClick..->..Starting..>>', null); 
    if (this.localMaqBRecordList.length < 1) { 
      // show message that there are no MAQ B in this bar
      return;  
    }    
    const modalInitialState = {
      selectOption    :1,
      selectTitle1    :"Resumen Máquinas B",
      selectTitle2    :"Seleccionar Fechas y Máquina B",
      selectMessage   :"Seleccione la Fecha y La Máquina Tragaperras de la Recaudación",
      selectFooter    :"Recaudación Máquinas Tragaperras",
      callback        :'OK',   
    };
    this.globalVar.openModal(SelectManyVRListComponent, modalInitialState, 'modalLgTop0Left0').
    then((result:any)=>{
      if (result != null) {
        if (result[2].value === "") { return; }
        else {
          //check if exist already in DB
          this.createMaqFlag = true;
          this.checkIfMaqsIsInDB(result);
        }
      } 
    })
  }
  createMaq(maqName :string, maqDateStart :string, maqDateEnd :string){
    this.newMaq = new Maq();
    this.newMaq.maqMaqBName  = maqName;
    this.newMaq.maqStartDate = this.globalVar.stringToDate(maqDateStart);
    this.newMaq.maqEndDate   = this.globalVar.stringToDate(maqDateEnd);
    this.createMaqModal();
  }  
  editMaqBtnClick(modMaq: Maq){
    if (this.isMaqNotBlocked(modMaq)){ 
      this.newMaq = modMaq;
      this.createMaqFlag = false;
      var editDateRangeMaqBNameVRList :ValueRecord[];
      editDateRangeMaqBNameVRList = this.globalVar.prepareDateRangeStringValueRecordList(this.newMaq.maqStartDate,
                                                                                          this.newMaq.maqEndDate,this.newMaq.maqMaqBName,);
      this.getHdsMaqs(editDateRangeMaqBNameVRList);
    }
  } 
  createMaqModal(){
    this.globalVar.consoleLog(this.logToConsole,'...MAQS..>..createMaqModal..->..Starting..>>', null); 
    const modalInitialState = {
      newMaq          :this.newMaq,
      maqRecHdsList   :this.maqRecHdsList,
      createMaqFlag   :this.createMaqFlag,
      callback        :'OK',   
    };
    this.globalVar.openModal(MaqsCreateComponent, modalInitialState, 'modalXlTop0Left0').
    then((result:any)=>{
      if (result != null) {
        this.getMaqsLits(this.workingFirstAndLastDayOfYear);    
      }
    })
  }  
  isMaqNotBlocked(selectedMaq: Maq):boolean{
    this.globalVar.consoleLog(this.logToConsole,'->MAQS->isMaqNotBlocked->starting->', null);
    if (selectedMaq.maqStatus === false){
      this._errorService.showErrorModal( "RESUMEN MÁQUINAS", "RESUMEN MÁQUINAS", "RESUMEN MÁQUINAS BLOQUEADO"+"->",
                        "Máquina->"+selectedMaq.maqMaqBName,"","","","","");
      return false;
    } else return true;
  }
  lockMaqBtnClick(modMaq: Maq){
    if (modMaq.maqStatus === true) modMaq.maqStatus = false;
    else  modMaq.maqStatus = true;
    this.updateOneMaq(modMaq);
  }
  showSearchMaqsListBtnClick() {
    if (this.showSearchMaqsList === true) {
      this.showSearchMaqsList = false;
    } else {
      this.showSearchMaqsList = true;
    }
    return;
  }
  setMaqsListOrderBtnClick(value: string) {
    if (this.orderMaqsList === value) {
      this.reverseMaqsList = !this.reverseMaqsList;
    }
    this.orderMaqsList = value;
  }
  /*--------------------------------------- MAQS DATABASE --------------------------------------------------------------*/
  prepareMaqListSumm(maqsSummList :Maq[]){
    this.sortedCollection = this.orderPipe.transform(maqsSummList,this.orderMaqsList,this.reverseMaqsList,this.caseInsensitive);
    this.maqsList = this.sortedCollection;
    if (maqsSummList.length === 0) this.formTitles.maqsListTotalString= "0..Maqs..Total.Recaud->0__Recaud.1.Maq-> 0";
    else {
      let totalDinRec = 0;
      for (let k=0;k<maqsSummList.length;k++){
        totalDinRec = totalDinRec + maqsSummList[k].maqCoin4EndInputDinEnd;
      }
      this.formTitles.maqsListTotalString= maqsSummList.length.toString()+"..Maqs..Total.Recaud->"+totalDinRec.toFixed(2)+
      "__Recaud.1.Maq->"+(totalDinRec/maqsSummList.length).toFixed(2);
    }
  }
  getMaqsLits(firstAndLastDayOfYear: Date[]){
    this.globalVar.consoleLog(this.logToConsole, '->MAQS->getMaqsLits->...Starting->', null);
    this._maqsService.getMaqsLits(firstAndLastDayOfYear)
      .subscribe(data => { this.maqsList = data;
                           this.prepareMaqListSumm(this.maqsList);
                         },
                error => { if (this.globalVar.handleError(error)) { throwError(error) }; });  
  }
  checkIfMaqsIsInDB(dateRangeMaqBNameVRList: ValueRecord[]){
    this.globalVar.consoleLog(this.logToConsole, '->MAQS->checkIfMaqsIsInDB->...Starting->', null);
    this._maqsService.checkIfMaqsIsInDB(dateRangeMaqBNameVRList)
      .subscribe(data => {  if ( (data === null) || (data.length === 0) ){
                              this.getHdsMaqs(dateRangeMaqBNameVRList);
                            } else {
                               // show message maqRec already exist
                            }
                        },
                error => { if (this.globalVar.handleError(error)) { throwError(error) }; });  
  }
  getHdsMaqs(dateRangeMaqBNameVRList: ValueRecord[]){
    this.globalVar.consoleLog(this.logToConsole, '->MAQS->getHdsMaqs->...Starting->', null);
    this._maqsService.getHdsMaqs(dateRangeMaqBNameVRList)
      .subscribe(data => { this.maqRecHdsList = data;
                           this.sortedCollection = this.orderPipe.transform(this.maqRecHdsList,'hdDate',false,false);
                           this.maqRecHdsList = this.sortedCollection; 
                           this.createMaq(dateRangeMaqBNameVRList[2].value,dateRangeMaqBNameVRList[0].value,dateRangeMaqBNameVRList[1].value);
                        },
                error => { if (this.globalVar.handleError(error)) { throwError(error) }; });  
  }
  updateOneMaq(updateMaq: Maq): void {
    this.globalVar.consoleLog(this.logToConsole,'->FACTS->updateOneFact->updating->', null);
    this._maqsService.updateOneMaq(updateMaq)
      .subscribe( data  => { this.getMaqsLits(this.workingFirstAndLastDayOfYear); },
                  error => { if (this.globalVar.handleError(error)) { throwError(error) }; });
  }

  /*--------------------------------------- MAQS DELETE ----------------------------------------------------------------*/  
  confirmDeleteOneMaqBtnClick(maqRec: Maq){
        this._errorService.getDeleteMsgConfirmation("deleteMsg",this.formTitles.maqsHeader,this.formTitles.maqsHeader,
                            maqRec,this.formTitles.maqsDeleteOne,maqRec.maqMaqBName,
                            maqRec.maqStartDate.toString(),maqRec.maqEndDate.toString(),"","","",).
        then((deleteOK:boolean)=>{
          if (deleteOK === true){ this.deleteOneMaq(maqRec); }           
        })
  }
  confirmDeleteAllMaqsListBtnClick(){

    this._errorService.getDeleteMsgConfirmation("deleteMsg",this.formTitles.maqsHeader,this.formTitles.maqsHeader,
                            null,this.formTitles.maqsDeleteAll,"","","","","","",).
        then((deleteOK:boolean)=>{
          if (deleteOK === true){ this.deleteAllMaqs(); }
        })
  } 
  deleteAllMaqs(){
    this.globalVar.consoleLog(this.logToConsole,'->MAQS->deleteAllMaqs->...Deleting All->', null);
    this._maqsService.deleteAllMaqs(this.workingFirstAndLastDayOfYear)
    .subscribe(data => { this.getMaqsListData();  },
              error => { if (this.globalVar.handleError(error)) { throwError(error) }; });
  }
  deleteOneMaq(delMaq: Maq){
    this.globalVar.consoleLog(this.logToConsole,'->MAQS->deleteOneMaq->...Deleting One->', null);
    this._maqsService.deleteOneMaq(delMaq)
    .subscribe(data => { this.getMaqsListData();  },
              error => { if (this.globalVar.handleError(error)) { throwError(error) }; });
  }

}