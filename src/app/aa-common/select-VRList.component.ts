import { Component, OnInit } from '@angular/core';
import { FormGroup,FormBuilder } from '@angular/forms';
import { Subject } from 'rxjs';

import { BsModalRef } from 'ngx-bootstrap/modal';
import { ValueRecord } from '../dbc-fields/field';
 
class ValueRecordObject {
    valueRecord : ValueRecord;    
  }
@Component({
  selector      :'app-select-VRList',
  templateUrl   :'./select-VRList.component.html',
  styleUrls     :['./common.component.css'] 
})

export class SelectValueRecordListComponent implements OnInit {
  localValueRecordList  :ValueRecord[];
  selectTitle1          :string;
  selectTitle2          :string;
  selectMessage         :string;
  selectLabel           : string;  
  selectFooter          :string;
  callback              :any;
  result                :Subject<any> = new Subject<any>();

  valueRecordForm       :FormGroup;
  valueRecord           :ValueRecordObject;

  constructor ( public bsModalRef: BsModalRef, private fb: FormBuilder ) {  }
 
  ngOnInit(): void {
    this.valueRecordForm = this.fb.group({
        valueRecord: null,
      });    
    this.valueRecordForm.patchValue( { valueRecord: this.localValueRecordList[0].value } );
    this.valueRecord = new ValueRecordObject();   
  }
  /*---------------------------------------CARDTPVS CREATE -- FORMS-----------------------------------------------------------*/
  closeSelectValueRecordListModalBtnClick(){
    if (this.bsModalRef.content.callback != null){
        //this.bsModalRef.content.callback(null);
        this.result.next(null);
        this.bsModalRef.hide();
      } 
  }
  saveSelectedValueRecordBtnClick() {
    if (this.bsModalRef.content.callback != null){
      //this.bsModalRef.content.callback(this.valueRecordForm.get('valueRecord').value);
      this.result.next(this.valueRecordForm.get('valueRecord').value);
      this.bsModalRef.hide();
    }
  }
}