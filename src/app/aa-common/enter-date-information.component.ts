import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { BsDatepickerConfig, BsDaterangepickerConfig, BsDatepickerViewMode } from 'ngx-bootstrap/datepicker';
import { Subject } from 'rxjs';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ValidationService } from '../aa-common/validation.service';

@Component({
  selector      :'app-enter-date-information',
  templateUrl   :'./enter-date-information.component.html',
  styleUrls     :['./common.component.css']
})

export class EnterDateInformationComponent implements OnInit {
  public  msgColor      :string;
  public  msgHeader     :string;
  public  msgFooter     :string;
  public  msgDates      :Date[];
  public  msgDatesMode  :string;
  public  msg1          :string;
  public  msg2          :string;
  public  msg3          :string;
  public  msg4          :string;
  public  msg5          :string;
  public  msg6          :string;
  public  msg7          :string;
  public  callback      :any;

  public tLabels = { 
    'header'            :'CAMBIO FECHA TRABAJO',
    'currentDate'       :'Fecha Actual',
    'selDate'           :'Día Seleccionado',  
    'changeSelDate'     :'Cambiar Día Seleccionado', 
    'selDateRange'      :'Rango de Fechas Seleccionado', 
    'selDateRangeStart' :'Desde el Día',  
    'selDateRangeEnd'   :'Hasta el Día',  
    'changeSelDateRange':'Cambiar Rango Fechas Seleccionado', 
    'selNumber'         :'Número Seleccionado',
    'enterNumber'       :'Introduzca un Número',
    'workingDate'       :'Fecha Trabajo',  
    'changeDate'        :'Cambiar Fecha Trabajo',  
    'workingDay'        :'Día de Trabajo', 
    'changeDay'         :'Cambiar Día Trabajo',  
    'workingMonth'      :'Mes de Trabajo', 
    'changeMonth'       :'Cambiar Mes Trabajo',   
    'workingYear'       :'Año de Trabajo',
    'changeYear'        :'Cambiar Año Trabajo',   
  }
  public  currentDate             :Date;
  public  localWorkingDate        :Date;
  public  localWorkingDateRange   :Date[];
  private resultDatesRange        :Date[];
  private resultSubjectDatesRange: Subject<any[]> = new Subject<any[]>();
  public  selectedNumber          :number;
  public  inputForm               :FormGroup;
  private resultNumbersRange      :number[];

  public  localDatePickerConfig   :Partial <BsDatepickerConfig>; 
  public  localDateRangePickerConfig  :Partial <BsDaterangepickerConfig>; 
  private localMinMode            :BsDatepickerViewMode;
  
  constructor(private fb:FormBuilder, public bsModalRef: BsModalRef) { }
  
  ngOnInit(): void {
    this.currentDate = new Date();
    this.localWorkingDate = this.msgDates[0];
    this.localWorkingDateRange = this.msgDates; 
    this.resultDatesRange = new Array();
    this.resultNumbersRange = new Array();
    this.selectedNumber = 0;
    this.inputForm = this.fb.group({
      inputIntegerNumber       :[{value:this.selectedNumber,disabled:false}, [Validators.required, ValidationService.positiveIntegerValidation] ],
    })
    this.inputForm.get('inputIntegerNumber').valueChanges.subscribe(val => {
      this.onNumberChanged(val);
    });
    this.setDatePickerConfig(this.localWorkingDate, this.msgDates, this.msgDatesMode);
  }
  saveDatesBtnClick(){
    if (this.bsModalRef.content.callback != null){
      this.resultSubjectDatesRange.next(this.resultDatesRange);
      this.bsModalRef.hide();
    }
  }
  saveNumbersBtnClick(){
    if (this.bsModalRef.content.callback != null){
      this.resultSubjectDatesRange.next(this.resultNumbersRange);
      this.bsModalRef.hide();
    }
  }
  closeMessageBtnClick() {
    if (this.bsModalRef.content.callback != null){
      this.resultSubjectDatesRange.next(null);
      this.bsModalRef.hide();
    }
  }
  onNumberChanged(changedNumber :number){
    this.selectedNumber  = changedNumber;
    this.resultNumbersRange[0] = changedNumber;
  }
  onDateChanged(changedDate: Date){
    this.localWorkingDate  = changedDate;
    var dateChanged = new Date(changedDate);
    this.resultDatesRange[0] = dateChanged;
  }
  onDateRangeChanged(changedDateRange: Date[]){    
    this.localWorkingDateRange  = changedDateRange;
    var dateChangedRange0 = new Date(changedDateRange[0]);
    this.resultDatesRange[0] = dateChangedRange0;
    var dateChangedRange1 = new Date(changedDateRange[1]);
    this.resultDatesRange[1] = dateChangedRange1;
  }
  setDatePickerConfig(selectedDate :Date, dateRange :Date[], mode :string){
    /*
    console.log("...selectedDate...");
    console.log(JSON.stringify(selectedDate, null, '    ') );
    console.log("...mode...");
    console.log(JSON.stringify(mode, null, '    ') );
    console.log("...dateRange[0]...");
    console.log(JSON.stringify(dateRange[0], null, '    ') );
    console.log("...dateRange[1]...");
    console.log(JSON.stringify(dateRange[1], null, '    ') ); */

    var year  = selectedDate.getFullYear();
    var month = selectedDate.getMonth();
    var startDate = new Date();  
    var endDate   = new Date();
    var day;  
    switch (mode){
      case 'day':   startDate = dateRange[0]; //first Day Of Date Range
                    endDate   = dateRange[1]; //last Day Of Date Range
                    break;
      case 'yearBefore':  day = selectedDate.getDate();                
                    endDate   = new Date(year, month, 1); //first day current month
                    startDate = new Date(year-1,month, 1); //one year before end date
                    break; 
      case 'range': startDate = dateRange[0]; //first Day Of Date Range
                    endDate   = dateRange[1]; //last Day Of Date Range
                    break; 
      case 'range1year':  day = selectedDate.getDate();
                    if (month > 5) {
                      startDate = new Date(year,0, 1); //second semester, start january this year
                      endDate   = new Date(year+1, 5, 30); //end june next year
                    } else {
                      startDate = new Date(year-1, 6, 1); //first semester, start july year before
                      endDate   = new Date(year, 11, 31); //end december this year
                    }
                    break;                    
      case 'month': startDate = new Date(year,month,1); //firstDayOfMonth
                    endDate   = new Date(year,month+1,0); //lastDayOfMonth
                    break;
      case 'year':  day = selectedDate.getDate();
                    if (month > 5) {
                      startDate = new Date(year, month-6, day); //startCurrentYear
                      endDate   = new Date(year+1, month, day); //endCurrentYear
                    } else {
                      startDate = new Date(year-1, month - 6, day); //
                      endDate   = new Date(year, month+6, day); //endCurrentYear
                    }
                    break;
      case '2year': day = selectedDate.getDate();
                    startDate = new Date(year - 1, month, day); //startCurrentYear
                    endDate   = new Date(year + 1, month, day); //endCurrentYear
                    break;
      case 'period':day = selectedDate.getDate();
                    startDate = new Date(year - 5, month, day); //fiveYearsBefore
                    endDate   = new Date(year + 5, month, day); //fiveYearsAfter
                    break;
      default:      day = selectedDate.getDate();
                    startDate = new Date(year - 5, month, day); //fiveYearsBefore
                    endDate   = new Date(year + 5, month, day); //fiveYearsAfter
                    break;
    }
    /*
    console.log("...startDate...");
    console.log(JSON.stringify(startDate, null, '    ') );
    console.log("...endDate...");
    console.log(JSON.stringify(endDate, null, '    ') );*/
    var dateContainerClass  = 'theme-dark-blue';
    var dateInputFormat     = 'DD-MMM-YYYY';
    var rangeInputFormat    = 'DD-MMM-YYYY';
    this.localMinMode       =  "day";
    if ((mode === 'range') || (mode === 'range1year')) {
      this.localDatePickerConfig = Object.assign ( {}, {
        containerClass      :dateContainerClass,
        showWeekNumbers     :true, 
        minDate             :startDate,      
        maxDate             :endDate,
        dateInputFormat     :dateInputFormat,
        rangeInputFormat    :rangeInputFormat,
        minMode             :this.localMinMode,
        adaptivePosition    :true,
        isAnimated          :true,
        } );
    } else {
      this.localDatePickerConfig = Object.assign ( {}, {
        containerClass      :dateContainerClass,
        showWeekNumbers     :true, 
        minDate             :startDate,      
        maxDate             :endDate,
        dateInputFormat     :dateInputFormat,
        minMode             :this.localMinMode,
        adaptivePosition    :true,
        isAnimated          :true,
        } );
    }
  }

  getButtonClass(msgType:string){
    var classList='';
    switch (msgType){
                                //class="btn btn-outline-danger rounded"
      case 'deleteMsg': { classList = 'btn btn-outline-primary rounded'; break; }
      case 'errorMsg':  { classList = 'btn btn-outline-danger  rounded'; break; }
      case 'infoMsg':   { classList = 'btn btn-outline-primary rounded'; break; }
      default:          { classList = 'btn btn-outline-danger  rounded'; break; }
    };
    return classList;
  }
  getCardClass(msgType:string){
    var classList='';
    switch (msgType){
                                    // class="card bg-error text-danger rounded border border-danger text-center"
      case 'deleteMsg': { classList = 'card bg-delete  text-primary rounded border border-primary text-center'; break; }
      case 'errorMsg':  { classList = 'card bg-error  text-danger  rounded border border-danger  text-center'; break; }
      case 'infoMsg':   { classList = 'card bg-msg    text-primary rounded border border-primary text-center'; break; }
      default:          { classList = 'card bg-error  text-danger  rounded border border-danger  text-center'; break; }
    };
    return classList;
  }
  getCardBodyClass(msgType:string){
    var classList='';
    switch (msgType){
                                    // class="card-body"
      case 'deleteMsg': { classList = 'card-body'; break; }
      case 'errorMsg':  { classList = 'card-body'; break; }
      case 'infoMsg':   { classList = 'card-body'; break; }
      default:          { classList = 'card-body'; break; }
    };
    return classList;
  }
  getCardHeaderClass(msgType:string){
    var classList='';
    switch (msgType){
                                //class="card-header" 
      case 'deleteMsg': { classList = 'card-header bg-delete text-primary rounded border border-primary'; break; }
      case 'errorMsg':  { classList = 'card-header bg-error text-danger  rounded border border-danger  text-center'; break; }
      case 'infoMsg':   { classList = 'card-header bg-msg   text-primary rounded border border-primary text-center'; break; }
      default:          { classList = 'card-header bg-error text-danger  rounded border border-danger  text-center'; break; }
    };
    return classList;
  }
  getCardFooterClass(msgType:string){
    var classList='';
    switch (msgType){
                                //class="card-footer" 
      case 'deleteMsg': { classList = 'card-footer bg-delete text-primary rounded border border-primary'; break; }
      case 'errorMsg':  { classList = 'card-footer bg-error text-danger  rounded border border-danger  text-center'; break; }
      case 'infoMsg':   { classList = 'card-footer bg-msg   text-primary rounded border border-primary text-center'; break; }
      default:          { classList = 'card-footer bg-error text-danger  rounded border border-danger  text-center'; break; }
    };
    return classList;
  }   
  getNavbarClass(msgType:string){
    var classList='';
    switch (msgType){
                        //class="navbar navbar-expand-lg navbar-light text-primary bg-facts rounded border border-primary"
      case 'deleteMsg': { classList = 'navbar navbar-expand-lg navbar-light text-primary bg-delete rounded border border-primary'; break; }
      case 'errorMsg':  { classList = 'navbar navbar-expand-lg navbar-light text-danger  bg-error rounded border border-primary'; break; }
      case 'infoMsg':   { classList = 'navbar navbar-expand-lg navbar-light text-danger  bg-msg   rounded border border-primary'; break; }
      default:          { classList = 'navbar navbar-expand-lg navbar-light text-danger  bg-error rounded border border-primary'; break; }
    };
    return classList;
  }
  getNavbarBrandClass(msgType:string){
    var classList='';
    switch (msgType){
                        //class="navbar-brand text-primary bg-facts rounded"
      case 'deleteMsg': { classList = 'navbar-brand bg-delete text-primary rounded'; break; }
      case 'errorMsg':  { classList = 'navbar-brand bg-error text-danger  rounded'; break; }
      case 'infoMsg':   { classList = 'navbar-brand bg-msg   text-primary rounded'; break; }
      default:          { classList = 'navbar-brand bg-error text-danger  rounded'; break; }
    };
    return classList;
  }
  getTableClass(msgType:string){
    var classList='';
    switch (msgType){
                        //class="table table-hover bg-enterDate" 
      case 'deleteMsg': { classList = 'table table-hover bg-delete text-primary'; break; }
      case 'errorMsg':  { classList = 'table table-hover bg-error  text-danger'; break; }
      case 'infoMsg':   { classList = 'table table-hover bg-msg    text-primary'; break; }
      default:          { classList = 'table table-hover bg-error  text-danger'; break; }
    };
    return classList;
  }
}