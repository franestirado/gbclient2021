import { Component, OnInit } from '@angular/core';
import { FormGroup,FormBuilder } from '@angular/forms';
import { Subject } from 'rxjs';

import { BsModalRef } from 'ngx-bootstrap/modal';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';

import { GlobalVarService } from './global-var.service';
class DateObject {
    inputDate : Date;    
  }
@Component({
  selector: 'app-enter-date',
  template: `
  <div class="card bg-enterDate text-primary rounded border border-primary text-center">
      <div class="card-header">{{dateTitle}}</div>
      <div class="card-body">
        <div ng-if="!dateMessage">{{dateMessage}}</div>
        <div [formGroup]="dateForm">
            <div class="form-row ">
                <div class="form-group row">
                    <label for="inputDate" class="col-5 col-form-label">{{dateLabel}}</label>
                    <input type="text" class="col-5 form-control" placeholder="Datepicker" bsDatepicker [bsConfig]="datePickerConfig"                    
                            value="{{ dateObjet.inputDate | date:'dd-MM-yyyy' }}" formControlName="inputDate" />
                </div>
                <div class="col-2">     
                    <div class="btn-group ml-auto" role="group">
                        <button type="button" class="btn btn-primary" (click)="saveDateBtnClick()"><span><i class="far fa-check-circle"></i></span></button> 
                    </div>
                </div>
            </div>
        </div>
      </div>
      <div class="card-footer">   
        <div ng-if="!dateFooter"> <p>{{dateFooter}}</p> </div>        
      </div>       
  </div>
  `
})

export class EnterDateComponent implements OnInit {
  public dateTitle            :string;
  public dateMessage          :string;
  public dateLabel            :string;  
  public dateFooter           :string;
  public dateContainerClass   :string;
  public inputDate            :Date;
  public minDate              :Date;
  public maxDate              :Date;
  public dateInputFormat      :string;
  callback                    :any;
  result: Subject<any> = new Subject<any>();

  datePickerConfig: Partial <BsDatepickerConfig>;  
  dateForm: FormGroup;
  dateObjet: DateObject;

  dateErrorRequired:    'Hay que introducir una fecha';

  constructor ( public bsModalRef: BsModalRef, public globalVar: GlobalVarService,  private fb: FormBuilder ) {  }
 
  ngOnInit(): void {
    this.dateForm = this.fb.group({
        inputDate: null,
      });    
    this.dateForm.get('inputDate').setValue(this.globalVar.workingDate);
    this.dateObjet = new DateObject();
    this.datePickerConfig = Object.assign ( {}, {
        containerClass:     this.dateContainerClass,
        showWeekNumbers:    true,
        minDate:            this.minDate,      
        maxDate:            this.maxDate,
        dateInputFormat:    this.dateInputFormat
        } );
    this.dateObjet.inputDate = this.inputDate;    
  }
  saveDateBtnClick() {
    if (this.bsModalRef.content.callback != null){
      //this.bsModalRef.content.callback(this.dateForm.get('inputDate').value);
      this.result.next(this.dateForm.get('inputDate').value);
      this.bsModalRef.hide();
    }
  }
}