import {Directive, HostBinding, Self} from '@angular/core';
import {NgControl} from '@angular/forms';

@Directive({
    selector: '[formControlName]',
})
export class ValidInputBootstrapCssDirective {
    constructor(@Self() private cd: NgControl) {}

    @HostBinding('class.border-success')
    get isInvalid(): boolean {
        const control = this.cd.control;
        return control ? control.valid && control.touched : false;
    }
    // [class.text-danger] class.is-invalid class.border-danger
    // '[col-form-label][form-control][formControlName],[ngModel],[formControl]',
    // console.log(JSON.stringify('BootstrapValidationCssDirective......control........'+control));
    //console.log(JSON.stringify(control.value, null, '    ') );
}