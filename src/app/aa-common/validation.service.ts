import { AbstractControl } from '@angular/forms';
export class ValidationService {

/*-------------------------------- Error Messages --------------------------------------------------------------*/
static getValidatorErrorMessage(validatorName: string, validatorValue?: any) {
  let config = {
    'required'                :'Valor Requerido',
    'positiveIntegerAllowed'  :'Números enteros positivos',
    'percentageAllowed'       :'Porcentaje xx.xx %',
    'twoDecimalAllowed'       :'Números 2 decimales..',
    'twoDecimalsAllowed'      :'Números 2 decimales',
    'threeDecimalsAllowed'    :'Números 3 decimales',
    'fourDecimalsAllowed'     :'Números 4 decimales',
    'sixDecimalsAllowed'      :'Números 6 decimales',
    'dateFormat'              :'Fecha Incorrecta',
    'invalidNumber'           :'Sólo números',
    'invalidCIF'              :'CIF sólo formato X99999999',
    'invalidNIF'              :'NIF sólo formato 99999999X',    
    'invalidNIE'              :'NIE sólo formato  ',
    'defaultSelected'         :'Seleccionar uno',
    'invalidEmail'            :'EMAIL sólo formato  ',
    'invalidWeb'              :'WEB sólo formato  ',
    'invalidÑ_Mobile'         :'MÓVIL ESPAÑA sólo formato  ',
    'invalidÑ_Phone'          :'FIJO ESPAÑA sólo formato  ',
    'invalidMobile'           :'MÓVIL INERNACIONAL sólo formato  ',
    'invalidPhone'            :'FIJO INTERNACIONAL sólo formato  ',
    'invalidCreditCard'       :'Is invalid credit card number',
    'invalidEmailAddress'     :'Invalid email address',
    'invalidPassword'         :'New password and confirm password does not match',
    'invalidDob'              :'User must be minimum 16 Years old.',
    'invalidUrl'              :'Invalid URL',
    'alphaNumericAllowed'     :'Only apha numeric input is allowed',
    'numericAllowed'          :'Números enteros',
    'emailTaken'              :'Email id already taken',
    'minlength'               :`Mínimo ${validatorValue.requiredLength} caracteres`,
    'maxlength'               :`Máximo ${validatorValue.requiredLength} caracteres`
  };
  return config[validatorName];
}
/*------------------------------- dateValidation --------------------------------------------------------------*/
// ng-pattern="/^(31[ \/ ](0[13578]|1[02])[ \/ ](18|19|20)[0-9]{2})|((29|30)[\/](01|0[3-9]|1[1-2])[\/](18|19|20)[0-9]{2})|((0[1-9]|1[0-9]|2[0-8])[\/](0[1-9]|1[0-2])[\/](18|19|20)[0-9]{2})|(29[\/](02)[\/](((18|19|20)(04|08|[2468][048]|[13579][26]))|2000))$/"
//  /^\d{1,2}\.\d{1,2}\.\d{4}$/g
static dateValidation(control: AbstractControl) {
  console.log(JSON.stringify('dateValidation......control.value........'+control.value));
  let myDate = new Date(control.value);
  console.log(JSON.stringify('dateValidation......myDate........'+myDate));
  if ( (myDate instanceof Date) && ( !isNaN(myDate.valueOf()) ) ) {
    return null;
  } else {
    return { 'dateFormat': true };
  }
}
/*------------------------------- positiveIntegerValidation --------------------------------------------------------------*/
static positiveIntegerValidation(control: AbstractControl) {
  if (String(control.value).match(/^\d*[1-9]+\d*$/g)) {
    return null;
  } else {
    return { 'positiveIntegerAllowed': true };
  }
}
/*------------------------------- percentageValidation --------------------------------------------------------------*/
static percentageValidation(control: AbstractControl) {
  if (String(control.value).match(/^(?:100(?:\.0(?:0)?)?|\d{1,2}(?:\.\d{1,2})?)$/g)) {
    return null;
  } else {
    return { 'percentageAllowed': true };
  }
} 
/*------------------------------- decimalValidation --------------------------------------------------------------*/
static decimalValidation(control: AbstractControl) {
  if (String(control.value).match(/^-?\d*\.?\d{0,2}$/g)) {
    return null;
  } else {
    return { 'twoDecimalAllowed': true };
  }
}
/*------------------------------- twoDecimalsValidation --------------------------------------------------------------*/
static twoDecimalsValidation(control: AbstractControl) {
  if (String(control.value).match(/^\d*\.?\d{0,2}$/g)) {
    return null;
  } else {
    return { 'twoDecimalsAllowed': true };
  }
}
/*------------------------------- threeDecimalsValidation --------------------------------------------------------------*/
static threeDecimalsValidation(control: AbstractControl) {
  if (String(control.value).match(/^\d*\.?\d{0,3}$/g)) {
    return null;
  } else {
    return { 'threeDecimalsAllowed': true };
  }
}
/*------------------------------- fourDecimalsValidation --------------------------------------------------------------*/
static fourDecimalsValidation(control: AbstractControl) {
  if (String(control.value).match(/^\d*\.?\d{0,4}$/g)) {
    return null;
  } else {
    return { 'fourDecimalsAllowed': true };
  }
}
/*------------------------------- sixDecimalsValidation --------------------------------------------------------------*/
static sixDecimalsValidation(control: AbstractControl) {
  if (String(control.value).match(/^[+-]?\d*\.?\d{0,6}$/g)) {
    return null;
  } else {
    return { 'sixDecimalsAllowed': true };
  }
}
/* ^[0-9]+(\.[0-9]{1,2})?$ 
    ^                         # Start of string
    [0-9]+                   # Require one or more numbers
        (                  # Begin optional group
            \.                # Point must be escaped or it is treated as "any character"
            [0-9]{1,2}      # One or two numbers
                        )?    # End group--signify that it's optional with "?"
                        $   # End of string
*/
/*-------------------------------- numberValidator --------------------------------------------------------------*/
static numberValidator(control: AbstractControl) {
  if (String(control.value).length == 0 || String(control.value).match(/^[0-9]*$/)) {
    return null;
  } else {
    return { 'numericAllowed': true };
  }
}
/*-------------------------------- valid Nif --------------------------------------------------------------*/
static validNif(parameter: string) {
  return (control: AbstractControl): { [key: string]: any } | null => {
    var nif: string = control.value;
    if (nif === '') {
      return null
    } else {
      var niePrefix,
      nifRegex = /^[0-9]{8}[TRWAGMYFPDXBNJZSQVHLCKE]{1}$/i,
      nifLetters = 'TRWAGMYFPDXBNJZSQVHLCKE',
      valid = false;
      nif = nif.toUpperCase();
      if (9 === nif.length) {
        if (nif.match(nifRegex)) {
          valid = (nif.charAt(8) === nifLetters.charAt(parseInt(nif, 10) % 23));
        }
      }
      if (valid) {
        return null;
      } else {
        return { 'invalidNIF': true };
      }
    }
  }
}
/* --------------------------------valid Nie------------------------------------------------------------------*/
static validNie(parameter: string) {
  return (control: AbstractControl): { [key: string]: any } | null => {
    var nie: string = control.value;
    if (nie === '') {
      return null
    } else {
      var niePrefix = null,
      nifRegex = /^[0-9]{8}[TRWAGMYFPDXBNJZSQVHLCKET]{1}$/i,
      nifLetters = 'TRWAGMYFPDXBNJZSQVHLCKE',
      valid = false;
      nie = nie.toUpperCase();
      if (9 === nie.length) {
        niePrefix = nie.charAt(0);
        switch (niePrefix) {
          case 'X':  niePrefix = 0;
                     break;
          case 'Y':  niePrefix = 1;
                     break;
          case 'Z':  niePrefix = 2;
                     break;
          default:   niePrefix = null
                     break;
        }
        nie = niePrefix + nie.substr(1);
        if (nie.match(nifRegex)) {
          valid = (nie.charAt(8) === nifLetters.charAt(parseInt(nie, 10) % 23));
        }
      }
      if (valid) {
        return null;
      } else {
        return { 'invalidNIE': true };
      }
    }
  }
}
/* --------------------------------valid Cif------------------------------------------------------------------*/
static validCif(parameter: string) {
  return (controlForm: AbstractControl): { [key: string]: any } | null => {
    var cif: string = controlForm.value;
    if (cif === '') {
      return null
    } else {
      var control, sum_a, sum_b, aux_a, aux_b, sum_c, sum_d, letters,
      r_expression1 = /^[ABEH][0-9]{8}$/i,
      r_expression2 = /^[KPQS][0-9]{7}[A-J]$/i,
      r_expression3 = /^[CDFGJLMNRUVW][0-9]{7}[0-9A-J]$/i,
      valid = false;
      cif = cif.toUpperCase();
      if (cif.match(r_expression1) || cif.match(r_expression2) || cif.match(r_expression3)) {
        control = cif[cif.length - 1];
        sum_a = 0;
        sum_b = 0;
        for (var i = 1; i < 8; i++) {
          if (i % 2 == 0) {
            sum_a += parseInt(cif[i], 10);
          } else {
            aux_a = parseInt(cif[i], 10) * 2;
            aux_b = 0;
            if (aux_a > 9) {
              aux_b = 1 + (aux_a - 10);
            } else {
              aux_b = aux_a;
            }
            sum_b += aux_b;
          }
        }
        sum_c = (parseInt(sum_a + sum_b, 10)) + "";
        sum_d = (10 - (parseInt(sum_c[sum_c.length - 1], 10))) % 10;
        letters = "JABCDEFGHI";
        if (control >= "0" && control <= "9") {
          if (control == sum_d) {
            valid = true;
          } else {
            valid = false;
          }
        } else {
          if ((control.toUpperCase()) == letters[sum_d]) {
            valid = true;
          } else {
            valid = false;
          }
        }
      } else {
        valid = false;
      }
      if (valid) {
        return null;
      } else {
        return { 'invalidCIF': true };
      }
    }
  }
}
/*--------------------------------valid EMAIL------------------------------------------------------------------*/
static validEmail(parameter: string) {
  return (control: AbstractControl): { [key: string]: any } | null => {
    var email: string = control.value;
    /*
    console.log(JSON.stringify('validEmail......parameter........'+parameter));
    console.log(JSON.stringify('validEmail......email........'+email));
    */
    const validEmailRegEx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (validEmailRegEx.test(email)) {       
      return null;        
    }else {
      return { 'invalidEmail': true };      
    }
  }
}
/*--------------------------------valid WEB------------------------------------------------------------------*/
static validWeb(parameter: string) {
  return (control: AbstractControl): { [key: string]: any } | null => {
    var webAddress: string = control.value;        
    const urlRegex = /^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/;
    if (urlRegex.test(webAddress)) {       
      return null;        
    }else {
      return { 'invalidWeb': true };      
    }
  }
}
/*--------------------------------valid Ñ MOBILE------------------------------------------------------------------*/
static validÑ_Mobile(parameter: string) {
  return (control: AbstractControl): { [key: string]: any } | null => {         
    const mobilePhoneRegex = /^34 ?(?:6[0-9]{2}|7[1-9][0-9])(?: ?[0-9]{3}){2}$/;        
    var mobilePhoneNumber: string = control.value;                
    if (mobilePhoneRegex.test(mobilePhoneNumber)) {       
      return null;        
    }else {
      return { 'invalidÑ_Mobile': true };      
    }
  }
}
/*--------------------------------valid Ñ PHONE------------------------------------------------------------------*/
static validÑ_Phone(parameter: string) {
  return (control: AbstractControl): { [key: string]: any } | null => {
    var phoneno = /^\+?([0-9]{2})\)?[-. ]?([0-9]{4})[-. ]?([0-9]{4})$/;
    const phoneRegex = /^34 ?(?:^[0-9]{2,3}-? ?[0-9]{6,7})$/;
    const phone1Regex = /^\+(?:[0-9] ?){6,14}[0-9]$/;
    var phoneNumber: string = control.value;                
    if (phoneRegex.test(phoneNumber)) {       
      return null;        
    }else {
      return { 'invalidÑ_Phone': true };      
    }
  }
}
/*--------------------------------valid MOBILE------------------------------------------------------------------*/
static validMobile(parameter: string) {
  return (control: AbstractControl): { [key: string]: any } | null => { 
    const mobile1PhoneRegex = /^((\\+91-?)|0)?[0-9]{10}$/; 
    const mobilePhoneRegex = /^34 ?(?:6[0-9]{2}|7[1-9][0-9])(?: ?[0-9]{3}){2}$/;        
    var mobilePhoneNumber: string = control.value;                
    if (mobilePhoneRegex.test(mobilePhoneNumber)) {       
      return null;        
    }else {
      return { 'invalidMobile': true };      
    }
  }
}
/*--------------------------------valid PHONE------------------------------------------------------------------*/
static validPhone(parameter: string) {
  return (control: AbstractControl): { [key: string]: any } | null => {
    var phoneno = /^\+?([0-9]{2})\)?[-. ]?([0-9]{4})[-. ]?([0-9]{4})$/;
    const phoneRegex = /^\+(?:[0-9] ?){6,14}[0-9]$/;
    var phoneNumber: string = control.value;                
    if (phoneRegex.test(phoneNumber)) {       
      return null;        
    }else {
      return { 'invalidPhone': true };      
    }
  }
}




/* --------------------------------  --------------------------------------------------------------*/
static creditCardValidator(control: AbstractControl) {
    // Visa, MasterCard, American Express, Diners Club, Discover, JCB
    if (control.value.match(/^(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\d{3})\d{11})$/)) {
        return null;
    } else {
        return { 'invalidCreditCard': true };
    }
}
/* --------------------------------  --------------------------------------------------------------*/
static emailValidator(control: AbstractControl) {
    if (control.value.length == 0 || control.value.match(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/)) {
        return null;
    } else {
        return { 'invalidEmailAddress': true };
    }
}
/* --------------------------------  --------------------------------------------------------------*/
static alpaNumValidator(control: AbstractControl) {
    if (String(control.value).match(/^[a-zA-Z0-9]*$/)) {
        return null;
    } else {
        return { 'alphaNumericAllowed': true };
    }
}
/* --------------------------------  --------------------------------------------------------------*/
// function to validate that dob should be 16 years old
static dobValidator(control: AbstractControl) {
    let currentDate = new Date();
    if (control.value) {
        let dob = new Date(control.value);
        let dobYear = dob.getFullYear();
        let maxDobYear = currentDate.getFullYear() - 16;
        //console.log(dobYear, maxDobYear)
        if (maxDobYear < dobYear) {
            return { 'invalidDob': true };
        }
        else {
            return null
        }
    }
}
/* --------------------------------  --------------------------------------------------------------*/
static urlValidator(control: AbstractControl) {
    const URL_REGEXP = /^(http?|ftp):\/\/([a-zA-Z0-9.-]+(:[a-zA-Z0-9.&%$-]+)*@)*((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9][0-9]?)(\.(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9]?[0-9])){3}|([a-zA-Z0-9-]+\.)*[a-zA-Z0-9-]+\.(com|in|edu|gov|int|mil|net|org|biz|arpa|info|name|pro|aero|coop|museum|[a-zA-Z]{2}))(:[0-9]+)*(\/($|[a-zA-Z0-9.,?'\\+&%$#=~_-]+))*$/;
    if (control.value.match(URL_REGEXP)) {
        return null;
    } else {
        return { 'invalidUrl': true };
    }
}
/* --------------------------------  --------------------------------------------------------------*/
static confirmPasswordValidator(control: AbstractControl) {
    const password: string = control.get('password').value; // get password from our password form control
    const confirmPassword: string = control.get('confirmPassword').value; // get password from our confirmPassword form control
    if (password !== confirmPassword) {
        control.get('confirmPassword').setErrors({ invalidPassword: true });
    }
    return null
}

}