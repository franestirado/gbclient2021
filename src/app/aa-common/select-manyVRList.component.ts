import { Component, OnInit } from '@angular/core';
import { throwError, Subject  } from 'rxjs';

import { OrderPipe } from 'ngx-order-pipe';
import { FormGroup,FormBuilder } from '@angular/forms';
import { ValidationService } from '../aa-common/validation.service';

import { BsModalRef } from 'ngx-bootstrap/modal';
import { BsDatepickerConfig, BsDatepickerViewMode } from 'ngx-bootstrap/datepicker';

import { ValueRecord } from '../dbc-fields/field';
import { GlobalVarService } from '../aa-common/global-var.service';
import { Prov } from '../dbc-provs/prov';
import { ProvsService } from '../dbc-provs/provs.service';

@Component({
  selector      :'app-select-manyVRList',
  templateUrl   :'./select-manyVRList.component.html',
  styleUrls     :['./common.component.css'] 
})

export class SelectManyVRListComponent implements OnInit {
  public  selectOption          :number;
  public  selectTitle1          :string;
  public  selectTitle2          :string;
  public  selectMessage         :string;
  public  selectFooter          :string;
  public  callback              :any;
  result: Subject<any> = new Subject<any>();

  public  formLabels = {
    'selectOneDate'             :'Día:',
    'selectDateRange'           :'Rango Fechas:',
    'selectProv'                :'Proveedor:',
    'selectMaqB'                :'Máquina B:',
    'selectFactType'            :'Tipo Factura:',
    'selectPayType'             :'Forma Pago:',
    'selectFactOrNote'          :'Factura o Nota:',
  }
  public  localProvsRecordList          :ValueRecord [];
  public  localFactTypesRecordList      :ValueRecord [];
  public  localPayFactTypesRecordList   :ValueRecord [];
  public  localFactOrNoteRecordList     :ValueRecord [];
  public  localMaqBRecordList           :ValueRecord [];
  public  resultValueRecordList         :ValueRecord [];
  public  selectRecordForm      :FormGroup;
  private selDataRange          :Date[];
  private selProvName           :string;
  private selMaqBName           :string;
  private selFactType           :string;
  private selPayType            :string;
  private selFactOrNote         :string;
  public  showSelects           :boolean;
  public  provs                 :Prov[];
  public  orderProvsList        :string;
  public  reverseProvsList      :boolean;
  public  caseInsensitive       :boolean;
  private sortedCollection      :any[];
  private logToConsole          :boolean;
  public  selectDatePickerConfig:Partial <BsDatepickerConfig>;
  public  localDateRange        :Date[]; 
  public  currentDate           :Date;
  public  localWorkingDate      :Date;
  private localMinMode          :BsDatepickerViewMode;
  private dateContainerClass    :string;
  private dateInputFormat       :string;

  constructor ( public bsModalRef: BsModalRef, private fb: FormBuilder, private orderPipe: OrderPipe,
                public globalVar: GlobalVarService, private _provsService: ProvsService ) {  }
                
  ngOnInit(): void {
    this.logToConsole = this.globalVar.logToConsole;
    this.logToConsole = true;
    this.setDatesData();
    this.showSelects = false;
    this.localFactTypesRecordList       = this.globalVar.factTypesSubDepsRecordList;
    this.localPayFactTypesRecordList    = this.globalVar.factPayTypesRecordList;
    this.localMaqBRecordList            = this.globalVar.maqBRecordList;
    this.localFactOrNoteRecordList      = this.globalVar.prepareRecordListFromArrayOfStrings(["Factura","Nota"]);  
    this.selectRecordForm = this.fb.group({
        oneDate:        [{value:'',disabled:false}],
        dateRange:      [{value:'',disabled:false}],
        maqBName:       [{value:'',disabled:false}],
        provName:       [{value:'',disabled:false}],
        factType:       [{value:'',disabled:false}],
        payType:        [{value:'',disabled:false}],
        factOrNote:     [{value:'',disabled:false}],
    });    
    this.localProvsRecordList = new Array();
    this.globalVar.consoleLog(this.logToConsole,'->SELECT MANY VR LIST->OnInit->...Exiting...->', null);        
    this.getAllProvs();
  }
  /*--------------------------------------- SELECT MANY VR LIST -- FORMS-----------------------------------------------------------*/
  setDatesData(){
    this.globalVar.consoleLog(this.logToConsole,'->SELECT MANY VR LIST->setDatesData->...Starting...->', null);        
    this.currentDate = new Date();
    this.localWorkingDate = this.globalVar.workingDate;
    this.localDateRange = this.globalVar.workingFirstAndLastDayOfYear  
    this.dateInputFormat = 'DD-MMM-YYYY';
    this.dateContainerClass = 'theme-dark-blue';
    var selectedDate = this.localWorkingDate;
    var year = selectedDate.getFullYear();
    var month = selectedDate.getMonth();
    var day = selectedDate.getDate();
    var fiveYearsBefore = new Date(year - 5, month, day);
    var fiveYearsAfter = new Date(year + 5, month, day);
    this.localMinMode = "day";
    this.selectDatePickerConfig = Object.assign ( {}, {
        containerClass:     this.dateContainerClass,
        showWeekNumbers:    true, 
        minDate:            fiveYearsBefore,      
        maxDate:            fiveYearsAfter,
        dateInputFormat:    this.dateInputFormat,
        minMode:            this.localMinMode,
        adaptivePosition:   true,
        isAnimated:         true,
        } );          
  }
  setInitialFormValues(){
    this.globalVar.consoleLog(this.logToConsole,'->SELECT MANY VR LIST->setInitialFormValues->...Starting...->', null);   
    this.selectRecordForm.get('oneDate').setValue(      this.localWorkingDate);
    this.selectRecordForm.get('dateRange').setValue(    this.localDateRange);     
    this.selectRecordForm.get('provName').patchValue(   this.localProvsRecordList[0].value);
    this.selectRecordForm.get('maqBName').patchValue(   this.localMaqBRecordList[0].value);
    this.selectRecordForm.get('factType').patchValue(   this.localFactTypesRecordList[0].value);
    this.selectRecordForm.get('payType').patchValue(    this.localPayFactTypesRecordList[0].value);
    this.selectRecordForm.get('factOrNote').patchValue( this.localFactOrNoteRecordList[0].value);
    this.showSelects = true;
  }
  closeSelectValueRecordListModalBtnClick(){
    if (this.bsModalRef.content.callback != null){
        //this.bsModalRef.content.callback(null);
        this.result.next(null);
        this.bsModalRef.hide();
      } 
  }
  saveSelectedValueRecordBtnClick() {
    this.globalVar.consoleLog(this.logToConsole,'->SELECT MANY VR LIST->saveSelectedValueRecordBtnClick->...Starting...->', null);   
    if (this.bsModalRef.content.callback != null){
      this.selDataRange   = this.selectRecordForm.get('dateRange').value;
      this.selMaqBName    = this.selectRecordForm.get('maqBName').value;
      this.selProvName    = this.selectRecordForm.get('provName').value;
      this.selFactType    = this.selectRecordForm.get('factType').value;
      this.selPayType     = this.selectRecordForm.get('payType').value;
      this.selFactOrNote  = this.selectRecordForm.get('factOrNote').value;
      this.resultValueRecordList = new Array();
      let index = 0;
      if ( (this.selectOption === 0) || (this.selectOption === 1) ) {
        this.resultValueRecordList[index] = new ValueRecord();
        this.resultValueRecordList[index].id = index;
        this.resultValueRecordList[index].value = this.globalVar.transformDate(this.selDataRange[0]); 
        index = index + 1;
        this.resultValueRecordList[index] = new ValueRecord();
        this.resultValueRecordList[index].id = index;
        this.resultValueRecordList[index].value = this.globalVar.transformDate(this.selDataRange[1]); 
        index = index + 1;
      }
      if ( (this.selectOption === 1) ) {
        this.resultValueRecordList[index] = new ValueRecord();
        this.resultValueRecordList[index].id = index;
        var positionInRecordList = this.getIndexOfStringInValueRecord(this.selMaqBName,this.localMaqBRecordList);
        if (positionInRecordList < 1) this.resultValueRecordList[index].value = "";
        else this.resultValueRecordList[index].value = this.selMaqBName;
        index = index + 1;
      }
      if ( (this.selectOption === 0) ) {
        this.resultValueRecordList[index] = new ValueRecord();
        this.resultValueRecordList[index].id = index;
        var positionInRecordList = this.getIndexOfStringInValueRecord(this.selProvName,this.localProvsRecordList);
        if (positionInRecordList < 1) this.resultValueRecordList[index].value = "";
        else this.resultValueRecordList[index].value = this.selProvName;
        index = index + 1;
        this.resultValueRecordList[index] = new ValueRecord();
        this.resultValueRecordList[index].id = index;
        var positionInRecordList = this.getIndexOfStringInValueRecord(this.selFactType,this.localFactTypesRecordList);
        if (positionInRecordList < 1) this.resultValueRecordList[index].value = "";
        else this.resultValueRecordList[index].value = this.globalVar.getSubDepTagFromSubDepName(this.selFactType);
        index = index + 1;
        this.resultValueRecordList[index] = new ValueRecord();
        this.resultValueRecordList[index].id = index;
        var positionInRecordList = this.getIndexOfStringInValueRecord(this.selPayType,this.localPayFactTypesRecordList);
        if (positionInRecordList < 1) this.resultValueRecordList[index].value = "";
        else this.resultValueRecordList[index].value = this.selPayType;
        index = index + 1;
        this.resultValueRecordList[index] = new ValueRecord();
        this.resultValueRecordList[index].id = index;
        var positionInRecordList = this.getIndexOfStringInValueRecord(this.selFactOrNote,this.localFactOrNoteRecordList);
        if (positionInRecordList < 1) this.resultValueRecordList[index].value = "";
        else this.resultValueRecordList[index].value = this.selFactOrNote;
      }
      //this.bsModalRef.content.callback(this.resultValueRecordList);
      this.result.next(this.resultValueRecordList);
      this.bsModalRef.hide();
    }
  }
  getIndexOfStringInValueRecord(selName :string, selRecordList :ValueRecord[]):number{
    let index = -1;
    for (let i=0; i<selRecordList.length; i++) {
      if (selRecordList[i].value === selName) index = i;
    }
    return (index);
  }
  /*---------------------------------------SELECT MANY VR LIST -- PROVS  -- DATABASE-----------------------------------------------------------*/   
  getAllProvs() {
    this.globalVar.consoleLog(this.logToConsole,'->SELECT MANY VR LIST->getAllProvs->...Starting->', null);        
    this._provsService.getAllProvs()
        .subscribe(data => {
                            this.provs = data;
                            this.orderProvsList = 'provName';
                            this.reverseProvsList = false;
                            this.sortedCollection = this.orderPipe.transform(this.provs, 'provName');
                            this.provs = this.sortedCollection;
                            this.localProvsRecordList = this.globalVar.prepareProvsValueRecordList(this.provs);
                            this.setInitialFormValues();
                            },
                error => {
                            if (this.globalVar.handleError(error)) { throwError(error) };
                            });
  }
}