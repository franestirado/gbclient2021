import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Subject } from 'rxjs';

import { BsModalRef } from 'ngx-bootstrap/modal';
import { ValidationService } from './validation.service';
import { ValueRecord } from '../dbc-fields/field';
@Component({
  selector: 'app-enter-select-decimals',
  templateUrl: './enter-select-decimals.component.html',
  styleUrls: ['./common.component.css']
})
export class EnterSelectDecimalsComponent implements OnInit { 
  /*-------------------------------------- ENTER DECIMAL SELECT -- INPUT PARAMETERS-----------------------------------------------------------*/
  public displayMessage1          :string;
  public displayMessage2          :string;
  public displayMessage3          :string;
  public selectHdTypesRecordList  :ValueRecord [];
  public displayLabel             :string;  
  public displayNumber            :number;
  public callback   :any;
  //result: Subject<any> = new Subject<any>();


  public displayForm : FormGroup;
  /*--------------------------------------- ENTER DECIMAL SELECT -- CONSTRUCTOR-----------------------------------------------------------*/
  constructor(public bsModalRef: BsModalRef, private fb:FormBuilder,) { }
  /*--------------------------------------- ENTER DECIMAL SELECT -- NG ON INIT-----------------------------------------------------------*/
  ngOnInit(): void {    
    this.displayForm = this.fb.group({     
      hdSumSelected:  [ {value: this.displayLabel}, [Validators.required] ],
      updateNumber: [ {value: this.displayNumber}, [Validators.required, ValidationService.decimalValidation] ],
    })
    this.displayForm.get('hdSumSelected').setValue(this.selectHdTypesRecordList[0].value);
    if (this.displayNumber === 0) {
      this.displayForm.get('updateNumber').setValue('');       
    } else {
      this.displayForm.get('updateNumber').setValue(this.displayNumber.toFixed(2));   
    }
  }
    
  returnNumber() {
    if (this.bsModalRef.content.callback != null){
      this.displayLabel  = this.displayForm.get('hdSumSelected').value;
      this.displayNumber = this.displayForm.get('updateNumber').value;
      this.bsModalRef.content.callback(this.displayLabel,this.displayNumber);
      //this.result.next(this.displayLabel,this.displayNumber);
      this.bsModalRef.hide();
    }
  }
  closeModalBtnClick(){
    if (this.bsModalRef.content.callback != null){
        this.bsModalRef.content.callback(null, null);
        this.bsModalRef.hide();
      }
  }
}