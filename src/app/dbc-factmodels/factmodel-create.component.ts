import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { OrderPipe } from 'ngx-order-pipe';
import { throwError, Subject } from 'rxjs';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';

import { FactModels } from './factmodel';
import { FactModelsService } from './factmodel.service';
import { Art } from '../dbc-arts/art';
import { Prov } from '../dbc-provs/prov';
import { ProvsSelectComponent } from '../dbc-provs/provs-select.component';
import { ArtsSelectComponent } from '../dbc-arts/arts-select.component';
import { ArtsCreateComponent } from '../dbc-arts/arts-create.component';

import { SubDep } from '../dbc-arts/art';
import { ValueRecord } from '../dbc-fields/field';
import { GlobalVarService } from '../aa-common/global-var.service';

@Component({
  selector      :'app-factmodel-create',
  templateUrl   :'./factmodel-create.component.html',
  styleUrls     :['./factmodel-create.component.css']  
})
export class FactModelCreateComponent implements OnInit {
  public  newFactModel          :FactModels;
  public  modelsTitle           :string;
  public  modelsDate            :Date;
  public  showEditFactModel     :boolean;  
  public  showCreateFactModel   :boolean;
  callback: any;  
  result: Subject<any> = new Subject<any>();

  public  formTitles = {
    'modelsSelTitle'      :'Seleccionar Modelo Factura de Proveedor',
    'modelsTitle'         :'',   
    'modelsDate'          :'',
    'factModelssList'     :'Lista de MODELOS para Facturas',
    'factModelss'         :'MODELOS para Facturas',
    'factModelssCreate'   :'Crear MODELO para Facturas del Proveedor',
    'factModelssEdit'     :'Modificar MODELO para Facturas del Proveedor',
    'factModelsArts'      :'Artículos Factura Modelo',
    'factModelsArtsTotal' :'',
    'factModelsDeleteOne' :'¿ Seguro que quiere BORRAR este MODELO de Factura ?',
    'factModelsDeleteAll' :'¿ Seguro que quiere BORRAR TODOS los MODELOS de Facturas ?',
    'factDelete'          :'Borrar',
    'edit'                :'Editar',
  };
  public  formLabels = {
    'factModelsProvId'    :'Prov_Id',
    'factModelsProvName'  :'Proveedor',
    'factModelsId'        :'Model_Id',
    'factModelsDate'      :'Fecha',
    'factModelsName'      :'Nombre Modelo Factura',
    'factModelsFactType'  :'Tipo Factura',
    'factModelsPayType'   :'Forma Pago',
    'factModelsNote'      :'Nota/Fact',
    'factModelName'       :'Modelo',
    'factModelsSelect'    :'Seleccionar',
    'provId'              :'Prov_Id',
    'factProvName'        :'Proveedor',
    'artId'               :'Art_Id',
    'artReference'        :'Referencia',
    'artDate'             :'Fecha',
    'artName'             :'Nombre',
    'artLongName'         :'Descripción',
    'artType'             :'Tipo',
    'artSelect'           :'Seleccionar',
    'artAdd'              :'Añadir',
    'artNameF'            :'Artículo',
    'artUnit'             :'Ud.',
    'artTypeF'            :'Tipo',
    'artSize'             :'Size',
    'artPrice'            :'€/Ud.',            
    'units'               :'Uds/Pk.',
    'price'               :'€/Pk',
    'quantity'            :'Qty',
    'discount'            :'Dcto', 
    'netCost'             :'Coste',
    'artTax'              :'IVA',
    'tax'                 :'Imp',
    'totalCost'           :'Total',   
    };
  public  title                         :string;
  private logToConsole                  :boolean;
  public  factForm                      :FormGroup;
  public  factModelsForm                :FormGroup;
  public  artsFactForm                  :FormGroup;
  public  artsFactFormArray             :FormArray;
  public  localFactTypesRecordList      :ValueRecord [];
  public  subDeps                       :SubDep[];
  public  orderSubDepsList              :string;
  public  reverseSubDepsList            :boolean;
  public  localPayFactTypesRecordList   :ValueRecord [];
  public  showSelectProvFactModel       :boolean;
  public  arts                          :Art[];
  private selectedArts                  :Art[];
  public  factModelsArts                :Art[];
  public  searchArt                     :Art;
  public  showFactModel                 :boolean;
  public  editFactModel                 :boolean;  
  public  createFactModel               :boolean;
  public  showSearchArts                :boolean;
  public  orderArtsList                 :string;
  public  reverseArtsList               :boolean;
  public  caseInsensitive               :boolean;
  public  provs                         :Prov[];
  private selectedProv                  :Prov;
  public  currentDate                   :Date;
  public  factModelssDatePickerConfig   :Partial <BsDatepickerConfig>;
  //---------------------------------------MODEL FACTS CREATE -- CONSTRUCTOR--------------------------------------------------------------
  constructor ( private modalService: BsModalService, public bsModalRef: BsModalRef, private orderPipe: OrderPipe, private fb:FormBuilder,   
                public globalVar: GlobalVarService, private _factModelsService: FactModelsService ) {
    this.factModelssDatePickerConfig = Object.assign ( {}, {
      containerClass    :'theme-dark-blue',
      showWeekNumbers   :true,
      minDate           :this.globalVar.workingPreviousYear,
      maxDate           :this.globalVar.workingNextYear,
      dateInputFormat   :'DD-MM-YYYY'
      } );
  }
  //---------------------------------------MODEL FACTS  CREATE -- NG ON INIT--------------------------------------------------------------
  ngOnInit(): void {
    this.logToConsole = this.globalVar.logToConsole;
    this.logToConsole = true;
    this.globalVar.consoleLog(this.logToConsole,'-> FACTS MODEL CREATE- > ngOnInit->...Starting...->', null);
    this.createFactModel = false;
    this.editFactModel = false;
    if (this.modelsDate === null) {      
      this.currentDate = this.globalVar.currentDate;
      this.formTitles.modelsDate = '';
    } else {
      this.currentDate = this.modelsDate;
      this.formTitles.modelsDate = this.modelsDate.toLocaleDateString();
    }
    this.formTitles.modelsTitle = this.modelsTitle;
    this.factModelsForm = this.fb.group({
        factModelsId         :[''],
        factModelsDate       :[{value:this.currentDate,disabled:true},[Validators.required]],
        factModelsName       :['', [Validators.required, Validators.minLength(1), Validators.maxLength(64)] ],
        factModelsProvId     :['', Validators.required ],
        factModelsProvName   :['', Validators.required ],
        factModelsFactType   :['', [Validators.required, Validators.minLength(1), Validators.maxLength(32)] ],
        factModelsPayType    :['', [Validators.required, Validators.minLength(1), Validators.maxLength(32)] ],
        factModelsNote       :['', Validators.required ],
      })
    this.artsFactFormArray= new FormArray([]);
    this.artsFactForm = new FormGroup({
      artsFactFormArray: this.artsFactFormArray
    });
    this.localFactTypesRecordList = this.globalVar.factTypesSubDepsRecordList;
    this.localPayFactTypesRecordList = this.globalVar.factPayTypesRecordList 
    if ((this.showCreateFactModel === true) && (this.showEditFactModel === false)) {
      this.cleanFactModelFormData();
      this.factModelsForm.get('factModelsDate').patchValue(new Date(this.currentDate));
      this.factModelsForm.get('factModelsPayType').patchValue(this.localPayFactTypesRecordList[0].value);
      this.factModelsArts = new Array();
      this.newFactModel = new FactModels();
      this.createFactModel = true;
      this.formTitles.factModelsArtsTotal = '..'+this.factModelsArts.length+'..Arts';
      this.showFactModel = true;
    }  
    if ((this.showCreateFactModel === false) && (this.showEditFactModel === true)) {
      this.getOneFactModel(this.newFactModel);
    }
    if ((this.showCreateFactModel === true) && (this.showEditFactModel === true)) {
      this.showCreateFactModel = false;
      this.createProvFactModel(this.newFactModel.factModelsProvName);
    }
  }

  //---------------------------------------FACT MODELS  CREATE -- FORMS --------------------------------------------------------------
  copyFactModelFormDataToNewFactModel(){
    this.newFactModel.factModelsDate         = this.factModelsForm.controls.factModelsDate.value;
    this.newFactModel.factModelsName         = this.factModelsForm.controls.factModelsName.value;
    this.newFactModel.factModelsProvId       = this.factModelsForm.controls.factModelsProvId.value;
    this.newFactModel.factModelsProvName     = this.factModelsForm.controls.factModelsProvName.value; 
    this.newFactModel.factModelsFactType     = this.factModelsForm.controls.factModelsFactType.value; 
    this.newFactModel.factModelsPayType      = this.factModelsForm.controls.factModelsPayType.value; 
    this.newFactModel.factModelsNote = this.factModelsForm.controls.factModelsNote.value;  
    
    var factTypeTag = this.globalVar.getSubDepTagFromSubDepName(this.newFactModel.factModelsFactType);
    if (factTypeTag != null) this.newFactModel.factModelsFactType = factTypeTag; 
    else this.newFactModel.factModelsFactType = "NO SubDep Tag";
  }
  copyNewFactModelDataToFactModelForm(){
    this.factModelsForm.patchValue( {factModelsId: this.newFactModel.factModelsId, factModelsName: this.newFactModel.factModelsName, 
                                    factModelsProvId: this.newFactModel.factModelsProvId, factModelsProvName: this.newFactModel.factModelsProvName,
                                    factModelsFactType:this.newFactModel.factModelsFactType, factModelsPayType:this.newFactModel.factModelsPayType,
                                    factModelsNote:this.newFactModel.factModelsNote} ); 
    this.currentDate = this.newFactModel.factModelsDate;
    this.factModelsForm.get('factModelsDate').patchValue(new Date(this.currentDate)); 
    
    var subDepName = this.globalVar.getSubDepNameFromSubDepTag(this.newFactModel.factModelsFactType);
    if (subDepName != null) this.factModelsForm.get('factModelsFactType').patchValue(subDepName);
    else this.factModelsForm.get('factModelsFactType').patchValue(this.localFactTypesRecordList[0].value);
  }
  cleanFactModelFormData(){
    this.factModelsForm.patchValue( {factModelsId:'',factModelsName:'',factModelsProvId:'',factModelsProvName:'',
                                     factModelsFactType:this.localFactTypesRecordList[0].value, factModelsPayType:this.localPayFactTypesRecordList[0].value,
                                     factModelsNote: true} );                    
  }
  //---------------------------------------FACT MODELS  CREATE -- GENERAL --------------------------------------------------------------
  //---------------------------------------FACT MODELS  CREATE -- BTNCLICK--------------------------------------------------------------
  closeFactModelCreateModalBtnClick(){
    if (this.bsModalRef.content.callback != null){
        //this.bsModalRef.content.callback('OK');
        this.result.next('OK');
        this.bsModalRef.hide();
      }
  }
  createProvFactModelBtnClick(){
    this.globalVar.consoleLog(this.logToConsole,'-> FACTS MODEL CREATE- > createProvFactModelBtnClick ->...Starting->', null);
    this.showFactModel = false;
    var selectedProvName = this.factModelsForm.controls.factModelsProvName.value;
    if ((selectedProvName === null) || (selectedProvName === "") ) return;
    this.createProvFactModel(selectedProvName);
  }
  createFactModelBtnClick(factModelsForm: FormGroup){
    this.globalVar.consoleLog(this.logToConsole,'-> FACTS MODEL CREATE- > createFactModelBtnClick ->...Starting->', null);
    if (this.factModelsArts.length ===0) return;
    this.copyFactModelFormDataToNewFactModel();
    this.createOneFactModel(this.newFactModel,this.factModelsArts);  
  }
  updateFactModelBtnClick(factModelsForm: FormGroup){
    this.globalVar.consoleLog(this.logToConsole,'-> FACTS MODEL CREATE- > updateFactModelBtnClick ->...Starting->', null);
    this.showFactModel = false;
    this.copyFactModelFormDataToNewFactModel();
    this.deleteAndCreateOneFactModel(this.newFactModel,this.factModelsArts);   
  }
  addArtFactModelBtnClick(){
    this.globalVar.consoleLog(this.logToConsole,'-> FACTS MODEL CREATE- > addArtFactModelBtnClick ->...Starting->', null);
    const modalInitialState = {
      artsTitle     :'Factura',
      artsDate      :this.modelsDate,
      artsSelMany   :true,
      callback      :'OK',   
    };
    this.globalVar.openModal(ArtsSelectComponent, modalInitialState, 'modalLgTop0Left0').
    then((result:any)=>{
      if (result != null){            
        this.selectedArts = result;
        for(let j=0; j<this.selectedArts.length; j++){
          let found = this.factModelsArts.some(item => item.artId === this.selectedArts[j].artId);
          if (found === false) { this.factModelsArts.push(this.selectedArts[j])}
        }
        this.formTitles.factModelsArtsTotal = '..'+this.factModelsArts.length+'..Arts';
      }
    })
  }
  changeSelectedProvFactBtnClick(){
    const modalInitialState = {
      provTitle   :'Factura',
      provDate    :this.modelsDate,
      callback    :'OK',   
    };
    this.globalVar.openModal(ProvsSelectComponent, modalInitialState, 'modalLgTop0Left0').
    then((result:any)=>{
      if (result != null){            
        this.selectedProv = result;                       
        this.factModelsForm.patchValue( { factModelsProvId: this.selectedProv.provId, factModelsProvName: this.selectedProv.provName,
                                          factModelsFactType: this.selectedProv.provFactType, factModelsPayType: this.selectedProv.provPayType,
                                          factModelsNote: this.selectedProv.provFactNote } ); 
        var subDepName = this.globalVar.getSubDepNameFromSubDepTag(this.selectedProv.provFactType);
        if (subDepName != null) this.factModelsForm.get('factModelsFactType').patchValue(subDepName);
        else this.factModelsForm.get('factModelsFactType').patchValue(this.localFactTypesRecordList[0].value);                                                       
      }
    })
  }
  editArtBtnClick(selArt: Art){
    const modalInitialState = {
      newArt      :selArt,
      artsTitle   :'Model Factura, Editar Artículos',
      artsDate    :null,
      createArt   :false,
      callback    :'OK',   
    };
    this.globalVar.openModal(ArtsCreateComponent, modalInitialState, 'modalXlTop0Left3').
    then((result:any)=>{ }) 
  }
  setOrderArtBtnClick(value: string) {
    if (this.orderArtsList === value) {
      this.reverseArtsList = !this.reverseArtsList;
    }
    this.orderArtsList = value;
  }  
  deleteOneArtFactModelBtnClick(delArt: Art){
    this.factModelsArts.splice( this.factModelsArts.indexOf(delArt),1);
    this.formTitles.factModelsArtsTotal = '..'+this.factModelsArts.length+'..Arts';

  }
  deleteAllFactModelArtsBtnClick(){
    this.factModelsArts = new Array();
    this.formTitles.factModelsArtsTotal = '..'+this.factModelsArts.length+'..Arts';
  }
  //---------------------------------------FACT MODELS  CREATE -- DATABASE--------------------------------------------------------------
 
  createOneFactModel(newFactModel: FactModels, artsFactModel: Art[]): void {
    this.globalVar.consoleLog(this.logToConsole,'-> FACTS MODEL CREATE- > createModelOneFact ->...Starting->', null);
    this._factModelsService.createOneFactModel(newFactModel)
      .subscribe(data => { this.newFactModel = data;
                           this.createArtsFactModel(this.newFactModel, artsFactModel);
                         },
                error => { if (this.globalVar.handleError(error)) { throwError(error) };
                           this.deleteOneFactModel(this.newFactModel);
                         });
  }
  getOneFactModel(modFactModel: FactModels): void {
    this.globalVar.consoleLog(this.logToConsole,'-> FACTS MODEL CREATE- > getOneFactModel ->...Starting->', null);
    this._factModelsService.getOneFactModel(modFactModel)
      .subscribe(data => { this.newFactModel = data;
                           this.copyNewFactModelDataToFactModelForm();
                           this.getArtsFactModel(modFactModel,false);
                         },
                error => { if (this.globalVar.handleError(error)) { throwError(error) }; });
  }
  createProvFactModel(provNameFactModel: String): void {
    this.globalVar.consoleLog(this.logToConsole,'-> FACTS MODEL CREATE- > createProvFactModel->...Creating->', null);
    this._factModelsService.createProvFactModel(provNameFactModel)
      .subscribe(data => { this.newFactModel = data;
                           this.copyNewFactModelDataToFactModelForm();
                           this.getArtsFactModel(this.newFactModel,true);
                         },
                error => { if (this.globalVar.handleError(error)) { throwError(error) }; });
  }
  deleteOneFactModel(delFactModel: FactModels){
    this.globalVar.consoleLog(this.logToConsole,'-> FACTS MODEL CREATE- > deleteOneFactModel ->...Starting->', null);
    this._factModelsService.delOneFactModel(delFactModel)
      .subscribe(data => {  },
                error => { if (this.globalVar.handleError(error)) { throwError(error) }; });
  } 
  deleteAndCreateOneFactModel(updFactModel: FactModels,updArtsFactModel: Art[]){
    this.globalVar.consoleLog(this.logToConsole,'-> FACTS MODEL CREATE- > deleteOneFactModel ->...Starting->', null);
    this._factModelsService.delOneFactModel(updFactModel)
      .subscribe(data => { this.createOneFactModel(updFactModel,updArtsFactModel); },
                error => { if (this.globalVar.handleError(error)) { throwError(error) }; });
  } 
  //----------------------------------------FACT MODELS  CREATE -- ARTS MODEL--------------------------------------------------------------
  createArtsFactModel(newFactModel: FactModels,newArtsFactModel: Art[]): void {
    this.globalVar.consoleLog(this.logToConsole,'-> FACTS MODEL CREATE- > createArtsFactModel ->...Starting->', null);
    // Insert factModelsId at the begining of arts array
    let factModelsIdArt = new Art();
    factModelsIdArt.artId = newFactModel.factModelsId;
    newArtsFactModel.splice(0,0,factModelsIdArt);
    this._factModelsService.createArtsFactModel(newArtsFactModel)
      .subscribe(data => { this.closeFactModelCreateModalBtnClick(); },
                error => { if (this.globalVar.handleError(error)) { throwError(error) };
                           this.deleteOneFactModel(this.newFactModel);
                         });
  }
  getArtsFactModel(newFactModel: FactModels, createArtsFactForm: boolean): void {
    this.globalVar.consoleLog(this.logToConsole,'-> FACTS MODEL CREATE- > getArtsFactModel ->...Starting->', null);
    this._factModelsService.getArtsFactModel(newFactModel)
      .subscribe(data => { this.factModelsArts = data;
                           this.createFactModel = false;
                           this.editFactModel = true;
                           this.formTitles.factModelsArtsTotal = '..'+this.factModelsArts.length+'..Arts';
                           this.showFactModel = true;
                         },
                error => { if (this.globalVar.handleError(error)) { throwError(error) }; });
  }

}