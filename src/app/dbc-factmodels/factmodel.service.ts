import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import { ArtsFactModels, FactModels } from './factmodel';
import { Art } from '../dbc-arts/art';
import { Prov } from '../dbc-provs/prov';
import { ValueRecord } from '../dbc-fields/field';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Authorization': 'my-auth-token'
  })
};

@Injectable()
export class FactModelsService {

  constructor(private http: HttpClient) { }

  private json: string;
  private params: string;
  private headers: HttpHeaders;

  /* ---------------------------------------MODEL FACTS-----------------------------------------------------------*/
  getAllFactModels(): Observable<FactModels[]>  {
    return this.http.get<FactModels[]>('/modelFacts/getAllModels')
  }
  getFactModelsProv(selectedProv: Prov): Observable<FactModels[]>  {
    return this.http.post<FactModels[]>('/modelFacts/getModelsProv',selectedProv)
  }
  createOneFactModel(newFactModels: FactModels): Observable<FactModels>  {
    return this.http.post<FactModels>('/modelFacts/createOneModel', newFactModels)
  }  
  createProvFactModel(provNameFactModel: String): Observable<FactModels>  {
    return this.http.post<FactModels>('/modelFacts/createProvFactModel', provNameFactModel)
  }
  getOneFactModel(editFactModels: FactModels): Observable<FactModels>  {
    return this.http.post<FactModels>('/modelFacts/getOneModel', editFactModels)
  }
  delOneFactModel(delFactModels: FactModels): Observable<any> {
    return this.http.post('/modelFacts/delOneModel', delFactModels)
  }
  delAllFactModels(): Observable<any> {
    return this.http.post('/modelFacts/delAllModels',0)
  }
  /* ---------------------------------------ARTS MODEL FACTS-----------------------------------------------------------*/
  createArtsFactModel(artsFactModels: Art[]): Observable<Art[]>  {
    return this.http.post<Art[]>('/modelFacts/createArtsModel', artsFactModels)
  }
  getArtsFactModel(editFactModels: FactModels): Observable<Art[]>  {
    return this.http.post<Art[]>('/modelFacts/getArtsModel', editFactModels)
  }
  getArtFactModelsWithArt(selArt :Art): Observable<ArtsFactModels[]>{
    return this.http.post<ArtsFactModels[]>('/modelFacts/getArtFacModelsWithArt', selArt)
  }

}