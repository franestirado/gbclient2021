export class FactModels {
    factModelsId             :number;
    factModelsDate           :Date;
    factModelsName           :string;
    factModelsProvId         :number;
    factModelsProvName       :string;
    factModelsFactType       :string;
    factModelsPayType        :string;
    factModelsNote           :boolean;
}
export class FactModelsString {
    factModelsId             :number;
    factModelsDate           :string;
    factModelsName           :string;
    factModelsProvId         :number;
    factModelsProvName       :string;
    factModelsFactType       :string;
    factModelsPayType        :string;
    factModelsNote           :boolean;
}
export class ArtsFactModels {
    artsFactModelsId        :number;
    FactModelsId            :number;
    FactModelsName          :string;
    artId                   :number;
    artName                 :string;
}

