import { TestBed } from '@angular/core/testing';

import { HdService } from './hd.service';

describe('HdService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: HdService = TestBed.get(HdService);
    expect(service).toBeTruthy();
  });
});
