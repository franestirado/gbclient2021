export class Hd {
    hdId        :number;
    hdDate      :Date;
    hdName      :string;
    hdType      :string;
    hdPlace     :string;
    hdOrder     :number;
    hdAmount    :number;
    hdNotes     :string;
    hdStatus    :boolean;
    constructor (hdDate: Date, hdName: string, hdType: string, hdPlace: string, hdOrder: number, hdAmount: number,  hdStatus: boolean ){
        this.hdDate     = hdDate;
        this.hdName     = hdName;
        this.hdType     = hdType;
        this.hdPlace    = hdPlace;
        this.hdOrder    = hdOrder;
        this.hdAmount   = hdAmount;  
        this.hdStatus   = hdStatus;    
    }
}
export class HdString {
    hdId        :string;
    hdDate      :string;
    hdName      :string;
    hdType      :string; 
    hdPlace     :string;
    hdOrder     :string;
    hdAmount    :string;
    hdNotes     :string;
    hdStatus    :string;
}
export class HdTpv {
    hdTpvId         :number;
    hdTpvDate       :Date;
    hdTpvName       :string;
    hdTpvType       :string;
    hdTpvOrder      :number;
    hdTpvNumber     :number;
	hdTpvTickets    :number;
	hdTpvTicket     :number;	
	hdTpvMoney      :number;	
    constructor ( hdTpvDate: Date,hdTpvName:string,hdTpvType:string,hdTpvOrder:number,
                    hdTpvNumber:number,hdTpvTickets:number,hdTpvTicket: number,hdTpvMoney:number ){
        this.hdTpvDate      = hdTpvDate;
        this.hdTpvName      = hdTpvName;
        this.hdTpvType      = hdTpvType;
        this.hdTpvOrder     = hdTpvOrder;
        this.hdTpvNumber    = hdTpvNumber;
        this.hdTpvTickets   = hdTpvTickets;
        this.hdTpvTicket    = hdTpvTicket;	
        this.hdTpvMoney     = hdTpvMoney;    
    }
}
export class HdType {
    hdTypeId        :number;
    hdTypeDate      :Date;
    hdTypeName      :string;
    hdTypeType      :string;
    hdTypePlace     :string;
    hdTypeOrder     :number;
    hdTypeStatus    :boolean;
}
export class HdTypeString {
    hdTypeId        :number;
    hdTypeDate      :string;
    hdTypeName      :string;
    hdTypeType      :string;
    hdTypePlace     :string;
    hdTypeOrder     :number;
    hdTypeStatus    :boolean;
}