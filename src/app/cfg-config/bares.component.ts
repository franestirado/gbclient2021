import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { throwError } from 'rxjs';

import { OrderPipe } from 'ngx-order-pipe';
import { BsModalService } from 'ngx-bootstrap/modal';

import { ErrorService } from '../aa-errors/error.service';
import { GbViewsService } from '../dbc-gbviews/gbviews.service';
import { GlobalVarService } from '../aa-common/global-var.service';

import { ValueRecord } from '../dbc-fields/field';
import { SelectValueRecordListComponent } from '../aa-common/select-VRList.component';

import { Bares, BaresString } from './config';
import { BaresCreateComponent } from './bares-create.component';
import { ConfigService } from './config.service';

@Component({
  selector      :'app-bares',
  templateUrl   :'./bares.component.html',
  styleUrls     :['./config.component.css']
})
export class BaresComponent implements OnInit {
  formTitles = {
    'edit'                :'Editar',
    'delete'              :'Borrar',
    'bares'               :'Bares',
    'baresTotalString'    :'Bares',
    'barDeleteOne'        :'¿ Seguro que quiere BORRAR los datos de este BAR ?',
    'baresDeleteAll'      :'¿ Seguro que quiere BORRAR los datos de TODOS los BARES ?',
  };
  formLabels = { 
    'id'                    :'#####',
    'barId'                 :'id',
    'barDate'               :'Fecha',
    'barName'               :'Bar',
    'barLongName'           :'Detalle',
    'barLegalName'          :'Oficial',
    'barCIF'                :'CIF',
    'barAddress'            :'Dirección',
    'barWebSite'            :'Web',
    'barEmail'              :'Email',
    'barPhone'              :'Teléfono',
    'barDbName'             :'DB-Bar',
    'barDbUrl'              :'Url',
    'barDbPassword'         :'Pwd',
    'barDbUserName'         :'Usuario',
    'barDbDriverClassName'  :'Driver',
    'barDbDdlAuto'          :'Ddl',
    'barDbPhysNameStrategy' :'Phys',
    'barStatus'             :'',
  };  
  private createBar         :boolean;
  public  showBaresList     :boolean;
  public  bares             :Bares[];
  private newBar            :Bares;
  public  searchBar         :BaresString;
  private selectedBar       :Bares;
  public  showSearchBares   :boolean;
  public  orderBaresList    :string;
  public  reverseBaresList  :boolean;
  public  localBaresRecordList   :ValueRecord [];
  private selectedBarName   :string;

  public  caseInsensitive   :boolean;
  private sortedCollection  :any[];
  private logToConsole      :boolean;

  /*--------------------------------------- BARES - DBBARXX -- CONSTRUCTOR-----------------------------------------------------------*/
  constructor(  private orderPipe: OrderPipe, private viewContainer: ViewContainerRef, public globalVar: GlobalVarService, 
                private _gbViewsService: GbViewsService, private _configService: ConfigService, private _errorService: ErrorService,
                private modalService: BsModalService ) {
        //   this.getGbViews('gbbars'); 
  }
  /*--------------------------------------- BARES - DBBARXX -- NG ON INIT-----------------------------------------------------------*/
  ngOnInit(): void {    
    this.logToConsole = this.globalVar.logToConsole;
    this.logToConsole = true;
    this.caseInsensitive = true;
    this.newBar = new Bares();
    this.searchBar = new BaresString();
    this.showBaresList     = true;
    this.createBar        = false;
    this.getAllBares();
  }
  /*--------------------------------------- BARES - DBBARXX -- FORMS --------------------------------------------------------------*/


  /*--------------------------------------- BARES - DBBARXX -- GENERAL ------------------------------------------------------------*/
  getBarFromBarName(barName: string) : Bares{
    for(let j=0; j<this.bares.length; j++){
      if (this.bares[j].barName === barName) return this.bares[j];
    }
    return null;
  }
  /*--------------------------------------- BARES - DBBARXX -- BTN CLICK-----------------------------------------------------------*/
  changeShowBaresListBtnClick(){    
    if (this.showBaresList === true) { 
      this.showBaresList     = false;
    } else {     
      this.showBaresList     = true;  
      this.getAllBares();
    }   
  }
  changeCurrentBarBtnClick(){
    this.localBaresRecordList = this.globalVar.prepareBaresValueRecordList(this.bares);
    const modalInitialState = {
      localValueRecordList  :this.localBaresRecordList,
      selectTitle1          :"BARES",
      selectTitle2          :"Seleccionar BAR",
      selectMessage         :"Seleccione el BAR de trabajo",
      selectLabel           :"Bares:",       
      selectFooter          :"BARES",
      callback              :'OK',   
    };
    this.globalVar.openModal(SelectValueRecordListComponent, modalInitialState, 'modalLgTop0Left0').
    then((result:any)=>{
      if (result != null){                                           
        this.selectedBarName = result;
        this.selectedBar = this.getBarFromBarName(this.selectedBarName);  
        this.newBar = this.globalVar.changeWorkingBarByBarName(this.selectedBarName);
      }
    })
    return;
  }
  createBarBtnClick(){
    this.newBar = new Bares();
    this.createBar = true;
    this.createBarModal();
  }  
  editBarBtnClick(editBar: Bares){
    if (this.isBarNotBlocked(editBar)){ 
      this.newBar = editBar;
      this.createBar = false;
      this.createBarModal();
    }
  } 
  createBarModal(){
    const modalInitialState = {
      newBar      :this.newBar,
      baresTitle  :'Bares',
      baresDate   :null,
      createBar   :this.createBar,
      callback    :'OK',   
    };
    this.globalVar.openModal(BaresCreateComponent, modalInitialState, 'modalLgTop0Left0').
    then((result:any)=>{
      if (result != null) this.newBar = result;           
      this.getAllBares();  
    })
  }  
  isBarNotBlocked(selectedBar: Bares):boolean{
    this.globalVar.consoleLog(this.logToConsole,'->BARES->isBarNotBlocked->stbaring->', null);
    if (selectedBar.barStatus === false){
      this._errorService.showErrorModal( "BARES", "BARES", "BAR BLOQUEADO"+"->",
                        "Bar->"+selectedBar.barName, "Url->"+selectedBar.barDbUrl, 
                        "User->"+selectedBar.barDbUserName, "DdlAuto->"+selectedBar.barDbDdlAuto,
                        "Id->"+selectedBar.barId, "DriverClassName->"+selectedBar.barDbDriverClassName);
      return false;
    } else return true;
  }
  searchBaresBtnClick() {
    if (this.showSearchBares === true) {
      this.showSearchBares = false;
    } else {
      this.showSearchBares = true;
    }
  }
  lockBarBtnClick(modBar: Bares){
    if (modBar.barStatus === true) modBar.barStatus = false;
    else  modBar.barStatus = true;
    this.updateOneBar(modBar);
  }
  setOrderBtnClick(value: string) {
    if (this.orderBaresList === value) {
      this.reverseBaresList = !this.reverseBaresList;
    }
    this.orderBaresList = value;
  }
  /*--------------------------------------- BARES - DBBARXX -- DATABASE------------------------------------------------------------*/
  getAllBares() {
    this.globalVar.consoleLog(this.logToConsole,'->BARES->getAllBares->statring->', null);
    this._configService.getAllBares()
      .subscribe(data => { this.bares = data;
                           this.orderBaresList = 'barName';
                           this.reverseBaresList = false;
                           this.sortedCollection = this.orderPipe.transform(this.bares,this.orderBaresList,this.reverseBaresList,this.caseInsensitive);
                           this.bares = this.sortedCollection;
                           this.formTitles.baresTotalString =this.bares.length+'..Bares';                 
                         },
                error => { if (this.globalVar.handleError(error)) { throwError(error) }; });
  }
  updateOneBar(modBar: Bares): void {
    this.globalVar.consoleLog(this.logToConsole,'->BARES->updateOneBar->updating->', modBar);
    this._configService.updateOneBar(modBar)
      .subscribe(data => { this.newBar = data;
                           this.getAllBares();
                         },
                error => { if (this.globalVar.handleError(error)) { throwError(error) }; });
  }
  /*
  changeWorkingBar(selectedBar: Bares): void {
    this.globalVar.consoleLog(this.logToConsole,'->BARES->changeWorkingBar->changing->', selectedBar);
    this._configService.changeWorkingBar(selectedBar)
      .subscribe(data => {
                          this.newBar = data;
                          this.globalVar.currentBar = this.newBar.barName;
                          this.globalVar.getFactPayTypesAndBankAccountsFromHdTypes();
                          this.globalVar.getAllCardTpvs();
                          this.getAllBares();
                        },
                error => { if (this.globalVar.handleError(error)) { throwError(error) }; });
  }*/
  /*--------------------------------------- BARES - DBBARXX -- DELETE--------------------------------------------------------------*/
  confirmDeleteOneBarBtnClick(bar: Bares){
    this._errorService.getDeleteMsgConfirmation("deleteMsg",this.formTitles.bares,this.formTitles.bares,
                            bar,this.formTitles.barDeleteOne,bar.barName,bar.barLongName,bar.barDbUrl,bar.barDbName,"","").
        then((deleteOK:boolean)=>{
          if (deleteOK === true){ this.deleteOneBar(bar); }
        })   
  }
  confirmDeleteAllBaresBtnClick(){
    this._errorService.getDeleteMsgConfirmation("deleteMsg",this.formTitles.bares,this.formTitles.bares,
                            null,this.formTitles.baresDeleteAll,"","","","","","").
        then((deleteOK:boolean)=>{
          if (deleteOK === true){  this.deleteAllBares(); }
        })   
  } 
  deleteAllBares(){
    this.globalVar.consoleLog(this.logToConsole,'->BARES->deleteAllBares->deleting All->', null);
    this._configService.delAllBares()
    .subscribe(data => { this.getAllBares(); },
              error => { if (this.globalVar.handleError(error)) { throwError(error) };});
  }
  deleteOneBar(delBar: Bares){
    this.globalVar.consoleLog(this.logToConsole,'->BARES->deleteOneBar->deleting One->', null);
    this._configService.delOneBar(delBar)
    .subscribe(data => { this.getAllBares(); },
              error => { if (this.globalVar.handleError(error)) { throwError(error) }; });
  }
}
