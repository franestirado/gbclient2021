import { Component, OnInit } from '@angular/core';
import { GlobalVarService } from '../aa-common/global-var.service';
import { HelloService } from './hello.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {
  public hello: string;
  public errorMessage;

  constructor(public globalVar: GlobalVarService, private _helloService: HelloService) {
    this.hello = 'Hola desde el client de Angular';
  }

  ngOnInit() {
  }

  traeSaludo() {
    this._helloService.getHello()
      .subscribe(data => {
                          console.log(JSON.stringify(data, null, '    ') );
                          this.hello = data.content;
                          this.globalVar.showErrorMsg1 = true;
                          this.globalVar.errorMsg1 = data.content;
                         },
                 error => {
                          console.log(JSON.stringify(error, null, '    ') );
                          this.errorMessage = error;
                          this.globalVar.showErrorMsg2 = true;
                          this.globalVar.errorMsg2 = error;
                          });
  }

}
