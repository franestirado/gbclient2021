import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { throwError, Subject } from 'rxjs';

import { BsModalRef } from 'ngx-bootstrap/modal';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { OrderPipe } from 'ngx-order-pipe';

import { GlobalVarService } from '../aa-common/global-var.service';
import { ValidationService } from '../aa-common/validation.service';

import { CardPayout, CardTpv } from './cards';
import { CardsService } from './cards.service';

@Component({
  selector      :'app-cardPayouts-create',
  templateUrl   :'./cardPayouts-create.component.html',
  styleUrls     :['./cards.component.css'] 
})
export class CardPayoutsCreateComponent implements OnInit {
  public createCardPayout       :boolean;
  public cardPayoutId           :any;
  public cardPayoutDate         :Date;
  public cardPayoutMinDate      :Date;
  public cardPayoutMaxDate      :Date;
  public cardPayoutOperationNr  :number;
  public cardPayoutList         :CardPayout;
  public selectedCardTpv        :CardTpv;
  public changeCardsDate        :boolean;
  callback                      :any;
  result: Subject<any> = new Subject<any>();

  public formTitles = { 
    'cardPayoutCreate'          :'Añadir Pagos',
    'cardPayoutEdit'            :'Cambiar Pago',
    'hdTpvSales'                :'Ventas Caja',
    'tpvName'                   :'',
    'hdDate'                    :'',
    'startDate'                 :'',
    'endDate'                   :'',
    'cardPayoutsSubTotalString' :'',
  };
  public formLabels = {
    'cardPayoutId'          :'Id',
    'cardPayoutDate'        :'Fecha',
    'cardPayoutOperationNr' :'NºOper.',
    'cardPayoutMulti'       :'Cantidad',
    'cardPayoutTotal'       :'Total',
    'cardPayoutTpvId'       :'Tpv.Id',
    'cardPayoutTotalId'     :'Total.Id',
    'cardPayoutDelete'      :'Borrar',
  };
  public  cardsDatePickerConfig       :Partial <BsDatepickerConfig>;
  public  cardPayoutsForm             :FormGroup;
  public  cardPayoutsFormArray        :FormArray;
  public  cardPayoutSubTotal          :number;
  public  cardPayoutTpvId             :number;
  public  cardPayoutTotalId           :number;
  public  cardPayoutDateDDMMYYYY      :Date;
  private nextCardPayoutOperationNr   :number;
  public  editAllFlag                 :boolean;

  private newCardPayoutsList          :CardPayout[];
  private previousCardPayoutsList     :CardPayout[];

  private orderCardPayoutsList        :string;
  private reverseCardPayoutsList      :boolean;
  public  caseInsensitive             :boolean;
  private sortedCollection            :any[];
  private logToConsole                :boolean;
  //--------------------------------------- CARD PAYOUTS CREATE -- CONSTRUCTOR---------------------------------------------------------------
  constructor (   private bsModalRef: BsModalRef, private fb:FormBuilder, private orderPipe: OrderPipe, 
                  private globalVar: GlobalVarService, private _cardsService: CardsService, ) {
    this.cardsDatePickerConfig = Object.assign ( {}, {
      containerClass    :'theme-dark-blue',
      showWeekNumbers   :true,
      minDate           :this.globalVar.workingPreviousYear,
      maxDate           :this.globalVar.workingNextYear,
      dateInputFormat   :'DD-MM-YYYY'
      } );
  }
  //--------------------------------------- CARD PAYOUTS CREATE -- NG ON INIT---------------------------------------------------------------
  ngOnInit(): void {     
    this.logToConsole = this.globalVar.logToConsole;
    this.logToConsole = true;
    this.editAllFlag = false;
    this.formTitles.hdDate = this.cardPayoutDate.toLocaleDateString();
    if (this.cardPayoutMinDate === this.cardPayoutMaxDate) {
      this.formTitles.startDate = '';
      this.formTitles.endDate = null;
    } else {
      this.formTitles.startDate = this.cardPayoutMinDate.toLocaleDateString();;
      this.formTitles.endDate = this.cardPayoutMaxDate.toLocaleDateString();
    }  
    this.formTitles.tpvName = this.selectedCardTpv.cardTpvName;
    this.formTitles.cardPayoutsSubTotalString = 'Subtotal....  0.00'
    this.cardPayoutsFormArray= new FormArray([]);   
    this.cardPayoutsForm = new FormGroup({       
      cardPayoutsFormArray: this.cardPayoutsFormArray
    });
    this.cardPayoutsFormArray.push(this.addCardPayoutsFormGroup(this.cardPayoutId, this.cardPayoutDate, this.cardPayoutOperationNr, 1, 0,
                                  this.selectedCardTpv.cardTpvId ,this.cardPayoutTotalId, false));
    if (this.createCardPayout === true) {    
      this.cardPayoutsFormArray.at(0).get('cardPayoutOperationNr').setValue(this.cardPayoutOperationNr);
      this.cardPayoutsFormArray.at(0).get('cardPayoutTotal').setValue('');      
      this.cardPayoutsFormArray.at(0).get('cardPayoutDate').patchValue(this.cardPayoutDate);  
      if (this.selectedCardTpv.cardTpvMulti === true) {
        this.cardPayoutsFormArray.at(0).get('cardPayoutMulti').setValue(''); 
      } else {
        this.cardPayoutsFormArray.at(0).get('cardPayoutMulti').setValue(1); 
      }          
    } else {
      this.cardPayoutsFormArray.at(0).get('cardPayoutId').setValue(this.cardPayoutList.cardPayoutId); 
      this.cardPayoutsFormArray.at(0).get('cardPayoutOperationNr').setValue(this.cardPayoutList.cardPayoutOperationNr); 
      this.cardPayoutsFormArray.at(0).get('cardPayoutMulti').setValue(this.cardPayoutList.cardPayoutMulti);  
      this.cardPayoutsFormArray.at(0).get('cardPayoutTotal').setValue(this.cardPayoutList.cardPayoutTotal.toFixed(2));    
      this.cardPayoutsFormArray.at(0).get('cardPayoutDate').patchValue(new Date(this.cardPayoutList.cardPayoutDate)); 
    }
    this.cardPayoutsFormArray.controls.forEach(
      control => {
          control.get('cardPayoutTotal').valueChanges.subscribe(
                () => { this.setCardPayoutSubTotal(); } )   
          control.get('cardPayoutMulti').valueChanges.subscribe(
                  () => { this.setCardPayoutSubTotal(); } )          
      }
    );
    if ( (this.createCardPayout === true) && (this.cardPayoutOperationNr === 1) ) {
      this.cardPayoutsFormArray.at(0).get('cardPayoutOperationNr').setValue('');
      this.getPrevDateCardPayouts(this.cardPayoutDate);      
    }
  }
  //--------------------------------------- CARD PAYOUTS CREATE -- FORMS ---------------------------------------------------------------
  private addCardPayoutsFormGroup(id:number, date:Date, operationNr:number, multi:number, total:number, tpvId:number, totalId:number, disable:boolean): FormGroup {
    return this.fb.group({  
      cardPayoutId:         [{value:id,disabled:true}],
      cardPayoutDate:       [{value:date,disabled:!this.changeCardsDate},[Validators.required]],
      cardPayoutOperationNr:[{value:operationNr,disabled:true},[Validators.required,ValidationService.numberValidator]] ,
      cardPayoutMulti:      [{value:multi,disabled:!this.selectedCardTpv.cardTpvMulti},[Validators.required,ValidationService.numberValidator]],
      cardPayoutTotal:      [{value:total},[Validators.required,ValidationService.decimalValidation]],
      cardPayoutTpvId:      [{value:tpvId,disabled:true},[Validators.required]],
      cardPayoutTotalId:    [{value:totalId,disabled:true}],
    });
  }
  private copyCardPayoutsFormArrayToCardPayoutsList(){
    this.globalVar.consoleLog(this.logToConsole,'-> CARDS PAYOUTS CREATE -> copyCardPayoutsFormArrayToCardPayoutsList ->...Starting...->', null);
    this.newCardPayoutsList = new Array();   
    for(let j=0; j< this.cardPayoutsFormArray.length; j++){
      if (this.cardPayoutsFormArray.at(j).get('cardPayoutTotal').value != 0 ) {
        let newCardPayout = new CardPayout();     
        newCardPayout.cardPayoutId          = this.cardPayoutsFormArray.at(j).get('cardPayoutId').value;
        newCardPayout.cardPayoutDate        = this.cardPayoutsFormArray.at(j).get('cardPayoutDate').value;
        newCardPayout.cardPayoutOperationNr = this.cardPayoutsFormArray.at(j).get('cardPayoutOperationNr').value;
        newCardPayout.cardPayoutMulti       = this.cardPayoutsFormArray.at(j).get('cardPayoutMulti').value;
        newCardPayout.cardPayoutTotal       = this.cardPayoutsFormArray.at(j).get('cardPayoutTotal').value;
        newCardPayout.cardPayoutTpvId       = this.cardPayoutsFormArray.at(j).get('cardPayoutTpvId').value;
        newCardPayout.cardPayoutTotalId     = this.cardPayoutsFormArray.at(j).get('cardPayoutTotalId').value;    
        this.newCardPayoutsList.push(newCardPayout);
      }
    }  
  }
  //--------------------------------------- CARD PAYOUTS CREATE -- GENERAL ---------------------------------------------------------------
  public setTwoNumberDecimal($event) {
    $event.target.value = parseFloat($event.target.value).toFixed(2);
  } 
  private setCardPayoutSubTotal(){
    this.cardPayoutSubTotal = 0;
    this.cardPayoutsFormArray.controls.forEach(        
        control => {
          let valueCardPayoutTotal = 0;
          let valueCardPayoutMulti = 0;
          if ( isNaN(control.get('cardPayoutTotal').value) === false ){
            valueCardPayoutTotal = control.get('cardPayoutTotal').value;
          }     
          if ( isNaN(control.get('cardPayoutMulti').value) === false ){
            valueCardPayoutMulti = control.get('cardPayoutMulti').value;
          } 
          if ( (valueCardPayoutMulti != 0) && (valueCardPayoutTotal != 0) ) {
            this.cardPayoutSubTotal = this.cardPayoutSubTotal + (valueCardPayoutMulti * valueCardPayoutTotal);         
          }
        }
    );
    this.formTitles.cardPayoutsSubTotalString = 'Subtotal....  ' + this.cardPayoutSubTotal.toFixed(2);
  }
  private previousMaxOperationNrCardPayoutList(selectedCardTpv: CardTpv):number{
    let maxOperationNr = 0;
    for(let j=0; j<this.previousCardPayoutsList.length; j++){
      if (this.previousCardPayoutsList[j].cardPayoutTpvId === selectedCardTpv.cardTpvId ) {
        maxOperationNr = Math.max ( maxOperationNr, this.previousCardPayoutsList[j].cardPayoutOperationNr );
      }     
    }
    return maxOperationNr;
  }
  //--------------------------------------- CARD PAYOUTS CREATE -- BTN CLICK ---------------------------------------------------------------
  public ClosedCardPayoutsCreateModalBtnClick(newCardPayoutsList: CardPayout[]){
    if (this.bsModalRef.content.callback != null){
      //this.bsModalRef.content.callback(newCardPayoutsList);
      this.result.next(newCardPayoutsList);
      this.bsModalRef.hide();
      }
  }
  public addCardPayoutsBtnClick(){
    this.cardPayoutsFormArray.push(this.addCardPayoutsFormGroup(this.cardPayoutId, this.cardPayoutDate, this.cardPayoutOperationNr+1 , 1, 0,
                                                                this.selectedCardTpv.cardTpvId ,this.cardPayoutTotalId, false));
    let index = this.cardPayoutsFormArray.length - 1;    
    this.cardPayoutsFormArray.at(index).get('cardPayoutDate').setValue(this.cardPayoutDate);
    this.cardPayoutsFormArray.at(index).get('cardPayoutTotal').setValue('');
    if (this.selectedCardTpv.cardTpvMulti === true) {
      this.cardPayoutsFormArray.at(index).get('cardPayoutMulti').setValue(''); 
    } else {
      this.cardPayoutsFormArray.at(index).get('cardPayoutMulti').setValue(1); 
    } 
    let indexBefore = this.cardPayoutsFormArray.length - 2;
    let nextCardPayoutOperationNr = 0;
    if ( (isNaN(Number(this.cardPayoutsFormArray.at(indexBefore).get('cardPayoutOperationNr').value))) || 
      (Number(this.cardPayoutsFormArray.at(indexBefore).get('cardPayoutOperationNr').value) === 0) ) {
      this.cardPayoutsFormArray.at(index).get('cardPayoutOperationNr').setValue(''); 
    }else {
      nextCardPayoutOperationNr = Number(this.cardPayoutsFormArray.at(indexBefore).get('cardPayoutOperationNr').value);
      nextCardPayoutOperationNr = nextCardPayoutOperationNr + 1;
      this.cardPayoutsFormArray.at(index).get('cardPayoutOperationNr').setValue(nextCardPayoutOperationNr);
    } 
    this.cardPayoutsFormArray.controls.forEach(
      control => {
          control.get('cardPayoutTotal').valueChanges.subscribe(
                () => { this.setCardPayoutSubTotal(); } )          
      }
    ); 
  }
  public deleteOneCardPayoutBtnClick(index: number){
    if (this.cardPayoutsFormArray.length > 1)  this.cardPayoutsFormArray.removeAt(index);   
  }
  public deleteAllCardPayoutsBtnClick(){
    this.cardPayoutsFormArray= new FormArray([]);
    this.cardPayoutsForm = new FormGroup({       
      cardPayoutsFormArray: this.cardPayoutsFormArray
    });
    this.cardPayoutsFormArray.push(this.addCardPayoutsFormGroup(this.cardPayoutId, this.cardPayoutDate, this.cardPayoutOperationNr , 1, 0 ,
              this.selectedCardTpv.cardTpvId ,this.cardPayoutTotalId, false));

    this.cardPayoutsFormArray.at(0).get('cardPayoutDate').setValue(this.cardPayoutDate); 
    this.cardPayoutsFormArray.at(0).get('cardPayoutOperationNr').setValue(this.cardPayoutOperationNr); 
    this.cardPayoutsFormArray.at(0).get('cardPayoutTotal').setValue(0); 
    this.cardPayoutsFormArray.controls.forEach(
      control => {
          control.get('cardPayoutTotal').valueChanges.subscribe(
                () => { this.setCardPayoutSubTotal(); } )          
      }
    );
  }
  public createCardPayoutsBtnClick(cardPayoutsForm: FormGroup){
    this.copyCardPayoutsFormArrayToCardPayoutsList();
    this.createCardPayoutsList(this.newCardPayoutsList);
  }
  public updateCardPayoutsBtnClick(cardPayoutsForm: FormGroup){
    this.copyCardPayoutsFormArrayToCardPayoutsList();
    this.updateCardPayoutsList(this.newCardPayoutsList);
  }
  public cleanFormCardPayoutsBtnClick(){
  }
  public editAllCardPayoutsBtnClick(){
    if (this.editAllFlag === false){
      this.editAllFlag = true;
      this.cardPayoutsFormArray.controls.forEach(
        control => {        
            control.get('cardPayoutOperationNr').enable();
        }
      )
    } else {
      this.editAllFlag = false;
      this.cardPayoutsFormArray.controls.forEach(
        control => {        
            control.get('cardPayoutOperationNr').disable();
        }
      )
    }
  }
  //---------------------------------------- CARD PAYOUTS CREATE -- DATABASE -------------------------------------------------------------
  private getPrevDateCardPayouts(selDate: Date) {
    this.globalVar.consoleLog(this.logToConsole,'-> CARDS PAYOUTS CREATE -> getPrevDateCardPayouts ->...Starting...->', null);
    var previousDate = new Date();
    previousDate.setDate(selDate.getDate() - 7);
    let cardPayoutVRList = this.globalVar.prepareStringDatesValueRecordList(this.selectedCardTpv.cardTpvName,previousDate,selDate);    
    this._cardsService.getPrevCardPayouts(cardPayoutVRList)
      .subscribe(data => { this.previousCardPayoutsList = data;
                           this.orderCardPayoutsList = 'cardPayoutTpvId';
                           this.reverseCardPayoutsList = false;
                           this.sortedCollection = this.orderPipe.transform(this.previousCardPayoutsList,this.orderCardPayoutsList,this.reverseCardPayoutsList,this.caseInsensitive);
                           this.previousCardPayoutsList = this.sortedCollection;
                           this.nextCardPayoutOperationNr = this.previousMaxOperationNrCardPayoutList(this.selectedCardTpv) + 1;
                           this.cardPayoutOperationNr = this.nextCardPayoutOperationNr;
                           if (this.cardPayoutOperationNr === 1){
                             this.cardPayoutsFormArray.at(0).get('cardPayoutOperationNr').setValue(''); } 
                           else {
                             this.cardPayoutsFormArray.at(0).get('cardPayoutOperationNr').setValue(this.cardPayoutOperationNr); 
                           }                         
                          },
                error => { if (this.globalVar.handleError(error)) { throwError(error) }; });
  }
  private createCardPayoutsList(newCardPayoutsList: CardPayout[]): void {
    this.globalVar.consoleLog(this.logToConsole,'-> CARDS PAYOUTS CREATE -> createCardPayoutsList ->...Starting...->', null);
    this._cardsService.createCardPayoutsList(newCardPayoutsList)
      .subscribe(data => { this.newCardPayoutsList = data;
                           this.ClosedCardPayoutsCreateModalBtnClick(this.newCardPayoutsList);                     
                         },
                error => { if (this.globalVar.handleError(error)) { throwError(error) }; });
  }
  private updateCardPayoutsList(newCardPayoutsList: CardPayout[]): void {
    this.globalVar.consoleLog(this.logToConsole,'->CARDS PAYOUTS CREATE->updateCardPayoutsList ->...Starting...->', null);
    this._cardsService.updateCardPayoutsList(newCardPayoutsList)
      .subscribe(data => { this.newCardPayoutsList = data;
                           this.ClosedCardPayoutsCreateModalBtnClick(this.newCardPayoutsList);                     
                         },
                error => { if (this.globalVar.handleError(error)) { throwError(error) }; });
  }

}