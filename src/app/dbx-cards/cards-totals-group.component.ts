import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';

import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { OrderPipe } from 'ngx-order-pipe';

import { GlobalVarService } from '../aa-common/global-var.service';
import { ErrorService } from '../aa-errors/error.service';
import { ValidationService } from '../aa-common/validation.service';

import { CardPayout, CardPayoutString, CardPayoutSelect, CardTpv, CardTotal, CardPayoutGroup } from './cards';

@Component({
  selector      :'app-cards-total-group',
  templateUrl   :'./cards-totals-group.component.html',
  styleUrls     :['./cards.component.css']   
})
export class CardsTotalsGroupComponent implements OnInit {
  public newCardTotal           :CardTotal;
  public workingDateRange       :Date[];
  public cardTpvs               :CardTpv[];
  public selectedCardTpv        :CardTpv;
  public cardPayoutsSelectList  :CardPayoutSelect[];
  callback                      :any;
  result: Subject<any> = new Subject<any>();
   
  public formTitles = {
    'title'           :'Totalización Tarjetas', 
    'create'          :'Crear',  
    'edit'            :'Actualizar',  
    'select'          :'Seleccionar', 
    'dates'           :'Desde...Hasta', 
    'totals'          :'xxx,xx euros en xx Pagos',
    'selectNumber'    :'Seleccione el NÚMERO de los Pagos a Añadir', 
    'selectDay'       :'Seleccione el DÍA de los Pagos a Añadir', 
    'selectDates'     :'Seleccione los DÍAS entre los que están los Pagos a Añadir ',
    'changeDateRange' :'Seleccione el PERIODO para seleccionar los pagos a TOTALIZAR',
  };
  public formLabels = {
    'Id'                        :'####',
    'percentage'                :' % -->',
    'euros'                     :' € -->',
    'cardTotalId'               :'Id',
    'cardTotalDate'             :'Fecha',
    'cardTotalDateStart'        :'Desde',
    'cardTotalDateEnd'          :'Hasta',
    'cardTotalCardTpvId'        :'Tpv Id',
    'cardTotalCardTpvName'      :'Tpv',
    'cardTotalCardTpvBank'      :'Banco',
    'cardTotalTotalOper'        :'Nº.Oper.',
    'cardTotalTotalMoney'       :'Din.Total',
    'cardTotalExp1Perc'         :'Comis.Fija',
    'cardTotalExp1Money'        :'Comis.Fija',
    'cardTotalExp2Perc'         :'Comis.X.Oper',
    'cardTotalExp2Money'        :'Comis.X.Oper',   
    'cardTotalExp3Perc'         :'Comis.1.%',
    'cardTotalExp3Money'        :'Comis.1.%',
    'cardTotalExp4Perc'         :'Comis.2.%',
    'cardTotalExp4Money'        :'Comis.2.%',
    'cardTotalExpTotal'         :'Comis.Total',
    'cardTotalExpTaxesPerc'     :'Comis.Imp.%',
    'cardTotalExpTaxesMoney'    :'Comis.Imp',
    'cardTotalBankMoney'        :'Din.Banco',
    'cardTotalStatus'           :'Estado',
    'cardPayoutId'              :'Id',
    'cardPayoutDate'            :'Fecha',
    'cardPayoutOperationNr'     :'NºOper.',
    'cardPayoutMulti'           :'Cantidad',
    'cardPayoutTotal'           :'Total',
    'cardPayoutTpvId'           :'Tpv.Id',
    'cardPayoutTotalId'         :'Total.Id',
  };
  private selectedCardTpvName           :string;
  public  changeCardTotalDates          :boolean;

  public  cardTotalDate                 :Date;
  public  cardTotalDateStart            :Date;
  public  cardTotalDateEnd              :Date;
  public  cardTotalExp1Flag             :boolean;
  public  cardTotalExp2Flag             :boolean;
  public  cardTotalExp3Flag             :boolean;
  public  cardTotalExp4Flag             :boolean;
  public  cardTotalExpTaxesFlag         :boolean;
  public  cardPayoutsList               :CardPayout[];
  private cardPayoutsGroupList          :CardPayoutGroup[];
  public  searchCardPayout              :CardPayoutString;
  public  showSearchCardPayoutsList     :boolean;
  public  showCardPayoutsList           :boolean;
  public  orderCardsPayoutsList         :string;
  public  reverseCardsPayoutsList       :boolean;
  public  caseInsensitive               :boolean;
  private sortedCollection              :any[];
  private logToConsole                  :boolean;
  //--------------------------------------- CARDS TOTALS GROUP --- CONSTRUCTOR -----------------------------------------------------------------
  constructor (   private orderPipe: OrderPipe, private bsModalRef: BsModalRef, private modalService: BsModalService, 
                  private globalVar: GlobalVarService, private _errorService: ErrorService) {  }
  //--------------------------------------- CARDS TOTALS GROUP --- NG ON INIT ------------------------------------------------------------------
  ngOnInit(): void { 
    this.logToConsole = this.globalVar.logToConsole;
    this.logToConsole = true;
    this.globalVar.consoleLog(this.logToConsole,'-> CARDS TOTALS GROUP -> ngOnInit ->...newCardTotal...->', this.newCardTotal); 
    this.globalVar.consoleLog(this.logToConsole,'-> CARDS TOTALS GROUP -> ngOnInit ->...workingDateRange...->', this.workingDateRange); 
    this.globalVar.consoleLog(this.logToConsole,'-> CARDS TOTALS GROUP -> ngOnInit ->...cardTpvs...->', this.cardTpvs); 
    this.globalVar.consoleLog(this.logToConsole,'-> CARDS TOTALS GROUP -> ngOnInit ->...selectedCardTpv...->', this.selectedCardTpv); 
    this.globalVar.consoleLog(this.logToConsole,'-> CARDS TOTALS GROUP -> ngOnInit ->...cardPayoutsSelectList...->', this.cardPayoutsSelectList); 


    this.orderCardsPayoutsList   = 'cardPayoutTotal';
    this.reverseCardsPayoutsList = false;
    this.caseInsensitive         = true;
    this.searchCardPayout        = new CardPayoutString();
    this.showSearchCardPayoutsList = false;
    this.selectedCardTpvName = this.selectedCardTpv.cardTpvName;
    this.cardTotalDate      = this.workingDateRange[0];
    this.cardTotalDateStart = this.workingDateRange[0];
    this.cardTotalDateEnd   = this.workingDateRange[1];
    this.groupSelectedCardPayouts();
    this.showCardPayoutsList = true;
 
  }
  //--------------------------------------- CARDS TOTALS GROUP --- GENERAL -------------------------------------------------------------------
  private groupSelectedCardPayouts(){
    this.globalVar.consoleLog(this.logToConsole,'-> CARDS TOTALS GROUP -> groupSelectedCardPayouts ->...Starting...->', null); 
    // ordering card payouts by total value
    this.sortedCollection = this.orderPipe.transform(this.cardPayoutsSelectList,'cardPayoutTotal',true,true);
    this.cardPayoutsSelectList = this.sortedCollection;
    // grouping card payouts by total value
    var cardPayoutMultiAdded = 0;
    var cardPayoutTTotalEach = 0;
    var cardPayoutTTotalAdded = 0;
    var totalNumberCardPayouts = 0;
    cardPayoutMultiAdded = this.cardPayoutsSelectList[0].cardPayoutMulti;
    this.cardPayoutsGroupList = new Array();
    for(let k=1;k<this.cardPayoutsSelectList.length;k++){
      if (this.cardPayoutsSelectList[k].cardPayoutTotal === this.cardPayoutsSelectList[k-1].cardPayoutTotal) {
        cardPayoutMultiAdded = cardPayoutMultiAdded + this.cardPayoutsSelectList[k].cardPayoutMulti;
      } else {
        var localCardPayoutGroup = new CardPayoutGroup();
        localCardPayoutGroup.cardPayoutId          = -1;
        localCardPayoutGroup.cardPayoutOperationNr = 'Nº x Valor Unitario = Subtotal';
        localCardPayoutGroup.cardPayoutSelected    = true;
        localCardPayoutGroup.cardPayoutDate        = this.cardPayoutsSelectList[k-1].cardPayoutDate;
        localCardPayoutGroup.cardPayoutTotalId     = this.cardPayoutsSelectList[k-1].cardPayoutTotalId;
        localCardPayoutGroup.cardPayoutTpvId       = this.cardPayoutsSelectList[k-1].cardPayoutTpvId;
        localCardPayoutGroup.cardPayoutTotal       = this.cardPayoutsSelectList[k-1].cardPayoutTotal;
        localCardPayoutGroup.cardPayoutMulti       = cardPayoutMultiAdded;
        localCardPayoutGroup.cardPayoutTTotal      = cardPayoutMultiAdded * this.cardPayoutsSelectList[k-1].cardPayoutTotal;
        this.cardPayoutsGroupList.push(localCardPayoutGroup);
        cardPayoutTTotalEach = cardPayoutMultiAdded * this.cardPayoutsSelectList[k-1].cardPayoutTotal;
        cardPayoutTTotalAdded = cardPayoutTTotalAdded + cardPayoutTTotalEach;
        totalNumberCardPayouts = totalNumberCardPayouts + cardPayoutMultiAdded;
        cardPayoutMultiAdded = this.cardPayoutsSelectList[k].cardPayoutMulti;
      }
    }
    var lastItem = this.cardPayoutsSelectList.length - 1;
    var localCardPayoutGroup = new CardPayoutGroup();
    localCardPayoutGroup.cardPayoutId          = -1;
    localCardPayoutGroup.cardPayoutOperationNr = 'Nº x Valor Unitario = Subtotal';
    localCardPayoutGroup.cardPayoutSelected    = true;
    localCardPayoutGroup.cardPayoutDate        = this.cardPayoutsSelectList[lastItem].cardPayoutDate;
    localCardPayoutGroup.cardPayoutTotalId     = this.cardPayoutsSelectList[lastItem].cardPayoutTotalId;
    localCardPayoutGroup.cardPayoutTpvId       = this.cardPayoutsSelectList[lastItem].cardPayoutTpvId;
    localCardPayoutGroup.cardPayoutTotal       = this.cardPayoutsSelectList[lastItem].cardPayoutTotal;
    localCardPayoutGroup.cardPayoutMulti       = cardPayoutMultiAdded;
    localCardPayoutGroup.cardPayoutTTotal      = cardPayoutMultiAdded * this.cardPayoutsSelectList[lastItem].cardPayoutTotal;
    this.cardPayoutsGroupList.push(localCardPayoutGroup);
    cardPayoutTTotalEach = cardPayoutMultiAdded * this.cardPayoutsSelectList[lastItem].cardPayoutTotal;
    cardPayoutTTotalAdded = cardPayoutTTotalAdded + cardPayoutTTotalEach;
    totalNumberCardPayouts = totalNumberCardPayouts + cardPayoutMultiAdded;
    var localCardPayoutGroup = new CardPayoutGroup();
    localCardPayoutGroup.cardPayoutId          = -1;
    localCardPayoutGroup.cardPayoutOperationNr = 'Nº Total x          = Total';
    localCardPayoutGroup.cardPayoutSelected    = true;
    localCardPayoutGroup.cardPayoutDate        = this.cardTotalDate;
    localCardPayoutGroup.cardPayoutTotalId     = this.cardPayoutsSelectList[lastItem].cardPayoutTotalId;
    localCardPayoutGroup.cardPayoutTpvId       = this.cardPayoutsSelectList[lastItem].cardPayoutTpvId;
    localCardPayoutGroup.cardPayoutTTotal      = cardPayoutTTotalAdded;
    localCardPayoutGroup.cardPayoutMulti       = totalNumberCardPayouts;
    this.cardPayoutsGroupList.push(localCardPayoutGroup);

    this.formTitles.dates = 'TPV...'+ this.selectedCardTpvName.slice(0,15)+'...Del..'+this.workingDateRange[0].toLocaleDateString()+'..Al..'+this.workingDateRange[1].toLocaleDateString();
    this.formTitles.totals = cardPayoutTTotalAdded.toFixed(2)+' € en '+totalNumberCardPayouts.toFixed(0)+' Pagos';

  }
  //--------------------------------------- CARDS TOTALS GROUP --- BTN CLICK -------------------------------------------------------------------
  public setOrderCardsPayoutBtnClick(value: string){
    if (this.orderCardsPayoutsList === value) {
      this.reverseCardsPayoutsList = !this.reverseCardsPayoutsList;
    }
    this.orderCardsPayoutsList = value;
  }
  public closeCardsTotalsGroupModalBtnClick(){
    if (this.bsModalRef.content.callback != null){
      //this.bsModalRef.content.callback('OK');
      this.result.next('OK');
      this.bsModalRef.hide();        
    }
  }

}